package ec.gob.petroecuador.aduanapetro.seguridad.enums;

public enum AccionCRUD {
	Registrado, Actualizado, Eliminado, Listado
}
