package ec.gob.petroecuador.aduanapetro.seguridad.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.dao.AccionDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Accion;

@Component("accionDao")
public class AccionDaoImpl extends GenericDAOImpl<Accion, Long> implements AccionDao {
	
}