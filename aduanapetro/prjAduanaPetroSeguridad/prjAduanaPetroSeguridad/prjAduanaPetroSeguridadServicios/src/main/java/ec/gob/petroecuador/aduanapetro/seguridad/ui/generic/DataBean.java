package ec.gob.petroecuador.aduanapetro.seguridad.ui.generic;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;

@Component("dataBean")
@ManagedBean
@ApplicationScoped
public class DataBean {
    
	/**
	 * Devuelve el enum Estado en formato de lista seleccionable
	 * @return
	 */
    public SelectItem[] getValoresEstados() {
        SelectItem[] items = new SelectItem[Estados.values().length];
        int i = 0;
        for(Estados g: Estados.values()) {
          items[i++] = new SelectItem(g, g.toString());
        }
        return items;
    }

}
