package ec.gob.petroecuador.aduanapetro.seguridad.consts;

public class ConstantesSeguridad {
	/*
	 * Comunes
	 */
	public static final boolean DEBUG = CommonProperties.getPropertyBoolean("ec.gob.petroecuador.aduanapetro.seguridad.debug");
	
	public static final String ADMIN_USER = CommonProperties.getProperty("ec.gob.petroecuador.aduanapetro.seguridad.admin.user");
	
	/*
	 * Acciones
	 */
	public static final String ACCION_CREAR = CommonProperties.getProperty("ec.gob.petroecuador.aduanapetro.seguridad.accion.crear");
	
	public static final String ACCION_MODIFICAR = CommonProperties.getProperty("ec.gob.petroecuador.aduanapetro.seguridad.accion.modificar");
	
	public static final String ACCION_ELIMINAR = CommonProperties.getProperty("ec.gob.petroecuador.aduanapetro.seguridad.accion.eliminar");
	
	public static final String ACCION_LISTAR = CommonProperties.getProperty("ec.gob.petroecuador.aduanapetro.seguridad.accion.listar");
	
	/*
	 * Mensajes
	 */
	public static final String MENSAJE_REGISTRO_ALTERADO = CommonProperties.getProperty("ec.gob.petroecuador.aduanapetro.seguridad.mensaje.registroalterado");
	/*
	 * Comunicacion 
	 * 
	 * */
	public static final String BASE_CONFIG_PATH_REPORTE = CommonProperties.getProperty("ec.gob.petroecuador.aduanapetro.comunicacion.BASE_CONFIG_PATH_REPORTE");
	public static final String BASE_CONFIG_PATH_JRXML = CommonProperties.getProperty("ec.gob.petroecuador.aduanapetro.comunicacion.BASE_CONFIG_PATH_JRXML");
	public static final String BASE_CONFIG_PATH_TEMPLATE = CommonProperties.getProperty("ec.gob.petroecuador.aduanapetro.comunicacion.BASE_CONFIG_PATH_TEMPLATE");
	public static final String BASE_CONFIG_PATH_DOCUMENTO = CommonProperties.getProperty("ec.gob.petroecuador.aduanapetro.comunicacion.BASE_CONFIG_PATH_DOCUMENTOS");
	
	
	
}
