package ec.gob.petroecuador.aduanapetro.seguridad.jpa;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

@NamedQueries({
	@NamedQuery(
		name = "buscarPorNombre",
		query = "select u from Usuario u where u.username = :username"
	),
})
@Entity
@Table(name = "AC_USUARIO", schema= "APS_SEGURIDAD")
public class Usuario extends DatabaseObject<Long> {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	/*
	 * Nombre o Given Name
	 */
	@Column(name = "nombre", length=64)
	private String nombre;

	@Column(name = "apellido", length=64)
	private String apellido;

	/*
	 * CN / username / logon
	 */
	@Column(unique=true, name = "username", length=64 )
	private String username;

	@Column(name = "password", length=256)
	private String password;

	@Column(name = "telefono_celular", length=20)
	private String telefonoCelular;

	@Column(name = "telefono_fijo", length=20)
	private String telefonoFijo;
	
	@Column(unique=true,name = "email", length=128)
	private String email;

	@Column(name = "cargo_funcion", length=200)
	private String cargoFuncion;
	
	/*
	 * Country
	 */
	@Column(name = "pais", length=32)
	private String pais;
	
	/*
	 * City
	 */
	@Column(name = "ciudad", length=32)
	private String ciudad;
	
	private boolean externo;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "usuario")
	private Set<RolUsuario> listaRoles = new HashSet<RolUsuario>();


	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	
	public Usuario(String string) {
		this.username=string ;
		// TODO Auto-generated constructor stub
	}
	public Usuario(){
		
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isExterno() {
		return externo;
	}

	public void setExterno(boolean externo) {
		this.externo = externo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Estados getEstado() {
		return estado;
	}

	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getTelefonoCelular() {
		return telefonoCelular;
	}

	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}

	public String getTelefonoFijo() {
		return telefonoFijo;
	}

	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}

	public String getCargoFuncion() {
		return cargoFuncion;
	}

	public void setCargoFuncion(String cargoFuncion) {
		this.cargoFuncion = cargoFuncion;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public Set<RolUsuario> getListaRoles() {
		return listaRoles;
	}

	public void setListaRoles(Set<RolUsuario> listaRoles) {
		this.listaRoles = listaRoles;
	}

	

	
}
