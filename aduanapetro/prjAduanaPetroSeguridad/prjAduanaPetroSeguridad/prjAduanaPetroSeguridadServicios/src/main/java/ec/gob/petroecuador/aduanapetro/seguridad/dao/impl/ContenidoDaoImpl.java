package ec.gob.petroecuador.aduanapetro.seguridad.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.dao.ContenidoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Contenido;

@Component("contenidoDao")
public class ContenidoDaoImpl extends GenericDAOImpl<Contenido, Long> implements ContenidoDao {
	
}