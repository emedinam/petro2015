package ec.gob.petroecuador.aduanapetro.seguridad.jpa;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Permiso;

@Entity
@Table(name = "AC_ROL", schema= "APS_SEGURIDAD")
public class Rol extends DatabaseObject<Long> {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "nombre", length=64 , unique=true)	
	private String nombre;

	private String descripcion;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "rol")
	private Set<Permiso> listaPermisos = new HashSet<Permiso>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "rol")
	private Set<RolMenu> listaMenus = new HashSet<RolMenu>();

	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Set<Permiso> getListaPermisos() {
		return listaPermisos;
	}

	public void setListaPermisos(Set<Permiso> listaPermisos) {
		this.listaPermisos = listaPermisos;
	}

	public Set<RolMenu> getListaMenus() {
		return listaMenus;
	}

	public void setListaMenus(Set<RolMenu> listaMenus) {
		this.listaMenus = listaMenus;
	}
	
	public Estados getEstado() {
		return estado;
	}

	public void setEstado(Estados estado) {
		this.estado = estado;
	}

}
