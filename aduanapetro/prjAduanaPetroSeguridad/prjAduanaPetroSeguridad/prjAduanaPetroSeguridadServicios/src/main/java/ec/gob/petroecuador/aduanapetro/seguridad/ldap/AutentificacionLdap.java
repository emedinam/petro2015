package ec.gob.petroecuador.aduanapetro.seguridad.ldap;

import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

public class AutentificacionLdap {

	static final String LDAP_URL = "ldap://spdom01:389/DC=andes,DC=com,DC=pe";

	public boolean login(String username, String password) {

		Hashtable<String, Object> env = new Hashtable<String, Object>();

		env.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, LDAP_URL);
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, "CN=" + username.toUpperCase()
				+ ", cn=Users, DC=andes,DC=com,DC=pe");
		env.put(Context.SECURITY_CREDENTIALS, password);

		try {
			DirContext ctx = new InitialDirContext(env);
			return true;

		} catch (NamingException ex) {
			Logger.getLogger(AutentificacionLdap.class.getName()).log(
					Level.SEVERE, null, ex);
		}

		return false;
	}
}
