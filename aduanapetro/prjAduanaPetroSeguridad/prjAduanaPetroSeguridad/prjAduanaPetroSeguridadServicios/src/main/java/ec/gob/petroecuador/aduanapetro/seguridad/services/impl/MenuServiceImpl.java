package ec.gob.petroecuador.aduanapetro.seguridad.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.seguridad.dao.MenuDao;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Menu;
import ec.gob.petroecuador.aduanapetro.seguridad.services.MenuService;

@Service
public class MenuServiceImpl extends GenericServiceImpl<Menu, Long> implements MenuService {

	@Autowired
	MenuDao dao;
	
	@Override
	public GenericDAO<Menu, Long> getDao() {
		return dao;
	}

	@Override
	public List<Menu> getMenusHijos(Long idPadre) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("idPadre", idPadre);
		return dao.findByNamedQuery("filterByPadre", map);
	}

	@Override
	@Transactional
	public List<Menu> getMenusByTipoYPosicion(boolean esVertical,
			boolean esTransversal) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("esVertical", esVertical);
		map.put("esTransversal", esTransversal);
		map.put ("esActivo",Estados.ACTIVO);
		return dao.findByNamedQuery("filterByTipoYPosicion", map);
	}

	@Override
	@Transactional
	public List<Menu> getMenusByTipo(boolean esVertical, Estados esActivo) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("esVertical", esVertical);
		map.put ("esActivo",esActivo);
		return dao.findByNamedQuery("filterByTipoYActivo", map);
	}
}
