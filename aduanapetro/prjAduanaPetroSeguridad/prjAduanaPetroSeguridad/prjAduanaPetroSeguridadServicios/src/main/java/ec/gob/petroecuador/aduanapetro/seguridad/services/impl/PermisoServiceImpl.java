package ec.gob.petroecuador.aduanapetro.seguridad.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.seguridad.dao.PermisoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Permiso;
import ec.gob.petroecuador.aduanapetro.seguridad.services.PermisoService;

@Service
public class PermisoServiceImpl extends GenericServiceImpl<Permiso, Long> implements PermisoService {

	@Autowired
	PermisoDao dao;
	
	@Override
	public GenericDAO<Permiso, Long> getDao() {
		return dao;
	}

}
