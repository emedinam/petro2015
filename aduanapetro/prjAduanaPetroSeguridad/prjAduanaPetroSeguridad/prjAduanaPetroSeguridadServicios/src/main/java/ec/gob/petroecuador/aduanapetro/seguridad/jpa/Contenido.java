package ec.gob.petroecuador.aduanapetro.seguridad.jpa;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

@Entity
@Table(name = "AC_CONTENIDO", schema= "APS_SEGURIDAD")
public class Contenido extends DatabaseObject<Long>  {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	/**
	 * Nombre unico de entidad
	 */
	@Column(name="nombre_entidad" , unique=true)
	private String nombreEntidad;
	
	private String descripcion;

	

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "contenido")
	private Set<Permiso> listaPermiso = new HashSet<Permiso>();

	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	
	public Set<Permiso> getListaPermiso() {
		return listaPermiso;
	}

	public void setListaPermiso(Set<Permiso> listaPermiso) {
		this.listaPermiso = listaPermiso;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Estados getEstado() {
		return estado;
	}

	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	public String getNombreEntidad() {
		return nombreEntidad;
	}

	public void setNombreEntidad(String nombreEntidad) {
		this.nombreEntidad = nombreEntidad;
	}

}
