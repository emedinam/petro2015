package ec.gob.petroecuador.aduanapetro.seguridad.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.seguridad.dao.AccionDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Accion;
import ec.gob.petroecuador.aduanapetro.seguridad.services.AccionService;

@Service
public class AccionServiceImpl extends GenericServiceImpl<Accion, Long> implements AccionService {

	@Autowired
	AccionDao dao;
	
	@Override
	public GenericDAO<Accion, Long> getDao() {
		return dao;
	}

}
