package ec.gob.petroecuador.aduanapetro.seguridad.jpa;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

@NamedQueries({
	@NamedQuery(
		name = "filterByPadre",
		query = "select m from Menu m where m.menuPadre.id = :idPadre"
	),
	@NamedQuery(
		name = "filterByTipoYPosicion",
		query = "select m from Menu m where m.vertical = :esVertical and m.transversales = :esTransversal and m.estado = :esActivo "
	),
	@NamedQuery(
			name = "filterByTipoYActivo",
			query = "select m from Menu m where m.vertical = :esVertical and  m.estado = :esActivo "
		),
})
@Entity
@Table(name = "AC_MENU", schema= "APS_SEGURIDAD")
public class Menu extends DatabaseObject<Long> {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	/**
	 * Nombre unico del menu
	 */
	@Column(name="nombre_menu" , unique=true)
	private String nombreMenu;
	/**
	 * Descripcion del menu
	 */
	private String descripcion;
	/**
	 * URL menu 
	 */
	private String url;
	/**
	 * Orden de aparicion
	 */
	private int orden;
	/**
	 * Vertical u horizontal
	 */
	private boolean vertical;
	/**
	 * Tipo
	 */
	private boolean transversales;
	/**
	 * Menu padre
	 */
	@ManyToOne
	@JoinColumn(name="menu_padre")
	private Menu menuPadre;
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	/**
	 * Roles
	 */
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "menu")
	private Set<RolMenu> listaRoles = new HashSet<RolMenu>();

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}

	public boolean getVertical() {
		return vertical;
	}

	public void setVertical(boolean vertical) {
		this.vertical = vertical;
	}

	public Set<RolMenu> getListaRoles() {
		return listaRoles;
	}

	public void setListaRoles(Set<RolMenu> listaRoles) {
		this.listaRoles = listaRoles;
	}

	public boolean getTransversales() {
		return transversales;
	}

	public void setTransversales(boolean transversales) {
		this.transversales = transversales;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Estados getEstado() {
		return estado;
	}

	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	public String getNombreMenu() {
		return nombreMenu;
	}

	public void setNombreMenu(String nombreMenu) {
		this.nombreMenu = nombreMenu;
	}

	public Menu getMenuPadre() {
		return menuPadre;
	}

	public void setMenuPadre(Menu menuPadre) {
		this.menuPadre = menuPadre;
	}

}
