package ec.gob.petroecuador.aduanapetro.seguridad.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Menu;

public interface MenuService extends GenericService<Menu, Long> {
	
	public List<Menu> getMenusHijos(Long idPadre);
	
	public List<Menu> getMenusByTipoYPosicion(boolean esVertical, boolean esTransversal);
	
	public List<Menu> getMenusByTipo(boolean esVertical, Estados esActivo);

}
