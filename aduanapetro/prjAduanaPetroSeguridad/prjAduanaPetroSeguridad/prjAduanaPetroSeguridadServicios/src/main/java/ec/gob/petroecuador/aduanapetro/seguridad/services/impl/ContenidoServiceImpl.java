package ec.gob.petroecuador.aduanapetro.seguridad.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.seguridad.dao.ContenidoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Contenido;
import ec.gob.petroecuador.aduanapetro.seguridad.services.ContenidoService;

@Service
public class ContenidoServiceImpl extends GenericServiceImpl<Contenido, Long> implements ContenidoService {

	@Autowired
	ContenidoDao dao;
	
	@Override
	public GenericDAO<Contenido, Long> getDao() {
		return dao;
	}

}
