package ec.gob.petroecuador.aduanapetro.seguridad.dao;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Menu;

public interface MenuDao extends GenericDAO<Menu, Long> {

}
