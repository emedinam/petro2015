package ec.gob.petroecuador.aduanapetro.seguridad.enums;

public enum Estados {
	ACTIVO("ACTIVO"),
	INACTIVO("INACTIVO"),
	ANULADO("ANULADO");
	
	private Estados(String name) {
		this.name = name;
	}
	
	private final String name;
	
	public String toString() {
		return name;
	}
}