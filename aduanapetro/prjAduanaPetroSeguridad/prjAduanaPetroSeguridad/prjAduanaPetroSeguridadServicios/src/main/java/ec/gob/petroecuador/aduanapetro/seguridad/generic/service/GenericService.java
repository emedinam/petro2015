package ec.gob.petroecuador.aduanapetro.seguridad.generic.service;

import java.io.Serializable;
import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

public interface GenericService<T extends DatabaseObject<PK>, PK extends Serializable> {
	
	 public void guardar(T entity);
	 
	 public List<T> obtenerTodos();
	 
	 public void eliminar(T entity);
	 
	 public void anular(T entity);
	 
	 public T buscar(PK id);

	 public GenericDAO<T, PK> getDao();
	 
	 public List<T> obtenerActivos();
	 
	 /**
	  * Busca elementos por una lista de criterios
	  * @use
	  * 	{"nombre like 'Diego'", "edad = 30"}
	  * @param criterios
	  * @return
	  */
	 public List<T> buscarPorCriterios(String[] criterios);
}
