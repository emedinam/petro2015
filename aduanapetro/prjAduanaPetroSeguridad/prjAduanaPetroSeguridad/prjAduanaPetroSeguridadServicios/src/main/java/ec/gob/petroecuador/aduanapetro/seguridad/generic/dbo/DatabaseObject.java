package ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo;

import java.io.Serializable;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;

public abstract class DatabaseObject<PK extends Serializable> implements Serializable  {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;
		
	public abstract Estados getEstado();
	
	public abstract void setEstado(Estados estado);

	public abstract PK getId();
	
	public abstract void setId(PK id);
	
}