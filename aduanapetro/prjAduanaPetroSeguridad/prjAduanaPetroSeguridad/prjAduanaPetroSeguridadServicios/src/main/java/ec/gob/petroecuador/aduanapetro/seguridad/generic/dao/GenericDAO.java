package ec.gob.petroecuador.aduanapetro.seguridad.generic.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

public interface GenericDAO<T extends DatabaseObject<PK>, PK extends Serializable> {
	public void delete(PK id);
	public void delete(T entity);
	public T get(PK id);
	public List<T> getAll();
	public List<T> findByNamedQuery(String queryName, Map<String,Object> map);
	public List<T> findByNamedQuery(String queryName, Map<String,Object> map, int maxResults);
	public List<PK> findByNamedQueryPK(String queryName, Map<String,Object> map);
	public void saveOrUpdate(T entity);
	public Long countExist(String queryName, Map<String,Object> map);
	public List<T> getAllActive();
	/**
	 * Busca por una lista de criterios
	 * @sample
	 * 	{"nombre like 'Diego'", "edad = 30"}
	 * @param criterios
	 * @return
	 */
	public List<T> searchByCriteria(String[] criterios);
}