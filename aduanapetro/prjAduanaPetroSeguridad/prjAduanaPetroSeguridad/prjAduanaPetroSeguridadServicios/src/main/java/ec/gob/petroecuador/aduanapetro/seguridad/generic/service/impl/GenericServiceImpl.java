package ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;

public abstract class GenericServiceImpl<T extends DatabaseObject<PK>, PK extends Serializable> implements GenericService<T, PK> {
	
	 @Override
	 @Transactional
	 public void guardar(T entity) {
		 if(entity.getEstado() == null) entity.setEstado(Estados.ACTIVO);
		 this.getDao().saveOrUpdate(entity);
	 }
	 
	 @Override
	 @Transactional
	 public List<T> obtenerTodos() {
		 return this.getDao().getAll();
	 }
	 
	 @Override
	 @Transactional
	 public void eliminar(T entity) {
		 this.getDao().delete(entity);
	 }
	 
	 @Override
	 @Transactional
	 public T buscar(PK id) {
		 return this.getDao().get(id);
	 }
	 
	 @Override
	 @Transactional
	 public void anular(T entity) {
		 entity.setEstado(Estados.ANULADO);
		 this.getDao().saveOrUpdate(entity);
	 }
	 
	 @Override
	 @Transactional
	 public List<T> obtenerActivos() {
		 return this.getDao().getAllActive();
	 }
	 
	 @Override
	 @Transactional
	 public List<T> buscarPorCriterios(String[] criterios) {
		 return this.getDao().searchByCriteria(criterios);
	 }
}
