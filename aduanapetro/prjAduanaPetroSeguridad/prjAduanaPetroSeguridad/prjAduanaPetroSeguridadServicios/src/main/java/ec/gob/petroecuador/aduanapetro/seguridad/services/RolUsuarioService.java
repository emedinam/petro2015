package ec.gob.petroecuador.aduanapetro.seguridad.services;


import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.RolUsuario;

public interface RolUsuarioService extends GenericService<RolUsuario, Long> {
	
}
