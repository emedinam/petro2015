package ec.gob.petroecuador.aduanapetro.seguridad.ui.generic;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ec.gob.petroecuador.aduanapetro.seguridad.consts.ConstantesSeguridad;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Registro;
import ec.gob.petroecuador.aduanapetro.seguridad.services.RegistroService;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.utils.JsfUtil;

/**
 * Clase de Registro Comun de Informacion
 * @author dgonzalez
 *
 */
public abstract class RegistroComun<T> {
	
	private static final Logger log = LoggerFactory.getLogger(RegistroComun.class);
	
	protected DatabaseObject<?> elementoSeleccionado;
	private Class<T> clazz;
	private String tablePage;
	private String formPage;
	private String elementName;
		
	public String getTablePage() {
		return tablePage;
	}

	public void setTablePage(String tablePage) {
		this.tablePage = tablePage;
	}

	public String getFormPage() {
		return formPage;
	}

	public void setFormPage(String formPage) {
		this.formPage = formPage;
	}
	
	public String editar() {
		return formPage+"?faces-redirect=true";
	}
	
	public abstract void inicializarVariables();
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected String eliminar(GenericService servicio) {
		try {
			servicio.anular(elementoSeleccionado);
			inicializarVariables();
			saveRegistro(AccionCRUD.Eliminado, (Long)elementoSeleccionado.getId());
			return tablePage+"?faces-redirect=true";
		} catch (Exception e) {
			addError(e);
		}
		return null;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected String guardar(GenericService servicio) {
		try {
			servicio.guardar(elementoSeleccionado);
			inicializarVariables();
			saveRegistro(AccionCRUD.Registrado, (Long)elementoSeleccionado.getId());
			return tablePage+"?faces-redirect=true";
		} catch (Exception e) {
			addError(e);
		}
		return null;
	}
	
	public String atras() {
		return tablePage+"?faces-redirect=true";
	}

	protected String nuevoRegistro(DatabaseObject<?> elemento) {
		setElementoSeleccionado(elemento);
		return formPage+"?faces-redirect=true";
	}
	
	@Autowired
	protected RegistroService registroService;
		
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RegistroComun(String formPage, String tablePage) {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		this.clazz = (Class) pt.getActualTypeArguments()[0];
		this.formPage = formPage;
		this.tablePage = tablePage;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RegistroComun(String formPage, String tablePage, String elementName) {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		this.clazz = (Class) pt.getActualTypeArguments()[0];
		this.formPage = formPage;
		this.tablePage = tablePage;
		this.elementName = elementName;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RegistroComun(Class clazz, String formPage, String tablePage) {
		this.clazz = clazz;
		this.formPage = formPage;
		this.tablePage = tablePage;
	}
	
	protected void saveRegistro(AccionCRUD accion, Long idEntidad) {
		saveRegistro(accion, idEntidad, clazz);
	}
	
	protected void saveRegistro(AccionCRUD accion, Long idEntidad, Class<?> classEntidad) {
		log.info("-> Actividad: "+accion+", Entidad: "+idEntidad+", Tipo Entidad: "+classEntidad.getSimpleName());
		Registro registro = new Registro();
		registro.setUsuarioRegistro((String) FacesContext.getCurrentInstance()
				.getExternalContext().getSessionMap().get("tokenUser"));
		registro.setFechaFegistro( new Date() );
		registro.setIdEntidad(idEntidad);
		registro.setTipoEntidad(classEntidad.getSimpleName());
		registro.setIpSesion(getIpSession());
		registro.setUrlContenido(getURL());
		registro.setAccionCRUD(accion);
		registroService.guardar(registro);
		String mensaje = String.format(ConstantesSeguridad.MENSAJE_REGISTRO_ALTERADO, 
				(idEntidad!=null? idEntidad : "--"),
				classEntidad.getSimpleName(), accion);
		FacesMessage msg = new FacesMessage(mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
	
	private String getIpSession() {
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		return ipAddress;
	}

	private String getURL() {
		return FacesContext.getCurrentInstance().getViewRoot().getViewId();
	}
	
	
	@SuppressWarnings({ "rawtypes" })
	public DatabaseObject<?> getElementoSeleccionado() {
			try {
				HttpSession session = (HttpSession) FacesContext
						.getCurrentInstance().getExternalContext().getSession(true);
				this.elementoSeleccionado = (DatabaseObject) session.getAttribute(elementName==null? clazz.getSimpleName():elementName);
			}catch(Exception e) {}
		return elementoSeleccionado;
	}

	public void setElementoSeleccionado(DatabaseObject<?> elementoSeleccionado) {
		this.elementoSeleccionado = elementoSeleccionado;
		HttpSession session = (HttpSession) FacesContext
				.getCurrentInstance().getExternalContext().getSession(true);
		session.setAttribute(elementName==null? clazz.getSimpleName():elementName, this.elementoSeleccionado);
		
	}
	
	public Object getElemento(String className) {
		try {
			HttpSession session = (HttpSession) FacesContext
					.getCurrentInstance().getExternalContext().getSession(true);
			return session.getAttribute(className);
		}catch(Exception e) {}
		return null;
	}
	
	public void setElemento(Object elemento, String className) {
		HttpSession session = (HttpSession) FacesContext
				.getCurrentInstance().getExternalContext().getSession(true);
		session.setAttribute(className, elemento);
	}
	
	public void addError(Exception e) {
		JsfUtil.addErrorMessage(e.getMessage());
		if(ConstantesSeguridad.DEBUG)
			e.printStackTrace();
	}
	
	public void addMessage(String msg) {
		JsfUtil.addInfoMessage(msg);
	}
	
	public Estados[] getEstados() {
        return Estados.values();
	}
	
	protected void printObj(Object object) {
		try {
			for (Field field : object.getClass().getDeclaredFields()) {
			    field.setAccessible(true);
			    String name = field.getName();
			    Object value = field.get(object);
			    System.out.printf("-> Field name: %s, Field value: %s%n", name, value);
			}
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
