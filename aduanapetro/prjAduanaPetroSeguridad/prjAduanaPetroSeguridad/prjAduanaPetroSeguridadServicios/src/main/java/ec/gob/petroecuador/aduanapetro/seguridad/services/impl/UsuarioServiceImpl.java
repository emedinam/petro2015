package ec.gob.petroecuador.aduanapetro.seguridad.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jasypt.util.password.BasicPasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.seguridad.dao.UsuarioDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Usuario;
import ec.gob.petroecuador.aduanapetro.seguridad.services.UsuarioService;

@Service
public class UsuarioServiceImpl extends GenericServiceImpl<Usuario, Long> implements UsuarioService {

	@Autowired
	UsuarioDao dao;
	
	@Override
	public GenericDAO<Usuario, Long> getDao() {
		return dao;
	}

	@Override
	public void almacenamientoSeguro(Usuario usuario) {
		BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
		String encryptedPassword = passwordEncryptor.encryptPassword(usuario.getPassword());
		usuario.setPassword(encryptedPassword);
		dao.saveOrUpdate(usuario);
	}

	@Override
	public boolean login(String username, String password) {
		try {
			Usuario usuario = buscarPorNombre(username);
			if(usuario!=null) {
				BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
				String encryptedPassword = passwordEncryptor.encryptPassword(usuario.getPassword());
				return passwordEncryptor.checkPassword(password, encryptedPassword);
			}
		}catch(Exception e) {}
		return false;
	}

	@Override
	public Usuario buscarPorNombre(String username) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("username", username);
		List<Usuario> resultados = dao.findByNamedQuery("buscarPorNombre", map );
		if(resultados!=null && resultados.size()>0) {
			return resultados.get(0);
		}
		return null;
	}


}
