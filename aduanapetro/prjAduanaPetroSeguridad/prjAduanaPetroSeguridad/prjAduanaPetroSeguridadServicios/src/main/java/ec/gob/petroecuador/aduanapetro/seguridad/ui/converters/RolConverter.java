package ec.gob.petroecuador.aduanapetro.seguridad.ui.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Rol;
import ec.gob.petroecuador.aduanapetro.seguridad.services.RolService;

@Component("RolConverter")
@Scope("request")

public class RolConverter implements Converter {

	@Autowired
	private RolService rolService;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		
		if (value == null || value.length() == 0) {
			return null;
		}
		Long id = Long.parseLong(value);
		return rolService.buscar(id);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		return value instanceof Rol ? ((Rol) value).getId().toString() : "";
	}

}
