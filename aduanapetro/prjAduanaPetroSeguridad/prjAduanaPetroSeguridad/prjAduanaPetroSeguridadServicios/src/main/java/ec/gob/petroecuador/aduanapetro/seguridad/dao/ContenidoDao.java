package ec.gob.petroecuador.aduanapetro.seguridad.dao;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Contenido;

public interface ContenidoDao extends GenericDAO<Contenido, Long> {

}
