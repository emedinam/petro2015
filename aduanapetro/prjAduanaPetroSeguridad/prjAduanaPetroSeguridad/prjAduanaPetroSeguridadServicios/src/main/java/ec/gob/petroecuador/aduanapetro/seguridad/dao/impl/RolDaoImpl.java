package ec.gob.petroecuador.aduanapetro.seguridad.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.dao.RolDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Rol;

@Component("rolDao")
public class RolDaoImpl extends GenericDAOImpl<Rol, Long> implements RolDao {
	
}