package ec.gob.petroecuador.aduanapetro.seguridad.ui.utils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class UtilidadFechas {
	
	public Date sumarRestarDiasFecha(Date fecha, int dias){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha); // Configuramos la fecha que se recibe
		calendar.add(Calendar.DAY_OF_YEAR, dias);  // numero de d�as a a�adir, o restar en caso de d�as<0
		return calendar.getTime(); // Devuelve el objeto Date con los nuevos d�as a�adidos
	}
	
    public static Double diferencia(Date fecha1, Date fecha2) throws Exception {
    	long MILLSECS_PER_DAY = 24 * 60 * 60 * 1000;
    	long diferencia = 0;
    	double dias;
    	int anio1=0, anio2=0;
    	int mes1=0, mes2=0;
    	int dia1=0, dia2=0;
    	Date fechaRec1, fechaRec2;
    	 
    	fecha1 = (Date) fecha1;
    	fecha2 = (Date) fecha2;
    	
    	anio1 = fechaFormato("yyyy", fecha1);
    	mes1 = fechaFormato("MM", fecha1);
    	dia1 = fechaFormato("dd", fecha1);
    	
    	Calendar calendar1 = new GregorianCalendar(anio1, mes1-1, dia1); 
 	    fechaRec1 = new java.sql.Date(calendar1.getTimeInMillis());
 	    
 	    anio2 = fechaFormato("yyyy", fecha2);
 	    mes2 = fechaFormato("MM", fecha2);
 	    dia2 = fechaFormato("dd", fecha2);
   	
 	    Calendar calendar2 = new GregorianCalendar(anio2, mes2-1, dia2); 
	    fechaRec2 = new java.sql.Date(calendar2.getTimeInMillis());
 	    
 	    diferencia= ( fechaRec1.getTime() - fechaRec2.getTime() )/ MILLSECS_PER_DAY;
 		dias = (double) diferencia;
 		
 		if (dias>=0)
 			return dias;
 		else
 			return 0.00;
    }

    public static int fechaFormato(String formato, Date fecha) throws Exception {
    	int resultado=0;
    	
	    SimpleDateFormat dateFormata = new SimpleDateFormat(formato);
	    resultado = Integer.parseInt(dateFormata.format(fecha));
	    
	    return resultado;
	    
    }
    
    public Date deStringADate(String fecha){
    	SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
    	String strFecha = fecha;
    	Date fechaDate = null;
        try {
        	fechaDate = formato.parse(strFecha);
        	return fechaDate;
    	} catch (ParseException ex) {
    		ex.printStackTrace();
    	    return fechaDate;
        }
	}

}
