package ec.gob.petroecuador.aduanapetro.seguridad.services;


import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Usuario;

public interface UsuarioService extends GenericService<Usuario, Long> {
	
	/**
	 * Almacenamiento Seguro de Usuario
	 * @param usuario
	 */
	public void almacenamientoSeguro(Usuario usuario);
	
	/**
	 * Login de usuario
	 * @param usuario
	 */
	public boolean login(String username, String password);
	
	/**
	 * Busca por nombre de usuario
	 * @param username
	 * @return
	 */
	public Usuario buscarPorNombre(String username);
}
