package ec.gob.petroecuador.aduanapetro.seguridad.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

@Entity
@Table(name = "AC_REGISTRO", schema= "APS_SEGURIDAD")
public class Registro extends DatabaseObject<Long> {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "ip_sesion", length=32)
	private String ipSesion;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss a")
	@Column(name = "fecha_registro")
	private Date fechaFegistro;

	@Column(name = "usuario_registro", length=64)
	private String usuarioRegistro;

	@Column(name = "tipo_entidad", length=64)
	private String tipoEntidad;
	
	@Column(name = "id_entidad")
	private Long idEntidad;
	
	@Column(name = "url_contenido", length=255)
	private String urlContenido ; 
		
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	
	/**
	 * Accion
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="accion_crud", length=13)
	private AccionCRUD accionCRUD;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIpSesion() {
		return ipSesion;
	}

	public void setIpSesion(String ipSesion) {
		this.ipSesion = ipSesion;
	}

	public Date getFechaFegistro() {
		return fechaFegistro;
	}

	public void setFechaFegistro(Date fechaFegistro) {
		this.fechaFegistro = fechaFegistro;
	}

	public String getUsuarioRegistro() {
		return usuarioRegistro;
	}

	public void setUsuarioRegistro(String usuarioRegistro) {
		this.usuarioRegistro = usuarioRegistro;
	}

	public String getTipoEntidad() {
		return tipoEntidad;
	}

	public void setTipoEntidad(String tipoEntidad) {
		this.tipoEntidad = tipoEntidad;
	}

	public Long getIdEntidad() {
		return idEntidad;
	}

	public void setIdEntidad(Long idEntidad) {
		this.idEntidad = idEntidad;
	}

	public Estados getEstado() {
		return estado;
	}

	public void setEstado(Estados estado) {
		this.estado = estado;
	}
	

	public String getUrlContenido() {
		return urlContenido;
	}

	public void setUrlContenido(String urlContenido) {
		this.urlContenido = urlContenido;
	}

	public AccionCRUD getAccionCRUD() {
		return accionCRUD;
	}

	public void setAccionCRUD(AccionCRUD accionCRUD) {
		this.accionCRUD = accionCRUD;
	}

}
