package ec.gob.petroecuador.aduanapetro.seguridad.services;


import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Accion;

public interface AccionService extends GenericService<Accion, Long> {
	
}
