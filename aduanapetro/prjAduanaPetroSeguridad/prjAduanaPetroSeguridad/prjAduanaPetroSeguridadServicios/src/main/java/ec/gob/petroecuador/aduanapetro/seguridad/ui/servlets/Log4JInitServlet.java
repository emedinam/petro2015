package ec.gob.petroecuador.aduanapetro.seguridad.ui.servlets;

import java.io.File;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;

import ec.gob.petroecuador.aduanapetro.seguridad.consts.CommonProperties;

public class Log4JInitServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void init(ServletConfig config) throws ServletException {
		System.out.println("Log4JInitServlet is initializing log4j");
		String log4jProp = CommonProperties.getResourceFileName("seguridad-log4j.properties");
		if (new File(log4jProp).exists()) {
			System.out.println("Initializing log4j with: " + log4jProp);
			PropertyConfigurator.configure(CommonProperties.getResourceFileName("seguridad-log4j.properties"));
		} else {
			System.err.println("*** " + log4jProp + " file not found, so initializing log4j with BasicConfigurator");
			BasicConfigurator.configure();
		}
		super.init(config);
	}
}