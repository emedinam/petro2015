package ec.gob.petroecuador.aduanapetro.seguridad.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.seguridad.dao.RolMenuDao;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Menu;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Rol;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.RolMenu;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.RolUsuario;
import ec.gob.petroecuador.aduanapetro.seguridad.services.RolMenuService;

@Service
public class RolMenuServiceImpl extends GenericServiceImpl<RolMenu, Long> implements RolMenuService {

	@Autowired
	RolMenuDao dao;
	
	@Override
	public GenericDAO<RolMenu, Long> getDao() {
		return dao;
	}

	@Override
	public List<Menu> getMenusByTipoPosicionYRol(boolean esVertical,
			boolean esTransversal, Set<RolUsuario> rolesUsuario) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("esVertical", esVertical);
		map.put("esTransversal", esTransversal);
		StringBuffer idsRoles = new StringBuffer();
		int i=0;
		for(RolUsuario rolUsuario : rolesUsuario) {
			idsRoles.append(rolUsuario.getRol().getId());
			if(i<rolesUsuario.size()-1) idsRoles.append(",");
		}
		map.put("idRoles", idsRoles.toString());
		List<RolMenu> rolesMenus = dao.findByNamedQuery("filterByTipoPosicionYRol", map);
		
		List<Menu> menus = new ArrayList<Menu>();
		for(RolMenu rolMenu : rolesMenus) {
			if(!menus.contains(rolMenu.getMenu()) && (rolMenu.getMenu().getEstado()==Estados.ACTIVO))
				menus.add(rolMenu.getMenu());
		}
		return menus;
	}

}
