package ec.gob.petroecuador.aduanapetro.seguridad.services;


import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Contenido;

public interface ContenidoService extends GenericService<Contenido, Long> {
	
}
