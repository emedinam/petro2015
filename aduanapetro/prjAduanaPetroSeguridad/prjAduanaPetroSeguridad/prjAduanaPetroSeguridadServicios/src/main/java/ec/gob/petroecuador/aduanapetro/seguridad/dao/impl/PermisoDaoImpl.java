package ec.gob.petroecuador.aduanapetro.seguridad.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.dao.PermisoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Permiso;

@Component("permisoDao")
public class PermisoDaoImpl extends GenericDAOImpl<Permiso, Long> implements PermisoDao {
	
}