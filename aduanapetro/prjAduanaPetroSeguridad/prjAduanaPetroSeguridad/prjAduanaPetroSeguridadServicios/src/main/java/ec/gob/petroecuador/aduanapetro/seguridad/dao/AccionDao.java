package ec.gob.petroecuador.aduanapetro.seguridad.dao;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Accion;

public interface AccionDao extends GenericDAO<Accion, Long> {

}
