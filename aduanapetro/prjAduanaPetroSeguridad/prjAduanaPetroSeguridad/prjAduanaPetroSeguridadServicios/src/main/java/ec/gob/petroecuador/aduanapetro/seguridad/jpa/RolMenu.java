package ec.gob.petroecuador.aduanapetro.seguridad.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

@NamedQueries({
	@NamedQuery(
		name = "filterByTipoPosicionYRol",
		query = "select m from RolMenu m where m.menu.vertical = :esVertical"
				+ " and m.menu.transversales = :esTransversal  and m.rol.id in (:idRoles)"
	),
})
@Entity
@Table(name = "AC_ROLMENU", schema= "APS_SEGURIDAD")
public class RolMenu extends DatabaseObject<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne
	private Menu menu = new Menu();

	@ManyToOne
	private Rol rol = new Rol();
	
	
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public Estados getEstado() {
		return null;
	}

	public void setEstado(Estados estado) {
		
	}

	
}
