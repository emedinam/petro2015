package ec.gob.petroecuador.aduanapetro.seguridad.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.dao.RegistroDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Registro;

@Component("registroDao")
public class RegistroDaoImpl extends GenericDAOImpl<Registro, Long> implements RegistroDao {
	
}