package ec.gob.petroecuador.aduanapetro.seguridad.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.seguridad.dao.RegistroDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Registro;
import ec.gob.petroecuador.aduanapetro.seguridad.services.RegistroService;

@Service
public class RegistroServiceImpl extends GenericServiceImpl<Registro, Long> implements RegistroService {

	@Autowired
	RegistroDao dao;
	
	@Override
	public GenericDAO<Registro, Long> getDao() {
		return dao;
	}

}