package ec.gob.petroecuador.aduanapetro.seguridad.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.seguridad.dao.RolDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Rol;
import ec.gob.petroecuador.aduanapetro.seguridad.services.RolService;

@Service
public class RolServiceImpl extends GenericServiceImpl<Rol, Long> implements RolService {

	@Autowired
	RolDao dao;
	
	@Override
	public GenericDAO<Rol, Long> getDao() {
		return dao;
	}

}
