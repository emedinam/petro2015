package ec.gob.petroecuador.aduanapetro.seguridad.ui.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Contenido;
import ec.gob.petroecuador.aduanapetro.seguridad.services.ContenidoService;

@Component("ContenidoConverter")
public class ContenidoConverter implements Converter {

	@Autowired
	private ContenidoService contenidoService;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value == null || value.length() == 0) {
			return null;
		}
		Long id = Long.parseLong(value);
		return contenidoService.buscar(id);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		return value instanceof Contenido ? ((Contenido) value).getId().toString() : "";
	}

}
