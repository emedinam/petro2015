package ec.gob.petroecuador.aduanapetro.seguridad.services;

import java.util.List;
import java.util.Set;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Menu;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.RolMenu;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.RolUsuario;

public interface RolMenuService extends GenericService<RolMenu, Long> {
	public List<Menu> getMenusByTipoPosicionYRol(boolean esVertical, boolean esTransversal, Set<RolUsuario> roles);
}
