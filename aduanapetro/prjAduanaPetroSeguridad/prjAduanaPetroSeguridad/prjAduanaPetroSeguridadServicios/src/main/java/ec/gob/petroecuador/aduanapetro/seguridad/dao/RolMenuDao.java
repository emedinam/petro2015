package ec.gob.petroecuador.aduanapetro.seguridad.dao;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.RolMenu;

public interface RolMenuDao extends GenericDAO<RolMenu, Long> {

}
