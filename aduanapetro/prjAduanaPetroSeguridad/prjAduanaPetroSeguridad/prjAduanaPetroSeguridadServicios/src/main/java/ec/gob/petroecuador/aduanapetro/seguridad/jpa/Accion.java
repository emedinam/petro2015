package ec.gob.petroecuador.aduanapetro.seguridad.jpa;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

@Entity
@Table(name = "AC_ACCION", schema= "APS_SEGURIDAD")
public class Accion extends DatabaseObject<Long> {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@NotNull
	@Column(unique=true)
	private String descripcion;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "accion")
	private Set<Permiso> listaPermiso = new HashSet<Permiso>();

	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Set<Permiso> getListaPermiso() {
		return listaPermiso;
	}

	public void setListaPermiso(Set<Permiso> listaPermiso) {
		this.listaPermiso = listaPermiso;
	}

	@Override
	public String toString() {
		return "Accion [descripcion=" + descripcion + ", listaPermiso="
				+ listaPermiso + " , id" + this.getId() +"]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Estados getEstado() {
		return estado;
	}

	public void setEstado(Estados estado) {
		this.estado = estado;
	}	

}
