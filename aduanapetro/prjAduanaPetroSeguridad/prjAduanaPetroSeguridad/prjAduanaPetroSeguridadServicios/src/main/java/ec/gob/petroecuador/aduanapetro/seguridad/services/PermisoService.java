package ec.gob.petroecuador.aduanapetro.seguridad.services;


import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Permiso;

public interface PermisoService extends GenericService<Permiso, Long> {
	
}
