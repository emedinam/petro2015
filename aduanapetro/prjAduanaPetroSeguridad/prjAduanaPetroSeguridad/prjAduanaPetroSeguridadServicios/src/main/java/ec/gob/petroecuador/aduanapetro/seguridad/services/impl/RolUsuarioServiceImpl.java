package ec.gob.petroecuador.aduanapetro.seguridad.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.seguridad.dao.RolUsuarioDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.RolUsuario;
import ec.gob.petroecuador.aduanapetro.seguridad.services.RolUsuarioService;

@Service
public class RolUsuarioServiceImpl extends GenericServiceImpl<RolUsuario, Long> implements RolUsuarioService {

	@Autowired
	RolUsuarioDao dao;
	
	@Override
	public GenericDAO<RolUsuario, Long> getDao() {
		return dao;
	}

}
