package ec.gob.petroecuador.aduanapetro.seguridad.dao;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Rol;

public interface RolDao extends GenericDAO<Rol, Long> {

}
