package ec.gob.petroecuador.aduanapetro.seguridad.ui.filters;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import org.apache.log4j.Logger;

import org.apache.log4j.Logger;

import ec.gob.petroecuador.aduanapetro.seguridad.consts.ConstantesSeguridad;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Permiso;

public class AutentificacionPhaseFilter implements PhaseListener {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	private static Logger log = Logger
			.getLogger(AutentificacionPhaseFilter.class);

	@Override
	public void afterPhase(PhaseEvent event) {

	}

	@Override
	public void beforePhase(PhaseEvent event) {
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();

		boolean noLoginPage = request.getRequestURL().lastIndexOf(
				"loginPage.jsf") > -1 ? true : false;
		boolean noErrorPage = request.getRequestURL().lastIndexOf(
				"errorPage.jsf") > -1 ? true : false;

		if (!noLoginPage && !noErrorPage) {
			if (!loggedIn()) {
				log.info("-> El usuario no esta logeado");
				System.out.println("El usuario no esta logeado" + loggedIn());
				FacesContext
						.getCurrentInstance()
						.getApplication()
						.getNavigationHandler()
						.handleNavigation(FacesContext.getCurrentInstance(),
								null, "/loginPage.jsf");
			} else {
				FacesContext
						.getCurrentInstance()
						.getApplication()
						.getNavigationHandler()
						.handleNavigation(FacesContext.getCurrentInstance(),
								null, "/pages/usuarioPage.jsf");
			}
		}
	}

	/*
	 * Verificacion de sesion y permisos
	 */
	/*
	 * if(checkSession(request)) {
	 * //log.info("-> La sesion "+request.getRequestedSessionId
	 * ()+" es correcta"); if(!isAdministrador()) { if(noPermiso(request,
	 * FacesContext .getCurrentInstance().getExternalContext())) {
	 * //log.info("-> El usuario no tiene permisos para acceder a esta pagina."
	 * );
	 * 
	 * try { FacesContext .getCurrentInstance() .getApplication()
	 * .getNavigationHandler()
	 * .handleNavigation(FacesContext.getCurrentInstance(), null,
	 * "/errors/noTienePermisos.jsf"); } catch (Exception e) { FacesContext
	 * .getCurrentInstance() .getApplication() .getNavigationHandler()
	 * .handleNavigation(FacesContext.getCurrentInstance(), null,
	 * "/pages/main.jsf"); } } } } else {
	 * 
	 * Login
	 * 
	 * log.info("-> La sesion es incorrecta "+request.getRequestedSessionId()+", "
	 * +request.isRequestedSessionIdValid());
	 * FacesContext.getCurrentInstance().getApplication()
	 * .getNavigationHandler()
	 * .handleNavigation(FacesContext.getCurrentInstance(), null,
	 * "/loginPage.jsf"); } }
	 * 
	 * }
	 */

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}

	private boolean loggedIn() {
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		if (request.getCookies() != null) {
			
			log.info("Cantidad de Cookies.." +request.getCookies().length);
			int i= 0 ;
			while(i <= request.getCookies().length-1){
				log.info("Revizando Elemento de Cookie.."+ i);
				log.info("Revizando Elemento de Cookie.."+ request.getCookies()[i].getName());
				if (request.getCookies()[i].getName().equals("USERID"))
					if (request.getCookies()[i].getValue() != null
							&& request.getCookies()[i].getValue().length() > 3) 
						return true;
				i++;
			}
		}
		return false;
	}

	/*
	 * @SuppressWarnings("unchecked") private boolean
	 * noPermiso(HttpServletRequest request, ExternalContext ctx) { // String
	 * username = (String) ctx.getSessionMap().get("tokenUser"); String url =
	 * request.getRequestURL().toString(); //
	 * log.info("URL "+url+", usuario "+username);
	 * 
	 * CookiesUtils cookie = new CookiesUtils(); List<Permiso> permisos = new
	 * ArrayList<Permiso>(); for (int i = 0; i <= cookie.getAllCookie().length;
	 * i++) { if (cookie.getAllCookie()[i].getName().contains("listaPermiso")) {
	 * if (url.contains(cookie.getAllCookie()[i].getValue())) return true; } }
	 * return false; }
	 */
}
