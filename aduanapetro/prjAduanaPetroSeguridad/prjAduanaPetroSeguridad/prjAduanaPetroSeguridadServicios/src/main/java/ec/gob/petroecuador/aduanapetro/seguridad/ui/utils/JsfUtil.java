
package ec.gob.petroecuador.aduanapetro.seguridad.ui.utils;

import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public class JsfUtil {
 
	private static void addMessage(String message, Severity severity) {
		//FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		FacesMessage facesMessage = new FacesMessage(severity, message, null);
		FacesContext.getCurrentInstance().addMessage(null, facesMessage);
	}

	public static void addInfoMessage(String message) {
		addMessage(message, FacesMessage.SEVERITY_INFO);
	}

	public static void addErrorMessage(String message) {
		addMessage(message, FacesMessage.SEVERITY_ERROR);
	}
	
	public static void rootRedirect () throws IOException{
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance()
									.getExternalContext()
									.getRequestContextPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw e;
		}
		
	}
	
	public static void redireccionarUrl (String path) throws IOException{
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance()
									.getExternalContext()
									.getRequestContextPath()+path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw e;
		}
		
	}
	
	public static String urlPath() {
		return FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
	}

}
