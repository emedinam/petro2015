package ec.gob.petroecuador.aduanapetro.seguridad.ui;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Accion;
import ec.gob.petroecuador.aduanapetro.seguridad.services.AccionService;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("accionBB")
@Scope("request")
public class AccionBackingBean extends RegistroComun<Accion> {

	public AccionBackingBean() {
		super("", "");
		// TODO Auto-generated constructor stub
	}

	private Accion accion = new Accion();
	private Accion selectedAccion = new Accion();
	private List<Accion> listaAcciones = new ArrayList<Accion>();
	private static final Logger log = LoggerFactory
			.getLogger(AccionBackingBean.class);

	@Autowired
	private AccionService accionService;

	public Accion getAccion() {
		return accion;
	}

	public void setAccion(Accion accion) {
		this.accion = accion;
	}

	public List<Accion> getListaAcciones() {
		if (listaAcciones == null || listaAcciones.isEmpty())
			listaAcciones = accionService.obtenerActivos();
		return listaAcciones;
	}

	public void setListaAcciones(List<Accion> listaAcciones) {
		this.listaAcciones = listaAcciones;
	}

	public Accion getSelectedAccion() {
		return selectedAccion;
	}

	public void setSelectedAccion(Accion selectedAccion) {

		this.selectedAccion = selectedAccion;

	}

	private boolean duplicado(Accion elemento) {
		for (Accion a : accionService.obtenerTodos()) {
			if (a.getDescripcion().contentEquals(elemento.getDescripcion()) && !a.getId().equals(elemento.getId()))
				return true;
		}
		return false;
	}

	public void guardar() {
		if (duplicado(accion)) {
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, null,
					"Nombre de la Accion : El campo nombre de la accion no puede estar repetido");
			FacesContext.getCurrentInstance().addMessage(null, message);
		} else {
			accionService.guardar(accion);
			saveRegistro(AccionCRUD.Registrado, accion.getId(), Accion.class);
			borrarListaAcciones();
			accion = new Accion();
		}
	}

	private void borrarListaAcciones() {
		listaAcciones = null;
	}

	public void modificar() {
		log.info("Objeto Selected......" + selectedAccion);
		System.out.println(selectedAccion);
		if (duplicado(selectedAccion)) {
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, null,
					"Nombre de la Accion : El campo nombre de la accion no puede estar repetido");
			FacesContext.getCurrentInstance().addMessage(null, message);
		} else {
			accionService.guardar(selectedAccion);
			saveRegistro(AccionCRUD.Actualizado, selectedAccion.getId(),
					Accion.class);
			selectedAccion = new Accion();
		}
	}

	public void eliminarAccion() {
		log.info("Objeto Selected......" + selectedAccion);
		accionService.anular(selectedAccion);
		saveRegistro(AccionCRUD.Eliminado, selectedAccion.getId(), Accion.class);
		borrarListaAcciones();

	}

	@Override
	public void inicializarVariables() {
		
	}

}
