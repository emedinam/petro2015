package ec.gob.petroecuador.aduanapetro.seguridad.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.consts.ConstantesSeguridad;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Menu;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Usuario;
import ec.gob.petroecuador.aduanapetro.seguridad.services.MenuService;
import ec.gob.petroecuador.aduanapetro.seguridad.services.RolMenuService;
import ec.gob.petroecuador.aduanapetro.seguridad.services.UsuarioService;


@Component("menuHorizontalBean")
@Scope("request")
public class MenuHorizontalBackinBean {
	private static Logger log = Logger
			.getLogger(MenuHorizontalBackinBean.class);
	@Autowired
	private MenuService service;
	@Autowired
	private UsuarioService serviceUsuario ;
	@Autowired
	private RolMenuService rolmenuService ;
	private Usuario usuario = new Usuario() ;
	private Menu men;
	private MenuModel modelo = new DefaultMenuModel();
	private List<Menu> listaHorizontal = new ArrayList<Menu>();

	@PostConstruct
	public void init() {
		
		if (getUsuario().getUsername()==null || getUsuario().getUsername().contentEquals(ConstantesSeguridad.ADMIN_USER)) {
			listaHorizontal = service.getMenusByTipo(false, Estados.ACTIVO);
		}
		if (listaHorizontal.isEmpty())
			listaHorizontal =rolmenuService.getMenusByTipoPosicionYRol(false, false, getUsuario().getListaRoles());
			
		drawMenu();
	}
	
	private Usuario getUsuario(){
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		if (request.getCookies() != null) {
			
			log.info("Cantidad de Cookies.." +request.getCookies().length);
			int i= 0 ;
			while(i <= request.getCookies().length-1){
				log.info("Revizando Elemento de Cookie.."+ i);
				log.info("Revizando Elemento de Cookie.."+ request.getCookies()[i].getName());
				if (request.getCookies()[i].getName().equals("USERID"))
					if (request.getCookies()[i].getValue() != null
							&& request.getCookies()[i].getValue().length() > 3) 
						if(request.getCookies()[i].getValue().equalsIgnoreCase(ConstantesSeguridad.ADMIN_USER)){
							usuario.setUsername(ConstantesSeguridad.ADMIN_USER);
							return usuario ;
						}else {
							return serviceUsuario.buscarPorNombre(request.getCookies()[i].getValue()) ;
						}
					
				i++;
			}
		}
		return null;
		
	}
		

	public MenuModel getModelo() {
		return modelo;
	}

	public void setModelo(DefaultMenuModel model) {
		this.modelo = model;
	}

	public Menu getMen() {
		return men;
	}

	public void setMen(Menu men) {
		this.men = men;
	}

	private void drawMenu() {
		log.info("Obteniendo menus...");
		log.info("Dibujando menu horizontal...");
		for (Menu padre : listaHorizontal) {
			if (padre.getMenuPadre() == null && tieneHijos(padre)) {
				DefaultSubMenu subMenuItem = new DefaultSubMenu(
						padre.getNombreMenu());

				for (Menu hijos : listaHorizontal) {
					if (hijos.getMenuPadre() != null) {
						if (hijos.getMenuPadre().equals(padre)) {
							if (tieneHijos(hijos)) {
								DefaultSubMenu subHijosMenuItem = new DefaultSubMenu(
						  				hijos.getNombreMenu());
						      for (Menu nietos : listaHorizontal) {
									if (nietos.getMenuPadre() != null) {
										if (nietos.getMenuPadre().equals(hijos)) {
											DefaultMenuItem menuItem = new DefaultMenuItem(
													nietos.getNombreMenu(),
													null, nietos.getUrl());
											subHijosMenuItem
													.addElement(menuItem);
										}
									}
						      }
						        
								subMenuItem.addElement(subHijosMenuItem);
						      						
							} else {
								 
								DefaultMenuItem menuItem = new DefaultMenuItem(
										hijos.getNombreMenu(), null,
										hijos.getUrl());
								subMenuItem.addElement(menuItem);
							}
					}
					}
				}
				modelo.addElement(subMenuItem);
			} else {
				 if(padre.getMenuPadre()==null){
				DefaultMenuItem menuItem = new DefaultMenuItem(
						padre.getNombreMenu(), null, padre.getUrl());
				modelo.addElement(menuItem);
				 }
			}
		}
		modelo.generateUniqueIds();
		log.info("Menu horizontal construido.");
	}

	private boolean tieneHijos(Menu temp) {
		for (Menu m : listaHorizontal)
			if (m.getMenuPadre()!=null && m.getMenuPadre().equals(temp))
				return true;
		return false;
	}

}
