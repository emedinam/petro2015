package ec.gob.petroecuador.aduanapetro.seguridad.ui;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.consts.ConstantesSeguridad;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Usuario;
import ec.gob.petroecuador.aduanapetro.seguridad.services.UsuarioService;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("passwordBB")
@Scope("request")
public class ContrasennaBackingBean extends RegistroComun<Usuario> {

	public ContrasennaBackingBean() {
		super("", "");
	}

	private Usuario usuario;
	private String nuevaContra;
	private String oldContra;
	private boolean admin;

	@Autowired
	private UsuarioService usuarioService;

	public void cambiarPassword() {
		if (usuarioService.login(usuario.getUsername(), oldContra)) {
			usuario.setPassword(nuevaContra);
			usuarioService.almacenamientoSeguro(usuario);
			saveRegistro(AccionCRUD.Registrado, usuario.getId());
		}
	}

	

	public void setUsuarios(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getNuevaContra() {
		return nuevaContra;
	}

	public void setNuevaContra(String nuevaContra) {
		this.nuevaContra = nuevaContra;
	}

	public boolean idAdmin() {
		return (getUsuario().getUsername().equals("admin")) ? false : true;
	}

	public boolean isAdmin() {
		admin = idAdmin();
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	@Override
	public void inicializarVariables() {
		// TODO Auto-generated method stub
		
	}
	private Usuario getUsuario(){
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		if (request.getCookies() != null) {
			int i= 0 ;
			while(i <= request.getCookies().length-1){
				if (request.getCookies()[i].getName().equals("USERID"))
					if (request.getCookies()[i].getValue() != null
							&& request.getCookies()[i].getValue().length() > 3) 
						if(request.getCookies()[i].getValue().equals(ConstantesSeguridad.ADMIN_USER)) {
							return new Usuario(ConstantesSeguridad.ADMIN_USER) ;
						}else {
							return (Usuario) usuarioService.buscarPorNombre(request.getCookies()[i].getValue()) ;
						}
				i++;
			}
		}
		return null;
		
	}

}
