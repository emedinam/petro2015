package ec.gob.petroecuador.aduanapetro.seguridad.ui;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Permiso;
import ec.gob.petroecuador.aduanapetro.seguridad.services.PermisoService;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("permisoBB")
@Scope("request")
public class PermisosBackingBean extends RegistroComun<Permiso> {

	public PermisosBackingBean() {
		super("", "");
		// TODO Auto-generated constructor stub
	}

	@Autowired
	private PermisoService permisoService;

	private Permiso permiso = new Permiso();
	private Permiso selectedPermiso = new Permiso();

	private List<Permiso> listaPermisos = new ArrayList<Permiso>();

	public void guardar() {

		permisoService.guardar(permiso);
		saveRegistro(AccionCRUD.Registrado, permiso.getId(), Permiso.class);
		borrarListas();

	}

	private void borrarListas() {

		listaPermisos.clear();
		permiso = new Permiso();
	}

	public void modificar() {

		permisoService.guardar(selectedPermiso);
		saveRegistro(AccionCRUD.Actualizado, selectedPermiso.getId(),
				Permiso.class);
		borrarListas();

	}

	public void eliminar() {

		permisoService.anular(selectedPermiso);
		saveRegistro(AccionCRUD.Eliminado, selectedPermiso.getId(),
				Permiso.class);
		borrarListas();
	}

	public Permiso getPermiso() {
		return permiso;
	}

	public void setPermiso(Permiso permiso) {
		this.permiso = permiso;
	}

	public List<Permiso> getListaPermisos() {
		if (listaPermisos.isEmpty())
			listaPermisos = permisoService.obtenerActivos();
		return listaPermisos;
	}

	public void setListaPermisos(List<Permiso> listaPermisos) {
		this.listaPermisos = listaPermisos;
	}

	public Permiso getSelectedPermiso() {
		return selectedPermiso;
	}

	public void setSelectedPermiso(Permiso selectedPermiso) {
		this.selectedPermiso = selectedPermiso;
	}

	@Override
	public void inicializarVariables() {
		// TODO Auto-generated method stub

	}

}
