package ec.gob.petroecuador.aduanapetro.seguridad.ui;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.consts.ConstantesSeguridad;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Permiso;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.RolUsuario;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Usuario;
import ec.gob.petroecuador.aduanapetro.seguridad.services.UsuarioService;

@Component("controlBB")
@Scope("session")
public class ControlSeguridad {

	private static final Logger log = LoggerFactory
			.getLogger(ControlSeguridad.class);

	@Autowired
	private UsuarioService usuariosService;
	private Usuario user;

	public List<Permiso> urlContenidoRol() {
		/*
		 * String username = (String) FacesContext.getCurrentInstance()
		 * .getExternalContext().getSessionMap().get("tokenUser");
		 * 
		 * Usuario usuario = (Usuario)
		 * usuariosService.buscarPorNombre(username);
		 */
		List<Permiso> temporal = new ArrayList<Permiso>();
		return temporal;
	}

	public Boolean isAdmin() {
		return (FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap().get("tokenUser").equals("admin")) ? true
				: false;
	}

	public Boolean listarObjeto() {
		String s = "leer";
		for (Permiso p : urlContenidoRol()) {
			if (s.compareToIgnoreCase(p.getAccion().getDescripcion()) > 0)
				return true;
		}
		return false;

	}

	public Boolean guardarObjeto() {
		String s = "guardar";
		for (Permiso p : urlContenidoRol()) {
			if (s.compareToIgnoreCase(p.getAccion().getDescripcion()) > 0)
				return true;
		}
		return false;

	}

	public Boolean modificarObjeto() {
		String s = "modificar";
		for (Permiso p : urlContenidoRol()) {
			if (s.compareToIgnoreCase(p.getAccion().getDescripcion()) > 0)
				return true;
		}
		return false;

	}

	public Boolean eliminarObjeto() {
		String s = "eliminar";
		for (Permiso p : urlContenidoRol()) {
			if (s.compareToIgnoreCase(p.getAccion().getDescripcion()) > 0)
				return true;
		}
		return false;
	}

	public boolean getTienePermisoUsuarioCrear() {
		return getTienePermisoUsuario(ConstantesSeguridad.ACCION_CREAR);
	}

	public boolean getTienePermisoUsuarioModificar() {
		return getTienePermisoUsuario(ConstantesSeguridad.ACCION_MODIFICAR);
	}

	public boolean getTienePermisoUsuarioEliminar() {
		return getTienePermisoUsuario(ConstantesSeguridad.ACCION_ELIMINAR);
	}

	public boolean getTienePermisoUsuarioListar() {
		return getTienePermisoUsuario(ConstantesSeguridad.ACCION_LISTAR);
	}

	private boolean getTienePermisoUsuario(String action) {
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();

		String file = request.getRequestURI();
		if (request.getQueryString() != null) {
			file += '?' + request.getQueryString();
		}

		String url = null;

		try {
			URL reconstructedURL = new URL(request.getScheme(),
					request.getServerName(), request.getServerPort(), file);
			url = reconstructedURL.toString();
		} catch (MalformedURLException e1) {
			request.getRequestURL().toString().toLowerCase();
		}

		try {
			Usuario usuario = (Usuario) usuariosService
					.buscarPorNombre(getUsuario());
			if (usuario != null) {
				log.info("Usando URL para buscar acceso: " + url);
				if (usuario.getListaRoles() != null
						&& usuario.getListaRoles().size() > 0) {
					for (RolUsuario rol : usuario.getListaRoles()) {
						log.info("-> Rol Usuario "
								+ rol.getRol().getDescripcion()
								+ " tiene "
								+ (rol.getRol().getListaPermisos() != null ? rol
										.getRol().getListaPermisos().size()
										: "---") + " permisos");
						for (Permiso permiso : rol.getRol().getListaPermisos()) {
							String contenidoPermiso = permiso.getContenido()
									.getNombreEntidad().toLowerCase();
							String accionPermiso = permiso.getAccion()
									.getDescripcion().toLowerCase();
							log.info("--> Verificando si " + getUsuario()
									+ " tiene permiso de " + accionPermiso
									+ " a " + contenidoPermiso);
							if (url.contains(contenidoPermiso)
									&& accionPermiso.contains(action
											.toLowerCase())) {
								log.info("<-- SI TIENE!");
								return true;
							}
						}
					}
				} else
					log.error("El usuario " + getUsuario()
							+ " no tiene roles asignados");
			} else {
				if (getUsuario().equals(ConstantesSeguridad.ADMIN_USER))
					return true;
				log.error("El usuario " + getUsuario() + " no existe");
			}
		} catch (Exception e) {
			log.error("Error al obtener permisos del usuario para " + url + "-"
					+ action + ": " + e.getMessage());
			e.printStackTrace();
		}
		return false;
	}

	public String getUsuario() {
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		if (request.getCookies() != null) {

			log.info("Cantidad de Cookies.." + request.getCookies().length);
			int i = 0;
			while (i <= request.getCookies().length - 1) {
				log.info("Revizando Elemento de Cookie.." + i);
				log.info("Revizando Elemento de Cookie.."
						+ request.getCookies()[i].getName());
				if (request.getCookies()[i].getName().equals("USERID"))
					if (request.getCookies()[i].getValue() != null
							&& request.getCookies()[i].getValue().length() > 3)
						return request.getCookies()[i].getValue();
				i++;
			}
		}
		return null;

	}

	public Usuario getUser() {

		return  ((usuariosService.buscarPorNombre(getUsuario()) != null) ? ((Usuario)usuariosService
				.buscarPorNombre(getUsuario())) : new Usuario(
				ConstantesSeguridad.ADMIN_USER));
	}

	public void setUser(Usuario user) {
		this.user = user;
	}

}
