package ec.gob.petroecuador.aduanapetro.seguridad.ui;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Usuario;
import ec.gob.petroecuador.aduanapetro.seguridad.services.UsuarioService;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("usuarioBB")
@Scope("request")
public class UsuariosBackingBean extends RegistroComun<Usuario> {

	public UsuariosBackingBean() {
		super("", "");
		// TODO Auto-generated constructor stub
	}

	@Autowired
	private UsuarioService usuarioService;

	private Usuario usuario = new Usuario();

	private Usuario selectedUsuario = new Usuario();

	private List<Usuario> listaUsuarios = new ArrayList<Usuario>();

	
	private boolean duplicado(Usuario elemento) {
		for (Usuario a : usuarioService.obtenerTodos()) {
			if (a.getUsername()
					.contentEquals(elemento.getUsername()) && !a.getId().equals(elemento.getId()))
				return true;
		}
		return false;
	}
	
	public void guardar() {
		if (duplicado(usuario)) {
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, null,
					"Username: El campo Username no puede estar repetido");
			FacesContext.getCurrentInstance().addMessage(null, message);
		} else {
		usuarioService.guardar(usuario);
		saveRegistro(AccionCRUD.Registrado, usuario.getId(), Usuario.class);
		borrarListaUsuarios();
	}
	}

	public void modificar() {
		if (duplicado(selectedUsuario)) {
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, null,
					"Username: El campo Username no puede estar repetido");
			FacesContext.getCurrentInstance().addMessage(null, message);
		} else {
		usuarioService.guardar(selectedUsuario);
		saveRegistro(AccionCRUD.Actualizado, selectedUsuario.getId(),
				Usuario.class);
		borrarListaUsuarios();
	}
	}

	public void eliminar() {
		usuarioService.anular(selectedUsuario);
		saveRegistro(AccionCRUD.Eliminado, selectedUsuario.getId(),
				Usuario.class);
		borrarListaUsuarios();
	}

	private void borrarListaUsuarios() {
		listaUsuarios.clear();
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Usuario> getListaUsuarios() {
		if (listaUsuarios.isEmpty())
			listaUsuarios = usuarioService.obtenerActivos();
		return listaUsuarios;
	}

	public void setListaUsuarios(List<Usuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public Usuario getSelectedUsuario() {
		return selectedUsuario;
	}

	public void setSelectedUsuario(Usuario selectedUsuario) {
		this.selectedUsuario = selectedUsuario;
	}

	@Override
	public void inicializarVariables() {
		// TODO Auto-generated method stub
		
	}

}
