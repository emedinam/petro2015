package ec.gob.petroecuador.aduanapetro.seguridad.ui;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Contenido;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Registro;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Usuario;
import ec.gob.petroecuador.aduanapetro.seguridad.services.ContenidoService;
import ec.gob.petroecuador.aduanapetro.seguridad.services.RegistroService;
import ec.gob.petroecuador.aduanapetro.seguridad.services.UsuarioService;

@Component("registroBB")
@Scope("request")
public class RegistroBackingBean {

	@Autowired
	private RegistroService registroService;
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private ContenidoService contenidoService;

	private Usuario usuario = new Usuario();
	private Contenido contenido = new Contenido();
	private Registro registro = new Registro();

	private List<Registro> listaRegistro = new ArrayList<Registro>();
	private List<Registro> listaRegistroFiltrado = new ArrayList<Registro>();

	public void guardar() {
		registroService.guardar(registro);
		registro = new Registro();
		borrarListas();
	}

	private void borrarListas() {
		listaRegistro.clear();
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Contenido getContenido() {
		return contenido;
	}

	public void setContenido(Contenido contenido) {
		this.contenido = contenido;
	}

	public Registro getRegistro() {
		return registro;
	}

	public void setRegistro(Registro registro) {
		this.registro = registro;
	}

	public List<Registro> getListaRegistro() {
		listaRegistro = registroService.obtenerActivos();
		return listaRegistro;
	}

	public void setListaRegistro(List<Registro> listaRegistro) {
		this.listaRegistro = listaRegistro;
	}

	public List<Registro> getListaRegistroFiltrado() {
			return listaRegistroFiltrado;
	}

	public void setListaRegistroFiltrado(List<Registro> listaRegistroFiltrado) {
		this.listaRegistroFiltrado = listaRegistroFiltrado;
	}

	public RegistroService getRegistroService() {
		return registroService;
	}

	public void setRegistroService(RegistroService registroService) {
		this.registroService = registroService;
	}

	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public ContenidoService getContenidoService() {
		return contenidoService;
	}

	public void setContenidoService(ContenidoService contenidoService) {
		this.contenidoService = contenidoService;
	}

}
