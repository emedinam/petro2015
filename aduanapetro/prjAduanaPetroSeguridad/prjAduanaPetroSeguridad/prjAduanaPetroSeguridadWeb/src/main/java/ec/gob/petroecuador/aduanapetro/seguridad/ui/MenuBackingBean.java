package ec.gob.petroecuador.aduanapetro.seguridad.ui;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Menu;
import ec.gob.petroecuador.aduanapetro.seguridad.services.MenuService;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("menuBB")
@Scope("request")
public class MenuBackingBean extends RegistroComun<Menu> {
	
	private static Logger log = Logger.getLogger(MenuBackingBean.class);
	
	private boolean vertical = false ;

	public MenuBackingBean() {
		super("", "");
		// TODO Auto-generated constructor stub
	}

	private Menu menu = new Menu();
	private Menu selectedMenu = new Menu();
	private List<Menu> listaMenus = new ArrayList<Menu>();
	private List<Menu> listaPadre = new ArrayList<Menu>();

	@Autowired
	private MenuService service;

	private boolean duplicado(Menu elemento) {
		for (Menu a : service.obtenerTodos()) {
			if (a.getNombreMenu().contentEquals(elemento.getNombreMenu()) && !a.getId().equals(elemento.getId()))
				return true;
		}
		return false;
	}

	public void guardar() {
		if (duplicado(menu)) {
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, null,
					"Nombre de la Accion : El campo nombre de la accion no puede estar repetido");
			FacesContext.getCurrentInstance().addMessage(null, message);
		} else {
			service.guardar(menu);
			saveRegistro(AccionCRUD.Registrado, menu.getId(), Menu.class);
			refreshAll();
			borrarListaMenus();
		}
	}

	private void refreshAll() {
		menu = new Menu();
	}

	public void modificar() {
		if (duplicado(selectedMenu)) {
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, null,
					"Nombre de la Accion : El campo nombre de la accion no puede estar repetido");
			FacesContext.getCurrentInstance().addMessage(null, message);
		} else {
		}

		service.guardar(selectedMenu);
		saveRegistro(AccionCRUD.Actualizado, selectedMenu.getId(), Menu.class);
		refreshAll();
		borrarListaMenus();
	}

	public void eliminar() {

		service.anular(selectedMenu);
		saveRegistro(AccionCRUD.Eliminado, selectedMenu.getId(), Menu.class);
		refreshAll();
		borrarListaMenus();
	}

	private void borrarListaMenus() {
		listaMenus.clear();
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public List<Menu> getListaMenus() {
		if (listaMenus.isEmpty())
			listaMenus = service.obtenerActivos();
		return listaMenus;
	}

	public void setListaMenus(List<Menu> listaMenus) {
		this.listaMenus = listaMenus;
	}

	public List<Menu> getListaPadre() {
		if (listaMenus.isEmpty())
			listaPadre.add(new Menu());
		else
			listaPadre = listaMenus;
		return listaPadre;
	}

	public void setListaPadre(List<Menu> listaPadre) {
		this.listaPadre = listaPadre;
	}

	public Estados[] getEstados() {
		return Estados.values();
	}

	public Menu getSelectedMenu() {
		return selectedMenu;
	}

	public void setSelectedMenu(Menu selectedMenu) {
		this.selectedMenu = selectedMenu;
	}

	@Override
	public void inicializarVariables() {
		// TODO Auto-generated method stub

	}

	public boolean isVertical() {
		return vertical;
	}

	public void setVertical(boolean vertical) {
		this.vertical = vertical;
	}
	
	public void onChange(){
	
		log.info(this.isVertical());
	}

}
