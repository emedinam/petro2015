package ec.gob.petroecuador.aduanapetro.seguridad.ui;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.RolUsuario;
import ec.gob.petroecuador.aduanapetro.seguridad.services.RolUsuarioService;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("rolUsuarioBB")
@Scope("request")
public class RolesUsuarioBackingBean extends RegistroComun <RolUsuario> {

	public RolesUsuarioBackingBean() {
		super("","");
	}

	@Autowired
	private RolUsuarioService rolUsuarioService;

	private RolUsuario selectedRolUsuario = new RolUsuario();

	private RolUsuario rolUsuario = new RolUsuario();

	private List<RolUsuario> listaRolesUsuarios = new ArrayList<RolUsuario>();

	public void guardar() {

		rolUsuarioService.guardar(rolUsuario);
		saveRegistro(AccionCRUD.Registrado, rolUsuario.getId(),
				RolUsuario.class);
		borrarListas();
	}

	public void modificar() {
		rolUsuarioService.guardar(selectedRolUsuario);
		saveRegistro(AccionCRUD.Actualizado, selectedRolUsuario.getId(),
				RolUsuario.class);
		borrarListas();
	}

	public void eliminar() {
		rolUsuarioService.eliminar(selectedRolUsuario);
		saveRegistro(AccionCRUD.Eliminado, selectedRolUsuario.getId(),
				RolUsuario.class);
		borrarListas();
	}

	private void borrarListas() {

		listaRolesUsuarios.clear();
		rolUsuario = new RolUsuario();
		selectedRolUsuario = new RolUsuario();
	}

	public List<RolUsuario> getListaRolesUsuarios() {
		if (listaRolesUsuarios.isEmpty())
			listaRolesUsuarios = rolUsuarioService.obtenerTodos();
		return listaRolesUsuarios;
	}

	public RolUsuario getRolUsuario() {
		return rolUsuario;
	}

	public void setRolUsuario(RolUsuario rolUsuario) {
		this.rolUsuario = rolUsuario;
	}

	public void setListaRolesUsuarios(List<RolUsuario> listaRolesUsuarios) {
		this.listaRolesUsuarios = listaRolesUsuarios;
	}

	public RolUsuario getSelectedRolUsuario() {
		return selectedRolUsuario;
	}

	public void setSelectedRolUsuario(RolUsuario selectedRolUsuario) {
		this.selectedRolUsuario = selectedRolUsuario;
	}

	@Override
	public void inicializarVariables() {
		// TODO Auto-generated method stub
		
	}

	

}
