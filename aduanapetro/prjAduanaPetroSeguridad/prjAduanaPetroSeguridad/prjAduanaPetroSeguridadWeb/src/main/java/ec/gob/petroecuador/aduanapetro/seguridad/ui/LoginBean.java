package ec.gob.petroecuador.aduanapetro.seguridad.ui;

import java.io.IOException;
import java.io.Serializable;



import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;



import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;







import ec.gob.petroecuador.aduanapetro.seguridad.consts.ConstantesSeguridad;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Usuario;
import ec.gob.petroecuador.aduanapetro.seguridad.services.UsuarioService;


@Component("loginBean")
@Scope("request")
public class LoginBean  implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(LoginBean.class);
			
	@Autowired
	private UsuarioService service;

	private Usuario user = new Usuario();
	private Cookie cookie = null ;

	public void isLogin() {
		HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest() ;
		
		
		try {
			if (user.getUsername().equalsIgnoreCase(ConstantesSeguridad.ADMIN_USER)
					&& user.getPassword().equalsIgnoreCase(ConstantesSeguridad.ADMIN_USER)) {
				   		cookie = new Cookie("USERID", URLDecoder.decode(ConstantesSeguridad.ADMIN_USER,"UTF-8"));
						cookie.setSecure(false);
						cookie.setPath("/");
						cookie.setMaxAge(-1);	
				response.addCookie(cookie);
		        log.info("->-> Creando Cookies ADMINUSER en cliente "+ request.getCookies());
		    	 response.sendRedirect("pages/usuariosPage.jsf");
				 }
			
			if (service.login(user.getUsername(), user.getPassword())) {
		   		cookie = new Cookie("USERID",URLDecoder.decode(user.getUsername(),"UTF-8"));
				cookie.setSecure(false);
				cookie.setPath("/");
				cookie.setMaxAge(-1);	
	          	response.addCookie(cookie);
                log.info("->-> Creando Cookies " + user.getUsername() + " en cliente "+ request.getCookies());
                response.sendRedirect("pages/usuariosPages.jsf");
		        }
			else {
				cookie = new Cookie("USERID",null);
				cookie.setSecure(false);
				cookie.setPath("/");
				cookie.setMaxAge(-1);	
	          	response.addCookie(cookie);
                log.info("->-> Creando Cookies NULL en cliente "+ request.getCookies());
            	FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Usuario y Contraseña Incorrectos ", null);
				FacesContext.getCurrentInstance().addMessage(null, msg);
				
			}
				
				
			}catch(Exception e) {
			log.error("Error al obtener el usuario: "+e.getMessage());
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Usuario no existe ", null);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
	}

	public void logout() {
		HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest() ;
		cookie = new Cookie("USERID",null);
		cookie.setSecure(false);
		cookie.setPath("/");
		cookie.setMaxAge(-1);	
      	response.addCookie(cookie);
      	try {
      		 log.info("->-> Creando Cookies NULL en cliente "+ request.getCookies());
			response.sendRedirect("loginPages.jsf");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}

		
	public Usuario getUser() {
		return user;
	}

	private void loadUser() {
		
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		if (request.getCookies() != null) {
			
			log.info("Cantidad de Cookies.." +request.getCookies().length);
			int i= 0 ;
			while(i <= request.getCookies().length-1){
				log.info("Revizando Elemento de Cookie.."+ i);
				log.info("Revizando Elemento de Cookie.."+ request.getCookies()[i].getName());
				if (request.getCookies()[i].getName().equals("USERID"))
					if (request.getCookies()[i].getValue() != null
							&& !request.getCookies()[i].getValue().isEmpty())
						if (request.getCookies()[i].getValue().equalsIgnoreCase(ConstantesSeguridad.ADMIN_USER)){
							user.setUsername(ConstantesSeguridad.ADMIN_USER);
						}else {
							user = service.buscarPorNombre(request.getCookies()[i].getValue()) ;
						}
							
		    	i++;
			}
		}
		
	}

	public void setUser(Usuario user) {
		this.user = user;
	}


}
