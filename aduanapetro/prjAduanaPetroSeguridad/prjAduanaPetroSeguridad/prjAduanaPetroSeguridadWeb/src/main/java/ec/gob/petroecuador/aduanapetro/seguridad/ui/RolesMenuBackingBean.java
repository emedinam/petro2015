package ec.gob.petroecuador.aduanapetro.seguridad.ui;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Menu;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.RolMenu;
import ec.gob.petroecuador.aduanapetro.seguridad.services.RolMenuService;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("rolmenuBB")
@Scope("request")
public class RolesMenuBackingBean extends RegistroComun<RolMenu> {

	public RolesMenuBackingBean() {
		super("", "");
	}

	@Autowired
	private RolMenuService rolMenuService;

	private Menu menu = new Menu();
	private RolMenu rolmenu = new RolMenu();
	private RolMenu selectedRolMenu = new RolMenu();

	private List<RolMenu> listaRolesMenus = new ArrayList<RolMenu>();

	public void guardar() {
		rolMenuService.guardar(rolmenu);
		saveRegistro(AccionCRUD.Registrado, rolmenu.getId(), RolMenu.class);
		borrarListas();
	}

	public void modificar() {
		rolMenuService.guardar(selectedRolMenu);
		saveRegistro(AccionCRUD.Actualizado, selectedRolMenu.getId(),
				RolMenu.class);
		borrarListas();
	}

	public void eliminar() {
		rolMenuService.eliminar(selectedRolMenu);
		saveRegistro(AccionCRUD.Eliminado, selectedRolMenu.getId(),
				RolMenu.class);
		borrarListas();
	}

	private void borrarListas() {

		listaRolesMenus.clear();
		rolmenu = new RolMenu();
		selectedRolMenu = new RolMenu();
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public RolMenu getRolmenu() {
		return rolmenu;
	}

	public void setRolmenu(RolMenu rolmenu) {
		this.rolmenu = rolmenu;
	}

	public List<RolMenu> getListaRolesMenus() {
		if (listaRolesMenus.isEmpty())
			listaRolesMenus = rolMenuService.obtenerTodos();
		return listaRolesMenus;
	}

	public void setListaRolesMenus(List<RolMenu> listaRolesMenus) {
		this.listaRolesMenus = listaRolesMenus;
	}

	public RolMenu getSelectedRolMenu() {
		return selectedRolMenu;
	}

	public void setSelectedRolMenu(RolMenu selectedRolMenu) {
		this.selectedRolMenu = selectedRolMenu;
	}

	@Override
	public void inicializarVariables() {
		// TODO Auto-generated method stub
		
	}

}
