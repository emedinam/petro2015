package ec.gob.petroecuador.aduanapetro.seguridad.ui;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Rol;
import ec.gob.petroecuador.aduanapetro.seguridad.services.RolService;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("rolesBB")
@Scope("request")
public class RolesBackingBean extends RegistroComun<Rol> {

	public RolesBackingBean() {
		super("","");
	}

	private Rol rol = new Rol();
	private Rol selectedRol = new Rol();
	private List<Rol> listaRoles = new ArrayList<Rol>();

	@Autowired
	private RolService rolService;
	
	private boolean duplicado(Rol elemento) {
		for (Rol a : rolService.obtenerTodos()) {
			if (a.getNombre()
					.contentEquals(elemento.getNombre()) && !a.getId().equals(elemento.getId()))
				return true;
		}
		return false;
	}

	public void guardar() {
		if (duplicado(rol)) {
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, null,
					"Nombre del Rol : El campo nombre del rol no puede estar repetido");
			FacesContext.getCurrentInstance().addMessage(null, message);
		} else {
		rolService.guardar(rol);
		saveRegistro(AccionCRUD.Registrado, rol.getId(), Rol.class);
		borrarListaRoles();
	}
	}

	public void modificar() {
		if (duplicado(selectedRol)) {
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, null,
					"Nombre del Rol : El campo nombre del rol no puede estar repetido");
			FacesContext.getCurrentInstance().addMessage(null, message);
		} else {
		rolService.guardar(selectedRol);
		saveRegistro(AccionCRUD.Actualizado, selectedRol.getId(), Rol.class);
		borrarListaRoles();
	}
	}

	public void eliminar() {
		rolService.anular(selectedRol);
		saveRegistro(AccionCRUD.Eliminado, selectedRol.getId(), Rol.class);
		borrarListaRoles();
	}

	private void borrarListaRoles() {
		listaRoles.clear();
		rol = new Rol();
	}

	public List<Rol> getListaRoles() {
		if (listaRoles.isEmpty())
			listaRoles = rolService.obtenerActivos();
		return listaRoles;
	}

	public void setListaRoles(List<Rol> listaRoles) {
		this.listaRoles = listaRoles;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public Rol getSelectedRol() {
		return selectedRol;
	}

	public void setSelectedRol(Rol selectedRol) {
		this.selectedRol = selectedRol;
	}

	@Override
	public void inicializarVariables() {
		// TODO Auto-generated method stub
		
	}

}
