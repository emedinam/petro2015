package ec.gob.petroecuador.aduanapetro.seguridad.ui;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Contenido;
import ec.gob.petroecuador.aduanapetro.seguridad.services.ContenidoService;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("contenidoBB")
@Scope("request")
public class ContenidoBackingBean extends RegistroComun<Contenido> {

	public ContenidoBackingBean() {
		super("", "");
	}

	private Contenido contenido = new Contenido();
	private Contenido selectedContenido = new Contenido();

	private List<Contenido> listaContenidos = new ArrayList<Contenido>();
	private static final Logger log = LoggerFactory
			.getLogger(ContenidoBackingBean.class);
	@Autowired
	private ContenidoService contenidoService;

	private boolean duplicado(Contenido elemento) {
		for (Contenido a : contenidoService.obtenerTodos()) {
			if (a.getNombreEntidad()
					.contentEquals(elemento.getNombreEntidad())&& !a.getId().equals(elemento.getId()))
				return true;
		}
		return false;
	}

	public void guardar() {
		if (duplicado(contenido)) {
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, null,
					"Nombre del Contenido : El campo nombre del contenido no puede estar repetido");
			FacesContext.getCurrentInstance().addMessage(null, message);
		} else {
			contenidoService.guardar(contenido);
			saveRegistro(AccionCRUD.Registrado, contenido.getId(),
					Contenido.class);
			borrarListaContenidos();
		}
	}

	public void modificar() {
		log.info("Objeto Selected......" + selectedContenido);
		System.out.println(selectedContenido);
		if (duplicado(selectedContenido)) {
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, null,
					"Nombre del Contenido : El campo nombre del contenido no puede estar repetido");
			FacesContext.getCurrentInstance().addMessage(null, message);
		} else {
			contenidoService.guardar(selectedContenido);
			saveRegistro(AccionCRUD.Actualizado, selectedContenido.getId(),
					Contenido.class);
			borrarListaContenidos();
		}
	}

	public void eliminar() {
		log.info("Objeto Selected......" + selectedContenido);
		System.out.println(selectedContenido);
		contenidoService.anular(selectedContenido);
		saveRegistro(AccionCRUD.Eliminado, selectedContenido.getId(),
				Contenido.class);
		borrarListaContenidos();
	}

	private void borrarListaContenidos() {
		listaContenidos.clear();
		contenido = new Contenido();
	}

	public Contenido getContenido() {
		return contenido;
	}

	public void setContenido(Contenido contenido) {
		this.contenido = contenido;
	}

	public List<Contenido> getListaContenidos() {
		if (listaContenidos.isEmpty())
			listaContenidos = contenidoService.obtenerActivos();
		return listaContenidos;
	}

	public void setListaContenidos(List<Contenido> listaContenidos) {
		this.listaContenidos = listaContenidos;
	}

	public Contenido getSelectedContenido() {
		return selectedContenido;
	}

	public void setSelectedContenido(Contenido selectedContenido) {
		this.selectedContenido = selectedContenido;
	}

	@Override
	public void inicializarVariables() {
		// TODO Auto-generated method stub
		
	}

}
