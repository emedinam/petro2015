package ec.gob.petroecuador.aduanapetro.seguridad.ui;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.seguridad.consts.ConstantesSeguridad;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Menu;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.RolMenu;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Rol;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Usuario;
import ec.gob.petroecuador.aduanapetro.seguridad.services.MenuService;
import ec.gob.petroecuador.aduanapetro.seguridad.services.RolMenuService;
import ec.gob.petroecuador.aduanapetro.seguridad.services.UsuarioService;


@Component("templateMenuBB")
@Scope("request")
public class TemplateMenuBackingBean {
	private static final Logger log = LoggerFactory.getLogger(TemplateMenuBackingBean.class);
	
	@Autowired
	private MenuService service;
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private RolMenuService rolmenuService;

	private Menu menu = new Menu();
	private Usuario usuario = new Usuario();
	private Rol rol = new Rol();
	private RolMenu rolmenu = new RolMenu();

	private List<Menu> listaMenu = new ArrayList<Menu>();
	private List<Usuario> listaUsuarios = new ArrayList<Usuario>();
	private List<Rol> listaRoles = new ArrayList<Rol>();
	private List<RolMenu> listaRolesMenus = new ArrayList<RolMenu>();
	private List<Menu> listaMenuTransversales = new ArrayList<Menu>();
	private List<Menu> listaMenuHorizantales = new ArrayList<Menu>();
	private List<Menu> listaMenuAcceso = new ArrayList<Menu>();

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	private Usuario getUsuario(){
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		if (request.getCookies() != null) {
			
			log.info("Cantidad de Cookies.." +request.getCookies().length);
			int i= 0 ;
			while(i <= request.getCookies().length-1){
				log.info("Revizando Elemento de Cookie.."+ i);
				log.info("Revizando Elemento de Cookie.."+ request.getCookies()[i].getName());
				if (request.getCookies()[i].getName().equals("USERID"))
					if (request.getCookies()[i].getValue() != null
							&& request.getCookies()[i].getValue().length() > 3) 
						if(request.getCookies()[i].getValue().equalsIgnoreCase(ConstantesSeguridad.ADMIN_USER)){
							usuario.setUsername(ConstantesSeguridad.ADMIN_USER);
							return usuario ;
						}else {
							return usuarioService.buscarPorNombre(request.getCookies()[i].getValue()) ;
						}
					
				i++;
			}
		}
		return null;
		
	}
	
		public RolMenu getRolmenu() {
		return rolmenu;
	}

	public void setRolmenu(RolMenu rolmenu) {
		this.rolmenu = rolmenu;
	}

	public List<Menu> getListaMenu() {
		if (getUsuario().getUsername().contentEquals(ConstantesSeguridad.ADMIN_USER)) {
			listaMenuAcceso = service.obtenerTodos();
		}
		if (listaMenu.isEmpty())
			for (RolMenu rolmenu : rolmenuService.obtenerActivos())
				if (rolmenu.getId().equals(rol.getId()))
					listaMenu.add(rolmenu.getMenu());
		return listaMenu;
	}

	public void setListaMenu(List<Menu> listaMenu) {
		this.listaMenu = listaMenu;
	}

	public List<Usuario> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<Usuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public List<Rol> getListaRoles() {
		return listaRoles;
	}

	public void setListaRoles(List<Rol> listaRoles) {
		this.listaRoles = listaRoles;
	}

	public List<RolMenu> getListaRolesMenus() {

		return listaRolesMenus;
	}

	public void setListaRolesMenus(List<RolMenu> listaRolesMenus) {
		this.listaRolesMenus = listaRolesMenus;
	}

	public List<Menu> getListaMenuTransversales() {
		if (listaMenuTransversales.isEmpty()) {
			if (getUsuario().getUsername().contentEquals(ConstantesSeguridad.ADMIN_USER)) {
				listaMenuTransversales = service.getMenusByTipoYPosicion(true, true);
			}
			else {
				listaMenuTransversales = rolmenuService.getMenusByTipoPosicionYRol(true,
						true,getUsuario().getListaRoles());
			}
		}
		return listaMenuTransversales;
	}

	public void setListaMenuTransversales(List<Menu> listaMenuTransversales) {
		this.listaMenuTransversales = listaMenuTransversales;
	}

	public List<Menu> getListaMenuHorizantales() {

		return listaMenuHorizantales;
	}

	public void setListaMenuHorizantales(List<Menu> listaMenuHorizantales) {
		this.listaMenuHorizantales = listaMenuHorizantales;
	}

	public List<Menu> getListaMenuAcceso() {
		if (listaMenuAcceso.isEmpty()) {
			if (getUsuario().getUsername().contentEquals(ConstantesSeguridad.ADMIN_USER)) {
				listaMenuAcceso = service.getMenusByTipoYPosicion(true, false);
			}
			else {
				listaMenuAcceso = rolmenuService.getMenusByTipoPosicionYRol(true,
						false, getUsuario().getListaRoles());
			}
		}
		return listaMenuAcceso;
	}

	public void setListaMenuAcceso(List<Menu> listaMenuAcceso) {
		this.listaMenuAcceso = listaMenuAcceso;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

}
