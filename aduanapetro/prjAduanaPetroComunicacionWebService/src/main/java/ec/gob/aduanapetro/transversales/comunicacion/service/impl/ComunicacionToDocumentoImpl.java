package ec.gob.aduanapetro.transversales.comunicacion.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.Policy.Parameters;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.ws.RequestWrapper;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRCsvDataSource;
import net.sf.jasperreports.engine.data.JRCsvDataSourceProvider;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.cxf.common.util.ReflectionInvokationHandler.Optional;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.jsf.FacesContextUtils;

import ec.gob.aduanapetro.transversales.comunicacion.consts.ConstantesSeguridad;
import ec.gob.aduanapetro.transversales.comunicacion.service.ComunicacionToDocumento;

@WebService(endpointInterface = "ec.gob.aduanapetro.transversales.comunicacion.service.ComunicacionToDocumento")
public class ComunicacionToDocumentoImpl implements ComunicacionToDocumento {

		
	public  void initFiles() {
		 File r = new File(ConstantesSeguridad.BASE_CONFIG_PATH_REPORTE) ;
		 File t = new File(ConstantesSeguridad.BASE_CONFIG_PATH_TEMPLATE) ;
		 File d = new File(ConstantesSeguridad.BASE_CONFIG_PATH_DOCUMENTO) ;
		 File x = new File(ConstantesSeguridad.BASE_CONFIG_PATH_JRXML) ;
		   if(!r.exists())
			  r.mkdir();
		  
		   if(!t.exists())
			   t.mkdir();
		   
		   if(!d.exists())
			   d.mkdir();
		   if(!x.exists())
			   x.mkdir();
		   
		
		// TODO Auto-generated constructor stub
	}
	@Override
	public List<String> getTipodocumentoParametros() {
		this.initFiles();
		List<String> temporal = new ArrayList<String>();
		File directorio = new File(ConstantesSeguridad.BASE_CONFIG_PATH_JRXML);
		try {
			for (File f : directorio.listFiles()) {
				
					JasperDesign template = JRXmlLoader.load(f);
					String cadena = "";
					for (JRParameter parametro : template.getParametersList()) {
						if (!parametro.getName().contains("REPORT")
								&& !parametro.getName().contains("PAGINATION")
								&& !parametro.getName().contains("FILTER")
								&& !parametro.getName().contains("SORT")
								)
							cadena = cadena.concat(parametro.getName() + " , "
									+ parametro.getValueClassName() + ";  ");
					}

					
					temporal.add(cadena);
				
			}
			} catch (JRException e) {
				new Exception("No existe plantillas del reporte en el directorio" ,e) ;
			}
		return temporal;
	}

	@Override
	public Boolean uploadPlantilla(File file) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@WebMethod(operationName="toDocument")
	@RequestWrapper(localName = "toDocumentRequestWrapper", 
    className = "ec.gob.aduanapetro.transversales.comunicacion.helper.WebServicesWrappeRequest")
	public String toDocumento(@WebParam(name="tipoDocumento")String tipoDocumento, @WebParam(name="codigoTramite")String codigoTramite, @WebParam(name="parameter")
			String parameter) {
		if(codigoTramite == "?")
			codigoTramite = tipoDocumento ;
		String destino = "";
        this.initFiles();
		Map<String, Object> parameters = new Hashtable<String, Object>();
		// Convertir Parametros a map
		for (String sub : parameter.split(";")) {
			String elemento[] = sub.split(",");
			parameters.put(elemento[0], elemento[1]);
		}
		File in = new File(ConstantesSeguridad.BASE_CONFIG_PATH_TEMPLATE+ System.getProperty("file.separator")+tipoDocumento + ".jasper");
		JasperPrint jasper;
		try {
			jasper = JasperFillManager.fillReport(in.getPath(), parameters);
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			Date date = new Date();
			destino = ConstantesSeguridad.BASE_CONFIG_PATH_DOCUMENTO+ System.getProperty("file.separator")+codigoTramite+"_"+dateFormat.format(date)+ ".pdf";
			JasperExportManager.exportReportToPdfFile(jasper, destino);

		} catch (JRException e) {
					new Exception("No existe plantillas del reporte en el directorio" ,e);
		}

		return destino;
	}
}
