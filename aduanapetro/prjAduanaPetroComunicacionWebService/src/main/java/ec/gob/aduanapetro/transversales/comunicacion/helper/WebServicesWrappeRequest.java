package ec.gob.aduanapetro.transversales.comunicacion.helper;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlType(propOrder = { "tipoDocumento", "String codigoTramite", "parameter" })
@XmlAccessorType(XmlAccessType.FIELD)
public class WebServicesWrappeRequest {
	
	@XmlElement(name="tipoDocumento", required=true)
	public String tipoDocumento  ;
	@XmlElement(name="codigoTramite", required=true)
	public String codigoTramite  ;
	@XmlElement(name="parameter", required=true)
	public String parameter  ;
	
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getCodigoTramite() {
		return codigoTramite;
	}
	public void setCodigoTramite(String codigoTramite) {
		this.codigoTramite = codigoTramite;
	}
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	
}
