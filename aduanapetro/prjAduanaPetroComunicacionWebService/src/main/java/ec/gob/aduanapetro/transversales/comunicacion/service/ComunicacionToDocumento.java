package ec.gob.aduanapetro.transversales.comunicacion.service;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.jws.WebService;

@WebService
public interface ComunicacionToDocumento {
	List<String> getTipodocumentoParametros() ;
    String toDocumento(String tipoDocumento,String codigoTramite,String parameter);
    Boolean uploadPlantilla(File file);
}

