package ec.gob.aduanapetro.transversales.comunicacion.consts;

import java.util.Properties;

public class DefaultProperties {

	private static Properties defaultProperties;

	public static Properties getDefaultProperties() {
		if (defaultProperties == null) {
			defaultProperties = new Properties();
			defaultProperties.setProperty(
					"ec.gob.petroecuador.aduanapetro.seguridad.debug", "true");
			defaultProperties.setProperty(
					"ec.gob.petroecuador.aduanapetro.seguridad.accion.crear",
					"Crear");
			defaultProperties
					.setProperty(
							"ec.gob.petroecuador.aduanapetro.seguridad.accion.modificar",
							"Modificar");
			defaultProperties
					.setProperty(
							"ec.gob.petroecuador.aduanapetro.seguridad.accion.eliminar",
							"Eliminar");
			defaultProperties.setProperty(
					"ec.gob.petroecuador.aduanapetro.seguridad.accion.listar",
					"Listar");
			defaultProperties.setProperty(
					"ec.gob.petroecuador.aduanapetro.seguridad.admin.user",
					"admin");
			defaultProperties
					.setProperty(
							"ec.gob.petroecuador.aduanapetro.seguridad.mensaje.registroalterado",
							"El registro %s de %s ha sido %s");
			defaultProperties
					.setProperty(
							"ec.gob.petroecuador.aduanapetro.comunicacion.BASE_CONFIG_PATH_REPORTE",
							System.getProperty("user.home")
									+ System.getProperty("file.separator")
									+ "aduanapetro"
									+ System.getProperty("file.separator")
									+ "reportes");
			defaultProperties
					.setProperty(
							"ec.gob.petroecuador.aduanapetro.comunicacion.BASE_CONFIG_PATH_TEMPLATE",
							System.getProperty("user.home")
									+ System.getProperty("file.separator")
									+ "aduanapetro"
									+ System.getProperty("file.separator")
									+ "reportes"
									+ System.getProperty("file.separator")
									+ "templates");
			defaultProperties
					.setProperty(
							"ec.gob.petroecuador.aduanapetro.comunicacion.BASE_CONFIG_PATH_JRXML",
							System.getProperty("user.home")
									+ System.getProperty("file.separator")
									+ "aduanapetro"
									+ System.getProperty("file.separator")
									+ "reportes"+ System.getProperty("file.separator")
									+ "JRXML");
			defaultProperties
					.setProperty(
							"ec.gob.petroecuador.aduanapetro.comunicacion.BASE_CONFIG_PATH_DOCUMENTOS",
							System.getProperty("user.home")
									+ System.getProperty("file.separator")
									+ "aduanapetro"
									+ System.getProperty("file.separator")
									+ "reportes"+ System.getProperty("file.separator")
									+ "documentos");

		}
		return defaultProperties;
	}

}
