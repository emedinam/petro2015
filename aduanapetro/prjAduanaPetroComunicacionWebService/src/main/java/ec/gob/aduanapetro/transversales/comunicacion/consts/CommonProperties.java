package ec.gob.aduanapetro.transversales.comunicacion.consts;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;


public class CommonProperties {
	
	static Logger log = Logger.getLogger(CommonProperties.class);
			
	public static final String BASE_CONFIG_PATH = System.getProperty("user.home") + System.getProperty("file.separator") + "aduanapetro"; 
	
	private static Properties prop = new Properties();
	
	private static String resourceFilePath;
	
	private static final String RESOURCE_NAME = "seguridad.properties";
	
	static {
		loadResourceFile();
	}
	
	private static String getFilePath() {
		if(resourceFilePath==null) {
			resourceFilePath = getResourceFileName(RESOURCE_NAME);
		}
		return resourceFilePath;
	}
	
	private static void loadResourceFile() {
        FileInputStream configFileStream = null;
        
        //Si el archivo de propiedades no existe se lo crea
        //con propiedades por defecto
        if(!existResourceKeyFile()) {
        	createResourceFile();
        }
        
		try {
			configFileStream = new FileInputStream(resourceFilePath);
			prop.load(configFileStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally { 
			if(configFileStream!=null) {
				try {
					configFileStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
            }
        }
	}
	
	/**
	 * Crea un archivo de propiedades
	 */
	private static void createResourceFile() {
		String filePath = getFilePath();
		File rf = new File(filePath);
		try {
			rf.createNewFile();
			saveResources(DefaultProperties.getDefaultProperties());
		} catch (IOException e) {
			System.err.println("Imposible crear el archivo "+filePath);
			e.printStackTrace();
		}

	}
	
	/**
	 * Exist resource
	 * @param resourceKeyName
	 * @return
	 */
	private static boolean existResourceKeyFile() {
		File rf = new File(getFilePath());
		return rf.exists();
	}
	
	private static void saveResources(Properties properties) throws IOException {
		String filePath = getFilePath();
		FileOutputStream configFileStream = new FileOutputStream(filePath);
		try {
			properties.store(configFileStream, "");
		} catch (FileNotFoundException e) {
			throw new IOException("Error: "+e.getMessage());
		} finally {
			configFileStream.close();
		}
	}
	
	private static String createProperty(String key) {
		String value = DefaultProperties.getDefaultProperties().getProperty(key, "");
		prop.setProperty(key, value);
		try {
			saveResources(prop);
		} catch (IOException e) {
			log.error("Error al crear la propiedad "+key+" debido a: "+e.getMessage());
		}
		return value;
	}
	
	public static String getProperty(String key) {
		String value = prop.getProperty(key);
		if(value == null) { 
			log.error("La propiedad "+key+" no existe");
			value = createProperty(key);
		}
		return value;
	}

	public static boolean getPropertyBoolean(String key) {
		String value = prop.getProperty(key);
		if(value == null) { 
			log.error("La propiedad "+key+" no existe");
			value = createProperty(key);
		}
		if(value.isEmpty()) return false;
		return new Boolean(value);
	}	
	
	public static int getPropertyInteger(String key) {
		String value = prop.getProperty(key);
		if(value == null) { 
			log.error("La propiedad "+key+" no existe");
			value = createProperty(key);
		}
		if(value.isEmpty()) return 0;
		return new Integer(prop.getProperty(key));
	}
	
	/**
	 * Get resource file name
	 * @param resourceKeyName
	 * @return
	 */
	public static String getResourceFileName(String resourceFileName) {
		File baseDir = new File(BASE_CONFIG_PATH);
		if(!baseDir.exists()) {
			baseDir.mkdir();
		}
		return BASE_CONFIG_PATH + System.getProperty("file.separator") + resourceFileName;
	}
		
}
