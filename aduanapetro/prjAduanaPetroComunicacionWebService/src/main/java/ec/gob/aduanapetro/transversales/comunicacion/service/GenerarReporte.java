package ec.gob.aduanapetro.transversales.comunicacion.service;

public interface GenerarReporte {

	String toPDF() ;
	String toWord();
	String toExcel() ;
}
