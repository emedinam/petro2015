package ec.gob.petroecuador.aduanapetro.catalogos.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.CatalogoDetalle;

public interface MtCatalogoDetalleService extends GenericService<CatalogoDetalle, Long> {
	public List<CatalogoDetalle> getMtCatalogoDetalles(Long idPadre);
	public List<CatalogoDetalle> getMtCatalogoDetallesOrder();
}
