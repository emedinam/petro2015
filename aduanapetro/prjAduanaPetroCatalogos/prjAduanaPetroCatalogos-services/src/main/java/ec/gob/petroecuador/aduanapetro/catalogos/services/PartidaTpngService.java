package ec.gob.petroecuador.aduanapetro.catalogos.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTpng;

public interface PartidaTpngService extends GenericService<PartidaTpng, Long> {
	
	//public List<PartidaTnan> getPartidaTnans(Long idPadre);
	public List<PartidaTpng> getListaPartidaTpngs(Long idPadre);

}
