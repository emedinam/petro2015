package ec.gob.petroecuador.aduanapetro.catalogos.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaConvenio;

public interface PartidaConvenioService extends GenericService<PartidaConvenio, Long> {
	
	//public List<PartidaTnan> getPartidaTnans(Long idPadre);
	public List<PartidaConvenio> getListaPartidaConvenios(Long idPadre);

}
