package ec.gob.petroecuador.aduanapetro.catalogos.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;
@NamedQueries({
	@NamedQuery(
		name = "filterByPartidaTnanRestriccion",
		query = "select m from PartidaTnanRestriccion m where m.ifPartidaTnan.id = :idPartidaTnan"
	),
	@NamedQuery(
			name = "orderByRestriccion",
			query = "select m from PartidaTnanRestriccion m order by m.ifEntidad"
		),
})
@Entity
@Table(name = "MT_PARTIDA_TNAN_RESTRICCION", schema= "APS_ARANCEL")
public class PartidaTnanRestriccion extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="if_partida_tnan")
	private Partida ifPartidaTnan;
	
	//@Column(name="if_tributo")
	@ManyToOne
	@JoinColumn(name="if_entidad")
	private CatalogoDetalle ifEntidad;
	
	private String observacion;
	//@Column(name="if_tipo")
	@ManyToOne
	@JoinColumn(name="if_pais")
	private CatalogoDetalle ifPais;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
 

	
	
	
	public Partida getIfPartidaTnan() {
		return ifPartidaTnan;
	}

	public void setIfPartidaTnan(Partida ifPartidaTnan) {
		this.ifPartidaTnan = ifPartidaTnan;
	}

	public CatalogoDetalle getIfEntidad() {
		return ifEntidad;
	}

	public void setIfEntidad(CatalogoDetalle ifEntidad) {
		this.ifEntidad = ifEntidad;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public CatalogoDetalle getIfPais() {
		return ifPais;
	}

	public void setIfPais(CatalogoDetalle ifPais) {
		this.ifPais = ifPais;
	}

	public Estados getEstado() {
		return estado;
	}

	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;	
	}

}
