package ec.gob.petroecuador.aduanapetro.catalogos.ui.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.catalogos.jpa.Capitulo;
import ec.gob.petroecuador.aduanapetro.catalogos.services.CapituloService;

@Component("CapituloConverter")
public class CapituloConverter implements Converter {

	@Autowired 
	private CapituloService service;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value == null || value.length() == 0) {
			return null;
		}
		Long id = Long.parseLong(value);
		return service.buscar(id);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		return value instanceof Capitulo ? ((Capitulo) value).getId().toString() : "";
	}

}
