package ec.gob.petroecuador.aduanapetro.catalogos.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.catalogos.dao.PartidaDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.Partida;
import ec.gob.petroecuador.aduanapetro.catalogos.services.PartidaService;

@Service
public class PartidaServiceImpl extends GenericServiceImpl<Partida, Long> implements PartidaService {

	@Autowired
	PartidaDao dao;
	
	@Override
	public GenericDAO<Partida, Long> getDao() {
		return dao;
	}

	@Override
	public List<Partida> getPartidas(Long idPadre) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("idPadre", idPadre);
		return dao.findByNamedQuery("filterByPadre", map);
	}
}
