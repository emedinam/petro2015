package ec.gob.petroecuador.aduanapetro.catalogos.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.catalogos.dao.MtCatalogoDao;
import ec.gob.petroecuador.aduanapetro.catalogos.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.Catalogo;

@Component("mtCatalogoDao")
public class MtCatalogoDaoImpl extends GenericDAOImpl<Catalogo, Long> implements MtCatalogoDao {

}