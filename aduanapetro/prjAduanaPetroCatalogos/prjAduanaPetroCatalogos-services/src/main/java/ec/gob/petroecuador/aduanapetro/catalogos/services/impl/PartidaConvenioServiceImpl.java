package ec.gob.petroecuador.aduanapetro.catalogos.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.catalogos.dao.PartidaConvenioDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaConvenio;
import ec.gob.petroecuador.aduanapetro.catalogos.services.PartidaConvenioService;

@Service
public class PartidaConvenioServiceImpl extends GenericServiceImpl<PartidaConvenio, Long> implements PartidaConvenioService {

	@Autowired
	PartidaConvenioDao dao;
	
	@Override
	public GenericDAO<PartidaConvenio, Long> getDao() {
		return dao;
	}

	@Override
	public List<PartidaConvenio> getListaPartidaConvenios(Long idPadre) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("idPartida", idPadre);
		return dao.findByNamedQuery("filterByPartidaConvenio", map);
	}
}
