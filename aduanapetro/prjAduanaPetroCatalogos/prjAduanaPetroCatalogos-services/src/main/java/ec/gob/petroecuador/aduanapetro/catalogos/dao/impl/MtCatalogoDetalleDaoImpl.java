package ec.gob.petroecuador.aduanapetro.catalogos.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.catalogos.dao.MtCatalogoDetalleDao;
import ec.gob.petroecuador.aduanapetro.catalogos.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.CatalogoDetalle;

@Component("mtCatalogoDetalleDao")
public class MtCatalogoDetalleDaoImpl extends GenericDAOImpl<CatalogoDetalle, Long> implements MtCatalogoDetalleDao {

}