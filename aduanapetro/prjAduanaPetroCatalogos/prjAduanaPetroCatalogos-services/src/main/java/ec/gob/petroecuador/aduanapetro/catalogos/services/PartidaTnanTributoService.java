package ec.gob.petroecuador.aduanapetro.catalogos.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTnanTributo;

public interface PartidaTnanTributoService extends GenericService<PartidaTnanTributo, Long> {
	
	//public List<PartidaTnan> getPartidaTnans(Long idPadre);
	public List<PartidaTnanTributo> getListaPartidaTnanTributos(Long idPadre);

}
