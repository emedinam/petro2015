package ec.gob.petroecuador.aduanapetro.catalogos.dao;

import ec.gob.petroecuador.aduanapetro.catalogos.jpa.Capitulo;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

public interface CapituloDao extends GenericDAO<Capitulo, Long> {

}
