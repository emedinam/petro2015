package ec.gob.petroecuador.aduanapetro.catalogos.dao;

import ec.gob.petroecuador.aduanapetro.catalogos.jpa.Catalogo;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

public interface MtCatalogoDao extends GenericDAO<Catalogo, Long> {

}
