package ec.gob.petroecuador.aduanapetro.catalogos.dao;

import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaConvenio;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

public interface PartidaConvenioDao extends GenericDAO<PartidaConvenio, Long> {

}
