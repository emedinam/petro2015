package ec.gob.petroecuador.aduanapetro.catalogos.jpa;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;
@NamedQueries({
	@NamedQuery(
		name = "filterByPartidaConvenio",
		query = "select m from PartidaConvenio m where m.ifPartida.id = :idPartida order by m.ifConvenio.codigo, m.ifPais.descripcion"
	),
	@NamedQuery(
			name = "orderByConvenio",
			query = "select m from PartidaConvenio m order by m.ifConvenio"
		),
})
@Entity
@Table(name = "MT_PARTIDA_CONVENIO", schema= "APS_ARANCEL")
public class PartidaConvenio extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="if_partida")
	private Partida ifPartida;
	
	@ManyToOne
	@JoinColumn(name="if_convenio")
	private CatalogoDetalle ifConvenio;
	
	@ManyToOne
	@JoinColumn(name="if_pais")
	private CatalogoDetalle ifPais;
	
	@ManyToOne
	@JoinColumn(name="if_tpci")
	private CatalogoDetalle ifTpci;
	
	private Float porcentaje;
	private Integer margen;
	private String observacion;
	private String naladisa;
	@Column(name="fecha_inicio")
	private Date fechaInicio;
	@Column(name="fecha_fin")
	private Date fechaFin;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
 
	
	
	
	public Partida getIfPartida() {
		return ifPartida;
	}

	public void setIfPartida(Partida ifPartida) {
		this.ifPartida = ifPartida;
	}

	public CatalogoDetalle getIfConvenio() {
		return ifConvenio;
	}

	public void setIfConvenio(CatalogoDetalle ifConvenio) {
		this.ifConvenio = ifConvenio;
	}

	public CatalogoDetalle getIfPais() {
		return ifPais;
	}

	public void setIfPais(CatalogoDetalle ifPais) {
		this.ifPais = ifPais;
	}

	public CatalogoDetalle getIfTpci() {
		return ifTpci;
	}

	public void setIfTpci(CatalogoDetalle ifTpci) {
		this.ifTpci = ifTpci;
	}

	public Float getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Float porcentaje) {
		this.porcentaje = porcentaje;
	}

	public Integer getMargen() {
		return margen;
	}

	public void setMargen(Integer margen) {
		this.margen = margen;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getNaladisa() {
		return naladisa;
	}

	public void setNaladisa(String naladisa) {
		this.naladisa = naladisa;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Estados getEstado() {
		return estado;
	}

	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;	
	}

}
