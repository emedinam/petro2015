package ec.gob.petroecuador.aduanapetro.catalogos.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;
@NamedQueries({
	@NamedQuery(
		name = "filterByPartidaTpng",
		query = "select m from PartidaTpng m where m.ifPartida.id = :idPartida"
	),
	@NamedQuery(
			name = "orderByTpng",
			query = "select m from PartidaTpng m order by m.ifTpng"
		),
})
@Entity
@Table(name = "MT_PARTIDA_TPNG", schema= "APS_ARANCEL")
public class PartidaTpng extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="if_partida")
	private Partida ifPartida;
	
	@ManyToOne
	@JoinColumn(name="if_tpng")
	private CatalogoDetalle ifTpng;
	
	private String observacion;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
 
	

	public Partida getIfPartida() {
		return ifPartida;
	}

	public void setIfPartida(Partida ifPartida) {
		this.ifPartida = ifPartida;
	}

	public CatalogoDetalle getIfTpng() {
		return ifTpng;
	}

	public void setIfTpng(CatalogoDetalle ifTpng) {
		this.ifTpng = ifTpng;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Estados getEstado() {
		return estado;
	}

	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;	
	}

}
