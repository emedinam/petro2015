package ec.gob.petroecuador.aduanapetro.catalogos.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.Catalogo;

public interface MtCatalogoService extends GenericService<Catalogo, Long> {
	
	public List<Catalogo> getMtCatalogos(Long idPadre);
	
	//public List<Menu> getMenusByTipoYPosicion(boolean esVertical, boolean esTransversal);

}
