package ec.gob.petroecuador.aduanapetro.catalogos.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.catalogos.dao.PartidaTpngDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTpng;
import ec.gob.petroecuador.aduanapetro.catalogos.services.PartidaTpngService;

@Service
public class PartidaTpngServiceImpl extends GenericServiceImpl<PartidaTpng, Long> implements PartidaTpngService {

	@Autowired
	PartidaTpngDao dao;
	
	@Override
	public GenericDAO<PartidaTpng, Long> getDao() {
		return dao;
	}

	@Override
	public List<PartidaTpng> getListaPartidaTpngs(Long idPadre) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("idPartida", idPadre);
		return dao.findByNamedQuery("filterByPartidaTpng", map);
	}
}
