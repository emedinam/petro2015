package ec.gob.petroecuador.aduanapetro.catalogos.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.catalogos.dao.PartidaTnanRestriccionDao;
import ec.gob.petroecuador.aduanapetro.catalogos.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTnanRestriccion;

@Component("partidaTnanRestriccionDao")
public class PartidaTnanRestriccionDaoImpl extends GenericDAOImpl<PartidaTnanRestriccion, Long> implements PartidaTnanRestriccionDao {

}