package ec.gob.petroecuador.aduanapetro.catalogos.dao;

import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTnanRestriccion;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

public interface PartidaTnanRestriccionDao extends GenericDAO<PartidaTnanRestriccion, Long> {

}
