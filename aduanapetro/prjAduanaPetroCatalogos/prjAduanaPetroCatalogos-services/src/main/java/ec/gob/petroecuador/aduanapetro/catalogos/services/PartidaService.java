package ec.gob.petroecuador.aduanapetro.catalogos.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.Partida;

public interface PartidaService extends GenericService<Partida, Long> {
	
	public List<Partida> getPartidas(Long idPadre);

}
