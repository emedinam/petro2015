package ec.gob.petroecuador.aduanapetro.catalogos.dao;

import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTnanTributo;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

public interface PartidaTnanTributoDao extends GenericDAO<PartidaTnanTributo, Long> {

}
