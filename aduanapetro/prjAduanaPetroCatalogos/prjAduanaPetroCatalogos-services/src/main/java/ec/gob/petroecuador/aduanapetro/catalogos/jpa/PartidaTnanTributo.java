package ec.gob.petroecuador.aduanapetro.catalogos.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;
@NamedQueries({
	@NamedQuery(
		name = "filterByPartidaTnan",
		query = "select m from PartidaTnanTributo m where m.ifPartidaTnan.id = :idPartidaTnan"
	),
	@NamedQuery(
			name = "orderByTributo",
			query = "select m from PartidaTnanTributo m order by m.ifTributo"
		),
})
@Entity
@Table(name = "MT_PARTIDA_TNAN_TRIBUTO", schema= "APS_ARANCEL")
public class PartidaTnanTributo extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="if_partida_tnan")
	private Partida ifPartidaTnan;
	
	//@Column(name="if_tributo")
	@ManyToOne
	@JoinColumn(name="if_tributo")
	private CatalogoDetalle ifTributo;
	
	private Float valor;
	//@Column(name="if_tipo")
	@ManyToOne
	@JoinColumn(name="if_tipo")
	private CatalogoDetalle ifTipo;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
 

	public Partida getIfPartidaTnan() {
		return ifPartidaTnan;
	}

	public void setIfPartidaTnan(Partida ifPartidaTnan) {
		this.ifPartidaTnan = ifPartidaTnan;
	}

	public CatalogoDetalle getIfTributo() {
		return ifTributo;
	}

	public void setIfTributo(CatalogoDetalle ifTributo) {
		this.ifTributo = ifTributo;
	}

	public Float getValor() {
		return valor;
	}

	public void setValor(Float valor) {
		this.valor = valor;
	}

	public CatalogoDetalle getIfTipo() {
		return ifTipo;
	}

	public void setIfTipo(CatalogoDetalle ifTipo) {
		this.ifTipo = ifTipo;
	}

	public Estados getEstado() {
		return estado;
	}

	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;	
	}

}
