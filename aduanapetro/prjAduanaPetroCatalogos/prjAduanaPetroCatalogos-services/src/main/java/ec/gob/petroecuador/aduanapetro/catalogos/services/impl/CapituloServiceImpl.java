package ec.gob.petroecuador.aduanapetro.catalogos.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.catalogos.dao.CapituloDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.Capitulo;
import ec.gob.petroecuador.aduanapetro.catalogos.services.CapituloService;

@Service
public class CapituloServiceImpl extends GenericServiceImpl<Capitulo, Long> implements CapituloService {

	@Autowired
	CapituloDao dao;
	
	@Override
	public GenericDAO<Capitulo, Long> getDao() {
		return dao;
	}

	@Override
	public List<Capitulo> getCapitulos(Long idPadre) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("idPadre", idPadre);
		return dao.findByNamedQuery("filterByPadre", map);
	}

}
