package ec.gob.petroecuador.aduanapetro.catalogos.dao;

import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTpng;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

public interface PartidaTpngDao extends GenericDAO<PartidaTpng, Long> {

}
