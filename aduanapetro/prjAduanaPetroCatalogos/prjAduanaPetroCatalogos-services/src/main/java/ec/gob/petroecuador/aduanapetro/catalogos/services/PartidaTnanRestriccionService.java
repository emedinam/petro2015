package ec.gob.petroecuador.aduanapetro.catalogos.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTnanRestriccion;

public interface PartidaTnanRestriccionService extends GenericService<PartidaTnanRestriccion, Long> {
	
	//public List<PartidaTnan> getPartidaTnans(Long idPadre);
	public List<PartidaTnanRestriccion> getListaPartidaTnanRestriccions(Long idPadre);

}
