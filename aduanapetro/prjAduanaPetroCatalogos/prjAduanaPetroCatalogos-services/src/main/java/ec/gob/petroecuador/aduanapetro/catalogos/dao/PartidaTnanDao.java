package ec.gob.petroecuador.aduanapetro.catalogos.dao;

import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTnan;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

public interface PartidaTnanDao extends GenericDAO<PartidaTnan, Long> {

}
