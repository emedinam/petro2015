package ec.gob.petroecuador.aduanapetro.catalogos.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.Capitulo;

public interface CapituloService extends GenericService<Capitulo, Long> {
	
	public List<Capitulo> getCapitulos(Long idPadre);
	
	//public List<Menu> getMenusByTipoYPosicion(boolean esVertical, boolean esTransversal);

}
