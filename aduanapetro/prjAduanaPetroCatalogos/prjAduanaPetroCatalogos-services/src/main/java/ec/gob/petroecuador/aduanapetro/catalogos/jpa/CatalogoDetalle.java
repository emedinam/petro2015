package ec.gob.petroecuador.aduanapetro.catalogos.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;
@NamedQueries({
	@NamedQuery(
		name = "filterByCatalogo",
		query = "select m from CatalogoDetalle m where m.ifCatalogo = :idCatalogo"
	),
	@NamedQuery(
			name = "orderByCatalogo",
			query = "select m from CatalogoDetalle m order by m.ifCatalogo.descripcion, m.descripcion"
		),
})
@Entity
@Table(name = "MT_CATALOGO_DETALLE", schema= "APS_CATALOGOS")
public class CatalogoDetalle extends DatabaseObject<Long> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	private String codigo;

	private String descripcion;

	@ManyToOne
	@JoinColumn(name="if_catalogo")
	private Catalogo ifCatalogo;
		
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
 
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Catalogo getIfCatalogo() {
		return ifCatalogo;
	}

	public void setIfCatalogo(Catalogo ifCatalogo) {
		this.ifCatalogo = ifCatalogo;
	}

	public Estados getEstado() {
		return estado;
	}

	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;	
	}

}
