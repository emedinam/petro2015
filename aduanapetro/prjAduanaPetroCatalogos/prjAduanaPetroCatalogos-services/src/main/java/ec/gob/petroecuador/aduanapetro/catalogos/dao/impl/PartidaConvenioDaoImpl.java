package ec.gob.petroecuador.aduanapetro.catalogos.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.catalogos.dao.PartidaConvenioDao;
import ec.gob.petroecuador.aduanapetro.catalogos.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaConvenio;

@Component("partidaConvenioDao")
public class PartidaConvenioDaoImpl extends GenericDAOImpl<PartidaConvenio, Long> implements PartidaConvenioDao {

}