package ec.gob.petroecuador.aduanapetro.catalogos.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.catalogos.dao.PartidaTnanDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTnan;
import ec.gob.petroecuador.aduanapetro.catalogos.services.PartidaTnanService;

@Service
public class PartidaTnanServiceImpl extends GenericServiceImpl<PartidaTnan, Long> implements PartidaTnanService {

	@Autowired
	PartidaTnanDao dao;
	
	@Override
	public GenericDAO<PartidaTnan, Long> getDao() {
		return dao;
	}

	@Override
	public List<PartidaTnan> getListaPartidaTnans(Long idPadre) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("idPartida", idPadre);
		return dao.findByNamedQuery("filterByPartida", map);
	}
}
