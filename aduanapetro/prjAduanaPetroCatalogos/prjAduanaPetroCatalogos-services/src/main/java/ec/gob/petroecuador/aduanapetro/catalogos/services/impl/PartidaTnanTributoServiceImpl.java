package ec.gob.petroecuador.aduanapetro.catalogos.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.catalogos.dao.PartidaTnanTributoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTnanTributo;
import ec.gob.petroecuador.aduanapetro.catalogos.services.PartidaTnanTributoService;

@Service
public class PartidaTnanTributoServiceImpl extends GenericServiceImpl<PartidaTnanTributo, Long> implements PartidaTnanTributoService {

	@Autowired
	PartidaTnanTributoDao dao;
	
	@Override
	public GenericDAO<PartidaTnanTributo, Long> getDao() {
		return dao;
	}

	@Override
	public List<PartidaTnanTributo> getListaPartidaTnanTributos(Long idPadre) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("idPartidaTnan", idPadre);
		return dao.findByNamedQuery("filterByPartidaTnan", map);
	}
}
