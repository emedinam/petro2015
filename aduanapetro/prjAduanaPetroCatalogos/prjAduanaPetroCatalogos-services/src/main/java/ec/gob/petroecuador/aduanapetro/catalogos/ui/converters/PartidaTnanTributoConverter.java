package ec.gob.petroecuador.aduanapetro.catalogos.ui.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTnanTributo;
import ec.gob.petroecuador.aduanapetro.catalogos.services.PartidaTnanTributoService;

@Component("PartidaTnanTributoConverter")
public class PartidaTnanTributoConverter implements Converter {

	@Autowired 
	private PartidaTnanTributoService service;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value == null || value.length() == 0) {
			return null;
		}
		Long id = Long.parseLong(value);
		return service.buscar(id);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		return value instanceof PartidaTnanTributo ? ((PartidaTnanTributo) value).getId().toString() : "";
	}

}
