package ec.gob.petroecuador.aduanapetro.catalogos.generic.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.Session;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;
/**
 * 
 * @author dgonzalez
 *
 * @param <T> Cualquier entidad JPA
 * @param <PK> Cualquier tipo de dato objeto
 */
public class GenericDAOImpl<T extends DatabaseObject<PK>, PK extends Serializable>  implements GenericDAO<T, PK> {

	private Class<T> clazz;
	
    @PersistenceContext//(unitName = "seguridad-unit")
    protected EntityManager entityManager;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public GenericDAOImpl() {
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        clazz = (Class) pt.getActualTypeArguments()[0];
    }
	
	public void delete(PK id) {
		entityManager.remove( entityManager.find(clazz, id) );
	}
	
	public T get(PK id) {
		return (T)entityManager.find(clazz, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<T> getAll() {
		return (List<T>)entityManager.createQuery("select t from "+clazz.getSimpleName()+" t").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<T> getAllActive() {
		return (List<T>)entityManager.createQuery("select t from "+clazz.getSimpleName()+" t where estado='"+Estados.ACTIVO+"'").getResultList();
	}
	
	public void saveOrUpdate(T entity) {
		if (entity.getId()!=null && this.get(entity.getId()) != null) {
			entityManager.merge(entity);
		} else {
			entityManager.persist(entity);
		}
		entityManager.flush();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> findByNamedQuery(String queryName, java.util.Map<String,Object> map,
			int maxResults) {
		Session session = entityManager.unwrap(Session.class);
		Query q = session.getNamedQuery(queryName);
		if(map != null) {
			for (Entry<String, Object> entry : map.entrySet()) {
				if(entry.getValue() instanceof String) {
					q.setString(entry.getKey(), (String)entry.getValue());
				}
				else if(entry.getValue() instanceof Boolean) {
					q.setBoolean(entry.getKey(), (Boolean)entry.getValue());
				}
				else if(entry.getValue() instanceof Integer) {
					q.setInteger(entry.getKey(), (Integer)entry.getValue());
				}
				else if(entry.getValue() instanceof Date) {
					q.setDate(entry.getKey(), (Date)entry.getValue());
				}
				else if(entry.getValue() instanceof String[]) {
					q.setParameterList(entry.getKey(), (String[])entry.getValue());
				}
				else if(entry.getValue() instanceof Object[]) {					
					q.setParameterList(entry.getKey(), (Object[])entry.getValue());
				}
				else {
					if(entry!=null && entry.getValue()!=null) {
						String valor = entry.getValue().toString();
						q.setString(entry.getKey(), valor);
					}
					else {
						System.err.println("findByNamedQuery - Entrada vacia");
					}
				}
			}
		}
		q.setMaxResults(maxResults);
		return (List<T>)q.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> findByNamedQuery(String queryName, Map<String, Object> map) {
		Session session = entityManager.unwrap(Session.class);
		Query q = session.getNamedQuery(queryName);
		if(map != null) {
			for (Entry<String, Object> entry : map.entrySet()) {
				if(entry.getValue() instanceof String) {
					q.setString(entry.getKey(), (String)entry.getValue());
				}
				else if(entry.getValue() instanceof Boolean) {
					q.setBoolean(entry.getKey(), (Boolean)entry.getValue());
				}
				else if(entry.getValue() instanceof Integer) {
					q.setInteger(entry.getKey(), (Integer)entry.getValue());
				}
				else if(entry.getValue() instanceof Date) {
					q.setDate(entry.getKey(), (Date)entry.getValue());
				}
				else if(entry.getValue() instanceof String[]) {
					q.setParameterList(entry.getKey(), (String[])entry.getValue());
				}
				else if(entry.getValue() instanceof Object[]) {					
					q.setParameterList(entry.getKey(), (Object[])entry.getValue());
				}
				else {
					if(entry!=null && entry.getValue()!=null) {
						String valor = entry.getValue().toString();
						q.setString(entry.getKey(), valor);
					}
					else {
						System.err.println("findByNamedQuery - Entrada vacia");
					}
				}
			}
		}
		List<T> results = (List<T>)q.list();
		return results;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PK> findByNamedQueryPK(String queryName, Map<String, Object> map) {
		Session session = entityManager.unwrap(Session.class);
		Query q = session.getNamedQuery(queryName);
		if(map != null) {
			for (Entry<String, Object> entry : map.entrySet()) {
				if(entry.getValue() instanceof String) {
					q.setString(entry.getKey(), (String)entry.getValue());
				}
				else if(entry.getValue() instanceof Boolean) {
					q.setBoolean(entry.getKey(), (Boolean)entry.getValue());
				}
				else if(entry.getValue() instanceof Integer) {
					q.setInteger(entry.getKey(), (Integer)entry.getValue());
				}
				else {
					if(entry!=null && entry.getValue()!=null) {
						String valor = entry.getValue().toString();
						q.setString(entry.getKey(), valor);
					}
					else {
						System.err.println("findByNamedQuery - Entrada vacia");
					}
				}
			}
		}
		return (List<PK>)q.list();
	}
	
	@Override
	public Long countExist(String queryName, Map<String, Object> map) {
		Session session = entityManager.unwrap(Session.class);
		Query q = session.getNamedQuery(queryName);
		if(map != null) {
			for (Entry<String, Object> entry : map.entrySet()) {
				if(entry.getValue() instanceof String) {
					q.setString(entry.getKey(), (String)entry.getValue());
				}
				else if(entry.getValue() instanceof Boolean) {
					q.setBoolean(entry.getKey(), (Boolean)entry.getValue());
				}
				else if(entry.getValue() instanceof Integer) {
					q.setInteger(entry.getKey(), (Integer)entry.getValue());
				}
				else {
					if(entry!=null && entry.getValue()!=null) {
						String valor = entry.getValue().toString();
						q.setString(entry.getKey(), valor);
					}
					else {
						System.err.println("countExist - Entrada vacia");
					}
				}
			}
		}
		return (Long)q.uniqueResult(); 
	}

	@Override
	public void delete(T entity) {
		entityManager.remove( entity );
	}

}