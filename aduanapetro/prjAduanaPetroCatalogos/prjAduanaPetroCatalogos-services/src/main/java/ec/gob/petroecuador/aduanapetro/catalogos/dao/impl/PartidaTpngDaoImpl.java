package ec.gob.petroecuador.aduanapetro.catalogos.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.catalogos.dao.PartidaTpngDao;
import ec.gob.petroecuador.aduanapetro.catalogos.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTpng;

@Component("partidaTpngDao")
public class PartidaTpngDaoImpl extends GenericDAOImpl<PartidaTpng, Long> implements PartidaTpngDao {

}