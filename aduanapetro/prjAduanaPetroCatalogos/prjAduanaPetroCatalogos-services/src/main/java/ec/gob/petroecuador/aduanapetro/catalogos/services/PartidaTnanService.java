package ec.gob.petroecuador.aduanapetro.catalogos.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTnan;

public interface PartidaTnanService extends GenericService<PartidaTnan, Long> {
	
	//public List<PartidaTnan> getPartidaTnans(Long idPadre);
	public List<PartidaTnan> getListaPartidaTnans(Long idPadre);

}
