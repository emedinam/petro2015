package ec.gob.petroecuador.aduanapetro.catalogos.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.catalogos.dao.PartidaTnanDao;
import ec.gob.petroecuador.aduanapetro.catalogos.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTnan;

@Component("partidaTnanDao")
public class PartidaTnanDaoImpl extends GenericDAOImpl<PartidaTnan, Long> implements PartidaTnanDao {

}