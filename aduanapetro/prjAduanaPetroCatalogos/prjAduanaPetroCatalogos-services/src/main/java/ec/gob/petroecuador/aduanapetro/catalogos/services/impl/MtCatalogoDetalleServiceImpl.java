package ec.gob.petroecuador.aduanapetro.catalogos.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.catalogos.dao.MtCatalogoDetalleDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.CatalogoDetalle;
import ec.gob.petroecuador.aduanapetro.catalogos.services.MtCatalogoDetalleService;

@Service
public class MtCatalogoDetalleServiceImpl extends GenericServiceImpl<CatalogoDetalle, Long> implements MtCatalogoDetalleService {

	@Autowired
	MtCatalogoDetalleDao dao;
	
	@Override
	public GenericDAO<CatalogoDetalle, Long> getDao() {
		return dao;
	}

	@Override
	public List<CatalogoDetalle> getMtCatalogoDetalles(Long idPadre) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("idPadre", idPadre);
		return dao.findByNamedQuery("filterByPadre", map);
	}
	
	@Override
	public List<CatalogoDetalle> getMtCatalogoDetallesOrder() {
		Map<String, Object> map = new HashMap<String, Object>();
		//map.put("idPadre", idPadre);
		return dao.findByNamedQuery("orderByCatalogo", map);
	}
}
