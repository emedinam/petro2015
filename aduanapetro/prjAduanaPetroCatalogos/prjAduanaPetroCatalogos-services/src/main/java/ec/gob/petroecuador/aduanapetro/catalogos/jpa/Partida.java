package ec.gob.petroecuador.aduanapetro.catalogos.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

@Entity
@Table(name = "MT_PARTIDA", schema= "APS_ARANCEL")
public class Partida extends DatabaseObject<Long> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="if_capitulo")
	private Capitulo ifCapitulo;
	
	private String partida;
	private String descripcion;
	private Integer digito;
	
	@ManyToOne
	@JoinColumn(name="if_unidad_fisica")
	private CatalogoDetalle ifUnidadFisica;
		
	@ManyToOne
	@JoinColumn(name="if_unidad_comercial")
	private CatalogoDetalle ifUnidadComercial;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
 
	
	
	
	
	public Capitulo getIfCapitulo() {
		return ifCapitulo;
	}

	public void setIfCapitulo(Capitulo ifCapitulo) {
		this.ifCapitulo = ifCapitulo;
	}

	public String getPartida() {
		return partida;
	}

	public void setPartida(String partida) {
		this.partida = partida;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getDigito() {
		return digito;
	}

	public void setDigito(Integer digito) {
		this.digito = digito;
	}

	
	public CatalogoDetalle getIfUnidadFisica() {
		return ifUnidadFisica;
	}

	public void setIfUnidadFisica(CatalogoDetalle ifUnidadFisica) {
		this.ifUnidadFisica = ifUnidadFisica;
	}

	public CatalogoDetalle getIfUnidadComercial() {
		return ifUnidadComercial;
	}

	public void setIfUnidadComercial(CatalogoDetalle ifUnidadComercial) {
		this.ifUnidadComercial = ifUnidadComercial;
	}

	public Estados getEstado() {
		return estado;
	}

	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;	
	}

}
