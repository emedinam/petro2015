package ec.gob.petroecuador.aduanapetro.catalogos.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.catalogos.dao.MtCatalogoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.Catalogo;
import ec.gob.petroecuador.aduanapetro.catalogos.services.MtCatalogoService;

@Service
public class MtCatalogoServiceImpl extends GenericServiceImpl<Catalogo, Long> implements MtCatalogoService {

	@Autowired
	MtCatalogoDao dao;
	
	@Override
	public GenericDAO<Catalogo, Long> getDao() {
		return dao;
	}

	@Override
	public List<Catalogo> getMtCatalogos(Long idPadre) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("idPadre", idPadre);
		return dao.findByNamedQuery("filterByPadre", map);
	}

}
