package ec.gob.petroecuador.aduanapetro.catalogos.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.catalogos.dao.PartidaTnanTributoDao;
import ec.gob.petroecuador.aduanapetro.catalogos.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTnanTributo;

@Component("partidaTnanTributoDao")
public class PartidaTnanTributoDaoImpl extends GenericDAOImpl<PartidaTnanTributo, Long> implements PartidaTnanTributoDao {

}