package ec.gob.petroecuador.aduanapetro.catalogos.dao;

import ec.gob.petroecuador.aduanapetro.catalogos.jpa.CatalogoDetalle;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

public interface MtCatalogoDetalleDao extends GenericDAO<CatalogoDetalle, Long> {

}
