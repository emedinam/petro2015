package ec.gob.petroecuador.aduanapetro.catalogos.ui;

import java.util.ArrayList;
import java.util.List;



import org.apache.log4j.Logger;
//import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.catalogos.jpa.Capitulo;
import ec.gob.petroecuador.aduanapetro.catalogos.services.CapituloService;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
//import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Menu;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("capituloBB")
@Scope("request")
public class CapituloBackingBean extends RegistroComun {
	
	private Capitulo capitulo = new Capitulo();
	private Capitulo selectedCapitulo = new Capitulo();
	
	private List<Capitulo> listaCapitulos = new ArrayList<Capitulo>();
	
	private static Logger log = Logger.getLogger(CapituloBackingBean.class);
	
	@Autowired 
	private CapituloService service;

	
	public void modificar() {
		service.guardar(selectedCapitulo);
		saveRegistro(AccionCRUD.Actualizado, selectedCapitulo.getId(), Capitulo.class);
		refreshAll();
		borrarListaCapitulos();
	}
	
	
	public void guardar() {
		service.guardar(capitulo);
		saveRegistro(AccionCRUD.Registrado, capitulo.getId(), Capitulo.class);
		refreshAll();
		borrarListaCapitulos();
	}

	
	public void eliminar() {
		
		service.anular(selectedCapitulo);
		saveRegistro(AccionCRUD.Eliminado, selectedCapitulo.getId(), Capitulo.class);
		refreshAll();
		borrarListaCapitulos();
	}
	
	
	private void refreshAll() {
		capitulo = new Capitulo();
	}

	/*
	public void modificarMtCatalogoDetalle(RowEditEvent event) {
		mtCatalogoDetalle = (MtCatalogoDetalle) event.getObject();
		service.guardar(mtCatalogoDetalle);
		saveRegistro(AccionCRUD.Actualizado, mtCatalogoDetalle.getId(), MtCatalogoDetalle.class);
		refreshAll();
		borrarListaMtCatalogoDetalles();
	}

	public void eliminarMtCatalogoDetalle(RowEditEvent event) {
		mtCatalogoDetalle = (MtCatalogoDetalle) event.getObject();
		service.anular(mtCatalogoDetalle);
		saveRegistro(AccionCRUD.Eliminado, mtCatalogoDetalle.getId(), MtCatalogoDetalle.class);
		refreshAll();
		borrarListaMtCatalogoDetalles();
	}
*/
	private void borrarListaCapitulos() {
		listaCapitulos.clear();
	}

	public Capitulo getCapitulo() {
		return capitulo;
	}

	public void setCapitulo(Capitulo capitulo) {
		this.capitulo = capitulo;
	}

	public List<Capitulo> getListaCapitulos() {
		if (listaCapitulos.isEmpty()) {
			listaCapitulos = service.obtenerActivos();
			//listaCapitulos = service.getCapitulos();
			log.info("Se consultaron "+(listaCapitulos!=null? listaCapitulos.size():"0")+" detalles Capitulo");
		}
		return listaCapitulos;
	}

	public void setListaCapitulos(List<Capitulo> listaCapitulos) {
		this.listaCapitulos = listaCapitulos;
	}

	
	public Capitulo getSelectedCapitulo() {
		return selectedCapitulo;
	}

	public void setSelectedCapitulo(Capitulo selectedCapitulo) {
		this.selectedCapitulo = selectedCapitulo;
	}
	
	
	/*
	public List<Menu> getListaPadre() {
		if (listaMenus.isEmpty())
			listaPadre.add(new Menu());
		else
			listaPadre = listaMenus;
		return listaPadre;
	}

	public void setListaPadre(List<Menu> listaPadre) {
		this.listaPadre = listaPadre;
	}	
*/
	public Estados[] getEstados() {
	        return Estados.values();
    }

	
	public String botonInsertar(){
		return "mantenimiento";
	}
	
	public void botonEliminar() {
		service.anular(capitulo);
        //return null;
    }
	
	public String botonCancelar(){
		return "cancelar";
	}
	
	public String guardarDatos() {
		/*
        if (mtCatalogoDetalle.getIdCatalogoDetalle() != null) {
        	mtCatalogoDetalleDao.update(mtCatalogoDetalle);
        	return "cancelar";
        }
        else {
        	//existir();
        	//if (respuesta=="no"){
        		mtCatalogoDetalleDao.save(mtCatalogoDetalle);
        		mtCatalogoDetalle = new MtCatalogoDetalle();
        		invalidateMtCatalogoDetalles();
        		return "cancelar";
        	//}
        	//else{
        		//System.out.println("Ya existe");
        		//return "cancelar2";
        	//}
        }*/
		return null;
    }
	
}
