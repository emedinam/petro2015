package ec.gob.petroecuador.aduanapetro.catalogos.ui;

import java.util.ArrayList;
import java.util.List;





import org.apache.log4j.Logger;
//import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.catalogos.jpa.Partida;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTnan;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTnanTributo;
import ec.gob.petroecuador.aduanapetro.catalogos.services.PartidaTnanTributoService;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Menu;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("partidaTnanTributoBB")
@Scope("request")
public class PartidaTnanTributoBackingBean extends RegistroComun {
	private PartidaTnan selectedPartidaTnan = new PartidaTnan();
	private PartidaTnanTributo partidaTnanTributo = new PartidaTnanTributo();
	private PartidaTnanTributo selectedPartidaTnanTributo = new PartidaTnanTributo();
	
	private List<PartidaTnanTributo> listaPartidaTnanTributos = new ArrayList<PartidaTnanTributo>();
	
	private static Logger log = Logger.getLogger(PartidaTnanTributoBackingBean.class);
	
	@Autowired 
	private PartidaTnanTributoService service;

	
	public void modificar() {
		service.guardar(selectedPartidaTnanTributo);
		saveRegistro(AccionCRUD.Actualizado, selectedPartidaTnanTributo.getId(), PartidaTnanTributo.class);
		refreshAll();
		borrarListaPartidaTnanTributos();
	}
	
	/*
	public void guardar() {
		service.guardar(mtCatalogoDetalle);
		saveRegistro(AccionCRUD.Registrado, mtCatalogoDetalle.getId(), CatalogoDetalle.class);
		refreshAll();
		borrarListaMtCatalogoDetalles();
	}
	*/
	
	public void eliminar() {
		service.anular(selectedPartidaTnanTributo);
		saveRegistro(AccionCRUD.Eliminado, selectedPartidaTnanTributo.getId(), PartidaTnanTributo.class);
		refreshAll();
		borrarListaPartidaTnanTributos();
	}
	
	
	private void refreshAll() {
		partidaTnanTributo = new PartidaTnanTributo();
	}

	private void borrarListaPartidaTnanTributos() {
		listaPartidaTnanTributos.clear();
	}

	/* ========= get set ==================================== */
	public PartidaTnan getSelectedPartidaTnan() {
		return selectedPartidaTnan;
	}

	public void setSelectedPartidaTnan(PartidaTnan selectedPartidaTnan) {
		this.selectedPartidaTnan = selectedPartidaTnan;
	}
	
	
	public PartidaTnanTributo getPartidaTnanTributo() {
		return partidaTnanTributo;
	}

	public void setPartidaTnanTributo(PartidaTnanTributo partidaTnanTributo) {
		this.partidaTnanTributo = partidaTnanTributo;
	}

	public PartidaTnanTributo getSelectedPartidaTnanTributo() {
		return selectedPartidaTnanTributo;
	}

	public void setSelectedPartidaTnanTributo(PartidaTnanTributo selectedPartidaTnanTributo) {
		this.selectedPartidaTnanTributo = selectedPartidaTnanTributo;
	}
	
	public List<PartidaTnanTributo> getListaPartidaTnanTributos() {
		if (listaPartidaTnanTributos.isEmpty()) {
			//listaPartidaTnanTributos = service.obtenerActivos();
			if (selectedPartidaTnan.getId()==null){
				listaPartidaTnanTributos = service.obtenerActivos();
			}
			else{
				//listaPartidaTnanTributos = service.obtenerActivos();
				listaPartidaTnanTributos = service.getListaPartidaTnanTributos(selectedPartidaTnan.getId());
				log.info("Se consultaron "+(listaPartidaTnanTributos!=null? listaPartidaTnanTributos.size():"0")+" detalles partidas Tnan Tributo");
			}
		}
		return listaPartidaTnanTributos;
	}

	public void setListaPartidaTnanTributos(List<PartidaTnanTributo> listaPartidaTnanTributos) {
		this.listaPartidaTnanTributos = listaPartidaTnanTributos;
	}

	
	/*
	public List<Menu> getListaPadre() {
		if (listaMenus.isEmpty())
			listaPadre.add(new Menu());
		else
			listaPadre = listaMenus;
		return listaPadre;
	}

	public void setListaPadre(List<Menu> listaPadre) {
		this.listaPadre = listaPadre;
	}	
*/
	public Estados[] getEstados() {
	        return Estados.values();
    }

	
	/* ========= metodos ==================================== */
	public String botonCancelar(){
		return "cancelar";
	}
	
	public String botonConsultar(){
		return "consultar";
	}

	public String guardarDatos() {
		/*
        if (mtCatalogoDetalle.getIdCatalogoDetalle() != null) {
        	mtCatalogoDetalleDao.update(mtCatalogoDetalle);
        	return "cancelar";
        }
        else {
        	//existir();
        	//if (respuesta=="no"){
        		mtCatalogoDetalleDao.save(mtCatalogoDetalle);
        		mtCatalogoDetalle = new MtCatalogoDetalle();
        		invalidateMtCatalogoDetalles();
        		return "cancelar";
        	//}
        	//else{
        		//System.out.println("Ya existe");
        		//return "cancelar2";
        	//}
        }*/
		return null;
    }
	
}
