package ec.gob.petroecuador.aduanapetro.catalogos.ui;

import java.util.ArrayList;
import java.util.List;



import org.apache.log4j.Logger;
//import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.catalogos.jpa.CatalogoDetalle;
import ec.gob.petroecuador.aduanapetro.catalogos.services.MtCatalogoDetalleService;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
//import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Menu;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("mtCatalogoDetalleBB")
@Scope("request")
public class MtCatalogoDetalleBackingBean extends RegistroComun {
	
	private CatalogoDetalle mtCatalogoDetalle = new CatalogoDetalle();
	private CatalogoDetalle selectedmtCatalogoDetalle = new CatalogoDetalle();
	
	private List<CatalogoDetalle> listaMtCatalogoDetalles = new ArrayList<CatalogoDetalle>();
	
	private static Logger log = Logger.getLogger(MtCatalogoDetalleBackingBean.class);
	
	@Autowired 
	private MtCatalogoDetalleService service;

	
	public void modificar() {
		service.guardar(selectedmtCatalogoDetalle);
		saveRegistro(AccionCRUD.Actualizado, selectedmtCatalogoDetalle.getId(), CatalogoDetalle.class);
		refreshAll();
		borrarListaMtCatalogoDetalles();
	}
	
	
	public void guardar() {
		service.guardar(mtCatalogoDetalle);
		saveRegistro(AccionCRUD.Registrado, mtCatalogoDetalle.getId(), CatalogoDetalle.class);
		refreshAll();
		borrarListaMtCatalogoDetalles();
	}

	
	public void eliminar() {
		
		service.anular(selectedmtCatalogoDetalle);
		saveRegistro(AccionCRUD.Eliminado, selectedmtCatalogoDetalle.getId(), CatalogoDetalle.class);
		refreshAll();
		borrarListaMtCatalogoDetalles();
	}
	
	
	private void refreshAll() {
		mtCatalogoDetalle = new CatalogoDetalle();
	}

	/*
	public void modificarMtCatalogoDetalle(RowEditEvent event) {
		mtCatalogoDetalle = (MtCatalogoDetalle) event.getObject();
		service.guardar(mtCatalogoDetalle);
		saveRegistro(AccionCRUD.Actualizado, mtCatalogoDetalle.getId(), MtCatalogoDetalle.class);
		refreshAll();
		borrarListaMtCatalogoDetalles();
	}

	public void eliminarMtCatalogoDetalle(RowEditEvent event) {
		mtCatalogoDetalle = (MtCatalogoDetalle) event.getObject();
		service.anular(mtCatalogoDetalle);
		saveRegistro(AccionCRUD.Eliminado, mtCatalogoDetalle.getId(), MtCatalogoDetalle.class);
		refreshAll();
		borrarListaMtCatalogoDetalles();
	}
*/
	private void borrarListaMtCatalogoDetalles() {
		listaMtCatalogoDetalles.clear();
	}

	public CatalogoDetalle getMtCatalogoDetalle() {
		return mtCatalogoDetalle;
	}

	public void setMtCatalogoDetalle(CatalogoDetalle mtCatalogoDetalle) {
		this.mtCatalogoDetalle = mtCatalogoDetalle;
	}

	public List<CatalogoDetalle> getListaMtCatalogoDetalles() {
		if (listaMtCatalogoDetalles.isEmpty()) {
			//listaMtCatalogoDetalles = service.obtenerActivos();
			listaMtCatalogoDetalles = service.getMtCatalogoDetallesOrder();
			log.info("Se consultaron "+(listaMtCatalogoDetalles!=null? listaMtCatalogoDetalles.size():"0")+" detalles catalogos");
		}
		return listaMtCatalogoDetalles;
	}

	public void setListaMtCatalogoDetalles(List<CatalogoDetalle> listaMtCatalogoDetalles) {
		this.listaMtCatalogoDetalles = listaMtCatalogoDetalles;
	}

	
	public CatalogoDetalle getSelectedmtCatalogoDetalle() {
		return selectedmtCatalogoDetalle;
	}

	public void setSelectedmtCatalogoDetalle(CatalogoDetalle selectedmtCatalogoDetalle) {
		this.selectedmtCatalogoDetalle = selectedmtCatalogoDetalle;
	}
	
	
	/*
	public List<Menu> getListaPadre() {
		if (listaMenus.isEmpty())
			listaPadre.add(new Menu());
		else
			listaPadre = listaMenus;
		return listaPadre;
	}

	public void setListaPadre(List<Menu> listaPadre) {
		this.listaPadre = listaPadre;
	}	
*/
	public Estados[] getEstados() {
	        return Estados.values();
    }

	
	public String botonInsertar(){
		System.out.println("entra a formulario");
		return "mantenimiento";
	}
	
	public void botonEliminar() {
		service.anular(mtCatalogoDetalle);
        //return null;
    }
	
	public String botonCancelar(){
		System.out.println("cancelar");
		return "cancelar";
	}
	
	public String guardarDatos() {
		/*
        if (mtCatalogoDetalle.getIdCatalogoDetalle() != null) {
        	mtCatalogoDetalleDao.update(mtCatalogoDetalle);
        	return "cancelar";
        }
        else {
        	//existir();
        	//if (respuesta=="no"){
        		mtCatalogoDetalleDao.save(mtCatalogoDetalle);
        		mtCatalogoDetalle = new MtCatalogoDetalle();
        		invalidateMtCatalogoDetalles();
        		return "cancelar";
        	//}
        	//else{
        		//System.out.println("Ya existe");
        		//return "cancelar2";
        	//}
        }*/
		return null;
    }
	
}
