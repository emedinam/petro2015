package ec.gob.petroecuador.aduanapetro.catalogos.ui;

import java.util.ArrayList;
import java.util.List;




//import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.catalogos.jpa.Catalogo;
import ec.gob.petroecuador.aduanapetro.catalogos.services.MtCatalogoService;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("mtCatalogoBB")
@Scope("request")
public class MtCatalogoBackingBean extends RegistroComun {
	
	private Catalogo mtCatalogo = new Catalogo();
	private List<Catalogo> listaMtCatalogos = new ArrayList<Catalogo>();
	//private List<Menu> listaPadre = new ArrayList<Menu>();
	
	@Autowired 
	private MtCatalogoService service;

	public void guardar() {
		service.guardar(mtCatalogo);
		saveRegistro(AccionCRUD.Registrado, mtCatalogo.getId(), Catalogo.class);
		refreshAll();
		borrarListaMenus();
	}
	
	private void refreshAll() {
		mtCatalogo = new Catalogo();
	}
/*
	public void modificarMtCatalogo(RowEditEvent event) {
		mtCatalogo = (MtCatalogo) event.getObject();
		service.guardar(mtCatalogo);
		saveRegistro(AccionCRUD.Actualizado, mtCatalogo.getId(), MtCatalogo.class);
		refreshAll();
		borrarListaMenus();
	}

	public void eliminarMtCatalogo(RowEditEvent event) {
		mtCatalogo = (MtCatalogo) event.getObject();
		service.anular(mtCatalogo);
		saveRegistro(AccionCRUD.Eliminado, mtCatalogo.getId(), MtCatalogo.class);
		refreshAll();
		borrarListaMenus();
	}
*/
	private void borrarListaMenus() {
		listaMtCatalogos.clear();
	}

	public Catalogo getMtCatalogo() {
		return mtCatalogo;
	}

	public void setMtCatalogo(Catalogo mtCatalogo) {
		this.mtCatalogo = mtCatalogo;
	}

	public List<Catalogo> getListaMtCatalogos() {
		if (listaMtCatalogos.isEmpty())
			listaMtCatalogos = service.obtenerActivos();
		return listaMtCatalogos;
	}

	public void setListaMtCatalogos(List<Catalogo> listaMtCatalogos) {
		this.listaMtCatalogos = listaMtCatalogos;
	}
/*
	public List<Menu> getListaPadre() {
		if (listaMenus.isEmpty())
			listaPadre.add(new Menu());
		else
			listaPadre = listaMenus;
		return listaPadre;
	}

	public void setListaPadre(List<Menu> listaPadre) {
		this.listaPadre = listaPadre;
	}	
*/
	public Estados[] getEstados() {
	        return Estados.values();
    }

}
