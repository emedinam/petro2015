package ec.gob.petroecuador.aduanapetro.catalogos.ui;

import java.util.ArrayList;
import java.util.List;





import org.apache.log4j.Logger;
//import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.catalogos.jpa.Partida;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTnan;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTnanRestriccion;
import ec.gob.petroecuador.aduanapetro.catalogos.services.PartidaTnanRestriccionService;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Menu;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("partidaTnanRestriccionBB")
@Scope("request")
public class PartidaTnanRestriccionBackingBean extends RegistroComun {
	private PartidaTnan selectedPartidaTnan = new PartidaTnan();
	private PartidaTnanRestriccion partidaTnanRestriccion = new PartidaTnanRestriccion();
	private PartidaTnanRestriccion selectedPartidaTnanRestriccion = new PartidaTnanRestriccion();
	
	private List<PartidaTnanRestriccion> listaPartidaTnanRestriccions = new ArrayList<PartidaTnanRestriccion>();
	
	private static Logger log = Logger.getLogger(PartidaTnanRestriccionBackingBean.class);
	
	@Autowired 
	private PartidaTnanRestriccionService service;

	/*
	public void modificar() {
		service.guardar(selectedmtCatalogoDetalle);
		saveRegistro(AccionCRUD.Actualizado, selectedmtCatalogoDetalle.getId(), Menu.class);
		refreshAll();
		borrarListaMtCatalogoDetalles();
	}
	*/
	/*
	public void guardar() {
		service.guardar(mtCatalogoDetalle);
		saveRegistro(AccionCRUD.Registrado, mtCatalogoDetalle.getId(), CatalogoDetalle.class);
		refreshAll();
		borrarListaMtCatalogoDetalles();
	}
	*/
	/*
	public void eliminar() {
		
		service.anular(selectedmtCatalogoDetalle);
		saveRegistro(AccionCRUD.Eliminado, selectedmtCatalogoDetalle.getId(), Menu.class);
		refreshAll();
		borrarListaMtCatalogoDetalles();
	}
	
	*/
/*
	private void refreshAll() {
		mtCatalogoDetalle = new CatalogoDetalle();
	}

	private void borrarListaMtCatalogoDetalles() {
		listaMtCatalogoDetalles.clear();
	}
*/
	/* ========= get set ==================================== */
	public PartidaTnan getSelectedPartidaTnan() {
		return selectedPartidaTnan;
	}

	public void setSelectedPartidaTnan(PartidaTnan selectedPartidaTnan) {
		this.selectedPartidaTnan = selectedPartidaTnan;
	}
	
	
	public PartidaTnanRestriccion getPartidaTnanRestriccion() {
		return partidaTnanRestriccion;
	}

	public void setPartidaTnanRestriccion(PartidaTnanRestriccion partidaTnanRestriccion) {
		this.partidaTnanRestriccion = partidaTnanRestriccion;
	}

	public PartidaTnanRestriccion getSelectedPartidaTnanRestriccion() {
		return selectedPartidaTnanRestriccion;
	}

	public void setSelectedPartidaTnanRestriccion(PartidaTnanRestriccion selectedPartidaTnanRestriccion) {
		this.selectedPartidaTnanRestriccion = selectedPartidaTnanRestriccion;
	}
	
	public List<PartidaTnanRestriccion> getListaPartidaTnanRestriccions() {
		if (listaPartidaTnanRestriccions.isEmpty()) {
			//listaPartidaTnanTributos = service.obtenerActivos();
			if(selectedPartidaTnan.getId()==null){
			}
			else{
			listaPartidaTnanRestriccions = service.getListaPartidaTnanRestriccions(selectedPartidaTnan.getId());
			log.info("Se consultaron "+(listaPartidaTnanRestriccions!=null? listaPartidaTnanRestriccions.size():"0")+" detalles partidas Tnan Restriccion");
			}
		}
		return listaPartidaTnanRestriccions;
	}

	public void setListaPartidaTnanRestriccions(List<PartidaTnanRestriccion> listaPartidaTnanRestriccions) {
		this.listaPartidaTnanRestriccions = listaPartidaTnanRestriccions;
	}

	
	/*
	public List<Menu> getListaPadre() {
		if (listaMenus.isEmpty())
			listaPadre.add(new Menu());
		else
			listaPadre = listaMenus;
		return listaPadre;
	}

	public void setListaPadre(List<Menu> listaPadre) {
		this.listaPadre = listaPadre;
	}	
*/
	public Estados[] getEstados() {
	        return Estados.values();
    }

	
	/* ========= metodos ==================================== */
	public String botonCancelar(){
		return "cancelar";
	}
	
	public String botonConsultar(){
		return "consultar";
	}

	public String guardarDatos() {
		/*
        if (mtCatalogoDetalle.getIdCatalogoDetalle() != null) {
        	mtCatalogoDetalleDao.update(mtCatalogoDetalle);
        	return "cancelar";
        }
        else {
        	//existir();
        	//if (respuesta=="no"){
        		mtCatalogoDetalleDao.save(mtCatalogoDetalle);
        		mtCatalogoDetalle = new MtCatalogoDetalle();
        		invalidateMtCatalogoDetalles();
        		return "cancelar";
        	//}
        	//else{
        		//System.out.println("Ya existe");
        		//return "cancelar2";
        	//}
        }*/
		return null;
    }
	
}
