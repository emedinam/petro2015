package ec.gob.petroecuador.aduanapetro.catalogos.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
//import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.catalogos.jpa.Partida;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTnan;
import ec.gob.petroecuador.aduanapetro.catalogos.services.PartidaTnanService;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
//import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Menu;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;


@Component("partidaTnanBB")
@Scope("request")
public class PartidaTnanBackingBean extends RegistroComun {
	private Partida selectedPartida = new Partida();
	private PartidaTnan partidaTnan = new PartidaTnan();
	private PartidaTnan selectedPartidaTnan = new PartidaTnan();
	
	private List<PartidaTnan> listaPartidaTnans = new ArrayList<PartidaTnan>();
	
	private static Logger log = Logger.getLogger(PartidaTnanBackingBean.class);
	
	@Autowired 
	private PartidaTnanService service;

	
	public void guardar() {
		service.guardar(partidaTnan);
		saveRegistro(AccionCRUD.Registrado, partidaTnan.getId(), PartidaTnan.class);
		refreshAll();
		borrarListaPartidaTnans();
	}

	private void refreshAll() {
		partidaTnan = new PartidaTnan();
	}

	private void borrarListaPartidaTnans() {
		listaPartidaTnans.clear();
	}

	public void modificar() {
		service.guardar(selectedPartidaTnan);
		saveRegistro(AccionCRUD.Actualizado, selectedPartidaTnan.getId(), PartidaTnan.class);
		refreshAll();
		borrarListaPartidaTnans();
	}
	
	public void eliminar() {
		service.anular(selectedPartidaTnan);
		saveRegistro(AccionCRUD.Eliminado, selectedPartidaTnan.getId(), PartidaTnan.class);
		refreshAll();
		borrarListaPartidaTnans();
	}
	


	/* ========= get set ==================================== */
	public Partida getSelectedPartida() {
		return selectedPartida;
	}

	public void setSelectedPartida(Partida selectedPartida) {
		this.selectedPartida = selectedPartida;
	}
	
	public PartidaTnan getPartidaTnan() {
		return partidaTnan;
	}

	public void setPartidaTnan(PartidaTnan partidaTnan) {
		this.partidaTnan = partidaTnan;
	}

	public List<PartidaTnan> getListaPartidaTnans() {
		if (listaPartidaTnans.isEmpty()) {
			
			if(selectedPartida.getId()==null){
				listaPartidaTnans = service.obtenerActivos();
			}
			else{
			listaPartidaTnans = service.getListaPartidaTnans(selectedPartida.getId());
			log.info("Se consultaron "+(listaPartidaTnans!=null? listaPartidaTnans.size():"0")+" detalles partidas Tnan");
			}
		}
		return listaPartidaTnans;
	}

	public void setListaPartidaTnans(List<PartidaTnan> listaPartidaTnans) {
		this.listaPartidaTnans = listaPartidaTnans;
	}

	
	public PartidaTnan getSelectedPartidaTnan() {
		return selectedPartidaTnan;
	}

	public void setSelectedPartidaTnan(PartidaTnan selectedPartidaTnan) {
		this.selectedPartidaTnan = selectedPartidaTnan;
	}
	
	
	/*
	public List<Menu> getListaPadre() {
		if (listaMenus.isEmpty())
			listaPadre.add(new Menu());
		else
			listaPadre = listaMenus;
		return listaPadre;
	}

	public void setListaPadre(List<Menu> listaPadre) {
		this.listaPadre = listaPadre;
	}	
*/
	public Estados[] getEstados() {
	        return Estados.values();
    }

	
	/* ========= metodos ==================================== */
	public String botonCancelar(){
		return "cancelar";
	}
	
	public String botonConsultar(){
		return "consultar";
	}

	public String botonModificar(){
		return "modificar";
	}

	public String guardarDatos() {
		/*
        if (mtCatalogoDetalle.getIdCatalogoDetalle() != null) {
        	mtCatalogoDetalleDao.update(mtCatalogoDetalle);
        	return "cancelar";
        }
        else {
        	//existir();
        	//if (respuesta=="no"){
        		mtCatalogoDetalleDao.save(mtCatalogoDetalle);
        		mtCatalogoDetalle = new MtCatalogoDetalle();
        		invalidateMtCatalogoDetalles();
        		return "cancelar";
        	//}
        	//else{
        		//System.out.println("Ya existe");
        		//return "cancelar2";
        	//}
        }*/
		return null;
    }
}
