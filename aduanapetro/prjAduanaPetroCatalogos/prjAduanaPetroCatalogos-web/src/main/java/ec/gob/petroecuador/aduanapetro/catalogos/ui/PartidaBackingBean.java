package ec.gob.petroecuador.aduanapetro.catalogos.ui;

import java.util.ArrayList;
import java.util.List;



import org.apache.log4j.Logger;
//import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//import ec.gob.petroecuador.aduanapetro.catalogos.jpa.CatalogoDetalle;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.Partida;
//import ec.gob.petroecuador.aduanapetro.catalogos.services.MtCatalogoDetalleService;
import ec.gob.petroecuador.aduanapetro.catalogos.services.PartidaService;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
//import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Menu;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("partidaBB")
@Scope("request")
public class PartidaBackingBean extends RegistroComun {
	
	private Partida partida = new Partida();
	private Partida selectedPartida = new Partida();
	
	private List<Partida> listaPartidas = new ArrayList<Partida>();
	
	private static Logger log = Logger.getLogger(PartidaBackingBean.class);
	
	@Autowired 
	private PartidaService service;

	/* ========= get set ==================================== */
	public Partida getPartida() {
		return partida;
	}

	public void setPartida(Partida partida) {
		this.partida = partida;
	}

	public List<Partida> getListaPartidas() {
		if (listaPartidas.isEmpty()) {
			listaPartidas = service.obtenerActivos();
			log.info("Se consultaron "+(listaPartidas!=null? listaPartidas.size():"0")+" detalles partidas");
		}
		return listaPartidas;
	}

	public void setListaPartidas(List<Partida> listaPartidas) {
		this.listaPartidas = listaPartidas;
	}

	public Partida getSelectedPartida() {
		return selectedPartida;
	}

	public void setSelectedPartida(Partida selectedPartida) {
		this.selectedPartida = selectedPartida;
	}
	
	
	/*
	public List<Menu> getListaPadre() {
		if (listaMenus.isEmpty())
			listaPadre.add(new Menu());
		else
			listaPadre = listaMenus;
		return listaPadre;
	}

	public void setListaPadre(List<Menu> listaPadre) {
		this.listaPadre = listaPadre;
	}	
*/
	public Estados[] getEstados() {
	        return Estados.values();
    }

	
	/* ========= metodos ==================================== */
	
	public String botonSuplementario(){
		System.out.println("llama a tnans"+selectedPartida.getPartida());
		return "suplementario";
	}
	
	public String botonCancelar(){
		return "cancelar";
	}
	
	public void guardar() {
		service.guardar(partida);
		saveRegistro(AccionCRUD.Registrado, partida.getId(), Partida.class);
		refreshAll();
		borrarListaPartidas();
	}
	
	private void refreshAll() {
		partida = new Partida();
	}

	private void borrarListaPartidas() {
		listaPartidas.clear();
	}
	
	public void eliminar() {
		service.anular(selectedPartida);
		saveRegistro(AccionCRUD.Eliminado, selectedPartida.getId(), Partida.class);
		refreshAll();
		borrarListaPartidas();
	}

	public void modificar() {
		service.guardar(selectedPartida);
		saveRegistro(AccionCRUD.Actualizado, selectedPartida.getId(), Partida.class);
		refreshAll();
		borrarListaPartidas();
	}

	public String guardarDatos() {
		/*
        if (mtCatalogoDetalle.getIdCatalogoDetalle() != null) {
        	mtCatalogoDetalleDao.update(mtCatalogoDetalle);
        	return "cancelar";
        }
        else {
        	//existir();
        	//if (respuesta=="no"){
        		mtCatalogoDetalleDao.save(mtCatalogoDetalle);
        		mtCatalogoDetalle = new MtCatalogoDetalle();
        		invalidateMtCatalogoDetalles();
        		return "cancelar";
        	//}
        	//else{
        		//System.out.println("Ya existe");
        		//return "cancelar2";
        	//}
        }*/
		return null;
    }
	
}
