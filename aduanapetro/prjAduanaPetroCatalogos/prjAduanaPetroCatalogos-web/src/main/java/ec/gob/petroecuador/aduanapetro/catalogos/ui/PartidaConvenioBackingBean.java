package ec.gob.petroecuador.aduanapetro.catalogos.ui;

import java.util.ArrayList;
import java.util.List;




import org.apache.log4j.Logger;
//import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.catalogos.jpa.Partida;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaConvenio;
import ec.gob.petroecuador.aduanapetro.catalogos.services.PartidaConvenioService;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Menu;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("partidaConvenioBB")
@Scope("request")
public class PartidaConvenioBackingBean extends RegistroComun {
	private Partida selectedPartida = new Partida();
	private PartidaConvenio partidaConvenio = new PartidaConvenio();
	private PartidaConvenio selectedPartidaConvenio = new PartidaConvenio();
	
	private List<PartidaConvenio> listaPartidaConvenios = new ArrayList<PartidaConvenio>();
	
	private static Logger log = Logger.getLogger(PartidaConvenioBackingBean.class);
	
	@Autowired 
	private PartidaConvenioService service;

	/*
	public void modificar() {
		service.guardar(selectedmtCatalogoDetalle);
		saveRegistro(AccionCRUD.Actualizado, selectedmtCatalogoDetalle.getId(), Menu.class);
		refreshAll();
		borrarListaMtCatalogoDetalles();
	}
	*/
	/*
	public void guardar() {
		service.guardar(mtCatalogoDetalle);
		saveRegistro(AccionCRUD.Registrado, mtCatalogoDetalle.getId(), CatalogoDetalle.class);
		refreshAll();
		borrarListaMtCatalogoDetalles();
	}
	*/
	/*
	public void eliminar() {
		
		service.anular(selectedmtCatalogoDetalle);
		saveRegistro(AccionCRUD.Eliminado, selectedmtCatalogoDetalle.getId(), Menu.class);
		refreshAll();
		borrarListaMtCatalogoDetalles();
	}
	
	*/
/*
	private void refreshAll() {
		mtCatalogoDetalle = new CatalogoDetalle();
	}

	private void borrarListaMtCatalogoDetalles() {
		listaMtCatalogoDetalles.clear();
	}
*/
	/* ========= get set ==================================== */
	public Partida getSelectedPartida() {
		return selectedPartida;
	}

	public void setSelectedPartida(Partida selectedPartida) {
		this.selectedPartida = selectedPartida;
	}
	
	public PartidaConvenio getPartidaConvenio() {
		return partidaConvenio;
	}

	public void setPartidaConvenio(PartidaConvenio partidaConvenio) {
		this.partidaConvenio = partidaConvenio;
	}

	public List<PartidaConvenio> getListaPartidaConvenios() {
		if (listaPartidaConvenios.isEmpty()) {
			//listaPartidaTnans = service.obtenerActivos();
			if (selectedPartida.getId()==null){
			}
			else{
			listaPartidaConvenios = service.getListaPartidaConvenios(selectedPartida.getId());
			log.info("Se consultaron "+(listaPartidaConvenios!=null? listaPartidaConvenios.size():"0")+" detalles partidas Convenio");
			}
		}
		return listaPartidaConvenios;
	}

	public void setListaPartidaConvenios(List<PartidaConvenio> listaPartidaConvenios) {
		this.listaPartidaConvenios = listaPartidaConvenios;
	}

	
	public PartidaConvenio getSelectedPartidaConvenio() {
		return selectedPartidaConvenio;
	}

	public void setSelectedPartidaConvenio(PartidaConvenio selectedPartidaConvenio) {
		this.selectedPartidaConvenio = selectedPartidaConvenio;
	}
	
	
	/*
	public List<Menu> getListaPadre() {
		if (listaMenus.isEmpty())
			listaPadre.add(new Menu());
		else
			listaPadre = listaMenus;
		return listaPadre;
	}

	public void setListaPadre(List<Menu> listaPadre) {
		this.listaPadre = listaPadre;
	}	
*/
	public Estados[] getEstados() {
	        return Estados.values();
    }

	
	/* ========= metodos ==================================== */
	public String botonCancelar(){
		return "cancelar";
	}
	
	public String botonConsultar(){
		return "consultar";
	}

	public String guardarDatos() {
		/*
        if (mtCatalogoDetalle.getIdCatalogoDetalle() != null) {
        	mtCatalogoDetalleDao.update(mtCatalogoDetalle);
        	return "cancelar";
        }
        else {
        	//existir();
        	//if (respuesta=="no"){
        		mtCatalogoDetalleDao.save(mtCatalogoDetalle);
        		mtCatalogoDetalle = new MtCatalogoDetalle();
        		invalidateMtCatalogoDetalles();
        		return "cancelar";
        	//}
        	//else{
        		//System.out.println("Ya existe");
        		//return "cancelar2";
        	//}
        }*/
		return null;
    }
	
}
