package ec.gob.petroecuador.aduanapetro.catalogos.ui;

import java.util.ArrayList;
import java.util.List;




import org.apache.log4j.Logger;
//import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.catalogos.jpa.Partida;
import ec.gob.petroecuador.aduanapetro.catalogos.jpa.PartidaTpng;
import ec.gob.petroecuador.aduanapetro.catalogos.services.PartidaTpngService;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.jpa.Menu;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("partidaTpngBB")
@Scope("request")
public class PartidaTpngBackingBean extends RegistroComun {
	private Partida selectedPartida = new Partida();
	private PartidaTpng partidaTpng = new PartidaTpng();
	private PartidaTpng selectedPartidaTpng = new PartidaTpng();
	
	private List<PartidaTpng> listaPartidaTpngs = new ArrayList<PartidaTpng>();
	
	private static Logger log = Logger.getLogger(PartidaTnanBackingBean.class);
	
	@Autowired 
	private PartidaTpngService service;

	/*
	public void modificar() {
		service.guardar(selectedmtCatalogoDetalle);
		saveRegistro(AccionCRUD.Actualizado, selectedmtCatalogoDetalle.getId(), Menu.class);
		refreshAll();
		borrarListaMtCatalogoDetalles();
	}
	*/
	/*
	public void guardar() {
		service.guardar(mtCatalogoDetalle);
		saveRegistro(AccionCRUD.Registrado, mtCatalogoDetalle.getId(), CatalogoDetalle.class);
		refreshAll();
		borrarListaMtCatalogoDetalles();
	}
	*/
	/*
	public void eliminar() {
		
		service.anular(selectedmtCatalogoDetalle);
		saveRegistro(AccionCRUD.Eliminado, selectedmtCatalogoDetalle.getId(), Menu.class);
		refreshAll();
		borrarListaMtCatalogoDetalles();
	}
	
	*/
/*
	private void refreshAll() {
		mtCatalogoDetalle = new CatalogoDetalle();
	}

	private void borrarListaMtCatalogoDetalles() {
		listaMtCatalogoDetalles.clear();
	}
*/
	/* ========= get set ==================================== */
	public Partida getSelectedPartida() {
		return selectedPartida;
	}

	public void setSelectedPartida(Partida selectedPartida) {
		this.selectedPartida = selectedPartida;
	}
	
	public PartidaTpng getPartidaTpng() {
		return partidaTpng;
	}

	public void setPartidaTpng(PartidaTpng partidaTpng) {
		this.partidaTpng = partidaTpng;
	}

	public List<PartidaTpng> getListaPartidaTpngs() {
		if (listaPartidaTpngs.isEmpty()) {
			//listaPartidaTpngs = service.obtenerActivos();
			if(selectedPartida.getId()==null){
			}
			else{
			listaPartidaTpngs = service.getListaPartidaTpngs(selectedPartida.getId());
			log.info("Se consultaron "+(listaPartidaTpngs!=null? listaPartidaTpngs.size():"0")+" detalles partidas Tpng");
			}
		}
		return listaPartidaTpngs;
	}

	public void setListaPartidaTpngs(List<PartidaTpng> listaPartidaTpngs) {
		this.listaPartidaTpngs = listaPartidaTpngs;
	}

	
	public PartidaTpng getSelectedPartidaTpng() {
		return selectedPartidaTpng;
	}

	public void setSelectedPartidaTpng(PartidaTpng selectedPartidaTpng) {
		this.selectedPartidaTpng = selectedPartidaTpng;
	}
	
	
	/*
	public List<Menu> getListaPadre() {
		if (listaMenus.isEmpty())
			listaPadre.add(new Menu());
		else
			listaPadre = listaMenus;
		return listaPadre;
	}

	public void setListaPadre(List<Menu> listaPadre) {
		this.listaPadre = listaPadre;
	}	
*/
	public Estados[] getEstados() {
	        return Estados.values();
    }

	
	/* ========= metodos ==================================== */
	public String botonCancelar(){
		return "cancelar";
	}
	
	public String botonConsultar(){
		return "consultar";
	}

	public String guardarDatos() {
		/*
        if (mtCatalogoDetalle.getIdCatalogoDetalle() != null) {
        	mtCatalogoDetalleDao.update(mtCatalogoDetalle);
        	return "cancelar";
        }
        else {
        	//existir();
        	//if (respuesta=="no"){
        		mtCatalogoDetalleDao.save(mtCatalogoDetalle);
        		mtCatalogoDetalle = new MtCatalogoDetalle();
        		invalidateMtCatalogoDetalles();
        		return "cancelar";
        	//}
        	//else{
        		//System.out.println("Ya existe");
        		//return "cancelar2";
        	//}
        }*/
		return null;
    }
	
}
