package ec.gob.petroecuador.aduanapetro.comunicacion.ui;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.comunicacion.jpa.Plantilla;
import ec.gob.petroecuador.aduanapetro.comunicacion.services.PlantillaService;
import ec.gob.petroecuador.aduanapetro.seguridad.consts.ConstantesSeguridad;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("plantillaBB")
@Scope("request")
public class PlantillaBackingBean extends RegistroComun<Plantilla> {
	
	
	

	private static final Logger log = LoggerFactory
			.getLogger(PlantillaBackingBean.class);

	@Autowired
	PlantillaService plantillaService;

	private Plantilla plantilla = new Plantilla();
	private Plantilla selectedPlantilla = new Plantilla();
	private List<Plantilla> listaPlantilla = new ArrayList<Plantilla>();

	private UploadedFile filePlantilla;
	private UploadedFile fileJRXML;

	
	public PlantillaBackingBean() {
		// TODO Auto-generated constructor stub
		super("","");
	}
	public Plantilla getPlantilla() {
		return plantilla;
	}

	public void setPlantilla(Plantilla plantilla) {
		this.plantilla = plantilla;
	}

	public Plantilla getSelectedPlantilla() {
		return selectedPlantilla;
	}

	public void setSelectedPlantilla(Plantilla selectedPlantilla) {
		this.selectedPlantilla = selectedPlantilla;
	}

	public List<Plantilla> getListaPlantilla() {
		if (listaPlantilla == null || listaPlantilla.isEmpty())
			listaPlantilla = plantillaService.obtenerActivos();
		return listaPlantilla;
	}

	public void setListaPlantilla(List<Plantilla> listaPlantilla) {
		this.listaPlantilla = listaPlantilla;
	}

	public UploadedFile getFilePlantilla() {
		return filePlantilla;
	}

	public void setFilePlantilla(UploadedFile filePlantilla) {
		this.filePlantilla = filePlantilla;
	}

	public UploadedFile getFileJRXML() {
		return fileJRXML;
	}

	public void setFileJRXML(UploadedFile fileJRXML) {
		this.fileJRXML = fileJRXML;
	}

	public void guardar() {
		if(fileJRXML != null && filePlantilla != null){
		if (fileJRXML.getFileName().endsWith("jrxml")
				&&  filePlantilla.getFileName().endsWith("jasper")
				){
			try {
				plantilla.setUrlJRXML(copyFile(fileJRXML.getFileName(),
						fileJRXML.getInputstream(),
						ConstantesSeguridad.BASE_CONFIG_PATH_JRXML));

				plantilla.setUrlPlantilla(copyFile(filePlantilla.getFileName(),
						filePlantilla.getInputstream(),
						ConstantesSeguridad.BASE_CONFIG_PATH_TEMPLATE));

			} catch (IOException e) {
				e.printStackTrace();
			}
		plantillaService.guardar(plantilla);
		saveRegistro(AccionCRUD.Registrado, plantilla.getId(), Plantilla.class);
		borrarListaAcciones();
		plantilla = new Plantilla();
		fileJRXML= null ;
		filePlantilla = null ;
		}
		else {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,null,"Uno  o ambos ficheros seleccionado no tiene la extencion correcta ej: .jasper ó .jrxml");
            FacesContext.getCurrentInstance().addMessage("GRAVE", message);
		}
	 }else {
		 FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,null,"Ficheros Plantillas JRXML  y  Plantilla compilada  de caracter Obligatorio...");
         FacesContext.getCurrentInstance().addMessage("GRAVE", message);
	 }
	}

	private void borrarListaAcciones() {
		listaPlantilla = null;
	}

	public void modificar() {
		log.info("Objeto Selected......" + selectedPlantilla);
		plantillaService.guardar(selectedPlantilla);
		saveRegistro(AccionCRUD.Actualizado, selectedPlantilla.getId(),
				Plantilla.class);
		selectedPlantilla = new Plantilla();
	}

	public void eliminar() {
		log.info("Objeto Selected......" + selectedPlantilla);
		plantillaService.anular(selectedPlantilla);
		saveRegistro(AccionCRUD.Eliminado, selectedPlantilla.getId(),
				Plantilla.class);
		borrarListaAcciones();

	}

	private String copyFile(String fileName, InputStream in, String destino) {
		String fichero = "";
		try {
			// write the inputStream to a FileOutputStream
			OutputStream out = new FileOutputStream(new File(destino
					+ System.getProperty("file.separator") + fileName));

			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = in.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}

			in.close();
			out.flush();
			out.close();
			fichero = destino + fileName;
			log.info("Nuevo Fichero Creado!");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return fichero;
	}
	@Override
	public void inicializarVariables() {
		// TODO Auto-generated method stub
		
	}
}
