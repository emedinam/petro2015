package ec.gob.petroecuador.aduanapetro.comunicacion.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.comunicacion.dao.PlantillaDao;
import ec.gob.petroecuador.aduanapetro.comunicacion.jpa.Plantilla;
import ec.gob.petroecuador.aduanapetro.comunicacion.services.PlantillaService;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;

@Service
public class PlantillaServiceImpl extends GenericServiceImpl<Plantilla, Long> implements PlantillaService {

	@Autowired
	PlantillaDao dao;
	
	@Override
	public GenericDAO<Plantilla, Long> getDao() {
		// TODO Auto-generated method stub
		return dao;
	}

}
