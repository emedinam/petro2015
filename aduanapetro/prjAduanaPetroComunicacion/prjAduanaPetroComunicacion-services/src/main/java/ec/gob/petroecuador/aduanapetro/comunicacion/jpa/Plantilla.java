package ec.gob.petroecuador.aduanapetro.comunicacion.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

@Entity
@Table(name = "TRV_PLANTILLA", schema = "ADUANAPETRO_COMUNICACION")
public class Plantilla extends DatabaseObject<Long> {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 4073268752460166514L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "estado", length = 8)
	private Estados estado;

	@NotNull
	@Column(name = "nombre_plantilla", unique = true)
	private String nombre;

	private String descripcion;

	@NotNull
	@Column(name = "url_plantilla")
	private String urlPlantilla;

	@NotNull
	@Column(name = "url_jrxml")
	private String urlJRXML;

	@Override
	public Estados getEstado() {
		// TODO Auto-generated method stub
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {

		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;

	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getUrlPlantilla() {
		return urlPlantilla;
	}

	public void setUrlPlantilla(String urlPlantilla) {
		this.urlPlantilla = urlPlantilla;
	}

	public String getUrlJRXML() {
		return urlJRXML;
	}

	public void setUrlJRXML(String urlJRXML) {
		this.urlJRXML = urlJRXML;
	}

}
