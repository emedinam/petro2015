package ec.gob.petroecuador.aduanapetro.comunicacion.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.comunicacion.dao.PlantillaDao;
import ec.gob.petroecuador.aduanapetro.comunicacion.jpa.Plantilla;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;

@Component("plantillaDao")
public class PlantillaDaoImpl extends GenericDAOImpl<Plantilla, Long> implements
		PlantillaDao {

}
