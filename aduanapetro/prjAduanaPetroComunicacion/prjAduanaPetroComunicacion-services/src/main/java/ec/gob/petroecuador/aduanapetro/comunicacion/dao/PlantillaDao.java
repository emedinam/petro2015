package ec.gob.petroecuador.aduanapetro.comunicacion.dao;

import ec.gob.petroecuador.aduanapetro.comunicacion.jpa.Plantilla;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

public interface PlantillaDao extends GenericDAO<Plantilla,Long> {

}
