package ec.gob.petroecuador.aduanapetro.comunicacion.services;

import ec.gob.petroecuador.aduanapetro.comunicacion.jpa.Plantilla;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;


public interface PlantillaService extends GenericService<Plantilla, Long> {

}
