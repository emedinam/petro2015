$(document).ready(function(){   
	
	$.fn.extend( {
        limiter: function(limit) {
            $(this).on("keyup focus", function() {
                setCount(this);
            });
            function setCount(src) {
                var chars = src.value.length;
                if (chars > limit) {
                    src.value = src.value.substr(0, limit);
                    chars = limit;
               }
               $('#lblNumCaracter_'+src.id).html(src.value.length + ' caracteres utilizados de '+ limit +' disponibles');
            }
            setCount($(this)[0]);
        },
        
        validar: function(){
        	$(this).on("keypress keyup blur",function (event) {
        		key = event.which
        		// tab
        		if (key == 9)
        			return true

        			// backspace
        		if (key == 8)
        			return true
                //$(this).val($(this).val().replace(/[^0-9\,]/g,''));
                if ((event.which != 44 || $(this).val().indexOf(',') != -1) && (event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });
        },
        validarEnteros: function(){
        	$(this).on("keypress keyup blur",function (event) {
        		key = event.which
        		// tab
        		if (key == 9)
        			return true

        			// backspace
        		if (key == 8)
        			return true
                //$(this).val($(this).val().replace(/[^0-9\,]/g,''));
                if ((event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });
        },
        validarSoloLetras: function(){
        	$(this).on("keypress keyup blur",function (event) {
        		key = event.which;
        		caracter = String.fromCharCode(key);
            	var regex = /^[a-zA-ZáéíñóúüÁÉÍÑÓÚÜ]+$/;
            	// control keys
            	if ((key==null) || (key==0) || (key==8) || 
            	    (key==9) || (key==13) || (key==27) || (key==32) )
            	   return true;
            	
            	if (!regex.test(caracter)) {
                    event.preventDefault();
                }

        	});
        },
        validarSoloAlphas: function(){
        	$(this).on("keypress keyup blur",function (event) {
        		key = event.which;
        		caracter = String.fromCharCode(key);
            	var regex = /^[0-9a-zA-ZáéíñóúüÁÉÍÑÓÚÜ]+$/;
            	// control keys
            	if ((key==null) || (key==0) || (key==8) || 
            	    (key==9) || (key==13) || (key==27) || (key==32) )
            	   return true;
            	
            	if (!regex.test(caracter)) {
                    event.preventDefault();
                }

        	});
        },
        validarCorreo: function(){
        	$(this).on("keypress keyup blur",function (event) {
        		key = event.which;
        		caracter = String.fromCharCode(key);
            	var regex = /^[0-9a-zA-Z]+$/;
            	// control keys
            	if ((key==null) || (key==0) || (key==8) || 
            	    (key==9) || (key==13) || (key==27) || (key==45) || (key==64)
            	    || (key==95) || (key==46))
            	   return true;
            	
            	if (!regex.test(caracter)) {
                    event.preventDefault();
                }

        	});
        }
    });
	
	$( "<br>" ).insertBefore( $( "#confirmPane_content input:first-child" ));  
	
	$('.validated-length').maxlength({  
	    events: [], // Array of events to be triggerd   
	    maxCharacters: 400, // Characters limit  
	    status: false, // True to show status indicator bewlow the element   
	    statusClass: "status", // The class on the status div 
	    statusText: "caracteres restantes", // The status text 
	    notificationClass: "notification",  // Will be added when maxlength is reached 
	    showAlert: false, // True to show a regular alert message   
	    alertText: "Ha escrito demasiados caracteres.", // Text in alert message  
	    slider: false // True Use counter slider   
	  });	 
	  $('.numeric').numeric({
			maxPreDecimalPlaces : 20,
			maxDecimalPlaces    : 2
		});
	  
	  $('#txt_vision').change( function() {
		    $("#txt_vision").limiter(1500);
		});
	  
	  $('#txt_vision').bind('input propertychange', function() {
		  $("#txt_vision").limiter(1500);		    
		});
	  
	  $('#txt_objetivo').change( function() {
		    $("#txt_objetivo").limiter(500);
		});
	  
	  $('#txt_objetivo').bind('input propertychange', function() {
		  $("#txt_objetivo").limiter(500);		    
		});
	  
	  $('#txt_politica_publica').change( function() {
		    $("#txt_politica_publica").limiter(1000);
		});
	  
	  $('#txt_politica_publica').bind('input propertychange', function() {
		  $("#txt_politica_publica").limiter(1000);		    
		});
	  
	  $('#txt_descProblema').change( function() {
		    $("#txt_descProblema").limiter(500);
		});
	  
	  $('#txt_descProblema').bind('input propertychange', function() {
		  $("#txt_descProblema").limiter(500);		    
		});
	  
	  $('#txt_descProblema_pot').change( function() {
		    $("#txt_descProblema_pot").limiter(500);
		});
	  
	  $('#txt_descProblema_pot').bind('input propertychange', function() {
		  $("#txt_descProblema_pot").limiter(500);		    
		});
	  
	  $('#txt_descripcion').change( function() {
		    $("#txt_descripcion").limiter(400);
		});
	  
	  $('#txt_descripcion').bind('input propertychange', function() {
		  $("#txt_descripcion").limiter(400);		    
		});
	  
	  $('#txt_estrategia_articulacion').change( function() {
		    $("#txt_estrategia_articulacion").limiter(500);
		});
	  
	  $('#txt_estrategia_articulacion').bind('input propertychange', function() {
		  $("#txt_estrategia_articulacion").limiter(500);		    
		});
	  
	  $('#txt_nombre_institucion').change( function() {
		    $("#txt_nombre_institucion").limiter(250);
		});
	  
	  $('#txt_nombre_institucion').bind('input propertychange', function() {
		  $("#txt_nombre_institucion").limiter(250);		    
		});
	  
	  $('#txt_tematica').change( function() {
		    $("#txt_tematica").limiter(250);
		});
	  
	  $('#txt_tematica').bind('input propertychange', function() {
		  $("#txt_tematica").limiter(250);		    
		});
	  
	  $('#txt_mecanismo').change( function() {
		    $("#txt_mecanismo").limiter(500);
		});
	  
	  $('#txt_mecanismo').bind('input propertychange', function() {
		  $("#txt_mecanismo").limiter(500);		    
		});
	  
	  $('#txt_est_part_ciudadana').change( function() {
		    $("#txt_est_part_ciudadana").limiter(500);
		});
	  
	  $('#txt_est_part_ciudadana').bind('input propertychange', function() {
		  $("#txt_est_part_ciudadana").limiter(500);		    
		});
	  
	  $('#txt_observacion_part_ciudadana').change( function() {
		    $("#txt_observacion_part_ciudadana").limiter(400);
		});
	  
	  $('#txt_observacion_part_ciudadana').bind('input propertychange', function() {
		  $("#txt_observacion_part_ciudadana").limiter(400);		    
		});
	  
	  $('#txt_producto').change( function() {
		    $("#txt_producto").limiter(250);
		});
	  
	  $('#txt_producto').bind('input propertychange', function() {
		  $("#txt_producto").limiter(250);		    
		});
	  
	  $('#txt_otra_periodicidad').change( function() {
		    $("#txt_otra_periodicidad").limiter(50);
		});
	  
	  $('#txt_otra_periodicidad').bind('input propertychange', function() {
		  $("#txt_otra_periodicidad").limiter(50);		    
		});
	  
	  $('#txt_nombre_programa').change( function() {
		    $("#txt_nombre_programa").limiter(500);
		});
	  
	  $('#txt_nombre_programa').bind('input propertychange', function() {
		  $("#txt_nombre_programa").limiter(500);		    
		});
	  
	  $('#txt_otra_fuente').change( function() {
		    $("#txt_otra_fuente").limiter(250);
		});
	  
	  $('#txt_otra_fuente').bind('input propertychange', function() {
		  $("#txt_otra_fuente").limiter(250);		    
		});
	  
	  $('#txt_meta_programa').change( function() {
		    $("#txt_meta_programa").limiter(250);
		});
	  
	  $('#txt_meta_programa').bind('input propertychange', function() {
		  $("#txt_meta_programa").limiter(250);		    
		});
	  
	  $('#txt_indicador_gestion').change( function() {
		    $("#txt_indicador_gestion").limiter(250);
		});
	  
	  $('#txt_indicador_gestion').bind('input propertychange', function() {
		  $("#txt_indicador_gestion").limiter(250);		    
		});
	  
	  $('#txt_otra_tipologia').change( function() {
		    $("#txt_otra_tipologia").limiter(200);
		});
	  
	  $('#txt_otra_tipologia').bind('input propertychange', function() {
		  $("#txt_otra_tipologia").limiter(200);		    
		});
	  
	  $('#txt_categoria_ordenamiento_territorial').change( function() {
		    $("#txt_categoria_ordenamiento_territorial").limiter(400);
		});
	  
	  $('#txt_categoria_ordenamiento_territorial').bind('input propertychange', function() {
		  $("#txt_categoria_ordenamiento_territorial").limiter(400);		    
		});
	  
	  
	  $('.soloNumeros').validar();	  
	  $('.soloNumerosEnteros').validarEnteros();	
	  $('.soloLetras').validarSoloLetras();
	  $('.soloCorreo').validarCorreo();
	  $('.soloAlphas').validarSoloAlphas();
	  
	});