$(document).ready(function() {
	$("input.numDocClass").mask("999-999-999999999");
	$("input.numValorClass").mask("999,999.99");
});


PrimeFaces.locales['es'] = {
	closeText : 'Cerrar',
	prevText : 'Anterior',
	nextText : 'Siguiente',
	monthNames : [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
			'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
			'Diciembre' ],
	monthNamesShort : [ 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago',
			'Sep', 'Oct', 'Nov', 'Dic' ],
	dayNames : [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves',
			'Viernes', 'Sábado' ],
	dayNamesShort : [ 'Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab' ],
	dayNamesMin : [ 'D', 'L', 'M', 'X', 'J', 'V', 'S' ],
	weekHeader : 'Semana',
	firstDay : 1,
	isRTL : false,
	showMonthAfterYear : false,
	yearSuffix : '',
	timeOnlyTitle : 'Sólo hora',
	timeText : 'Tiempo',
	hourText : 'Hora',
	minuteText : 'Minuto',
	secondText : 'Segundo',
	currentText : 'Fecha actual',
	ampm : false,
	month : 'Mes',
	week : 'Semana',
	day : 'Día',
	allDayText : 'Todo el día'
};

function showStatus() {	
	widgetBlockGrid.show();
	widgetBlockToolbarDT.show();

}
function hideStatus() {	
	widgetBlockGrid.hide();
	widgetBlockToolbarDT.hide();
}

function noAlpha(e){
	if (e.which!=8 && e.which != 44 && e.which != 46 && (e.which < 48 || e.which > 57)&& e.which != 127) 
	 return false;
	return true;
}


/**
 * Desacopla el evento click de los elementos accodionPanel de los detalles,
 * para que el cambio entre el grid y el formulario de ingreso no le est�
 * permitido al usuario sino simplemente a trav�s del sistema.
 */
function blockClickAccordion() {
	$('div.styleAccordionPanelDetail > h3').each(function() {
		$(this).unbind('click');
	});
}
