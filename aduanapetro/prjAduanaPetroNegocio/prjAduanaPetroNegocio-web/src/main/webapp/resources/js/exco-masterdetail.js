/**
 * Cuando el DOM est� listo, desacopla el evento click de los tabs del
 * maestro-detalle y delega este evento a la funci�n handleClick(event). Adem�s
 * bloquea el comportamiento normal de accordionPanel por si al usuario le da
 * por refrescar la pantalla cuando est� dentro de un detalle.
 */
$(document).ready(function() {
	$('div.styleTabViewMasterDetail > ul > li').each(function() {
		$(this).unbind('click');
		$(this).click(function(event) {
			handleClick(event);
		});
		//blockClickAccordion();
	});
});

/**
 * Si el usuario cambia de tab env�a un request al servidor con los datos del maestro para su validaci�n.
 * Solo se env�a el request cuando se cambia del tab maestro a cualquiera de los detalles.
 * @param event Evento click del tab
 */
function handleClick(event) {
	var indexSenderTab = getIndexTab(event);
	if (indexSenderTab != 0 && tabViewMasterDetail.getActiveIndex() == 0) {
		PrimeFaces.ajax
				.AjaxRequest({
					formId : 'tabViewMasterDetail:formularioMaster',
					source : 'tabViewMasterDetail:tabMaster',
					process : 'tabViewMasterDetail:tabMaster',
					update : 'tabViewMasterDetail:formularioMaster:panelFormularioMaster',
					oncomplete : function(xhr, status, args, e) {
						onCompleteRequest(xhr, status, args, event);
					}
				});
	}
	else {
		changeActiveTab(event);
		cancelEvent(event);
	}
}

/**
 * @param xhr
 *            Elemento propio de AJAX
 * @param status
 *            Elemento propio de AJAX
 * @param args
 *            Elemento propio de AJAX
 * @param event
 *            Evento click del tab
 */
function onCompleteRequest(xhr, status, args, event) {
	var formValid = isFormValid(xhr);
	if (formValid) {
		changeActiveTab(event);
	} else {
		cancelEvent(event);
	}
}

/**
 * Indica si el XML HTTP Request (xhr) tiene sus argumentos v�lidos.
 * 
 * @param xhr
 *            Elemento propio de AJAX
 * @returns {bool}
 */
function isFormValid(xhr) {
	response = xhr.responseText;
	var formValid;
	if (response.search('validationFailed') != -1) { // Fall� la validaci�n
														// del formulario
		formValid = false;
	} else {// Formulario v�lido
		formValid = true;
	}
	return formValid;
}

/**
 * Cancela la ejecuci�n del evento proporcionado
 * 
 * @param event
 *            Evento click del tab
 */
function cancelEvent(event) {
	if (event.preventDefault) {
		event.preventDefault();
	} else {
		event.returnValue = false;
	}
}

/**
 * Cambia el tab activo del tabView Maestro-detalle. Si el cambio es desde el
 * tab maestro a cualquiera de los tabs detalle guarda los datos del formulario
 * maestro
 * 
 * @param eventEvento
 *            click del tab
 */
function changeActiveTab(event) {
	var indexSenderTab = getIndexTab(event);
	if (indexSenderTab != 0 && tabViewMasterDetail.getActiveIndex() == 0) {
		tabViewMasterDetail.select(indexSenderTab);
		clickButtonGuardarMaster();
	} else {
		tabViewMasterDetail.select(indexSenderTab);
	}
}

/**
 * Devuelve el �ndice del tab sobre el cual se ha hecho click.
 * 
 * @param event
 *            Evento click
 * @returns {int}
 */
function getIndexTab(event) {
	var tabTargets = getTabTargets();
	var target = event.target.toString();
	var lastPosition = target.indexOf("#");
	var smallTarget = target.substring(lastPosition);
	var indexSenderTab = jQuery.inArray(smallTarget, tabTargets);
	return indexSenderTab;
}
/**
 * Devuelve los targets asociados a cada uno de los tab del tabview
 * maestro-detalle
 * 
 * @returns {Array}
 */
function getTabTargets() {
	var tabTargets = [];
	$('div.styleTabViewMasterDetail > ul > li > a').each(function() {
		tabTargets.push($(this).attr('href'));
	});
	return tabTargets;

}

/**
 * Si los argumentos del formulario son v�lidos cierra la vista del formulario y
 * activa la visualizaci�n del grid.
 * 
 * @param xhr
 *            Elemento propio de AJAX
 * @param arguments
 *            Elemento propio de AJAX
 * @param widgetAccordion
 *            Nombre del widget del acordi�n.
 */
function onCompleteButtonGuardarDetailClick(xhr, arguments, widgetAccordion) {
	var formValid = isFormValid(xhr);
	if (formValid) {
		widgetAccordion.select(0);
	}
}

/**
 * Busca el bot�n guardar del maestro y desencadena el evento click de �ste.
 */
function clickButtonGuardarMaster() {
	$("#tabViewMasterDetail\\:tabMaster button[id$='buttonGuardar']").each(
			function() {
				$(this).trigger('click');
			});
}

/**
 * Desacopla el evento click de los elementos accodionPanel de los detalles,
 * para que el cambio entre el grid y el formulario de ingreso no le est�
 * permitido al usuario sino simplemente a trav�s del sistema.
 */
function blockClickAccordion() {
	$('div.styleAccordionPanelDetail > h3').each(function() {
		$(this).unbind('click');
	});
}
