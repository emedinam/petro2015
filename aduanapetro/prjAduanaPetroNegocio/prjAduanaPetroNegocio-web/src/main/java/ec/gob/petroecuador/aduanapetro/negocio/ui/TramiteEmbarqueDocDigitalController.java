package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TipoDocumento;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDocDigital;
import ec.gob.petroecuador.aduanapetro.negocio.services.TipoDocumentoService;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueDocDigitalService;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueDocDigitalDataManager;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("tramiteEmbarqueDocDigitalController")
@Scope("request")
public class TramiteEmbarqueDocDigitalController extends RegistroComun<TramiteEmbarqueDocDigital> {

	public TramiteEmbarqueDocDigitalController() {
		super("", "");
	}

	@Autowired
	private TramiteEmbarqueDocDigitalService tramiteEmbarqueDocDigitalService;

	@Autowired
	private TipoDocumentoService tipoDocumentoService;
	
	@Autowired
	private TramiteEmbarqueDocDigitalDataManager tramiteEmbarqueDocDigitalSeleccionado;

	@Autowired
	private TramiteEmbarqueDataManager tramiteEmbarqueSeleccionado;
	
	@Autowired
	private TramiteEmbarqueDesaduanamientoController tramiteEmbarqueDesaduanamientoController;

	private TramiteEmbarqueDocDigital tramiteEmbarqueDocDigital = new TramiteEmbarqueDocDigital();
	private List<TramiteEmbarqueDocDigital> lTramiteEmbarquesDocDigital = new ArrayList<>();
	
	private List<TipoDocumento> tipoDocumentos = new ArrayList<>();

	// METODOS
	public void inicializarVariables() {
		try {
			if (tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado()!=null && 
				tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId() != null)
				lTramiteEmbarquesDocDigital = tramiteEmbarqueDocDigitalService.obtenerPorPadre(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado());
			else
				lTramiteEmbarquesDocDigital = new ArrayList<TramiteEmbarqueDocDigital>();
			
			cargarDocumentos();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void nuevoRegistro() {
		tramiteEmbarqueDocDigitalSeleccionado.setTramiteEmbarqueDocDigitalSeleccionado(new TramiteEmbarqueDocDigital());
		nuevoRegistro(tramiteEmbarqueDocDigitalSeleccionado.getTramiteEmbarqueDocDigitalSeleccionado());
	}
	
	public void eliminar() {
		elementoSeleccionado = tramiteEmbarqueDocDigitalSeleccionado.getTramiteEmbarqueDocDigitalSeleccionado();
		eliminar(tramiteEmbarqueDocDigitalService);
	}

	public void guardar() {
		tramiteEmbarqueDocDigitalSeleccionado.getTramiteEmbarqueDocDigitalSeleccionado().setTramiteEmbarque(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado());
		elementoSeleccionado = tramiteEmbarqueDocDigitalSeleccionado.getTramiteEmbarqueDocDigitalSeleccionado();
		guardar(tramiteEmbarqueDocDigitalService);
	}
	
	public void cargarDocumentos() {
		try {
			if(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado()!=null && 
				tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId()!=null) {
				lTramiteEmbarquesDocDigital = tramiteEmbarqueDocDigitalService.obtenerPorPadre(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado());
				tramiteEmbarqueDesaduanamientoController.getDocumentos().clear();
				for (TramiteEmbarqueDocDigital d : lTramiteEmbarquesDocDigital){
					if (d.getNumero() != null)
						tramiteEmbarqueDesaduanamientoController.getDocumentos().add(d.getNumero().toString());
				}
			}
		} catch (Exception e) {
			addError(e);
		}
	}
	
	// GETS Y SETS
	
	public TramiteEmbarqueDocDigitalDataManager getTramiteEmbarqueDocDigitalSeleccionado() {
		return tramiteEmbarqueDocDigitalSeleccionado;
	}

	public void setTramiteEmbarqueDocDigitalSeleccionado(
			TramiteEmbarqueDocDigitalDataManager tramiteEmbarqueDocDigitalSeleccionado) {
		this.tramiteEmbarqueDocDigitalSeleccionado = tramiteEmbarqueDocDigitalSeleccionado;
	}

	public TramiteEmbarqueDataManager getTramiteEmbarqueSeleccionado() {
		return tramiteEmbarqueSeleccionado;
	}

	public void setTramiteEmbarqueSeleccionado(
			TramiteEmbarqueDataManager tramiteEmbarqueSeleccionado) {
		this.tramiteEmbarqueSeleccionado = tramiteEmbarqueSeleccionado;
	}

	public TramiteEmbarqueDocDigital getTramiteEmbarqueDocDigital() {
		return tramiteEmbarqueDocDigital;
	}

	public void setTramiteEmbarqueDocDigital(
			TramiteEmbarqueDocDigital tramiteEmbarqueDocDigital) {
		this.tramiteEmbarqueDocDigital = tramiteEmbarqueDocDigital;
	}

	public List<TramiteEmbarqueDocDigital> getlTramiteEmbarquesDocDigital() {
		if(lTramiteEmbarquesDocDigital.isEmpty())
			inicializarVariables();
		return lTramiteEmbarquesDocDigital;
	}

	public void setlTramiteEmbarquesDocDigital(
			List<TramiteEmbarqueDocDigital> lTramiteEmbarquesDocDigital) {
		this.lTramiteEmbarquesDocDigital = lTramiteEmbarquesDocDigital;
	}

	public TipoDocumentoService getTipoDocumentoService() {
		return tipoDocumentoService;
	}

	public void setTipoDocumentoService(TipoDocumentoService tipoDocumentoService) {
		this.tipoDocumentoService = tipoDocumentoService;
	}

	public List<TipoDocumento> getTipoDocumentos() {
		if(tipoDocumentos.isEmpty())
			tipoDocumentos = tipoDocumentoService.obtenerActivos();
		return tipoDocumentos;
	}

	public void setTipoDocumentos(List<TipoDocumento> tipoDocumentos) {
		this.tipoDocumentos = tipoDocumentos;
	}

	public TramiteEmbarqueDesaduanamientoController getTramiteEmbarqueDesaduanamientoController() {
		return tramiteEmbarqueDesaduanamientoController;
	}

	public void setTramiteEmbarqueDesaduanamientoController(
			TramiteEmbarqueDesaduanamientoController tramiteEmbarqueDesaduanamientoController) {
		this.tramiteEmbarqueDesaduanamientoController = tramiteEmbarqueDesaduanamientoController;
	}
	
}
