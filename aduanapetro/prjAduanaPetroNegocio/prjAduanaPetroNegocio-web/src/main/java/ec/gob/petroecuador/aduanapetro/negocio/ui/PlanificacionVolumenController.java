package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionVolumen;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PrecioPrevision;
import ec.gob.petroecuador.aduanapetro.negocio.services.PlanificacionVolumenService;
import ec.gob.petroecuador.aduanapetro.negocio.services.PrecioPrevisionService;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.PlanificacionVolumenDataManager;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("planificacionVolumenController")
@Scope("request")
public class PlanificacionVolumenController  extends RegistroComun<PrecioPrevision> {

	public PlanificacionVolumenController() {
		super("planificacionVolumenForm.xhtml", "planificacionVolumenTabla.xhtml");
	}

	@Autowired
	private PlanificacionVolumenService planificacionVolumenService; 
	
	@Autowired
	private PrecioPrevisionService precioPrevisionService;
	
	@Autowired
	private PlanificacionVolumenDataManager planificacionVolumenSeleccionado;
	
	private PlanificacionVolumen planificacionVolumen = new PlanificacionVolumen();
	private List<PlanificacionVolumen> lPlanificacionVolumens = new ArrayList<>();

	private List<PrecioPrevision> lPrecioPrevisions = new ArrayList<>();
	
	private BigDecimal pSeguro;
	

	// METODOS
	public void inicializarVariables() {
		//lPlanificacionVolumens = planificacionVolumenService.obtenerActivos();
		buscar();
	}

	public String editar() {
		planificacionVolumen = (PlanificacionVolumen)elementoSeleccionado;
		/*printObj(planificacionVolumen);
		planificacionVolumenSeleccionado.setSelectPrecioPrevision(planificacionVolumen.getPrecioPrevision());
		printObj(planificacionVolumenSeleccionado.getSelectPrecioPrevision());
		planificacionVolumenSeleccionado.setSeguro(planificacionVolumen.getPrecioPrevision().getParametroCalculo().getPorcentajeSeguro());*/
		return super.editar();
	}

	public String nuevoRegistro() {
		planificacionVolumenSeleccionado.nuevo();
		return nuevoRegistro(planificacionVolumenSeleccionado.getPlanificacionVolumenSeleccionado());
	}

	public String eliminar() {
		planificacionVolumen=(PlanificacionVolumen)elementoSeleccionado;
		return eliminar(planificacionVolumenService);
	}

	public String guardar() {
		planificacionVolumen=(PlanificacionVolumen)elementoSeleccionado;
		planificacionVolumen.setPrecioPrevision(planificacionVolumenSeleccionado.getPlanificacionVolumenSeleccionado().getPrecioPrevision());
		return guardar(planificacionVolumenService);
	}	
	
	public void cargarDatosParametroCalculo(){
		PrecioPrevision precioPrevision=null;
		planificacionVolumen=planificacionVolumenSeleccionado.refrescar();
		try {
			precioPrevision=precioPrevisionService.buscar(planificacionVolumenSeleccionado.getPlanificacionVolumenSeleccionado().getPrecioPrevision().getId());
			planificacionVolumen.setPrecioMarcador(precioPrevision.getParametroCalculo().getPrecioMarcador().getNombre());
			//Obtengo el seguro de la tabla T_PARAMETRO_CALCULO
			pSeguro=precioPrevision.getParametroCalculo().getPorcentajeSeguro();
			planificacionVolumenSeleccionado.setSeguro(pSeguro);
			///Obtengo el precio total del producto seleccionado de la Tabla T_PRECIO_PREVISION
			BigDecimal precioTotalPrevision=precioPrevision.getTotal();
			///El precio total lo pongo en los 12 meses
			planificacionVolumen.setGalonEnero(precioTotalPrevision);
			planificacionVolumen.setGalonFebrero(precioTotalPrevision);
			planificacionVolumen.setGalonMarzo(precioTotalPrevision);
			planificacionVolumen.setGalonAbril(precioTotalPrevision);
			planificacionVolumen.setGalonMayo(precioTotalPrevision);
			planificacionVolumen.setGalonJunio(precioTotalPrevision);
			planificacionVolumen.setGalonJulio(precioTotalPrevision);
			planificacionVolumen.setGalonAgosto(precioTotalPrevision);
			planificacionVolumen.setGalonSeptiembre(precioTotalPrevision);
			planificacionVolumen.setGalonOctubre(precioTotalPrevision);
			planificacionVolumen.setGalonNoviembre(precioTotalPrevision);
			planificacionVolumen.setGalonDiciembre(precioTotalPrevision);
			planificacionVolumenSeleccionado.setPlanificacionVolumenSeleccionado(planificacionVolumen);
			setElementoSeleccionado(planificacionVolumen);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		
	}

	
	///CALCULOS
	public void sumasTotales(){
		//Suma total barriles
		sumaTotalBarriles();
		//Suma total DAT
		sumaTotalDat();
		//Suma total seguro
		sumaTotalSeguro();
		//Suma total Cif
		sumaTotalCif();
		setElementoSeleccionado(planificacionVolumen);
	}
	public void calculoEnero(){
		planificacionVolumen=(PlanificacionVolumen)elementoSeleccionado;
		//Total DAT
		BigDecimal totalDatEnero=planificacionVolumen.getGalonEnero().multiply(planificacionVolumen.getBarrilEnero());
		planificacionVolumen.setTotalDatEnero(totalDatEnero);
		//Total Seguro
		BigDecimal porcentajeSeguro=planificacionVolumenSeleccionado.getSeguro().divide(BigDecimal.valueOf(100));
		BigDecimal seguroEnero=planificacionVolumen.getTotalDatEnero().multiply(porcentajeSeguro);
		planificacionVolumen.setSeguroEnero(seguroEnero);
		//Total CIF
		BigDecimal totalCifEnero=totalDatEnero.add(seguroEnero);
		planificacionVolumen.setTotalCifEnero(totalCifEnero);
		
		//Sumas totales 
		sumasTotales();
	}
	
	public void calculoFebrero(){
		planificacionVolumen=(PlanificacionVolumen)elementoSeleccionado;
		//Total DAT
		BigDecimal totalDatFebrero=planificacionVolumen.getGalonFebrero().multiply(planificacionVolumen.getBarrilFebrero());
		planificacionVolumen.setTotalDatFebrero(totalDatFebrero);
		//Total Seguro
		BigDecimal porcentajeSeguro=planificacionVolumenSeleccionado.getSeguro().divide(BigDecimal.valueOf(100));
		BigDecimal seguroFebrero=planificacionVolumen.getTotalDatFebrero().multiply(porcentajeSeguro);
		planificacionVolumen.setSeguroFebrero(seguroFebrero);
		//Total CIF
		BigDecimal totalCifFebrero=totalDatFebrero.add(seguroFebrero);
		planificacionVolumen.setTotalCifFebrero(totalCifFebrero);
				
		//Sumas totales 
		sumasTotales();
	}
	
	public void calculoMarzo(){
		planificacionVolumen=(PlanificacionVolumen)elementoSeleccionado;
		//Total DAT
		BigDecimal totalDatMarzo=planificacionVolumen.getGalonMarzo().multiply(planificacionVolumen.getBarrilMarzo());
		planificacionVolumen.setTotalDatMarzo(totalDatMarzo);
		//Total Seguro
		BigDecimal porcentajeSeguro=planificacionVolumenSeleccionado.getSeguro().divide(BigDecimal.valueOf(100));
		BigDecimal seguroMarzo=planificacionVolumen.getTotalDatMarzo().multiply(porcentajeSeguro);
		planificacionVolumen.setSeguroMarzo(seguroMarzo);
		//Total CIF
		BigDecimal totalCifMarzo=totalDatMarzo.add(seguroMarzo);
		planificacionVolumen.setTotalCifMarzo(totalCifMarzo);
						
		//Sumas totales 
		sumasTotales();
	}
	
	public void calculoAbril(){
		planificacionVolumen=(PlanificacionVolumen)elementoSeleccionado;
		//Total DAT
		BigDecimal totalDatAbril=planificacionVolumen.getGalonAbril().multiply(planificacionVolumen.getBarrilAbril());
		planificacionVolumen.setTotalDatAbril(totalDatAbril);
		//Total Seguro
		BigDecimal porcentajeSeguro=planificacionVolumenSeleccionado.getSeguro().divide(BigDecimal.valueOf(100));
		BigDecimal seguroAbril=planificacionVolumen.getTotalDatAbril().multiply(porcentajeSeguro);
		planificacionVolumen.setSeguroAbril(seguroAbril);
		//Total CIF
		BigDecimal totalCifAbril=totalDatAbril.add(seguroAbril);
		planificacionVolumen.setTotalCifAbril(totalCifAbril);
								
		//Sumas totales 
		sumasTotales();
	}
	public void calculoMayo(){
		planificacionVolumen=(PlanificacionVolumen)elementoSeleccionado;
		//Total DAT
		BigDecimal totalDatMayo=planificacionVolumen.getGalonMayo().multiply(planificacionVolumen.getBarrilMayo());
		planificacionVolumen.setTotalDatMayo(totalDatMayo);
		//Total Seguro
		BigDecimal porcentajeSeguro=planificacionVolumenSeleccionado.getSeguro().divide(BigDecimal.valueOf(100));
		BigDecimal seguroMayo=planificacionVolumen.getTotalDatMayo().multiply(porcentajeSeguro);
		planificacionVolumen.setSeguroMayo(seguroMayo);
		//Total CIF
		BigDecimal totalCifMayo=totalDatMayo.add(seguroMayo);
		planificacionVolumen.setTotalCifMayo(totalCifMayo);
										
		//Sumas totales 
		sumasTotales();
	}
	public void calculoJunio(){
		planificacionVolumen=(PlanificacionVolumen)elementoSeleccionado;
		//Total DAT
		BigDecimal totalDatJunio=planificacionVolumen.getGalonJunio().multiply(planificacionVolumen.getBarrilJunio());
		planificacionVolumen.setTotalDatJunio(totalDatJunio);
		//Total Seguro
		BigDecimal porcentajeSeguro=planificacionVolumenSeleccionado.getSeguro().divide(BigDecimal.valueOf(100));
		BigDecimal seguroJunio=planificacionVolumen.getTotalDatJunio().multiply(porcentajeSeguro);
		planificacionVolumen.setSeguroJunio(seguroJunio);
		//Total CIF
		BigDecimal totalCifJunio=totalDatJunio.add(seguroJunio);
		planificacionVolumen.setTotalCifJunio(totalCifJunio);
												
		//Sumas totales 
		sumasTotales();
	}
	
	public void calculoJulio(){
		planificacionVolumen=(PlanificacionVolumen)elementoSeleccionado;
		//Total DAT
		BigDecimal totalDatJulio=planificacionVolumen.getGalonJulio().multiply(planificacionVolumen.getBarrilJulio());
		planificacionVolumen.setTotalDatJulio(totalDatJulio);
		//Total Seguro
		BigDecimal porcentajeSeguro=planificacionVolumenSeleccionado.getSeguro().divide(BigDecimal.valueOf(100));
		BigDecimal seguroJulio=planificacionVolumen.getTotalDatJulio().multiply(porcentajeSeguro);
		planificacionVolumen.setSeguroJulio(seguroJulio);
		//Total CIF
		BigDecimal totalCifJulio=totalDatJulio.add(seguroJulio);
		planificacionVolumen.setTotalCifJulio(totalCifJulio);
														
		//Sumas totales 
		sumasTotales();
	}
	public void calculoAgosto(){
		planificacionVolumen=(PlanificacionVolumen)elementoSeleccionado;
		//Total DAT
		BigDecimal totalDatAgosto=planificacionVolumen.getGalonAgosto().multiply(planificacionVolumen.getBarrilAgosto());
		planificacionVolumen.setTotalDatAgosto(totalDatAgosto);
		//Total Seguro
		BigDecimal porcentajeSeguro=planificacionVolumenSeleccionado.getSeguro().divide(BigDecimal.valueOf(100));
		BigDecimal seguroAgosto=planificacionVolumen.getTotalDatAgosto().multiply(porcentajeSeguro);
		planificacionVolumen.setSeguroAgosto(seguroAgosto);
		//Total CIF
		BigDecimal totalCifAgosto=totalDatAgosto.add(seguroAgosto);
		planificacionVolumen.setTotalCifAgosto(totalCifAgosto);
																
		//Sumas totales 
		sumasTotales();
	}
	public void calculoSeptiembre(){
		planificacionVolumen=(PlanificacionVolumen)elementoSeleccionado;
		//Total DAT
		BigDecimal totalDatSeptiembre=planificacionVolumen.getGalonSeptiembre().multiply(planificacionVolumen.getBarrilSeptiembre());
		planificacionVolumen.setTotalDatSeptiembre(totalDatSeptiembre);
		//Total Seguro
		BigDecimal porcentajeSeguro=planificacionVolumenSeleccionado.getSeguro().divide(BigDecimal.valueOf(100));
		BigDecimal seguroSeptiembre=planificacionVolumen.getTotalDatSeptiembre().multiply(porcentajeSeguro);
		planificacionVolumen.setSeguroSeptiembre(seguroSeptiembre);
		//Total CIF
		BigDecimal totalCifSeptiembre=totalDatSeptiembre.add(seguroSeptiembre);
		planificacionVolumen.setTotalCifSeptiembre(totalCifSeptiembre);
																		
		//Sumas totales 
		sumasTotales();
	}
	public void calculoOctubre(){
		planificacionVolumen=(PlanificacionVolumen)elementoSeleccionado;
		//Total DAT
		BigDecimal totalDatOctubre=planificacionVolumen.getGalonOctubre().multiply(planificacionVolumen.getBarrilOctubre());
		planificacionVolumen.setTotalDatOctubre(totalDatOctubre);
		//Total Seguro
		BigDecimal porcentajeSeguro=planificacionVolumenSeleccionado.getSeguro().divide(BigDecimal.valueOf(100));
		BigDecimal seguroOctubre=planificacionVolumen.getTotalDatOctubre().multiply(porcentajeSeguro);
		planificacionVolumen.setSeguroOctubre(seguroOctubre);
		//Total CIF
		BigDecimal totalCifOctubre=totalDatOctubre.add(seguroOctubre);
		planificacionVolumen.setTotalCifOctubre(totalCifOctubre);
																				
		//Sumas totales 
		sumasTotales();
	}
	public void calculoNoviembre(){
		planificacionVolumen=(PlanificacionVolumen)elementoSeleccionado;
		//Total DAT
		BigDecimal totalDatNoviembre=planificacionVolumen.getGalonNoviembre().multiply(planificacionVolumen.getBarrilNoviembre());
		planificacionVolumen.setTotalDatNoviembre(totalDatNoviembre);
		//Total Seguro
		BigDecimal porcentajeSeguro=planificacionVolumenSeleccionado.getSeguro().divide(BigDecimal.valueOf(100));
		BigDecimal seguroNoviembre=planificacionVolumen.getTotalDatNoviembre().multiply(porcentajeSeguro);
		planificacionVolumen.setSeguroNoviembre(seguroNoviembre);
		//Total CIF
		BigDecimal totalCifNoviembre=totalDatNoviembre.add(seguroNoviembre);
		planificacionVolumen.setTotalCifNoviembre(totalCifNoviembre);
																						
		//Sumas totales 
		sumasTotales();
	}
	public void calculoDiciembre(){
		planificacionVolumen=(PlanificacionVolumen)elementoSeleccionado;
		//Total DAT
		BigDecimal totalDatDiciembre=planificacionVolumen.getGalonDiciembre().multiply(planificacionVolumen.getBarrilDiciembre());
		planificacionVolumen.setTotalDatDiciembre(totalDatDiciembre);
		//Total Seguro
		BigDecimal porcentajeSeguro=planificacionVolumenSeleccionado.getSeguro().divide(BigDecimal.valueOf(100));
		BigDecimal seguroDiciembre=planificacionVolumen.getTotalDatDiciembre().multiply(porcentajeSeguro);
		planificacionVolumen.setSeguroDiciembre(seguroDiciembre);
		//Total CIF
		BigDecimal totalCifDiciembre=totalDatDiciembre.add(seguroDiciembre);
		planificacionVolumen.setTotalCifDiciembre(totalCifDiciembre);
																								
		//Sumas totales 
		sumasTotales();
	}
	
	public void sumaTotalBarriles(){
		BigDecimal sumaTotal1=planificacionVolumen.getBarrilEnero().add(planificacionVolumen.getBarrilFebrero());
		BigDecimal sumaTotal2=planificacionVolumen.getBarrilMarzo().add(planificacionVolumen.getBarrilAbril());
		BigDecimal sumaTotal3=planificacionVolumen.getBarrilMayo().add(planificacionVolumen.getBarrilJunio());
		BigDecimal sumaTotal4=planificacionVolumen.getBarrilJulio().add(planificacionVolumen.getBarrilAgosto());
		BigDecimal sumaTotal5=planificacionVolumen.getBarrilSeptiembre().add(planificacionVolumen.getBarrilOctubre());
		BigDecimal sumaTotal6=planificacionVolumen.getBarrilNoviembre().add(planificacionVolumen.getBarrilDiciembre());
		BigDecimal sumaTotalBarril1=sumaTotal1.add(sumaTotal2).add(sumaTotal3);
		BigDecimal sumaTotalBarril2=sumaTotal4.add(sumaTotal5).add(sumaTotal6);
		BigDecimal sumaTotalBarril=sumaTotalBarril1.add(sumaTotalBarril2);
		planificacionVolumen.setTotalBarril(sumaTotalBarril);
	}
	
	public void sumaTotalDat(){
		BigDecimal sumaTotalD1=planificacionVolumen.getTotalDatEnero().add(planificacionVolumen.getTotalDatFebrero());
		BigDecimal sumaTotalD2=planificacionVolumen.getTotalDatMarzo().add(planificacionVolumen.getTotalDatAbril());
		BigDecimal sumaTotalD3=planificacionVolumen.getTotalDatMayo().add(planificacionVolumen.getTotalDatJunio());
		BigDecimal sumaTotalD4=planificacionVolumen.getTotalDatJulio().add(planificacionVolumen.getTotalDatAgosto());
		BigDecimal sumaTotalD5=planificacionVolumen.getTotalDatSeptiembre().add(planificacionVolumen.getTotalDatOctubre());
		BigDecimal sumaTotalD6=planificacionVolumen.getTotalDatNoviembre().add(planificacionVolumen.getTotalDatDiciembre());
		BigDecimal sumaTotalDat1=sumaTotalD1.add(sumaTotalD2).add(sumaTotalD3);
		BigDecimal sumaTotalDat2=sumaTotalD4.add(sumaTotalD5).add(sumaTotalD6);
		BigDecimal sumaTotalDat=sumaTotalDat1.add(sumaTotalDat2);
		planificacionVolumen.setTotalDat(sumaTotalDat);
	}
	
	public void sumaTotalSeguro(){
		BigDecimal sumaTotalS1=planificacionVolumen.getSeguroEnero().add(planificacionVolumen.getSeguroFebrero());
		BigDecimal sumaTotalS2=planificacionVolumen.getSeguroMarzo().add(planificacionVolumen.getSeguroAbril());
		BigDecimal sumaTotalS3=planificacionVolumen.getSeguroMayo().add(planificacionVolumen.getSeguroJunio());
		BigDecimal sumaTotalS4=planificacionVolumen.getSeguroJulio().add(planificacionVolumen.getSeguroAgosto());
		BigDecimal sumaTotalS5=planificacionVolumen.getSeguroSeptiembre().add(planificacionVolumen.getSeguroOctubre());
		BigDecimal sumaTotalS6=planificacionVolumen.getSeguroNoviembre().add(planificacionVolumen.getSeguroDiciembre());
		BigDecimal sumaTotalSeg1=sumaTotalS1.add(sumaTotalS2).add(sumaTotalS3);
		BigDecimal sumaTotalSeg2=sumaTotalS4.add(sumaTotalS5).add(sumaTotalS6);
		BigDecimal sumaTotalSeguro=sumaTotalSeg1.add(sumaTotalSeg2);
		planificacionVolumen.setTotalSeguro(sumaTotalSeguro);
	}
	
	public void sumaTotalCif(){
		BigDecimal sumaTotalC1=planificacionVolumen.getTotalCifEnero().add(planificacionVolumen.getTotalCifFebrero());
		BigDecimal sumaTotalC2=planificacionVolumen.getTotalCifMarzo().add(planificacionVolumen.getTotalCifAbril());
		BigDecimal sumaTotalC3=planificacionVolumen.getTotalCifMayo().add(planificacionVolumen.getTotalCifJunio());
		BigDecimal sumaTotalC4=planificacionVolumen.getTotalCifJulio().add(planificacionVolumen.getTotalCifAgosto());
		BigDecimal sumaTotalC5=planificacionVolumen.getTotalCifSeptiembre().add(planificacionVolumen.getTotalCifOctubre());
		BigDecimal sumaTotalC6=planificacionVolumen.getTotalCifNoviembre().add(planificacionVolumen.getTotalCifDiciembre());
		BigDecimal sumaTotalCif1=sumaTotalC1.add(sumaTotalC2).add(sumaTotalC3);
		BigDecimal sumaTotalCif2=sumaTotalC4.add(sumaTotalC5).add(sumaTotalC6);
		BigDecimal sumaTotalCif=sumaTotalCif1.add(sumaTotalCif2);
		planificacionVolumen.setTotalCif(sumaTotalCif);
		
		//Costo CIF
		planificacionVolumen.setCostoCif(sumaTotalCif);
		//Gastos 1%
		BigDecimal gastoT=planificacionVolumen.getCostoCif().divide(BigDecimal.valueOf(100));
		planificacionVolumen.setGasto(gastoT);
		//Costo Total
		BigDecimal costoTotal=planificacionVolumen.getCostoCif().add(planificacionVolumen.getGasto());
		planificacionVolumen.setCostoTotal(costoTotal);
	}

	public void buscar() {
		try {
			
			List<String> criterios = new ArrayList<String>();
			
			if (planificacionVolumenSeleccionado.getFiltroAnio() != null)
				if (!planificacionVolumenSeleccionado.getFiltroAnio().equals(""))
					criterios.add("anio ="+planificacionVolumenSeleccionado.getFiltroAnio());
			if (planificacionVolumenSeleccionado.getFiltroProducto()  != null)
				if(!planificacionVolumenSeleccionado.getFiltroProducto().equals(""))
					criterios.add("precioPrevision.parametroCalculo.CatalogoProducto.id ="+planificacionVolumenSeleccionado.getFiltroProducto().toString());
			criterios.add("id > 0");
			
			lPlanificacionVolumens = planificacionVolumenService.buscarPorCriterios(criterios.toArray(new String[0]));
			System.out.println("-> PararametrosCalculo encontrados: "+lPlanificacionVolumens.size());
			if(lPlanificacionVolumens.size()==0) {
				lPlanificacionVolumens.clear();
				addMessage("No se encontraron resultados con los filtros de busqueda seleccionados");
			}	
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// Getters and setters

	public PlanificacionVolumen getPlanificacionVolumen() {
		return planificacionVolumen;
	}

	public void setPlanificacionVolumen(PlanificacionVolumen planificacionVolumen) {
		this.planificacionVolumen = planificacionVolumen;
	}

	public List<PlanificacionVolumen> getlPlanificacionVolumens() {
		if(lPlanificacionVolumens.isEmpty())
			inicializarVariables();
		return lPlanificacionVolumens;
	}

	public void setlPlanificacionVolumens(
			List<PlanificacionVolumen> lPlanificacionVolumens) {
		this.lPlanificacionVolumens = lPlanificacionVolumens;
	}

	public List<PrecioPrevision> getlPrecioPrevisions() {
		if(lPrecioPrevisions.isEmpty())
			lPrecioPrevisions = precioPrevisionService.obtenerActivos();
		return lPrecioPrevisions;
	}

	public void setlPrecioPrevisions(List<PrecioPrevision> lPrecioPrevisions) {
		this.lPrecioPrevisions = lPrecioPrevisions;
	}

	public BigDecimal getpSeguro() {
		return pSeguro;
	}

	public void setpSeguro(BigDecimal pSeguro) {
		this.pSeguro = pSeguro;
	}

	public PlanificacionVolumenDataManager getPlanificacionVolumenSeleccionado() {
		return planificacionVolumenSeleccionado;
	}

	public void setPlanificacionVolumenSeleccionado(
			PlanificacionVolumenDataManager planificacionVolumenSeleccionado) {
		this.planificacionVolumenSeleccionado = planificacionVolumenSeleccionado;
	}
	
}
