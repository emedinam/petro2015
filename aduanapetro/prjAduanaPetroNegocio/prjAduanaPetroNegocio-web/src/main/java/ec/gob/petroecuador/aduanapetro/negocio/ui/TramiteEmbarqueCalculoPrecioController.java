package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueCalculoPrecio;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueCalculoPrecioService;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueCalculoPrecioDataManager;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("tramiteEmbarqueCalculoPrecioController")
@Scope("request")
public class TramiteEmbarqueCalculoPrecioController  extends RegistroComun<TramiteEmbarqueCalculoPrecio> {

	public TramiteEmbarqueCalculoPrecioController() {
		super("tramiteEmbarqueCalculoPrecioConvenioForm.xhtml", "tramiteEmbarqueCalculoPrecioConvenioTabla.xhtml");
	}

	@Autowired
	private TramiteEmbarqueCalculoPrecioService tramiteEmbarqueCalculoPrecioService;
	
	@Autowired
	private TramiteEmbarqueCalculoPrecioDataManager tramiteEmbarqueCalculoPrecioSeleccionado;

	private TramiteEmbarqueCalculoPrecio tramiteEmbarqueCalculoPrecio = new TramiteEmbarqueCalculoPrecio();
	private List<TramiteEmbarqueCalculoPrecio> ltramiteEmbarqueCalculoPrecios = new ArrayList<>();
	
	// METODOS
	public void inicializarVariables() {
		try {
			tramiteEmbarqueCalculoPrecio=tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado();
			ltramiteEmbarqueCalculoPrecios=tramiteEmbarqueCalculoPrecioService.obtenerActivos();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String nuevoRegistro() {
		return nuevoRegistro(tramiteEmbarqueCalculoPrecioSeleccionado.nuevo());
	}

	public String eliminar() {
		elementoSeleccionado = tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado();
		return eliminar(tramiteEmbarqueCalculoPrecioService);
	}

	public String guardar() {
		elementoSeleccionado = tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado();
		return guardar(tramiteEmbarqueCalculoPrecioService);
	}

	public void buscar() {
		
	}
	
	//VISUALIZACION DE FECHAS
	public void mostrarCamposActivosFechas(){
		//switch (tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().getVolumenPuertoArena().intValue()) {
	    int opcion=5;
		switch(opcion){
		case 1:
	           tramiteEmbarqueCalculoPrecioSeleccionado.setMostrarFecha1(false);
	           break;
	      case 2:
	    	  tramiteEmbarqueCalculoPrecioSeleccionado.setMostrarFecha2(false);
	           break;
	      case 3:
	    	  tramiteEmbarqueCalculoPrecioSeleccionado.setMostrarFecha3(false);
	           break;
	      case 4:
	    	  tramiteEmbarqueCalculoPrecioSeleccionado.setMostrarFecha4(false);
	           break;
	      case 5:
	    	  tramiteEmbarqueCalculoPrecioSeleccionado.setMostrarFecha5(false);
	           break;
	      case 6:
	    	  tramiteEmbarqueCalculoPrecioSeleccionado.setMostrarFecha6(false);
	           break;
	      case 7:
	    	  tramiteEmbarqueCalculoPrecioSeleccionado.setMostrarFecha7(false);
	           break;
	      case 8:
	    	  tramiteEmbarqueCalculoPrecioSeleccionado.setMostrarFecha8(false);
	           break;
	      case 9:
	    	  tramiteEmbarqueCalculoPrecioSeleccionado.setMostrarFecha9(false);
	           break;
	      case 10:
	    	  tramiteEmbarqueCalculoPrecioSeleccionado.setMostrarFecha10(false);
	           break;
	      case 11:
	    	  tramiteEmbarqueCalculoPrecioSeleccionado.setMostrarFecha11(false);
	           break;     
	      default:
	           System.out.println("error" );
	           break;
	      }
	}
	
	//CALCULOS
	public void conversionADolaresPorBarril(){
		//CONVERSION GALONES A BARRILES
		tramiteEmbarqueCalculoPrecio = tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado();
		BigDecimal precioBarril1=tramiteEmbarqueCalculoPrecio.getPrecioGalonUno().multiply(BigDecimal.valueOf(0.42));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecioBarrilUno(precioBarril1);
		
		BigDecimal precioBarril2=tramiteEmbarqueCalculoPrecio.getPrecioGalonDos().multiply(BigDecimal.valueOf(0.42));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecioBarrilDos(precioBarril2);
		
		BigDecimal precioBarril3=tramiteEmbarqueCalculoPrecio.getPrecioGalonTres().multiply(BigDecimal.valueOf(0.42));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecioBarrilTres(precioBarril3);
		
		BigDecimal precioBarril4=tramiteEmbarqueCalculoPrecio.getPrecioGalonCuatro().multiply(BigDecimal.valueOf(0.42));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecioBarrilCuatro(precioBarril4);
		
		BigDecimal precioBarril5=tramiteEmbarqueCalculoPrecio.getPrecioGalonCinco().multiply(BigDecimal.valueOf(0.42));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecioBarrilCinco(precioBarril5);
		
		BigDecimal precioBarril6=tramiteEmbarqueCalculoPrecio.getPrecioGalonSeis().multiply(BigDecimal.valueOf(0.42));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecioBarrilSeis(precioBarril6);
		
		BigDecimal precioBarril7=tramiteEmbarqueCalculoPrecio.getPrecioGalonSiete().multiply(BigDecimal.valueOf(0.42));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecioBarrilSiete(precioBarril7);
		
		BigDecimal precioBarril8=tramiteEmbarqueCalculoPrecio.getPrecioGalonOcho().multiply(BigDecimal.valueOf(0.42));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecioBarrilOcho(precioBarril8);
		
		BigDecimal precioBarrilB=tramiteEmbarqueCalculoPrecio.getPrecioGalonBl().multiply(BigDecimal.valueOf(0.42));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecioBarrilBl(precioBarrilB);
		
		BigDecimal precioBarril9=tramiteEmbarqueCalculoPrecio.getPrecioGalonNueve().multiply(BigDecimal.valueOf(0.42));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecioBarrilNueve(precioBarril9);
		
		BigDecimal precioBarrilD=tramiteEmbarqueCalculoPrecio.getPrecioGalonDiez().multiply(BigDecimal.valueOf(0.42));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecioBarrilDiez(precioBarrilD);
		
		BigDecimal precioBarrilO=tramiteEmbarqueCalculoPrecio.getPrecioGalonOnce().multiply(BigDecimal.valueOf(0.42));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecioBarrilOnce(precioBarrilO);
		
		BigDecimal precioBarrilDC=tramiteEmbarqueCalculoPrecio.getPrecioGalonDoce().multiply(BigDecimal.valueOf(0.42));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecioBarrilDoce(precioBarrilDC);
		
		BigDecimal precioBarrilT=tramiteEmbarqueCalculoPrecio.getPrecioGalonTrece().multiply(BigDecimal.valueOf(0.42));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecioBarrilTrece(precioBarrilT);
		
		BigDecimal precioBarrilC=tramiteEmbarqueCalculoPrecio.getPrecioGalonCatorce().multiply(BigDecimal.valueOf(0.42));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecioBarrilCatorce(precioBarrilC);
		
		BigDecimal precioBarrilQ=tramiteEmbarqueCalculoPrecio.getPrecioGalonQuince().multiply(BigDecimal.valueOf(0.42));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecioBarrilQuince(precioBarrilQ);
		
		BigDecimal precioBarrilDI=tramiteEmbarqueCalculoPrecio.getPrecioGalonDieciseis().multiply(BigDecimal.valueOf(0.42));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecioBarrilDieciseis(precioBarrilDI);
		
		BigDecimal precioBarrilDIE=tramiteEmbarqueCalculoPrecio.getPrecioGalonDiecisiete().multiply(BigDecimal.valueOf(0.42));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecioBarrilDiecisiete(precioBarrilDIE);
		
		BigDecimal suma1=tramiteEmbarqueCalculoPrecio.getPrecioGalonUno().add(tramiteEmbarqueCalculoPrecio.getPrecioGalonDos()).add(tramiteEmbarqueCalculoPrecio.getPrecioGalonTres()).add(tramiteEmbarqueCalculoPrecio.getPrecioGalonCuatro()).add(tramiteEmbarqueCalculoPrecio.getPrecioGalonCinco());
		BigDecimal suma2=tramiteEmbarqueCalculoPrecio.getPrecioGalonSeis().add(tramiteEmbarqueCalculoPrecio.getPrecioGalonSiete()).add(tramiteEmbarqueCalculoPrecio.getPrecioGalonOcho()).add(tramiteEmbarqueCalculoPrecio.getPrecioGalonBl());
		BigDecimal suma3=tramiteEmbarqueCalculoPrecio.getPrecioGalonNueve().add(tramiteEmbarqueCalculoPrecio.getPrecioGalonDiez()).add(tramiteEmbarqueCalculoPrecio.getPrecioGalonOnce()).add(tramiteEmbarqueCalculoPrecio.getPrecioGalonDoce());
		BigDecimal suma4=tramiteEmbarqueCalculoPrecio.getPrecioGalonTrece().add(tramiteEmbarqueCalculoPrecio.getPrecioGalonCatorce()).add(tramiteEmbarqueCalculoPrecio.getPrecioGalonQuince()).add(tramiteEmbarqueCalculoPrecio.getPrecioGalonDieciseis()).add(tramiteEmbarqueCalculoPrecio.getPrecioGalonDiecisiete());
		
		BigDecimal sumaGalon=suma1.add(suma2).add(suma3).add(suma4);
		
		//PROMEDIO GALON
		BigDecimal promedioGalon=sumaGalon.divide(BigDecimal.valueOf(5));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPromedioGalon(promedioGalon);
		
		//PROMEDIO BARRIL
		BigDecimal promedioBarril=promedioGalon.multiply(BigDecimal.valueOf(0.42));
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPromedioBarril(promedioBarril);
		
		//PRECIO (B) (PROMEDIO BARRIL + PREMIO + LIBERTAD)
		
		BigDecimal precioB=promedioBarril.add(tramiteEmbarqueCalculoPrecio.getPremio()).add(tramiteEmbarqueCalculoPrecio.getLibertad()).setScale(4, BigDecimal.ROUND_HALF_UP);
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecioPuerto(precioB);

		//PRECIO (C) (PUNTA ARENAS * PRECIO)
		BigDecimal precioC=tramiteEmbarqueCalculoPrecio.getVolumenPuertoArena().multiply(tramiteEmbarqueCalculoPrecio.getPrecioUno());
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setPrecio(precioC);
		
		//SUBTOTAL (D) (A (Volumen en Barriles) * B)
		BigDecimal subtotal=tramiteEmbarqueCalculoPrecio.getVolumenDescargadoPuerto().multiply(precioB);
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setSubtotal(subtotal);
		
		//VALOR TOTAL (D + C)
		BigDecimal valorTotal=subtotal.add(precioC);
		tramiteEmbarqueCalculoPrecioSeleccionado.getTramiteEmbarqueCalculoPrecioSeleccionado().setTotal(valorTotal);
	}
	
	// Getters and setters
	public TramiteEmbarqueCalculoPrecio getTramiteEmbarqueCalculoPrecio() {
		return tramiteEmbarqueCalculoPrecio;
	}

	public TramiteEmbarqueCalculoPrecioDataManager getTramiteEmbarqueCalculoPrecioSeleccionado() {
		return tramiteEmbarqueCalculoPrecioSeleccionado;
	}

	public void setTramiteEmbarqueCalculoPrecioSeleccionado(
			TramiteEmbarqueCalculoPrecioDataManager tramiteEmbarqueCalculoPrecioSeleccionado) {
		this.tramiteEmbarqueCalculoPrecioSeleccionado = tramiteEmbarqueCalculoPrecioSeleccionado;
	}

	public void setTramiteEmbarqueCalculoPrecio(
			TramiteEmbarqueCalculoPrecio tramiteEmbarqueCalculoPrecio) {
		this.tramiteEmbarqueCalculoPrecio = tramiteEmbarqueCalculoPrecio;
	}

	public List<TramiteEmbarqueCalculoPrecio> getLtramiteEmbarqueCalculoPrecios() {
		if(ltramiteEmbarqueCalculoPrecios.isEmpty())
			inicializarVariables();
		return ltramiteEmbarqueCalculoPrecios;
	}

	public void setLtramiteEmbarqueCalculoPrecios(
			List<TramiteEmbarqueCalculoPrecio> ltramiteEmbarqueCalculoPrecios) {
		this.ltramiteEmbarqueCalculoPrecios = ltramiteEmbarqueCalculoPrecios;
	}
}