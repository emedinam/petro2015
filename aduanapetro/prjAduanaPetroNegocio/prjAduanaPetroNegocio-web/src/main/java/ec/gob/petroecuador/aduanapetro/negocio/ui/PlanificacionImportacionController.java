package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionImportacion;
import ec.gob.petroecuador.aduanapetro.negocio.services.PlanificacionImportacionService;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.AdjudicacionDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.PlanificacionImportacionDataManager;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("planificacionImportacionController")
@Scope("request")
public class PlanificacionImportacionController extends RegistroComun<PlanificacionImportacion> {

	public PlanificacionImportacionController() {
		super("adjudicacionForm.xhtml", "adjudicacionTabla.xhtml");
	}

	@Autowired
	private PlanificacionImportacionService planificacionImportacionService;
	
	@Autowired
	private PlanificacionImportacionDataManager planificacionImportacionSeleccionado;

	@Autowired
	private AdjudicacionDataManager adjudicacionSeleccionado;

	private PlanificacionImportacion planificacionImportacion = new PlanificacionImportacion();
	private List<PlanificacionImportacion> lPlanificacionImportaciones = new ArrayList<>();

	// METODOS
	public void inicializarVariables() {
		planificacionImportacion = planificacionImportacionSeleccionado
				.getPlanificacionImportacionSeleccionado();
		System.out.println("-> ADJUDICACION IMPORTACION "+adjudicacionSeleccionado.getAdjudicacionSeleccionado().getId());
		if (adjudicacionSeleccionado.getAdjudicacionSeleccionado().getId() != null)
			try {
				lPlanificacionImportaciones = planificacionImportacionService.obtenerPorPadre(adjudicacionSeleccionado.getAdjudicacionSeleccionado());
			} catch (Exception e) {
				e.printStackTrace();
			}
		else
			lPlanificacionImportaciones = new ArrayList<>();
	}

	public void setAdjudicacionSeleccionado(
			AdjudicacionDataManager adjudicacionSeleccionado) {
		this.adjudicacionSeleccionado = adjudicacionSeleccionado;
	}

	public void nuevoRegistro() {
		nuevoRegistro(planificacionImportacionSeleccionado.nuevo(adjudicacionSeleccionado.getAdjudicacionSeleccionado()));
	}

	public void eliminar() {
		elementoSeleccionado = planificacionImportacionSeleccionado.getPlanificacionImportacionSeleccionado();
		eliminar(planificacionImportacionService);
	}

	public void guardar() {
		elementoSeleccionado = planificacionImportacionSeleccionado.getPlanificacionImportacionSeleccionado();
		planificacionImportacion = (PlanificacionImportacion)elementoSeleccionado;
		planificacionImportacion.setAdjudicacion( adjudicacionSeleccionado.getAdjudicacionSeleccionado() );
		planificacionImportacion.setActor( adjudicacionSeleccionado.getAdjudicacionSeleccionado().getActor() );
		guardar(planificacionImportacionService);
	}

	public void buscar() {
		try {
			if (planificacionImportacionSeleccionado.getFiltroDescripcion()==null || planificacionImportacionSeleccionado.getFiltroDescripcion().isEmpty()){
				addMessage("Ingrese un termino de busqueda");
			}else {
				String[] criterios = {"descripcion like '%"+planificacionImportacionSeleccionado.getFiltroDescripcion()+"%'"};
				lPlanificacionImportaciones = planificacionImportacionService.buscarPorCriterios(criterios);
			}				
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Getters and setters

	public PlanificacionImportacionDataManager getPlanificacionImportacionSeleccionado() {
		return planificacionImportacionSeleccionado;
	}

	public void setPlanificacionImportacionSeleccionado(
			PlanificacionImportacionDataManager planificacionImportacionSeleccionado) {
		this.planificacionImportacionSeleccionado = planificacionImportacionSeleccionado;
	}

	public PlanificacionImportacion getPlanificacionImportacion() {
		return planificacionImportacion;
	}

	public void setPlanificacionImportacion(
			PlanificacionImportacion planificacionImportacion) {
		this.planificacionImportacion = planificacionImportacion;
	}

	public List<PlanificacionImportacion> getlPlanificacionImportaciones() {
		if(lPlanificacionImportaciones.isEmpty())
			inicializarVariables();
		return lPlanificacionImportaciones;
	}

	public void setlPlanificacionImportaciones(
			List<PlanificacionImportacion> lPlanificacionImportaciones) {
		this.lPlanificacionImportaciones = lPlanificacionImportaciones;
	}

}
