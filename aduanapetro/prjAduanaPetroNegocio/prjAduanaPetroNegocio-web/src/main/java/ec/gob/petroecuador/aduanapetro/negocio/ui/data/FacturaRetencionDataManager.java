package ec.gob.petroecuador.aduanapetro.negocio.ui.data;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.FacturaRetencion;

@Component("facturaRetencionDataManager")
@Scope("session")
public class FacturaRetencionDataManager {
	
	private FacturaRetencion facturaRetencionSeleccionado = new FacturaRetencion();

	public FacturaRetencion getFacturaRetencionSeleccionado() {
		return facturaRetencionSeleccionado;
	}

	public void setFacturaRetencionSeleccionado(
			FacturaRetencion facturaRetencionSeleccionado) {
		this.facturaRetencionSeleccionado = facturaRetencionSeleccionado;
	}		
	
}

