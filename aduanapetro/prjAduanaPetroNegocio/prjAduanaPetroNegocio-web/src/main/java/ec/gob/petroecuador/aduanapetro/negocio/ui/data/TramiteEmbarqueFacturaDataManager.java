package ec.gob.petroecuador.aduanapetro.negocio.ui.data;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueFactura;

@Component("tramiteEmbarqueFacturaDataManager")
@Scope("session")
public class TramiteEmbarqueFacturaDataManager {
	
	private TramiteEmbarqueFactura tramiteEmbarqueFacturaSeleccionado;

	TramiteEmbarqueFacturaDataManager() {
		tramiteEmbarqueFacturaSeleccionado = new TramiteEmbarqueFactura();
	}
	
	public TramiteEmbarqueFactura getTramiteEmbarqueFacturaSeleccionado() {
		return tramiteEmbarqueFacturaSeleccionado;
	}

	public void setTramiteEmbarqueFacturaSeleccionado(
			TramiteEmbarqueFactura tramiteEmbarqueFacturaSeleccionado) {
		this.tramiteEmbarqueFacturaSeleccionado = tramiteEmbarqueFacturaSeleccionado;
	}
	
}

