package ec.gob.petroecuador.aduanapetro.negocio.ui.data;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueModPago;

@Component("tramiteEmbarqueModPagoDataManager")
@Scope("session")
public class TramiteEmbarqueModPagoDataManager {
	
	private TramiteEmbarqueModPago tramiteEmbarqueModPagoSeleccionado;
	
	private String proveedor;
	
	TramiteEmbarqueModPagoDataManager() {
		tramiteEmbarqueModPagoSeleccionado = new TramiteEmbarqueModPago();
	}
	
	public TramiteEmbarqueModPago getTramiteEmbarqueModPagoSeleccionado() {
		return tramiteEmbarqueModPagoSeleccionado;
	}
	public void setTramiteEmbarqueModPagoSeleccionado(
			TramiteEmbarqueModPago tramiteEmbarqueModPagoSeleccionado) {
		this.tramiteEmbarqueModPagoSeleccionado = tramiteEmbarqueModPagoSeleccionado;
	}
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	
}

