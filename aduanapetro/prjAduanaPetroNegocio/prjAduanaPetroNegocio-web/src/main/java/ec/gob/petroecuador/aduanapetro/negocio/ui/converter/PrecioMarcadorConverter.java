package ec.gob.petroecuador.aduanapetro.negocio.ui.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.PrecioMarcador;
import ec.gob.petroecuador.aduanapetro.negocio.services.PrecioMarcadorService;

@Component("precioMarcadorConverter")
public class PrecioMarcadorConverter implements Converter  {

	@Autowired
	private PrecioMarcadorService PrecioMarcadorService;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value == null || value.length() == 0) {
			return null;
		}
		Long id = Long.parseLong(value);
		return PrecioMarcadorService.buscar(id);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		return value instanceof PrecioMarcador ? ((PrecioMarcador) value).getId().toString() : "";
	}

}