package ec.gob.petroecuador.aduanapetro.negocio.ui.data;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDocDigital;

@Component("tramiteEmbarqueDocDigitalDataManager")
@Scope("session")
public class TramiteEmbarqueDocDigitalDataManager {
	
	private TramiteEmbarqueDocDigital tramiteEmbarqueDocDigitalSeleccionado;

	TramiteEmbarqueDocDigitalDataManager() {
		tramiteEmbarqueDocDigitalSeleccionado = new TramiteEmbarqueDocDigital();
	}
	
	public TramiteEmbarqueDocDigital getTramiteEmbarqueDocDigitalSeleccionado() {
		return tramiteEmbarqueDocDigitalSeleccionado;
	}

	public void setTramiteEmbarqueDocDigitalSeleccionado(
			TramiteEmbarqueDocDigital tramiteEmbarqueDocDigitalSeleccionado) {
		this.tramiteEmbarqueDocDigitalSeleccionado = tramiteEmbarqueDocDigitalSeleccionado;
	}
	
}

