package ec.gob.petroecuador.aduanapetro.negocio.ui.data;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.DocumentoAdjunto;

@Component("documentoAdjuntoDataManager")
@Scope("session")
public class DocumentoAdjuntoDataManager {

	private DocumentoAdjunto documentoAdjuntoSeleccionado = new DocumentoAdjunto();
	private String filtroDescripcion;
	public DocumentoAdjunto getDocumentoAdjuntoSeleccionado() {
		return documentoAdjuntoSeleccionado;
	}
	public void setDocumentoAdjuntoSeleccionado(
			DocumentoAdjunto documentoAdjuntoSeleccionado) {
		this.documentoAdjuntoSeleccionado = documentoAdjuntoSeleccionado;
	}
	public String getFiltroDescripcion() {
		return filtroDescripcion;
	}
	public void setFiltroDescripcion(String filtroDescripcion) {
		this.filtroDescripcion = filtroDescripcion;
	}
}
