package ec.gob.petroecuador.aduanapetro.negocio.ui.data;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Producto;

@Component("productoDataManager")
@Scope("session")
public class ProductoDataManager {
	
	private Producto productoSeleccionado = new Producto();

	public Producto getProductoSeleccionado() {
		return productoSeleccionado;
	}

	public void setProductoSeleccionado(Producto productoSeleccionado) {
		this.productoSeleccionado = productoSeleccionado;
	}	
	
}

