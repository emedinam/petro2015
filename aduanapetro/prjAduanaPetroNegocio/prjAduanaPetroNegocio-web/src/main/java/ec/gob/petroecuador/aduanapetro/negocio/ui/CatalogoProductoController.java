package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.CatalogoProducto;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.services.CatalogoProductoService;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("catalogoProductoController")
@Scope("request")
public class CatalogoProductoController extends RegistroComun<CatalogoProducto> {

	public CatalogoProductoController() {
		super("catalogoProductoForm.xhtml", "catalogoProductoTabla.xhtml");
	}
	
	@Autowired
	private CatalogoProductoService catalogoProductoService;	
	private CatalogoProducto catalogoProducto = new CatalogoProducto();
	private List<CatalogoProducto> lCatalogoProductos = new ArrayList<>();
	
	public void inicializarVariables() {	
		try {
			lCatalogoProductos = catalogoProductoService.obtenerActivos();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public TramiteEmbarque getTramiteembarqueSeleccionado() {
		return (TramiteEmbarque)this.getElemento(TramiteEmbarque.class.getSimpleName());
	}
	
	public String nuevoRegistro(){
		catalogoProducto = new CatalogoProducto();
		return nuevoRegistro(catalogoProducto);
	}
	
	public void nada() {
		System.out.println("NADA");
	}

	public String eliminar() {
		catalogoProducto = (CatalogoProducto)elementoSeleccionado;
		return eliminar(catalogoProductoService);
	}

	public String guardar(){
		return guardar(catalogoProductoService);
	}
	
	public CatalogoProducto getCatalogoProducto() {
		return catalogoProducto;
	}
	public void setCatalogoProducto(CatalogoProducto catalogoProducto) {
		this.catalogoProducto = catalogoProducto;
	}
	public List<CatalogoProducto> getlCatalogoProductos() {
		if(lCatalogoProductos.isEmpty())
			lCatalogoProductos = catalogoProductoService.obtenerActivos();
		return lCatalogoProductos;
	}
	public void setlCatalogoProductos(List<CatalogoProducto> lCatalogoProductos) {
		this.lCatalogoProductos = lCatalogoProductos;
	}
}
