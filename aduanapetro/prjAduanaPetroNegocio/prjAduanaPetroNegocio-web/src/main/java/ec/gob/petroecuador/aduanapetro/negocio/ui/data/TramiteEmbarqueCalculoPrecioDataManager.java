package ec.gob.petroecuador.aduanapetro.negocio.ui.data;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueCalculoPrecio;

@Component("tramiteEmbarqueCalculoPrecioDataManager")
@Scope("session")
public class TramiteEmbarqueCalculoPrecioDataManager {
	
	private TramiteEmbarqueCalculoPrecio tramiteEmbarqueCalculoPrecioSeleccionado;
	
	private Boolean mostrarFecha1=true;
	private Boolean mostrarFecha2=true;
	private Boolean mostrarFecha3=true;
	private Boolean mostrarFecha4=true;
	private Boolean mostrarFecha5=true;
	private Boolean mostrarFecha6=true;
	private Boolean mostrarFecha7=true;
	private Boolean mostrarFecha8=true;
	private Boolean mostrarFecha9=true;
	private Boolean mostrarFecha10=true;
	private Boolean mostrarFecha11=true;
	private Boolean mostrarFecha12=true;
	private Boolean mostrarFecha13=true;
	private Boolean mostrarFecha14=true;
	private Boolean mostrarFecha15=true;
	private Boolean mostrarFecha16=true;
	private Boolean mostrarFecha17=true;
	private Boolean mostrarFecha18=true;
	
	TramiteEmbarqueCalculoPrecioDataManager() {
		tramiteEmbarqueCalculoPrecioSeleccionado = new TramiteEmbarqueCalculoPrecio();
	}

	public TramiteEmbarqueCalculoPrecio getTramiteEmbarqueCalculoPrecioSeleccionado() {
		return tramiteEmbarqueCalculoPrecioSeleccionado;
	}

	public void setTramiteEmbarqueCalculoPrecioSeleccionado(
			TramiteEmbarqueCalculoPrecio tramiteEmbarqueCalculoPrecioSeleccionado) {
		this.tramiteEmbarqueCalculoPrecioSeleccionado = tramiteEmbarqueCalculoPrecioSeleccionado;
	}

	public Boolean getMostrarFecha1() {
		return mostrarFecha1;
	}

	public void setMostrarFecha1(Boolean mostrarFecha1) {
		this.mostrarFecha1 = mostrarFecha1;
	}

	public Boolean getMostrarFecha2() {
		return mostrarFecha2;
	}

	public void setMostrarFecha2(Boolean mostrarFecha2) {
		this.mostrarFecha2 = mostrarFecha2;
	}

	public Boolean getMostrarFecha3() {
		return mostrarFecha3;
	}

	public void setMostrarFecha3(Boolean mostrarFecha3) {
		this.mostrarFecha3 = mostrarFecha3;
	}

	public Boolean getMostrarFecha4() {
		return mostrarFecha4;
	}

	public void setMostrarFecha4(Boolean mostrarFecha4) {
		this.mostrarFecha4 = mostrarFecha4;
	}

	public Boolean getMostrarFecha5() {
		return mostrarFecha5;
	}

	public void setMostrarFecha5(Boolean mostrarFecha5) {
		this.mostrarFecha5 = mostrarFecha5;
	}

	public Boolean getMostrarFecha6() {
		return mostrarFecha6;
	}

	public void setMostrarFecha6(Boolean mostrarFecha6) {
		this.mostrarFecha6 = mostrarFecha6;
	}

	public Boolean getMostrarFecha7() {
		return mostrarFecha7;
	}

	public void setMostrarFecha7(Boolean mostrarFecha7) {
		this.mostrarFecha7 = mostrarFecha7;
	}

	public Boolean getMostrarFecha8() {
		return mostrarFecha8;
	}

	public void setMostrarFecha8(Boolean mostrarFecha8) {
		this.mostrarFecha8 = mostrarFecha8;
	}

	public Boolean getMostrarFecha9() {
		return mostrarFecha9;
	}

	public void setMostrarFecha9(Boolean mostrarFecha9) {
		this.mostrarFecha9 = mostrarFecha9;
	}

	public Boolean getMostrarFecha10() {
		return mostrarFecha10;
	}

	public void setMostrarFecha10(Boolean mostrarFecha10) {
		this.mostrarFecha10 = mostrarFecha10;
	}

	public Boolean getMostrarFecha11() {
		return mostrarFecha11;
	}

	public void setMostrarFecha11(Boolean mostrarFecha11) {
		this.mostrarFecha11 = mostrarFecha11;
	}

	public Boolean getMostrarFecha12() {
		return mostrarFecha12;
	}

	public void setMostrarFecha12(Boolean mostrarFecha12) {
		this.mostrarFecha12 = mostrarFecha12;
	}

	public Boolean getMostrarFecha13() {
		return mostrarFecha13;
	}

	public void setMostrarFecha13(Boolean mostrarFecha13) {
		this.mostrarFecha13 = mostrarFecha13;
	}

	public Boolean getMostrarFecha14() {
		return mostrarFecha14;
	}

	public void setMostrarFecha14(Boolean mostrarFecha14) {
		this.mostrarFecha14 = mostrarFecha14;
	}

	public Boolean getMostrarFecha15() {
		return mostrarFecha15;
	}

	public void setMostrarFecha15(Boolean mostrarFecha15) {
		this.mostrarFecha15 = mostrarFecha15;
	}

	public Boolean getMostrarFecha16() {
		return mostrarFecha16;
	}

	public void setMostrarFecha16(Boolean mostrarFecha16) {
		this.mostrarFecha16 = mostrarFecha16;
	}

	public Boolean getMostrarFecha17() {
		return mostrarFecha17;
	}

	public void setMostrarFecha17(Boolean mostrarFecha17) {
		this.mostrarFecha17 = mostrarFecha17;
	}

	public Boolean getMostrarFecha18() {
		return mostrarFecha18;
	}

	public void setMostrarFecha18(Boolean mostrarFecha18) {
		this.mostrarFecha18 = mostrarFecha18;
	}

	public TramiteEmbarqueCalculoPrecio nuevo() {
		tramiteEmbarqueCalculoPrecioSeleccionado = new TramiteEmbarqueCalculoPrecio();
		tramiteEmbarqueCalculoPrecioSeleccionado.setFechaCreacion(new Date());
		
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioGalonUno(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioGalonDos(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioGalonTres(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioGalonCuatro(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioGalonCinco(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioGalonSeis(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioGalonSiete(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioGalonOcho(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioGalonNueve(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioGalonDiez(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioGalonOnce(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioGalonDoce(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioGalonTrece(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioGalonCatorce(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioGalonQuince(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioGalonDieciseis(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioGalonDiecisiete(BigDecimal.valueOf(0.0));
		
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioBarrilUno(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioBarrilDos(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioBarrilTres(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioBarrilCuatro(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioBarrilCinco(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioBarrilSeis(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioBarrilSiete(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioBarrilOcho(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioBarrilNueve(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioBarrilDiez(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioBarrilOnce(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioBarrilDoce(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioBarrilTrece(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioBarrilCatorce(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioBarrilQuince(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioBarrilDieciseis(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioBarrilDiecisiete(BigDecimal.valueOf(0.0));
		
		tramiteEmbarqueCalculoPrecioSeleccionado.setPremio(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setLibertad(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioPuerto(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setVolumenPuertoArena(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecio(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setSubtotal(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setTotal(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioUno(BigDecimal.valueOf(0.0));
		
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioGalonBl(BigDecimal.valueOf(0.0));
		tramiteEmbarqueCalculoPrecioSeleccionado.setPrecioBarrilBl(BigDecimal.valueOf(0.0));
		
		tramiteEmbarqueCalculoPrecioSeleccionado.setVolumenDescargadoPuerto(BigDecimal.valueOf(259098));
		return tramiteEmbarqueCalculoPrecioSeleccionado;
	}
}
