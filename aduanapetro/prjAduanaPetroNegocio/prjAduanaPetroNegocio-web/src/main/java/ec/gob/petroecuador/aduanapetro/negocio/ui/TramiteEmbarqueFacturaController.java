package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueFactura;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueFacturaService;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueDesaduanamientoDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueFacturaDataManager;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("tramiteEmbarqueFacturaController")
@Scope("request")
public class TramiteEmbarqueFacturaController extends RegistroComun<TramiteEmbarqueFactura> {
	
	public TramiteEmbarqueFacturaController() {
		super("", "");
	}

	@Autowired
	private TramiteEmbarqueFacturaService tramiteEmbarqueFacturaService;
	
	@Autowired
	private TramiteEmbarqueFacturaDataManager tramiteEmbarqueFacturaSeleccionado;
	
	@Autowired
	private TramiteEmbarqueDataManager tramiteEmbarqueSeleccionado;
	
	private TramiteEmbarqueFactura tramiteEmbarqueFactura = new TramiteEmbarqueFactura();
	private List<TramiteEmbarqueFactura> lTramiteEmbarquesFactura = new ArrayList<>();
	
	@Autowired
	private TramiteEmbarqueDesaduanamientoDataManager tramiteEmbarqueDesaduanamientoSeleccionado;
	
	private void nuevoFacturaSeleccionado() {
		tramiteEmbarqueFactura = new TramiteEmbarqueFactura();
		//llenarDatosIniciales	
		tramiteEmbarqueFactura.setNumero("0");
		tramiteEmbarqueFactura.setFecha(new Date());
		if(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado()!=null)
			tramiteEmbarqueFactura.setVolumen(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getVolumen());
		tramiteEmbarqueFactura.setPrecio(BigDecimal.valueOf(0.0));
		tramiteEmbarqueFactura.setFleteBuque(BigDecimal.valueOf(0.0));
		tramiteEmbarqueFactura.setValorFob(BigDecimal.valueOf(0.0));
		tramiteEmbarqueFactura.setValorDat(BigDecimal.valueOf(0.0));
		
		tramiteEmbarqueFacturaSeleccionado.setTramiteEmbarqueFacturaSeleccionado(tramiteEmbarqueFactura);
	}
	
	//METODOS
	public void inicializarVariables() {	
		try {
			if (tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado() != null && 
				tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId() != null) {
				lTramiteEmbarquesFactura = tramiteEmbarqueFacturaService.obtenerPorPadre(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado());
				if (!lTramiteEmbarquesFactura.isEmpty()) {
					tramiteEmbarqueFactura = lTramiteEmbarquesFactura.get(0);
					tramiteEmbarqueFacturaSeleccionado.setTramiteEmbarqueFacturaSeleccionado(tramiteEmbarqueFactura);
				}
				else {
					nuevoFacturaSeleccionado();						
				}
			}
			else{
				lTramiteEmbarquesFactura = new ArrayList<TramiteEmbarqueFactura>();
				nuevoFacturaSeleccionado();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void nuevoRegistro(){
		tramiteEmbarqueFacturaSeleccionado.setTramiteEmbarqueFacturaSeleccionado(new TramiteEmbarqueFactura());
		nuevoRegistro( tramiteEmbarqueFacturaSeleccionado.getTramiteEmbarqueFacturaSeleccionado() );
	}
	
	public void eliminar() {
		elementoSeleccionado = tramiteEmbarqueFacturaSeleccionado.getTramiteEmbarqueFacturaSeleccionado();
		eliminar(tramiteEmbarqueFacturaService);
	}

	public void guardar(){
		tramiteEmbarqueFacturaSeleccionado.getTramiteEmbarqueFacturaSeleccionado().setTramiteEmbarque(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado());
		elementoSeleccionado = tramiteEmbarqueFacturaSeleccionado.getTramiteEmbarqueFacturaSeleccionado();
		guardar(tramiteEmbarqueFacturaService);
		
		//llenar datos para tramiteEmbarqueDesaduanamiento que corresponden al proceso de IMP y que son obligatorios
		System.out.println("-> Actualizando Desaduanamiento Datos de Factura");
		tramiteEmbarqueDesaduanamientoSeleccionado.setVolumen(tramiteEmbarqueFacturaSeleccionado.getTramiteEmbarqueFacturaSeleccionado().getVolumen());	
		tramiteEmbarqueDesaduanamientoSeleccionado.setFlete(tramiteEmbarqueFacturaSeleccionado.getTramiteEmbarqueFacturaSeleccionado().getFleteBuque());
		tramiteEmbarqueDesaduanamientoSeleccionado.setValorDat(tramiteEmbarqueFacturaSeleccionado.getTramiteEmbarqueFacturaSeleccionado().getValorDat());
		//printObj(tramiteEmbarqueDesaduanamientoSeleccionado);
	}
		
	public void generarDocumento() {}

	//GETS Y SETS
	
	public TramiteEmbarqueFacturaDataManager getTramiteEmbarqueFacturaSeleccionado() {
		return tramiteEmbarqueFacturaSeleccionado;
	}

	public void setTramiteEmbarqueFacturaSeleccionado(
			TramiteEmbarqueFacturaDataManager tramiteEmbarqueFacturaSeleccionado) {
		this.tramiteEmbarqueFacturaSeleccionado = tramiteEmbarqueFacturaSeleccionado;
	}

	public TramiteEmbarqueDataManager getTramiteEmbarqueSeleccionado() {
		return tramiteEmbarqueSeleccionado;
	}

	public void setTramiteEmbarqueSeleccionado(
			TramiteEmbarqueDataManager tramiteEmbarqueSeleccionado) {
		this.tramiteEmbarqueSeleccionado = tramiteEmbarqueSeleccionado;
	}

	public TramiteEmbarqueFactura getTramiteEmbarqueFactura() {
		return tramiteEmbarqueFactura;
	}

	public void setTramiteEmbarqueFactura(
			TramiteEmbarqueFactura tramiteEmbarqueFactura) {
		this.tramiteEmbarqueFactura = tramiteEmbarqueFactura;
	}

	public List<TramiteEmbarqueFactura> getlTramiteEmbarquesFactura() {
		if(lTramiteEmbarquesFactura.isEmpty())
			inicializarVariables();
		return lTramiteEmbarquesFactura;
	}

	public void setlTramiteEmbarquesFactura(
			List<TramiteEmbarqueFactura> lTramiteEmbarquesFactura) {
		this.lTramiteEmbarquesFactura = lTramiteEmbarquesFactura;
	}

	public TramiteEmbarqueDesaduanamientoDataManager getTramiteEmbarqueDesaduanamientoSeleccionado() {
		return tramiteEmbarqueDesaduanamientoSeleccionado;
	}

	public void setTramiteEmbarqueDesaduanamientoSeleccionado(
			TramiteEmbarqueDesaduanamientoDataManager tramiteEmbarqueDesaduanamientoSeleccionado) {
		this.tramiteEmbarqueDesaduanamientoSeleccionado = tramiteEmbarqueDesaduanamientoSeleccionado;
	}
		
}
