package ec.gob.petroecuador.aduanapetro.negocio.ui.data;


import java.math.BigDecimal;
import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDesaduanamiento;

@Component("tramiteEmbarqueDesaduanamientoDataManager")
@Scope("session")
public class TramiteEmbarqueDesaduanamientoDataManager {
	
	private TramiteEmbarqueDesaduanamiento tramiteEmbarqueDesaduanamientoSeleccionado;

	public String proveedor;
	public String buque;
	public String producto;
	public BigDecimal volumen;
	public BigDecimal flete;
	public BigDecimal valorDat;
	public Date fechaBl;
	public String cartaCredito;
	public String adjudicacion;
	public Date ventanaInicio;
	public Date ventanaFin;
	
	TramiteEmbarqueDesaduanamientoDataManager() {
		tramiteEmbarqueDesaduanamientoSeleccionado = new TramiteEmbarqueDesaduanamiento(); 
	}
	
	public TramiteEmbarqueDesaduanamiento getTramiteEmbarqueDesaduanamientoSeleccionado() {
		return tramiteEmbarqueDesaduanamientoSeleccionado;
	}

	public void setTramiteEmbarqueDesaduanamientoSeleccionado(
			TramiteEmbarqueDesaduanamiento tramiteEmbarqueDesaduanamientoSeleccionado) {
		this.tramiteEmbarqueDesaduanamientoSeleccionado = tramiteEmbarqueDesaduanamientoSeleccionado;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public String getBuque() {
		return buque;
	}

	public void setBuque(String buque) {
		this.buque = buque;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public BigDecimal getVolumen() {
		return volumen;
	}

	public void setVolumen(BigDecimal volumen) {
		this.volumen = volumen;
	}

	public BigDecimal getFlete() {
		return flete;
	}

	public void setFlete(BigDecimal flete) {
		this.flete = flete;
	}

	public BigDecimal getValorDat() {
		return valorDat;
	}

	public void setValorDat(BigDecimal valorDat) {
		this.valorDat = valorDat;
	}

	public Date getFechaBl() {
		return fechaBl;
	}

	public void setFechaBl(Date fechaBl) {
		this.fechaBl = fechaBl;
	}

	public String getCartaCredito() {
		return cartaCredito;
	}

	public void setCartaCredito(String cartaCredito) {
		this.cartaCredito = cartaCredito;
	}

	public String getAdjudicacion() {
		return adjudicacion;
	}

	public void setAdjudicacion(String adjudicacion) {
		this.adjudicacion = adjudicacion;
	}

	public Date getVentanaInicio() {
		return ventanaInicio;
	}

	public void setVentanaInicio(Date ventanaInicio) {
		this.ventanaInicio = ventanaInicio;
	}

	public Date getVentanaFin() {
		return ventanaFin;
	}

	public void setVentanaFin(Date ventanaFin) {
		this.ventanaFin = ventanaFin;
	}
	
}

