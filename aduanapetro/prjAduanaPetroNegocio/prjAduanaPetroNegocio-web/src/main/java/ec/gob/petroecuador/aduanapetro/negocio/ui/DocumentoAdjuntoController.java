package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.DocumentoAdjunto;
import ec.gob.petroecuador.aduanapetro.negocio.services.DocumentoAdjuntoService;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("documentoAdjuntoController")
@Scope("request")
public class DocumentoAdjuntoController extends RegistroComun<DocumentoAdjunto> {

	public DocumentoAdjuntoController() {
		super("adjudicacionForm.xhtml", "adjudicacionTabla.xhtml");
	}
	
	@Autowired
	private DocumentoAdjuntoService documentoAdjuntoService;

	private DocumentoAdjunto documentoAdjunto = new DocumentoAdjunto();
	private List<DocumentoAdjunto> lDocumentosAdjuntos = new ArrayList<>();
	
	//METODOS
	public void inicializarVariables() {	
		documentoAdjunto = new DocumentoAdjunto();
		lDocumentosAdjuntos = documentoAdjuntoService.obtenerActivos();
	}
	
	public String nuevoRegistro(){
		documentoAdjunto = new DocumentoAdjunto();
		return nuevoRegistro(documentoAdjunto);
	}
	
	public String eliminar() {
		return eliminar(documentoAdjuntoService);
	}

	public void guardar(){
		guardar(documentoAdjuntoService);
	}
	
	public void buscar() {		
	}
	
	//Getters and setters
	public DocumentoAdjunto getDocumentoAdjunto() {
		return documentoAdjunto;
	}
	public void setDocumentoAdjunto(DocumentoAdjunto documentoAdjunto) {
		this.documentoAdjunto = documentoAdjunto;
	}
	public List<DocumentoAdjunto> getlDocumentosAdjuntos() {
		if(lDocumentosAdjuntos.isEmpty())
			lDocumentosAdjuntos = documentoAdjuntoService.obtenerActivos();
		return lDocumentosAdjuntos;
	}
	public void setlDocumentosAdjuntos(List<DocumentoAdjunto> lDocumentosAdjuntos) {
		this.lDocumentosAdjuntos = lDocumentosAdjuntos;
	}

}
