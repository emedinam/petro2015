package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.ParametroCalculo;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PrecioPrevision;
import ec.gob.petroecuador.aduanapetro.negocio.services.ParametroCalculoService;
import ec.gob.petroecuador.aduanapetro.negocio.services.PrecioPrevisionService;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.PrecioPrevisionDataManager;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("precioPrevisionController")
@Scope("request")
public class PrecioPrevisionController extends RegistroComun<PrecioPrevision> {

	public PrecioPrevisionController() {
		super("precioPrevisionForm.xhtml", "precioPrevisionTabla.xhtml");
	}

	@Autowired
	private PrecioPrevisionService precioPrevisionService; 
	
	@Autowired
	private ParametroCalculoService parametroCalculoService;

	@Autowired
	private PrecioPrevisionDataManager precioPrevisionSeleccionado;

	private PrecioPrevision precioPrevision = new PrecioPrevision();
	private List<PrecioPrevision> lPrecioPrevisions = new ArrayList<>();	
	private List<ParametroCalculo> parametroCalculos = new ArrayList<>();
	
	// METODOS
	public void inicializarVariables() {
		try {
			//lPrecioPrevisions=precioPrevisionService.obtenerActivos();
			buscar();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String editar() {
		precioPrevision = (PrecioPrevision)elementoSeleccionado;
		precioPrevisionSeleccionado.setSelectParametroCalculo(precioPrevision.getParametroCalculo());
		precioPrevisionSeleccionado.setVariacion(precioPrevision.getParametroCalculo().getPorcentajeVariacion());
		return super.editar();
	}

	public String nuevoRegistro() {
		precioPrevisionSeleccionado.nuevo();
		return nuevoRegistro(precioPrevisionSeleccionado.getPrecioPrevisionSeleccionado());
	}

	public String eliminar() {
		return eliminar(precioPrevisionService);
	}

	public String guardar() {
		precioPrevision = (PrecioPrevision)elementoSeleccionado;
		precioPrevision.setParametroCalculo(precioPrevisionSeleccionado.getSelectParametroCalculo());
		return guardar(precioPrevisionService);
	}

	public void buscar() {
					
			try {
				
				List<String> criterios = new ArrayList<String>();
				
				if (precioPrevisionSeleccionado.getFiltroAnio() != null)
					if (!precioPrevisionSeleccionado.getFiltroAnio().equals(""))
						criterios.add("anioActual ="+precioPrevisionSeleccionado.getFiltroAnio());
				if (precioPrevisionSeleccionado.getFiltroProducto()  != null)
					if(!precioPrevisionSeleccionado.getFiltroProducto().equals(""))
						criterios.add("parametroCalculo.CatalogoProducto.id ="+precioPrevisionSeleccionado.getFiltroProducto().toString());
				criterios.add("id > 0");
				
				lPrecioPrevisions = precioPrevisionService.buscarPorCriterios(criterios.toArray(new String[0]));
				System.out.println("-> PararametrosCalculo encontrados: "+lPrecioPrevisions.size());
				if(lPrecioPrevisions.size()==0) {
					lPrecioPrevisions =  new ArrayList<PrecioPrevision>();
					addMessage("No se encontraron resultados con los filtros de busqueda seleccionados");
				}	
				
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
		
	public void cargarDatosParametroCalculo() {
		ParametroCalculo parametroCalculo=null;
		try {
			precioPrevision = precioPrevisionSeleccionado.refrescar();
			parametroCalculo=parametroCalculoService.buscar(precioPrevisionSeleccionado.getSelectParametroCalculo().getId());
			precioPrevision.setPrecioMarcador(parametroCalculo.getPrecioMarcador().getNombre());
			precioPrevision.setDiferencial(parametroCalculo.getDiferencial());
			precioPrevisionSeleccionado.setVariacion(parametroCalculo.getPorcentajeVariacion());
			precioPrevisionSeleccionado.setPrecioPrevisionSeleccionado(precioPrevision);
			setElementoSeleccionado(precioPrevision);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void calculoPrecio(){
		precioPrevision = (PrecioPrevision)elementoSeleccionado;
		
		BigDecimal sum1=precioPrevision.getGalonEnero().add(precioPrevision.getGalonFebrero());
		BigDecimal sum2=precioPrevision.getGalonMarzo().add(precioPrevision.getGalonAbril());
		BigDecimal sum3=precioPrevision.getGalonMayo().add(precioPrevision.getGalonJunio());
		BigDecimal sum4=precioPrevision.getGalonJulio().add(precioPrevision.getGalonAgosto());
		BigDecimal sum5=precioPrevision.getGalonSeptiembre().add(precioPrevision.getGalonOctubre());
		BigDecimal sum6=precioPrevision.getGalonNoviembre().add(precioPrevision.getGalonDiciembre());
		//suma de los 12 meses
		BigDecimal sumaTotal=sum1.add(sum2).add(sum3).add(sum4).add(sum5).add(sum6);
		//Promedio
		BigDecimal promedio=sumaTotal.divide(BigDecimal.valueOf(12),2, RoundingMode.HALF_UP);
		precioPrevision.setPromedio(promedio);
		
		//Diferencial
		BigDecimal subTotal=promedio.add(precioPrevision.getDiferencial());
		precioPrevision.setSubtotal(subTotal);
		
		//variacion de precios
		BigDecimal variacionP=subTotal.multiply(precioPrevisionSeleccionado.getSelectParametroCalculo().getPorcentajeVariacion().divide(BigDecimal.valueOf(100)));
		precioPrevision.setVariacionPrecio(variacionP);
				
		//Total precio
		BigDecimal total=subTotal.add(variacionP);
		precioPrevision.setTotal(total);
		//setElementoSeleccionado( precioPrevision );
	}
	
	// Getters and setters
	public PrecioPrevisionDataManager getPrecioPrevisionSeleccionado() {
		return precioPrevisionSeleccionado;
	}

	public void setPrecioPrevisionSeleccionado(
			PrecioPrevisionDataManager precioPrevisionSeleccionado) {
		this.precioPrevisionSeleccionado = precioPrevisionSeleccionado;
	}

	public PrecioPrevision getPrecioPrevision() {
		return precioPrevision;
	}

	public void setPrecioPrevision(PrecioPrevision precioPrevision) {
		this.precioPrevision = precioPrevision;
	}

	public List<PrecioPrevision> getlPrecioPrevisions() {
		if(lPrecioPrevisions.isEmpty())
			inicializarVariables();
		return lPrecioPrevisions;
	}

	public void setlPrecioPrevisions(List<PrecioPrevision> lPrecioPrevisions) {
		this.lPrecioPrevisions = lPrecioPrevisions;
	}

	public List<ParametroCalculo> getParametroCalculos() {
		if(parametroCalculos.isEmpty())
			parametroCalculos = parametroCalculoService.obtenerActivos();
		return parametroCalculos;
	}

	public void setParametroCalculos(List<ParametroCalculo> parametroCalculos) {
		this.parametroCalculos = parametroCalculos;
	}
	
}
