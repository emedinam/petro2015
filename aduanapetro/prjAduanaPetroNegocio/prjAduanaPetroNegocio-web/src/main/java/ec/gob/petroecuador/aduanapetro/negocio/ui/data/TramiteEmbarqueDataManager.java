package ec.gob.petroecuador.aduanapetro.negocio.ui.data;


import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;

@Component("tramiteEmbarqueDataManager")
@Scope("session")
public class TramiteEmbarqueDataManager {
	
	private TramiteEmbarque tramiteEmbarqueSeleccionado;
	private String filtroTipo;
	private String filtroTramite;
	private Date filtroFechaIngTra;
	private Date filtroFechaFinTra;
	private Date filtroFechaIniVen;
	private Date filtroFechaFinVen;
	private Integer filtroAdjudicacion;
	private String filtroVentana;
	private Integer filtroProveedor;
	private Boolean banderaModPago;
	
	public TramiteEmbarque getTramiteEmbarqueSeleccionado() {
		return tramiteEmbarqueSeleccionado;
	}
	public void setTramiteEmbarqueSeleccionado(
			TramiteEmbarque tramiteEmbarqueSeleccionado) {
		this.tramiteEmbarqueSeleccionado = tramiteEmbarqueSeleccionado;
	}
	public String getFiltroTramite() {
		return filtroTramite;
	}
	public void setFiltroTramite(String filtroTramite) {
		this.filtroTramite = filtroTramite;
	}
	public Date getFiltroFechaIngTra() {
		return filtroFechaIngTra;
	}
	public void setFiltroFechaIngTra(Date filtroFechaIngTra) {
		this.filtroFechaIngTra = filtroFechaIngTra;
	}
	public Date getFiltroFechaFinTra() {
		return filtroFechaFinTra;
	}
	public void setFiltroFechaFinTra(Date filtroFechaFinTra) {
		this.filtroFechaFinTra = filtroFechaFinTra;
	}
	public Date getFiltroFechaIniVen() {
		return filtroFechaIniVen;
	}
	public void setFiltroFechaIniVen(Date filtroFechaIniVen) {
		this.filtroFechaIniVen = filtroFechaIniVen;
	}
	public Date getFiltroFechaFinVen() {
		return filtroFechaFinVen;
	}
	public void setFiltroFechaFinVen(Date filtroFechaFinVen) {
		this.filtroFechaFinVen = filtroFechaFinVen;
	}
	public Integer getFiltroAdjudicacion() {
		return filtroAdjudicacion;
	}
	public void setFiltroAdjudicacion(Integer filtroAdjudicacion) {
		this.filtroAdjudicacion = filtroAdjudicacion;
	}
	public String getFiltroVentana() {
		return filtroVentana;
	}
	public void setFiltroVentana(String filtroVentana) {
		this.filtroVentana = filtroVentana;
	}
	public Integer getFiltroProveedor() {
		return filtroProveedor;
	}
	public void setFiltroProveedor(Integer filtroProveedor) {
		this.filtroProveedor = filtroProveedor;
	}
	public Boolean getBanderaModPago() {
		return banderaModPago;
	}
	public void setBanderaModPago(Boolean banderaModPago) {
		this.banderaModPago = banderaModPago;
	}
	public String getFiltroTipo() {
		return filtroTipo;
	}
	public void setFiltroTipo(String filtroTipo) {
		this.filtroTipo = filtroTipo;
	}
	
}

