package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.PuertoDescarga;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDescarga;
import ec.gob.petroecuador.aduanapetro.negocio.services.PuertoDescargaService;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueDescargaService;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueDescargaDataManager;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("tramiteEmbarqueDescargaController")
@Scope("request")
public class TramiteEmbarqueDescargaController extends RegistroComun<TramiteEmbarqueDescarga> {

	public TramiteEmbarqueDescargaController() {
		super("","");
	}

	@Autowired
	private TramiteEmbarqueDescargaService tramiteEmbarqueDescargaService;
	
	@Autowired
	private PuertoDescargaService puertoDescargaService;
	
	@Autowired
	private TramiteEmbarqueDescargaDataManager tramiteEmbarqueDescargaSeleccionado;

	@Autowired
	private TramiteEmbarqueDataManager tramiteEmbarqueSeleccionado;

	private TramiteEmbarqueDescarga tramiteEmbarqueDescarga = new TramiteEmbarqueDescarga();
	private List<TramiteEmbarqueDescarga> lTramiteEmbarqueDescargas = new ArrayList<>();

	private List<PuertoDescarga> puertos =  new ArrayList<>();

	// METODOS
	public void inicializarVariables() {
		if (tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado()!=null && 
				tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId() != null) {
			System.out.println("Cargando tramites embarque descargas...");
			lTramiteEmbarqueDescargas = tramiteEmbarqueDescargaService.obtenerPorPadre(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado());
			System.out.println("Encontrados "+lTramiteEmbarqueDescargas.size()+"tramites embarque descargas.");
		}
		else
			lTramiteEmbarqueDescargas = new ArrayList<TramiteEmbarqueDescarga>();
	}

	public void nuevoRegistro() {
		tramiteEmbarqueDescargaSeleccionado.setTramiteEmbarqueDescargaSeleccionado(new TramiteEmbarqueDescarga());
		nuevoRegistro(tramiteEmbarqueDescargaSeleccionado.getTramiteEmbarqueDescargaSeleccionado());
	}

	public void eliminar() {
		elementoSeleccionado = tramiteEmbarqueDescargaSeleccionado.getTramiteEmbarqueDescargaSeleccionado();
		eliminar(tramiteEmbarqueDescargaService);
	}
	
	public void guardar() {
		tramiteEmbarqueDescargaSeleccionado.getTramiteEmbarqueDescargaSeleccionado().setTramiteEmbarque(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado());
		elementoSeleccionado = tramiteEmbarqueDescargaSeleccionado.getTramiteEmbarqueDescargaSeleccionado();
		guardar(tramiteEmbarqueDescargaService);
	}
	
	// GETS Y SETS
	public TramiteEmbarqueDescargaDataManager getTramiteEmbarqueDescargaSeleccionado() {
		return tramiteEmbarqueDescargaSeleccionado;
	}

	public void setTramiteEmbarqueDescargaSeleccionado(
			TramiteEmbarqueDescargaDataManager tramiteEmbarqueDescargaSeleccionado) {
		this.tramiteEmbarqueDescargaSeleccionado = tramiteEmbarqueDescargaSeleccionado;
	}

	public TramiteEmbarqueDataManager getTramiteEmbarqueSeleccionado() {
		return tramiteEmbarqueSeleccionado;
	}

	public void setTramiteEmbarqueSeleccionado(
			TramiteEmbarqueDataManager tramiteEmbarqueSeleccionado) {
		this.tramiteEmbarqueSeleccionado = tramiteEmbarqueSeleccionado;
	}

	public TramiteEmbarqueDescarga getTramiteEmbarqueDescarga() {
		return tramiteEmbarqueDescarga;
	}

	public void setTramiteEmbarqueDescarga(
			TramiteEmbarqueDescarga tramiteEmbarqueDescarga) {
		this.tramiteEmbarqueDescarga = tramiteEmbarqueDescarga;
	}

	public List<TramiteEmbarqueDescarga> getlTramiteEmbarqueDescargas() {
		if(lTramiteEmbarqueDescargas.isEmpty())
			inicializarVariables();
		return lTramiteEmbarqueDescargas;
	}

	public void setlTramiteEmbarqueDescargas(
			List<TramiteEmbarqueDescarga> lTramiteEmbarqueDescargas) {
		this.lTramiteEmbarqueDescargas = lTramiteEmbarqueDescargas;
	}

	public PuertoDescargaService getPuertoDescargaService() {
		return puertoDescargaService;
	}

	public void setPuertoDescargaService(
			PuertoDescargaService puertoDescargaService) {
		this.puertoDescargaService = puertoDescargaService;
	}

	public List<PuertoDescarga> getPuertos() {
		if(puertos.isEmpty())
			puertos = puertoDescargaService.obtenerActivos();
		return puertos;
	}

	public void setPuertos(List<PuertoDescarga> puertos) {
		this.puertos = puertos;
	}	
}