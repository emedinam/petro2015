package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Adjudicacion;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.AplicacionSeguro;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.CompaniaSeguro;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionImportacion;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Poliza;
import ec.gob.petroecuador.aduanapetro.negocio.services.AdjudicacionService;
import ec.gob.petroecuador.aduanapetro.negocio.services.AplicacionSeguroService;
import ec.gob.petroecuador.aduanapetro.negocio.services.CompaniaSeguroService;
import ec.gob.petroecuador.aduanapetro.negocio.services.PlanificacionImportacionService;
import ec.gob.petroecuador.aduanapetro.negocio.services.PolizaService;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.AplicacionSeguroDataManager;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("aplicacionSeguroController")
@Scope("request")
public class AplicacionSeguroController extends RegistroComun<AplicacionSeguro> {

	public AplicacionSeguroController() {
		super("aplicacionSeguroForm.xhtml", "aplicacionSeguroTabla.xhtml");
	}

	@Autowired
	private AplicacionSeguroService aplicacionSeguroService;	
	@Autowired
	private PolizaService polizaService;
	@Autowired
	private CompaniaSeguroService companiaSeguroService;
	@Autowired
	private AdjudicacionService adjudicacionService;
	@Autowired
	private PlanificacionImportacionService planificacionImportacionService;
	
	@Autowired
	private AplicacionSeguroDataManager aplicacionSeguroSeleccionado;

	private List<AplicacionSeguro> lAplicacionSeguros = new ArrayList<>();
	private List<Poliza> polizas = new ArrayList<>();
	private List<CompaniaSeguro> companiaSeguros = new ArrayList<>();
	private List<Adjudicacion> adjudicaciones = new ArrayList<>();
	private List<PlanificacionImportacion> planificacionImportaciones = new ArrayList<>();
	

	// METODOS
	public void inicializarVariables() {
		lAplicacionSeguros = aplicacionSeguroService.obtenerActivos();
	}

	public String nuevoRegistro() {
		aplicacionSeguroSeleccionado.setAplicacionSeguroSeleccionado( new AplicacionSeguro() );
		return nuevoRegistro(aplicacionSeguroSeleccionado.getAplicacionSeguroSeleccionado());
	}

	public String editar() {
		cargarPlanificacion();
		return super.editar();
	}
	
	public String eliminar() {
		elementoSeleccionado = aplicacionSeguroSeleccionado.getAplicacionSeguroSeleccionado();
		return eliminar(aplicacionSeguroService);
	}

	public String guardar() {
		elementoSeleccionado = aplicacionSeguroSeleccionado.getAplicacionSeguroSeleccionado();
		return guardar(aplicacionSeguroService);
	}

	public void buscar() {
		try {
			
			List<String> criterios = new ArrayList<String>();
			
			if (aplicacionSeguroSeleccionado.getFiltroDescripcion() != null)
				if (!aplicacionSeguroSeleccionado.getFiltroDescripcion().equals(""))
					criterios.add("numero like '%"+aplicacionSeguroSeleccionado.getFiltroDescripcion()+"%'");
			criterios.add("id > 0");
			
			
			lAplicacionSeguros = aplicacionSeguroService.buscarPorCriterios(criterios.toArray(new String[0]));
			System.out.println("-> Aplicaciones Seguro encontradas: "+lAplicacionSeguros.size());
			if(lAplicacionSeguros.size()==0)	{
				lAplicacionSeguros = null;
				addMessage("No se encontraron resultados con los filtros de busqueda seleccionados");
			}
			
			/*if (aplicacionSeguroSeleccionado.getFiltroDescripcion()==null || aplicacionSeguroSeleccionado.getFiltroDescripcion().isEmpty()){
				addMessage("Ingrese un termino de busqueda");
			}else {
				String[] criterios = {"numero = '"+aplicacionSeguroSeleccionado.getFiltroDescripcion()+"'"};
				lAplicacionSeguros = aplicacionSeguroService.buscarPorCriterios(criterios);
			}*/				
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void cargarPlanificacion() {
		if(aplicacionSeguroSeleccionado.getAplicacionSeguroSeleccionado().getAdjudicacion()!=null &&
				aplicacionSeguroSeleccionado.getAplicacionSeguroSeleccionado().getAdjudicacion().getId() != null)
			planificacionImportaciones = planificacionImportacionService.obtenerPorPadre(aplicacionSeguroSeleccionado.getAplicacionSeguroSeleccionado().getAdjudicacion());
		else
			planificacionImportaciones = new ArrayList<PlanificacionImportacion>();
	}

	//Getters y setters

	public List<AplicacionSeguro> getlAplicacionSeguros() {
		if(lAplicacionSeguros.isEmpty())
			inicializarVariables();
		return lAplicacionSeguros;
	}

	public void setlAplicacionSeguros(List<AplicacionSeguro> lAplicacionSeguros) {
		this.lAplicacionSeguros = lAplicacionSeguros;
	}

	public List<Poliza> getPolizas() {
		if(polizas.isEmpty())
			polizas = polizaService.obtenerActivos();
		return polizas;
	}

	public void setPolizas(List<Poliza> polizas) {
		this.polizas = polizas;
	}

	public List<CompaniaSeguro> getCompaniaSeguros() {
		if(companiaSeguros.isEmpty())
			companiaSeguros = companiaSeguroService.obtenerActivos();
		return companiaSeguros;
	}

	public void setCompaniaSeguros(List<CompaniaSeguro> companiaSeguros) {
		this.companiaSeguros = companiaSeguros;
	}

	public List<Adjudicacion> getAdjudicaciones() {
		if(adjudicaciones.isEmpty())
			adjudicaciones = adjudicacionService.obtenerActivos();
		return adjudicaciones;
	}

	public void setAdjudicaciones(List<Adjudicacion> adjudicaciones) {
		this.adjudicaciones = adjudicaciones;
	}

	public List<PlanificacionImportacion> getPlanificacionImportaciones() {
		if (aplicacionSeguroSeleccionado.getAplicacionSeguroSeleccionado().getAdjudicacion()!=null){
			if (aplicacionSeguroSeleccionado.getAplicacionSeguroSeleccionado().getAdjudicacion().getId()!=null)
				planificacionImportaciones = planificacionImportacionService.obtenerPorPadre(aplicacionSeguroSeleccionado.getAplicacionSeguroSeleccionado().getAdjudicacion());
			else
				planificacionImportaciones = new ArrayList<PlanificacionImportacion>();
		}else
			planificacionImportaciones = new ArrayList<PlanificacionImportacion>();
		
		return planificacionImportaciones;
	}

	public void setPlanificacionImportaciones(
			List<PlanificacionImportacion> planificacionImportaciones) {
		this.planificacionImportaciones = planificacionImportaciones;
	}
	
	public AplicacionSeguroDataManager getAplicacionSeguroSeleccionado() {
		return aplicacionSeguroSeleccionado;
	}

	public void setAplicacionSeguroSeleccionado(
			AplicacionSeguroDataManager aplicacionSeguroSeleccionado) {
		this.aplicacionSeguroSeleccionado = aplicacionSeguroSeleccionado;
	}
}
