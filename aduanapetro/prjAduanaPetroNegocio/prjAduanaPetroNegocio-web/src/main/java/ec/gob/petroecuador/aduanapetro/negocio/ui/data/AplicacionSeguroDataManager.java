package ec.gob.petroecuador.aduanapetro.negocio.ui.data;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.AplicacionSeguro;

@Component("aplicacionSeguroDataManager")
@Scope("session")
public class AplicacionSeguroDataManager {
	private AplicacionSeguro aplicacionSeguroSeleccionado = new AplicacionSeguro();
	private String filtroDescripcion;
	public AplicacionSeguro getAplicacionSeguroSeleccionado() {
		return aplicacionSeguroSeleccionado;
	}
	public void setAplicacionSeguroSeleccionado(
			AplicacionSeguro aplicacionSeguroSeleccionado) {
		this.aplicacionSeguroSeleccionado = aplicacionSeguroSeleccionado;
	}
	public String getFiltroDescripcion() {
		return filtroDescripcion;
	}
	public void setFiltroDescripcion(String filtroDescripcion) {
		this.filtroDescripcion = filtroDescripcion;
	}
	
	
}
