package ec.gob.petroecuador.aduanapetro.negocio.ui.data;

import java.math.BigDecimal;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.ParametroCalculo;

@Component("parametroCalculoDataManager")
@Scope("session")
public class ParametroCalculoDataManager {
	private ParametroCalculo parametroCalculoSeleccionado = new ParametroCalculo();
	private Integer filtroProducto;
	private BigDecimal filtroAnio;
	
	public ParametroCalculo getParametroCalculoSeleccionado() {
		return parametroCalculoSeleccionado;
	}
	public void setParametroCalculoSeleccionado(
			ParametroCalculo parametroCalculoSeleccionado) {
		this.parametroCalculoSeleccionado = parametroCalculoSeleccionado;
	}
	public Integer getFiltroProducto() {
		return filtroProducto;
	}
	public void setFiltroProducto(Integer filtroProducto) {
		this.filtroProducto = filtroProducto;
	}
	public BigDecimal getFiltroAnio() {
		return filtroAnio;
	}
	public void setFiltroAnio(BigDecimal filtroAnio) {
		this.filtroAnio = filtroAnio;
	}
	
}
