package ec.gob.petroecuador.aduanapetro.negocio.ui.data;


import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionVolumen;

@Component("planificacionVolumenDataManager")
@Scope("session")
public class PlanificacionVolumenDataManager {
	private PlanificacionVolumen planificacionVolumenSeleccionado;
	private Integer filtroProducto;
	private BigDecimal filtroAnio;
	
	
	private BigDecimal seguro;
	
	public PlanificacionVolumen getPlanificacionVolumenSeleccionado() {
		return planificacionVolumenSeleccionado;
	}
	public void setPlanificacionVolumenSeleccionado(
			PlanificacionVolumen planificacionVolumenSeleccionado) {
		this.planificacionVolumenSeleccionado = planificacionVolumenSeleccionado;
	}

	public BigDecimal getSeguro() {
		return seguro;
	}
	public void setSeguro(BigDecimal seguro) {
		this.seguro = seguro;
	}
	public Integer getFiltroProducto() {
		return filtroProducto;
	}
	public void setFiltroProducto(Integer filtroProducto) {
		this.filtroProducto = filtroProducto;
	}
	public BigDecimal getFiltroAnio() {
		return filtroAnio;
	}
	public void setFiltroAnio(BigDecimal filtroAnio) {
		this.filtroAnio = filtroAnio;
	}
	
	
	public void nuevo() {
		planificacionVolumenSeleccionado = new  PlanificacionVolumen();
		planificacionVolumenSeleccionado.setFechaCreacion(new Date());
		refrescar();
		setSeguro(null);
	}

	public PlanificacionVolumen refrescar(){
		Calendar fecha = new GregorianCalendar();
		planificacionVolumenSeleccionado.setAnio(BigDecimal.valueOf(fecha.get(Calendar.YEAR)));
		planificacionVolumenSeleccionado.setGalonEnero(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setGalonFebrero(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setGalonMarzo(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setGalonAbril(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setGalonMayo(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setGalonJunio(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setGalonJulio(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setGalonAgosto(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setGalonSeptiembre(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setGalonOctubre(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setGalonNoviembre(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setGalonDiciembre(BigDecimal.valueOf(0.0));
		
		planificacionVolumenSeleccionado.setBarrilEnero(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setBarrilFebrero(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setBarrilMarzo(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setBarrilAbril(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setBarrilMayo(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setBarrilJunio(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setBarrilJulio(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setBarrilAgosto(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setBarrilSeptiembre(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setBarrilOctubre(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setBarrilNoviembre(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setBarrilDiciembre(BigDecimal.valueOf(0.0));
		
		planificacionVolumenSeleccionado.setTotalDatEnero(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalDatFebrero(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalDatMarzo(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalDatAbril(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalDatMayo(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalDatJunio(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalDatJulio(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalDatAgosto(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalDatSeptiembre(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalDatOctubre(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalDatNoviembre(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalDatDiciembre(BigDecimal.valueOf(0.0));
		
		planificacionVolumenSeleccionado.setSeguroEnero(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setSeguroFebrero(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setSeguroMarzo(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setSeguroAbril(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setSeguroMayo(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setSeguroJunio(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setSeguroJulio(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setSeguroAgosto(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setSeguroSeptiembre(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setSeguroOctubre(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setSeguroNoviembre(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setSeguroDiciembre(BigDecimal.valueOf(0.0));
		
		planificacionVolumenSeleccionado.setTotalCifEnero(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalCifFebrero(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalCifMarzo(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalCifAbril(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalCifMayo(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalCifJunio(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalCifJulio(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalCifAgosto(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalCifSeptiembre(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalCifOctubre(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalCifNoviembre(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalCifDiciembre(BigDecimal.valueOf(0.0));
		
		planificacionVolumenSeleccionado.setTotalBarril(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalDat(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalSeguro(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setTotalCif(BigDecimal.valueOf(0.0));
		
		planificacionVolumenSeleccionado.setCostoCif(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setGasto(BigDecimal.valueOf(0.0));
		planificacionVolumenSeleccionado.setCostoTotal(BigDecimal.valueOf(0.0));
		return planificacionVolumenSeleccionado;
	}
}
