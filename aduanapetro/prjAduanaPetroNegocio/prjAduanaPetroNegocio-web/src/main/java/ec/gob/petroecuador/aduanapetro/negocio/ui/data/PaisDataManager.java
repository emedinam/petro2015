package ec.gob.petroecuador.aduanapetro.negocio.ui.data;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Pais;

@Component("paisDataManager")
@Scope("session")
public class PaisDataManager {

	private Pais paisSeleccionado = new Pais();
	private String filtroDescripcion;
	public Pais getPaisSeleccionado() {
		return paisSeleccionado;
	}
	public void setPaisSeleccionado(Pais paisSeleccionado) {
		this.paisSeleccionado = paisSeleccionado;
	}
	public String getFiltroDescripcion() {
		return filtroDescripcion;
	}
	public void setFiltroDescripcion(String filtroDescripcion) {
		this.filtroDescripcion = filtroDescripcion;
	}
	
	
}
