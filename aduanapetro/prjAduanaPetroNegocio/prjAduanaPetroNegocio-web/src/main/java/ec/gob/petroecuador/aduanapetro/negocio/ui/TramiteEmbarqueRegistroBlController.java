package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueRegistroBl;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueRegistroBlService;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueService;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueRegistroBlDataManager;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("tramiteEmbarqueRegistroBlController")
@Scope("request")
public class TramiteEmbarqueRegistroBlController extends RegistroComun<TramiteEmbarqueRegistroBl> {

	public TramiteEmbarqueRegistroBlController() {
		super("", "");
	}

	@Autowired
	private TramiteEmbarqueRegistroBlService tramiteEmbarqueRegistroBlService;

	@Autowired
	private TramiteEmbarqueService tramiteEmbarqueService;
	
	@Autowired
	private TramiteEmbarqueRegistroBlDataManager tramiteEmbarqueRegistroBlSeleccionado;

	@Autowired
	private TramiteEmbarqueDataManager tramiteEmbarqueSeleccionado;

	private TramiteEmbarqueRegistroBl tramiteEmbarqueRegistroBl = new TramiteEmbarqueRegistroBl();
	private List<TramiteEmbarqueRegistroBl> lTramiteEmbarqueRegistroBls = new ArrayList<>();

	// METODOS
	public void inicializarVariables() {
		System.out.println("BLS - TRAMITE SELECCIONADO "+(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado()!=null 
				&& tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId() != null? 
						tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId() : "NINGUNO" ));
		if (tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado()!=null 
				&& tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId() != null) {
			System.out.println("BLS - BUSCANDO POR PADRE...");
			lTramiteEmbarqueRegistroBls = tramiteEmbarqueRegistroBlService.obtenerPorPadre(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado());
			System.out.println("BLS - ENCONTRADOS "+(lTramiteEmbarqueRegistroBls!=null? lTramiteEmbarqueRegistroBls.size() : "0"));
		}
		else {
			lTramiteEmbarqueRegistroBls = new ArrayList<TramiteEmbarqueRegistroBl>();
		}
			
	}

	public void nuevoRegistro() {
		tramiteEmbarqueRegistroBlSeleccionado.setTramiteEmbarqueRegistroBlSeleccionado(new TramiteEmbarqueRegistroBl());
		nuevoRegistro(tramiteEmbarqueRegistroBlSeleccionado.getTramiteEmbarqueRegistroBlSeleccionado());
	}
	
	public void eliminar() {
		elementoSeleccionado = tramiteEmbarqueRegistroBlSeleccionado.getTramiteEmbarqueRegistroBlSeleccionado();
		eliminar(tramiteEmbarqueRegistroBlService);
	}

	public void guardar() {
		tramiteEmbarqueRegistroBlSeleccionado.getTramiteEmbarqueRegistroBlSeleccionado().setTramiteEmbarque( tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado() );
		elementoSeleccionado = tramiteEmbarqueRegistroBlSeleccionado.getTramiteEmbarqueRegistroBlSeleccionado();
		guardar(tramiteEmbarqueRegistroBlService);
	}
	
	// GETS Y SETS
	public TramiteEmbarqueRegistroBlDataManager getTramiteEmbarqueRegistroBlSeleccionado() {
		return tramiteEmbarqueRegistroBlSeleccionado;
	}

	public void setTramiteEmbarqueRegistroBlSeleccionado(
			TramiteEmbarqueRegistroBlDataManager tramiteEmbarqueRegistroBlSeleccionado) {
		this.tramiteEmbarqueRegistroBlSeleccionado = tramiteEmbarqueRegistroBlSeleccionado;
	}

	public TramiteEmbarqueDataManager getTramiteEmbarqueSeleccionado() {
		return tramiteEmbarqueSeleccionado;
	}

	public void setTramiteEmbarqueSeleccionado(
			TramiteEmbarqueDataManager tramiteEmbarqueSeleccionado) {
		this.tramiteEmbarqueSeleccionado = tramiteEmbarqueSeleccionado;
	}

	public TramiteEmbarqueRegistroBl getTramiteEmbarqueRegistroBl() {
		return tramiteEmbarqueRegistroBl;
	}

	public void setTramiteEmbarqueRegistroBl(
			TramiteEmbarqueRegistroBl tramiteEmbarqueRegistroBl) {
		this.tramiteEmbarqueRegistroBl = tramiteEmbarqueRegistroBl;
	}

	public List<TramiteEmbarqueRegistroBl> getlTramiteEmbarqueRegistroBls() {
		if(lTramiteEmbarqueRegistroBls.isEmpty())
			inicializarVariables();
		return lTramiteEmbarqueRegistroBls;
	}

	public void setlTramiteEmbarqueRegistroBls(
			List<TramiteEmbarqueRegistroBl> lTramiteEmbarqueRegistroBls) {
		this.lTramiteEmbarqueRegistroBls = lTramiteEmbarqueRegistroBls;
	}

	public TramiteEmbarqueService getTramiteEmbarqueService() {
		return tramiteEmbarqueService;
	}

	public void setTramiteEmbarqueService(
			TramiteEmbarqueService tramiteEmbarqueService) {
		this.tramiteEmbarqueService = tramiteEmbarqueService;
	}	
}