package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionImportacion;
import ec.gob.petroecuador.aduanapetro.negocio.services.PlanificacionImportacionService;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.AdjudicacionDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.PlanificacionImportacionDataManager;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("planificacionCargamentosController")
@Scope("request")
public class PlanificacionCargamentosController extends RegistroComun<PlanificacionImportacion> {

	public PlanificacionCargamentosController() {
		super("planificacionCargamentosForm.xhtml", "planificacionCargamentosTabla.xhtml");
	}

	@Autowired
	private PlanificacionImportacionService planificacionImportacionService;

	@Autowired
	private PlanificacionImportacionDataManager planificacionImportacionSeleccionado;

	@Autowired
	private AdjudicacionDataManager adjudicacionSeleccionado;

	private PlanificacionImportacion planificacionImportacion = new PlanificacionImportacion();
	private List<PlanificacionImportacion> lPlanificacionImportaciones = new ArrayList<>();

	// METODOS
	public void inicializarVariables() {
		planificacionImportacion = planificacionImportacionSeleccionado
				.getPlanificacionImportacionSeleccionado();
		try {
			lPlanificacionImportaciones = planificacionImportacionService.obtenerActivos();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public AdjudicacionDataManager getAdjudicacionSeleccionado() {
		return adjudicacionSeleccionado;
	}

	public void setAdjudicacionSeleccionado(
			AdjudicacionDataManager adjudicacionSeleccionado) {
		this.adjudicacionSeleccionado = adjudicacionSeleccionado;
	}

	public String nuevoRegistro() {
		planificacionImportacionSeleccionado.nuevo(adjudicacionSeleccionado.getAdjudicacionSeleccionado());
		return nuevoRegistro(planificacionImportacionSeleccionado.getPlanificacionImportacionSeleccionado());
	}

	public String editar() {
		setElementoSeleccionado( planificacionImportacionSeleccionado.getPlanificacionImportacionSeleccionado() ); 
		return super.editar();
	}
	
	
	public void eliminar() {
		elementoSeleccionado = planificacionImportacionSeleccionado.getPlanificacionImportacionSeleccionado();
		eliminar(planificacionImportacionService);
	}

	public String guardar() {
		return guardar(planificacionImportacionService);	
	}

	public void buscar() {
		
		try {
			
			List<String> criterios = new ArrayList<String>();
			
			if (planificacionImportacionSeleccionado.getProveedor() != null)
				if (!planificacionImportacionSeleccionado.getProveedor().equals(""))
					criterios.add("Adjudicacion.Actor.id = "+planificacionImportacionSeleccionado.getProveedor());
			if (planificacionImportacionSeleccionado.getAdjudicacion() != null)
				if(!planificacionImportacionSeleccionado.getAdjudicacion().equals(""))
					criterios.add("Adjudicacion.numeroContrato like '%"+planificacionImportacionSeleccionado.getAdjudicacion()+"%'");
			criterios.add("id > 0 ");
			
			
			lPlanificacionImportaciones = planificacionImportacionService.buscarPorCriterios(criterios.toArray(new String[0]));
			System.out.println("-> Planif Cargamentos encontrados: "+lPlanificacionImportaciones.size());
			if(lPlanificacionImportaciones.size()==0)	{
				lPlanificacionImportaciones.clear();
				addMessage("No se encontraron resultados con los filtros de busqueda seleccionados");
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Getters and setters

	public PlanificacionImportacionDataManager getPlanificacionImportacionSeleccionado() {
		return planificacionImportacionSeleccionado;
	}

	public void setPlanificacionImportacionSeleccionado(
			PlanificacionImportacionDataManager planificacionImportacionSeleccionado) {
		this.planificacionImportacionSeleccionado = planificacionImportacionSeleccionado;
	}

	public PlanificacionImportacion getPlanificacionImportacion() {
		return planificacionImportacion;
	}

	public void setPlanificacionImportacion(
			PlanificacionImportacion planificacionImportacion) {
		this.planificacionImportacion = planificacionImportacion;
	}

	public List<PlanificacionImportacion> getlPlanificacionImportaciones() {
		if(lPlanificacionImportaciones.isEmpty())
			buscar();
			//lPlanificacionImportaciones = planificacionImportacionService.obtenerActivos();
		
		return lPlanificacionImportaciones;
	}

	public void setlPlanificacionImportaciones(
			List<PlanificacionImportacion> lPlanificacionImportaciones) {
		this.lPlanificacionImportaciones = lPlanificacionImportaciones;
	}

}
