package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.DistritoAduanero;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDesaduanamiento;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDocDigital;
import ec.gob.petroecuador.aduanapetro.negocio.services.DistritoAduaneroService;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueDesaduanamientoService;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueDocDigitalService;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueDesaduanamientoDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueFacturaDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueModPagoDataManager;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("tramiteEmbarqueDesaduanamientoController")
@Scope("request")
public class TramiteEmbarqueDesaduanamientoController extends RegistroComun<TramiteEmbarqueDesaduanamiento> {
	
	public TramiteEmbarqueDesaduanamientoController() {
		super("", "");
		setElementoSeleccionado(new TramiteEmbarqueDesaduanamiento());
	}

	@Autowired
	private TramiteEmbarqueDesaduanamientoService tramiteEmbarqueDesaduanamientoService;
	
	@Autowired
	private DistritoAduaneroService distritoAduaneroService;
	
	@Autowired
	private TramiteEmbarqueDocDigitalService tramiteEmbarqueDocDigitalService;
	
	@Autowired
	private TramiteEmbarqueDesaduanamientoDataManager tramiteEmbarqueDesaduanamientoSeleccionado;
	
	@Autowired
	private TramiteEmbarqueModPagoDataManager tramiteEmbarqueModPagoSeleccionado;
	
	@Autowired
	private TramiteEmbarqueFacturaDataManager tramiteEmbarqueFacturaSeleccionado;
	
	@Autowired
	private TramiteEmbarqueDataManager tramiteEmbarqueSeleccionado;
	
	private TramiteEmbarqueDesaduanamiento tramiteEmbarqueDesaduanamiento = new TramiteEmbarqueDesaduanamiento();
	private List<TramiteEmbarqueDesaduanamiento> lTramiteEmbarquesDesaduanamiento = new ArrayList<>();
	
	private List<DistritoAduanero> aduanas = new ArrayList<>();
	
	public String[] documentosSeleccionados;
	public List<String> documentos = new ArrayList<>();
	public String str_documento;
	
	//METODOS
	public void inicializarVariables() {	
		/*try {
			
			if (tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado() != null &&
				tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId() != null){
				lTramiteEmbarquesDesaduanamiento = tramiteEmbarqueDesaduanamientoService.obtenerPorPadre(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado());
				if (!lTramiteEmbarquesDesaduanamiento.isEmpty()) {
					tramiteEmbarqueDesaduanamiento = lTramiteEmbarquesDesaduanamiento.get(0);
					tramiteEmbarqueDesaduanamientoSeleccionado.setTramiteEmbarqueDesaduanamientoSeleccionado(tramiteEmbarqueDesaduanamiento);
					cargarDatosTramiteImportacion();
					//llenar lista de documentos seleccionados
					str_documento = tramiteEmbarqueDesaduanamientoSeleccionado.getTramiteEmbarqueDesaduanamientoSeleccionado().getDocumentos();
					if (str_documento != null)
						documentosSeleccionados = str_documento.split(":");
				}
				else {
					tramiteEmbarqueDesaduanamiento = new TramiteEmbarqueDesaduanamiento();
					//llenarDatosIniciales
					cargarDatosTramiteImportacion();
					List<DistritoAduanero> das = this.distritoAduaneroService.obtenerActivos();
					if (das.isEmpty())
						tramiteEmbarqueDesaduanamiento.setDistritoAduanero(new DistritoAduanero());
					else
						tramiteEmbarqueDesaduanamiento.setDistritoAduanero(das.get(0));
					
					tramiteEmbarqueDesaduanamientoSeleccionado.setTramiteEmbarqueDesaduanamientoSeleccionado(tramiteEmbarqueDesaduanamiento);
				}
			}
			else{
				lTramiteEmbarquesDesaduanamiento = new ArrayList<TramiteEmbarqueDesaduanamiento>();
				tramiteEmbarqueDesaduanamiento = new TramiteEmbarqueDesaduanamiento();
				//llenar datos obligatorios
				cargarDatosTramiteImportacion();
				List<DistritoAduanero> das = this.distritoAduaneroService.obtenerActivos();
				if (das.isEmpty())
					tramiteEmbarqueDesaduanamiento.setDistritoAduanero(new DistritoAduanero());
				else
					tramiteEmbarqueDesaduanamiento.setDistritoAduanero(das.get(0));
				
				tramiteEmbarqueDesaduanamientoSeleccionado.setTramiteEmbarqueDesaduanamientoSeleccionado(tramiteEmbarqueDesaduanamiento);
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}*/
	}
		
	public void cargarDatosTramiteImportacion(){
		if (tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId() != null)
			tramiteEmbarqueDesaduanamientoSeleccionado.setProveedor(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getAdjudicacion().getActor().getRazonSocial());
		if (tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId() != null)
			tramiteEmbarqueDesaduanamientoSeleccionado.setBuque(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getNombreBuque());
		if (tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId() != null)
			tramiteEmbarqueDesaduanamientoSeleccionado.setProducto(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getAdjudicacion().getCatalogoProducto().getNombre());
		if (tramiteEmbarqueFacturaSeleccionado.getTramiteEmbarqueFacturaSeleccionado()!= null)
			tramiteEmbarqueDesaduanamientoSeleccionado.setVolumen(tramiteEmbarqueFacturaSeleccionado.getTramiteEmbarqueFacturaSeleccionado().getVolumen());
		if (tramiteEmbarqueFacturaSeleccionado.getTramiteEmbarqueFacturaSeleccionado()!= null)
			tramiteEmbarqueDesaduanamientoSeleccionado.setFlete(tramiteEmbarqueFacturaSeleccionado.getTramiteEmbarqueFacturaSeleccionado().getFleteBuque());		
		if (tramiteEmbarqueFacturaSeleccionado.getTramiteEmbarqueFacturaSeleccionado()!= null)
			tramiteEmbarqueDesaduanamientoSeleccionado.setValorDat(tramiteEmbarqueFacturaSeleccionado.getTramiteEmbarqueFacturaSeleccionado().getValorDat());
		if (tramiteEmbarqueModPagoSeleccionado.getTramiteEmbarqueModPagoSeleccionado()!= null)
			tramiteEmbarqueDesaduanamientoSeleccionado.setCartaCredito(tramiteEmbarqueModPagoSeleccionado.getTramiteEmbarqueModPagoSeleccionado().getNumero());
		if (tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId()!= null)
			tramiteEmbarqueDesaduanamientoSeleccionado.setAdjudicacion(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getAdjudicacion().getNumeroContrato());
		if (tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId()!= null)
			tramiteEmbarqueDesaduanamientoSeleccionado.setVentanaInicio(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getPlanificacionImportacion().getVentanaInicio());		
		if (tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId()!= null)
			tramiteEmbarqueDesaduanamientoSeleccionado.setVentanaFin(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getPlanificacionImportacion().getVentanaFin());
	}
	
	public void nuevoRegistro() {
		tramiteEmbarqueDesaduanamientoSeleccionado.setTramiteEmbarqueDesaduanamientoSeleccionado(new TramiteEmbarqueDesaduanamiento());
		nuevoRegistro(tramiteEmbarqueDesaduanamientoSeleccionado.getTramiteEmbarqueDesaduanamientoSeleccionado());
	}
	
	public void eliminar() {
		elementoSeleccionado = tramiteEmbarqueDesaduanamientoSeleccionado.getTramiteEmbarqueDesaduanamientoSeleccionado();
		eliminar(tramiteEmbarqueDesaduanamientoService);
	}
	
	public void guardar(){
		try {
			//crear la cadena de documentos para guardar en el campo documentos
			str_documento = "";
			Integer count = documentosSeleccionados.length;
			Integer pos=0;
			for (int i = 0; i < documentosSeleccionados.length; i++) {
				pos++;
				if (pos.equals(count))
					str_documento = str_documento + documentosSeleccionados[i];
				else
					str_documento = str_documento + documentosSeleccionados[i] + ":";
	        }
			
			tramiteEmbarqueDesaduanamientoSeleccionado.getTramiteEmbarqueDesaduanamientoSeleccionado().setDocumentos(str_documento);;
			tramiteEmbarqueDesaduanamientoSeleccionado.getTramiteEmbarqueDesaduanamientoSeleccionado().setTramiteEmbarque(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado());
			tramiteEmbarqueDesaduanamientoService.guardar(tramiteEmbarqueDesaduanamientoSeleccionado.getTramiteEmbarqueDesaduanamientoSeleccionado());
			inicializarVariables();
			saveRegistro(AccionCRUD.Registrado, tramiteEmbarqueDesaduanamientoSeleccionado.getTramiteEmbarqueDesaduanamientoSeleccionado().getId());
		} catch (Exception e) {
			addError(e);			
		}
	}
		
	public void generarDocumento(){
		
	}

	//GETS Y SETS
	public TramiteEmbarqueDesaduanamientoService getTramiteEmbarqueDesaduanamientoService() {
		return tramiteEmbarqueDesaduanamientoService;
	}

	public void setTramiteEmbarqueDesaduanamientoService(
			TramiteEmbarqueDesaduanamientoService tramiteEmbarqueDesaduanamientoService) {
		this.tramiteEmbarqueDesaduanamientoService = tramiteEmbarqueDesaduanamientoService;
	}

	public TramiteEmbarqueDesaduanamientoDataManager getTramiteEmbarqueDesaduanamientoSeleccionado() {
		return tramiteEmbarqueDesaduanamientoSeleccionado;
	}

	public void setTramiteEmbarqueDesaduanamientoSeleccionado(
			TramiteEmbarqueDesaduanamientoDataManager tramiteEmbarqueDesaduanamientoSeleccionado) {
		this.tramiteEmbarqueDesaduanamientoSeleccionado = tramiteEmbarqueDesaduanamientoSeleccionado;
	}

	public TramiteEmbarqueDataManager getTramiteEmbarqueSeleccionado() {
		return tramiteEmbarqueSeleccionado;
	}

	public void setTramiteEmbarqueSeleccionado(
			TramiteEmbarqueDataManager tramiteEmbarqueSeleccionado) {
		this.tramiteEmbarqueSeleccionado = tramiteEmbarqueSeleccionado;
	}

	public TramiteEmbarqueDesaduanamiento getTramiteEmbarqueDesaduanamiento() {
		return tramiteEmbarqueDesaduanamiento;
	}

	public void setTramiteEmbarqueDesaduanamiento(
			TramiteEmbarqueDesaduanamiento tramiteEmbarqueDesaduanamiento) {
		this.tramiteEmbarqueDesaduanamiento = tramiteEmbarqueDesaduanamiento;
	}

	public List<TramiteEmbarqueDesaduanamiento> getlTramiteEmbarquesDesaduanamiento() {
		if(lTramiteEmbarquesDesaduanamiento.isEmpty()) {
			inicializarVariables();
		}
		return lTramiteEmbarquesDesaduanamiento;
	}

	public void setlTramiteEmbarquesDesaduanamiento(
			List<TramiteEmbarqueDesaduanamiento> lTramiteEmbarquesDesaduanamiento) {
		this.lTramiteEmbarquesDesaduanamiento = lTramiteEmbarquesDesaduanamiento;
	}

	public DistritoAduaneroService getDistritoAduaneroService() {
		return distritoAduaneroService;
	}

	public void setDistritoAduaneroService(
			DistritoAduaneroService distritoAduaneroService) {
		this.distritoAduaneroService = distritoAduaneroService;
	}

	public List<DistritoAduanero> getAduanas() {
		if(aduanas.isEmpty())
			aduanas = distritoAduaneroService.obtenerActivos();
		return aduanas;
	}

	public void setAduanas(List<DistritoAduanero> aduanas) {
		this.aduanas = aduanas;
	}

	public TramiteEmbarqueModPagoDataManager getTramiteEmbarqueModPagoSeleccionado() {
		return tramiteEmbarqueModPagoSeleccionado;
	}

	public void setTramiteEmbarqueModPagoSeleccionado(
			TramiteEmbarqueModPagoDataManager tramiteEmbarqueModPagoSeleccionado) {
		this.tramiteEmbarqueModPagoSeleccionado = tramiteEmbarqueModPagoSeleccionado;
	}

	public TramiteEmbarqueFacturaDataManager getTramiteEmbarqueFacturaSeleccionado() {
		return tramiteEmbarqueFacturaSeleccionado;
	}

	public void setTramiteEmbarqueFacturaSeleccionado(
			TramiteEmbarqueFacturaDataManager tramiteEmbarqueFacturaSeleccionado) {
		this.tramiteEmbarqueFacturaSeleccionado = tramiteEmbarqueFacturaSeleccionado;
	}

	public TramiteEmbarqueDocDigitalService getTramiteEmbarqueDocDigitalService() {
		return tramiteEmbarqueDocDigitalService;
	}

	public void setTramiteEmbarqueDocDigitalService(
			TramiteEmbarqueDocDigitalService tramiteEmbarqueDocDigitalService) {
		this.tramiteEmbarqueDocDigitalService = tramiteEmbarqueDocDigitalService;
	}

	public String[] getDocumentosSeleccionados() {
		if (tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado() != null &&
				tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId() != null){
			str_documento = tramiteEmbarqueDesaduanamientoSeleccionado.getTramiteEmbarqueDesaduanamientoSeleccionado().getDocumentos();
			if (str_documento != null)
				documentosSeleccionados = str_documento.split(":");
		}
		return documentosSeleccionados;
	}

	public void setDocumentosSeleccionados(String[] documentosSeleccionados) {
		this.documentosSeleccionados = documentosSeleccionados;
	}

	public String getStr_documento() {
		return str_documento;
	}

	public void setStr_documento(String str_documento) {
		this.str_documento = str_documento;
	}

	public List<String> getDocumentos() {
		documentos.clear();
		if(documentos.isEmpty()) {			
			if (tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado() != null) {
				if (tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId() != null) {
					List<TramiteEmbarqueDocDigital> ldocumentos = tramiteEmbarqueDocDigitalService.obtenerPorPadre(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado());	
					for (TramiteEmbarqueDocDigital tedg : ldocumentos){
						documentos.add(tedg.getNumero().toString()); 
					}
				}
			}
		}
		return documentos;
	}

	public void setDocumentos(List<String> documentos) {
		this.documentos = documentos;
	}
	
}
