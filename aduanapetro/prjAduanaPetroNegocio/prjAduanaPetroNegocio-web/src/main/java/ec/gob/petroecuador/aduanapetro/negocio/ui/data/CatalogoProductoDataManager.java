package ec.gob.petroecuador.aduanapetro.negocio.ui.data;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.CatalogoProducto;

@Component("catalogoProductoDataManager")
@Scope("session")
public class CatalogoProductoDataManager {

	private CatalogoProducto catalogoProductoSeleccionado = new CatalogoProducto();

	public CatalogoProducto getCatalogoProductoSeleccionado() {
		return catalogoProductoSeleccionado;
	}

	public void setCatalogoProductoSeleccionado(
			CatalogoProducto catalogoProductoSeleccionado) {
		this.catalogoProductoSeleccionado = catalogoProductoSeleccionado;
	}

}
