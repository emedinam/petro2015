package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.CatalogoProducto;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.ParametroCalculo;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PrecioMarcador;
import ec.gob.petroecuador.aduanapetro.negocio.services.CatalogoProductoService;
import ec.gob.petroecuador.aduanapetro.negocio.services.ParametroCalculoService;
import ec.gob.petroecuador.aduanapetro.negocio.services.PrecioMarcadorService;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.ParametroCalculoDataManager;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("parametroCalculoController")
@Scope("request")
public class ParametroCalculoController extends RegistroComun<ParametroCalculo> {

	public ParametroCalculoController() {
		super("parametroCalculoForm.xhtml", "parametroCalculoTabla.xhtml");
	}

	@Autowired
	private ParametroCalculoService parametroCalculoService;
	
	@Autowired
	private CatalogoProductoService catalogoProductoService;
	
	@Autowired 
	private PrecioMarcadorService precioMarcadorService;
	
	@Autowired
	private ParametroCalculoDataManager parametroCalculoSeleccionado;

	private ParametroCalculo parametroCalculo = new ParametroCalculo();
	private List<ParametroCalculo> lParametroCalculos = new ArrayList<>();
	
	private List<CatalogoProducto> catalogoProductos = new ArrayList<>();
	private List<PrecioMarcador> precioMarcadors = new ArrayList<>();
	
	
	// METODOS
	public void inicializarVariables() {
		try {
			lParametroCalculos = parametroCalculoService.obtenerActivos();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String nuevoRegistro() {
		Calendar fecha = new GregorianCalendar();
		parametroCalculo = new ParametroCalculo();
		parametroCalculo.setFechaCreacion(new Date());
		parametroCalculo.setAnio(BigDecimal.valueOf(fecha.get(Calendar.YEAR)));
		parametroCalculo.setDiferencial(BigDecimal.valueOf(0.0));
		parametroCalculo.setVolumen(BigDecimal.valueOf(0.0));
		parametroCalculo.setPorcentajeGasto(BigDecimal.valueOf(0.0));
		parametroCalculo.setPorcentajeSeguro(BigDecimal.valueOf(0.0));
		parametroCalculo.setPorcentajeVariacion(BigDecimal.valueOf(0.0));
		return nuevoRegistro(parametroCalculo);
	}

	public String eliminar() {
		parametroCalculo=(ParametroCalculo)elementoSeleccionado;
		return eliminar(parametroCalculoService);
	}

	public String guardar() {
		return guardar(parametroCalculoService);
	}

	public void buscar() {
		try {
			
			List<String> criterios = new ArrayList<String>();
			
			if (parametroCalculoSeleccionado.getFiltroAnio() != null)
				if (!parametroCalculoSeleccionado.getFiltroAnio().equals(""))
					criterios.add("anio ="+parametroCalculoSeleccionado.getFiltroAnio());
			if (parametroCalculoSeleccionado.getFiltroProducto()  != null)
				if(!parametroCalculoSeleccionado.getFiltroProducto().equals(""))
					criterios.add("CatalogoProducto.id ="+parametroCalculoSeleccionado.getFiltroProducto().toString());
			criterios.add("id > 0");
			
			lParametroCalculos = parametroCalculoService.buscarPorCriterios(criterios.toArray(new String[0]));
			System.out.println("-> PararametrosCalculo encontrados: "+lParametroCalculos.size());
			if(lParametroCalculos.size()==0) {
				lParametroCalculos =  new ArrayList<ParametroCalculo>();
				addMessage("No se encontraron resultados con los filtros de busqueda seleccionados");
			}	
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ParametroCalculo getParametroCalculo() {
		return parametroCalculo;
	}

	public void setParametroCalculo(ParametroCalculo parametroCalculo) {
		this.parametroCalculo = parametroCalculo;
	}

	public List<ParametroCalculo> getlParametroCalculos() {
		if(lParametroCalculos.isEmpty())
			buscar();
			//lParametroCalculos = parametroCalculoService.obtenerActivos();
		return lParametroCalculos;
	}

	public void setlParametroCalculos(List<ParametroCalculo> lParametroCalculos) {
		this.lParametroCalculos = lParametroCalculos;
	}

	public List<CatalogoProducto> getCatalogoProductos() {
		if(catalogoProductos.isEmpty())
			catalogoProductos = catalogoProductoService.obtenerActivos();
		return catalogoProductos;
	}

	public void setCatalogoProductos(List<CatalogoProducto> catalogoProductos) {
		this.catalogoProductos = catalogoProductos;
	}

	public List<PrecioMarcador> getPrecioMarcadors() {
		if(precioMarcadors.isEmpty())
			precioMarcadors = precioMarcadorService.obtenerActivos();
		return precioMarcadors;
	}

	public void setPrecioMarcadors(List<PrecioMarcador> precioMarcadors) {
		this.precioMarcadors = precioMarcadors;
	}

	public ParametroCalculoDataManager getParametroCalculoSeleccionado() {
		return parametroCalculoSeleccionado;
	}

	public void setParametroCalculoSeleccionado(
			ParametroCalculoDataManager parametroCalculoSeleccionado) {
		this.parametroCalculoSeleccionado = parametroCalculoSeleccionado;
	}
	

}
