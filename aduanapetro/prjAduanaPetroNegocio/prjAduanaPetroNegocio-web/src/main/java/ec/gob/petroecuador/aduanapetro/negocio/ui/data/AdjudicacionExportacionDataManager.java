package ec.gob.petroecuador.aduanapetro.negocio.ui.data;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Adjudicacion;

@Component("adjudicacionExportacionDataManager")
@Scope("session")
public class AdjudicacionExportacionDataManager {

	private Adjudicacion adjudicacionSeleccionado;
	private String filtroDescripcion;
	//variables para filtros adicionales
	private Date fechaDesde;
	private Date fechaHasta;
	private String adjudicacion;
	private Integer proveedor;
	
	public Adjudicacion getAdjudicacionSeleccionado() {
		return adjudicacionSeleccionado;
	}
	public void setAdjudicacionSeleccionado(Adjudicacion adjudicacionSeleccionado) {
		this.adjudicacionSeleccionado = adjudicacionSeleccionado;
	}
	public String getFiltroDescripcion() {
		return filtroDescripcion;
	}
	public void setFiltroDescripcion(String filtroDescripcion) {
		this.filtroDescripcion = filtroDescripcion;
	}
	public Date getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public Date getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public String getAdjudicacion() {
		return adjudicacion;
	}
	public void setAdjudicacion(String adjudicacion) {
		this.adjudicacion = adjudicacion;
	}
	public Integer getProveedor() {
		return proveedor;
	}
	public void setProveedor(Integer proveedor) {
		this.proveedor = proveedor;
	}
	
	public Adjudicacion nuevoExportacion() {
		adjudicacionSeleccionado = new Adjudicacion();
		adjudicacionSeleccionado.setTipo("EXPORTACION");
		return adjudicacionSeleccionado;
	}
	
}
