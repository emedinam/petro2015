package ec.gob.petroecuador.aduanapetro.negocio.ui.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.EstadoTramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.services.EstadoTramiteEmbarqueService;

@Component("estadoTramiteEmbarqueConverter")
public class EstadoTramiteEmbarqueConverter implements Converter  {

	@Autowired
	private EstadoTramiteEmbarqueService EstadoTramiteEmbarqueService;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value == null || value.length() == 0) {
			return null;
		}
		Long id = Long.parseLong(value);
		return EstadoTramiteEmbarqueService.buscar(id);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		return value instanceof EstadoTramiteEmbarque ? ((EstadoTramiteEmbarque) value).getId().toString() : "";
	}

}