package ec.gob.petroecuador.aduanapetro.negocio.ui.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TipoDocumento;
import ec.gob.petroecuador.aduanapetro.negocio.services.TipoDocumentoService;

@Component("tipoDocumentoConverter")
public class TipoDocumentoConverter implements Converter  {

	@Autowired
	private TipoDocumentoService TipoDocumentoService;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value == null || value.length() == 0) {
			return null;
		}
		Long id = Long.parseLong(value);
		return TipoDocumentoService.buscar(id);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		return value instanceof TipoDocumento ? ((TipoDocumento) value).getId().toString() : "";
	}

}