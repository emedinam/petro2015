package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionExportacion;
import ec.gob.petroecuador.aduanapetro.negocio.services.PlanificacionExportacionService;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.AdjudicacionExportacionDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.PlanificacionExportacionDataManager;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("planificacionExportacionController")
@Scope("request")
public class PlanificacionExportacionController extends RegistroComun<PlanificacionExportacion> {

	public PlanificacionExportacionController() {
		super("adjudicacionExportacionForm.xhtml", "adjudicacionExportacionTabla.xhtml");
	}

	@Autowired
	private PlanificacionExportacionService planificacionExportacionService;

	@Autowired
	private PlanificacionExportacionDataManager planificacionExportacionSeleccionado;

	@Autowired
	private AdjudicacionExportacionDataManager adjudicacionSeleccionado;
	
	private PlanificacionExportacion planificacionExportacion;
	private List<PlanificacionExportacion> lPlanificacionExportaciones = new ArrayList<>();

	// METODOS
	public void inicializarVariables() {
		System.out.println("-> ADJUDICACION EXPORTACION "+adjudicacionSeleccionado.getAdjudicacionSeleccionado().getId());
		if (adjudicacionSeleccionado.getAdjudicacionSeleccionado().getId() != null)
			lPlanificacionExportaciones = planificacionExportacionService.obtenerPorPadre(adjudicacionSeleccionado
							.getAdjudicacionSeleccionado());
		else
			lPlanificacionExportaciones = new ArrayList<>();
	}
	
	public void nuevoRegistro() {
		nuevoRegistro(planificacionExportacionSeleccionado.nuevo());
	}
	
	public void eliminar() {
		elementoSeleccionado = planificacionExportacionSeleccionado.getPlanificacionExportacionSeleccionado();
		eliminar(planificacionExportacionService);
	}

	public void guardar() {
		elementoSeleccionado = planificacionExportacionSeleccionado.getPlanificacionExportacionSeleccionado();
		planificacionExportacion = (PlanificacionExportacion)elementoSeleccionado;
		planificacionExportacion.setAdjudicacion( adjudicacionSeleccionado.getAdjudicacionSeleccionado() );
		guardar(planificacionExportacionService);
	}

	public void buscar() {
		
	}

	public PlanificacionExportacion getPlanificacionExportacion() {
		return planificacionExportacion;
	}

	public void setPlanificacionExportacion(
			PlanificacionExportacion planificacionExportacion) {
		this.planificacionExportacion = planificacionExportacion;
	}

	public List<PlanificacionExportacion> getlPlanificacionExportaciones() {
		if(lPlanificacionExportaciones.isEmpty()) {
			inicializarVariables();
		}
		return lPlanificacionExportaciones;
	}

	public void setlPlanificacionExportaciones(
			List<PlanificacionExportacion> lPlanificacionExportaciones) {
		this.lPlanificacionExportaciones = lPlanificacionExportaciones;
	}

	public PlanificacionExportacionDataManager getPlanificacionExportacionSeleccionado() {
		return planificacionExportacionSeleccionado;
	}

	public void setPlanificacionExportacionSeleccionado(
			PlanificacionExportacionDataManager planificacionExportacionSeleccionado) {
		this.planificacionExportacionSeleccionado = planificacionExportacionSeleccionado;
	}

	public AdjudicacionExportacionDataManager getAdjudicacionSeleccionado() {
		return adjudicacionSeleccionado;
	}

	public void setAdjudicacionSeleccionado(
			AdjudicacionExportacionDataManager adjudicacionSeleccionado) {
		this.adjudicacionSeleccionado = adjudicacionSeleccionado;
	}

}