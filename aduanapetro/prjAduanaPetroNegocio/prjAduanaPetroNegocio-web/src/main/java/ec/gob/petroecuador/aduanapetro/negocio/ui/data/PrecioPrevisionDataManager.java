package ec.gob.petroecuador.aduanapetro.negocio.ui.data;


import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.ParametroCalculo;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PrecioPrevision;


@Component("precioPrevisionDataManager")
@Scope("session")
public class PrecioPrevisionDataManager {
	private PrecioPrevision precioPrevisionSeleccionado;
	private Integer filtroProducto;
	private BigDecimal filtroAnio;
	
	private ParametroCalculo selectParametroCalculo;
		
	private BigDecimal variacion;
	
	public PrecioPrevision getPrecioPrevisionSeleccionado() {
		return precioPrevisionSeleccionado;
	}
	public void setPrecioPrevisionSeleccionado(
			PrecioPrevision precioPrevisionSeleccionado) {
		this.precioPrevisionSeleccionado = precioPrevisionSeleccionado;
	}
	
	
	public Integer getFiltroProducto() {
		return filtroProducto;
	}
	public void setFiltroProducto(Integer filtroProducto) {
		this.filtroProducto = filtroProducto;
	}
	public BigDecimal getFiltroAnio() {
		return filtroAnio;
	}
	public void setFiltroAnio(BigDecimal filtroAnio) {
		this.filtroAnio = filtroAnio;
	}
	public ParametroCalculo getSelectParametroCalculo() {
		return selectParametroCalculo;
	}
	public void setSelectParametroCalculo(ParametroCalculo selectParametroCalculo) {
		this.selectParametroCalculo = selectParametroCalculo;
	}
	public BigDecimal getVariacion() {
		return variacion;
	}
	public void setVariacion(BigDecimal variacion) {
		this.variacion = variacion;
	}
	public void nuevo() {		
		precioPrevisionSeleccionado = new PrecioPrevision();
		precioPrevisionSeleccionado.setFechaCreacion(new Date());		
		refrescar();
		setSelectParametroCalculo(null);
		setVariacion(null);
	}

	public PrecioPrevision refrescar() {
		Calendar fecha = new GregorianCalendar();
		precioPrevisionSeleccionado.setAnioActual(BigDecimal.valueOf(fecha.get(Calendar.YEAR)));
		precioPrevisionSeleccionado.setGalonEnero(BigDecimal.valueOf(0.0));
		precioPrevisionSeleccionado.setGalonFebrero(BigDecimal.valueOf(0.0));
		precioPrevisionSeleccionado.setGalonMarzo(BigDecimal.valueOf(0.0));
		precioPrevisionSeleccionado.setGalonAbril(BigDecimal.valueOf(0.0));
		precioPrevisionSeleccionado.setGalonMayo(BigDecimal.valueOf(0.0));
		precioPrevisionSeleccionado.setGalonJunio(BigDecimal.valueOf(0.0));
		precioPrevisionSeleccionado.setGalonJulio(BigDecimal.valueOf(0.0));
		precioPrevisionSeleccionado.setGalonAgosto(BigDecimal.valueOf(0.0));
		precioPrevisionSeleccionado.setGalonSeptiembre(BigDecimal.valueOf(0.0));
		precioPrevisionSeleccionado.setGalonOctubre(BigDecimal.valueOf(0.0));
		precioPrevisionSeleccionado.setGalonNoviembre(BigDecimal.valueOf(0.0));
		precioPrevisionSeleccionado.setGalonDiciembre(BigDecimal.valueOf(0.0));
		
		precioPrevisionSeleccionado.setPromedio(BigDecimal.valueOf(0.0));
		precioPrevisionSeleccionado.setTotal(BigDecimal.valueOf(0.0));
		precioPrevisionSeleccionado.setDiferencial(BigDecimal.valueOf(0.0));
		precioPrevisionSeleccionado.setSubtotal(BigDecimal.valueOf(0.0));
		precioPrevisionSeleccionado.setVariacionPrecio(BigDecimal.valueOf(0.0));
		
		precioPrevisionSeleccionado.setAnioActual(BigDecimal.valueOf(fecha.get(Calendar.YEAR)));
		return precioPrevisionSeleccionado;
	}
}
