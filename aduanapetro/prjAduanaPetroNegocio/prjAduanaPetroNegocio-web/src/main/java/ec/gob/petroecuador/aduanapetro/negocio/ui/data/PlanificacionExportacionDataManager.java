package ec.gob.petroecuador.aduanapetro.negocio.ui.data;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionExportacion;

@Component("planificacionExportacionDataManager")
@Scope("session")
public class PlanificacionExportacionDataManager {
	private PlanificacionExportacion planificacionExportacionSeleccionado = new PlanificacionExportacion();
	private String filtroDescripcion;
	public PlanificacionExportacion getPlanificacionExportacionSeleccionado() {
		return planificacionExportacionSeleccionado;
	}
	public void setPlanificacionExportacionSeleccionado(
			PlanificacionExportacion planificacionExportacionSeleccionado) {
		this.planificacionExportacionSeleccionado = planificacionExportacionSeleccionado;
	}
	public String getFiltroDescripcion() {
		return filtroDescripcion;
	}
	public void setFiltroDescripcion(String filtroDescripcion) {
		this.filtroDescripcion = filtroDescripcion;
	}
	public PlanificacionExportacion nuevo() {
		planificacionExportacionSeleccionado = new PlanificacionExportacion();
		planificacionExportacionSeleccionado.setFechaRegistro(new Date());
		return planificacionExportacionSeleccionado;
	}
}
