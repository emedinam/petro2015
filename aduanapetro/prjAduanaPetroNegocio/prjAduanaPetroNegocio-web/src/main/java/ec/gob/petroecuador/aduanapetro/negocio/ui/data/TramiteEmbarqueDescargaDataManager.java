package ec.gob.petroecuador.aduanapetro.negocio.ui.data;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDescarga;

@Component("tramiteEmbarqueDescargaDataManager")
@Scope("session")
public class TramiteEmbarqueDescargaDataManager {
	
	private TramiteEmbarqueDescarga tramiteEmbarqueDescargaSeleccionado;
	
	TramiteEmbarqueDescargaDataManager() {
		tramiteEmbarqueDescargaSeleccionado = new TramiteEmbarqueDescarga(); 
	}
	
	public TramiteEmbarqueDescarga getTramiteEmbarqueDescargaSeleccionado() {
		return tramiteEmbarqueDescargaSeleccionado;
	}

	public void setTramiteEmbarqueDescargaSeleccionado(
			TramiteEmbarqueDescarga tramiteEmbarqueDescargaSeleccionado) {
		this.tramiteEmbarqueDescargaSeleccionado = tramiteEmbarqueDescargaSeleccionado;
	}
	
}

