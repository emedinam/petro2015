package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ec.gob.petroecuador.aduanapetro.negocio.enums.TipoTramiteImportacion;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Actor;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Adjudicacion;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.CompaniaInspectora;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.DistritoAduanero;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.EstadoTramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionImportacion;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDesaduanamiento;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueFactura;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueModPago;
import ec.gob.petroecuador.aduanapetro.negocio.services.ActorService;
import ec.gob.petroecuador.aduanapetro.negocio.services.AdjudicacionService;
import ec.gob.petroecuador.aduanapetro.negocio.services.CompaniaInspectoraService;
import ec.gob.petroecuador.aduanapetro.negocio.services.DistritoAduaneroService;
import ec.gob.petroecuador.aduanapetro.negocio.services.EstadoTramiteEmbarqueService;
import ec.gob.petroecuador.aduanapetro.negocio.services.PlanificacionImportacionService;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueDesaduanamientoService;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueFacturaService;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueModPagoService;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueService;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueDesaduanamientoDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueFacturaDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueModPagoDataManager;
import ec.gob.petroecuador.aduanapetro.seguridad.enums.AccionCRUD;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.utils.UtilidadFechas;

public abstract class TramiteEmbarqueComun extends RegistroComun<TramiteEmbarque>  {	
	
	protected TramiteEmbarque tramiteEmbarque = new TramiteEmbarque();
	protected List<TramiteEmbarque> lTramiteEmbarques = new ArrayList<>();
	protected TipoTramiteImportacion tipoTramite;
	protected String ventana;
	protected UtilidadFechas utilidadFechas;
	
	protected List<Adjudicacion> adjudicaciones = new ArrayList<>();
	protected List<PlanificacionImportacion> planificacionesImp = new ArrayList<>();
	protected List<Actor> actores = new ArrayList<>();
	protected List<EstadoTramiteEmbarque> estadosTramiteEmbarques = new ArrayList<>();
	protected List<CompaniaInspectora> companiasInspectoras = new ArrayList<>();
	
	public TramiteEmbarqueComun(TipoTramiteImportacion tipoTramite, String formPage, String tablePage) {
		super(TramiteEmbarque.class, formPage, tablePage);
		this.tipoTramite = tipoTramite;
		//getTramiteEmbarqueSeleccionado().setFiltroTipo(tipoTramite.name());
	}
	
	//METODOS
	public void inicializarVariables() {	
		try {
			lTramiteEmbarques = getTramiteEmbarqueService().obtenerPorTipo(tipoTramite.name());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String editar() {
		cargarDatosAdjudicacion();
		setElementoSeleccionado( getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado() );		
		return super.editar();
	}
	
	public String nuevoRegistro() {
		tramiteEmbarque = new TramiteEmbarque(); 
		getTramiteEmbarqueSeleccionado().setTramiteEmbarqueSeleccionado(tramiteEmbarque);
		//setear datos iniciales
		getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().setFechaIngreso(new Date());
		getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().setFechaCreacion(new Date());
		getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().setFechaActualizacion(new Date());
		getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().setUsuarioCreacion("usuario");
		getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().setUsuarioActualizacion("usuario");
		
		getTramiteEmbarqueModPagoSeleccionado().setTramiteEmbarqueModPagoSeleccionado( new TramiteEmbarqueModPago());
		getTramiteEmbarqueDesaduanamientoSeleccionado().setTramiteEmbarqueDesaduanamientoSeleccionado( new TramiteEmbarqueDesaduanamiento() );
		getTramiteEmbarqueFacturaSeleccionado().setTramiteEmbarqueFacturaSeleccionado( new TramiteEmbarqueFactura() );
		
		getTramiteEmbarqueFacturaController().getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado().setNumero("0");
		getTramiteEmbarqueFacturaController().getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado().setFecha(new Date());
		getTramiteEmbarqueFacturaController().getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado().setPrecio(BigDecimal.valueOf(0.0));
		getTramiteEmbarqueFacturaController().getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado().setFleteBuque(BigDecimal.valueOf(0.0));
		getTramiteEmbarqueFacturaController().getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado().setValorFob(BigDecimal.valueOf(0.0));
		getTramiteEmbarqueFacturaController().getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado().setValorDat(BigDecimal.valueOf(0.0));
		
		
	
		List<DistritoAduanero> distritosAduaneros = getDistritoAduaneroService().obtenerActivos();
		if(distritosAduaneros!=null && distritosAduaneros.size()>0)
			getTramiteEmbarqueDesaduanamientoSeleccionado().getTramiteEmbarqueDesaduanamientoSeleccionado().setDistritoAduanero( distritosAduaneros.get(0) );
		//llenar datos para tramiteEmbarqueDesaduanamiento que corresponden al proceso de IMP y que son obligatorios
		System.out.println("-> Actualizando Desaduanamiento");
		
		getTramiteEmbarqueDesaduanamientoSeleccionado().setProveedor(null);
		getTramiteEmbarqueDesaduanamientoSeleccionado().setBuque(null);
		getTramiteEmbarqueDesaduanamientoSeleccionado().setProducto(null);
		getTramiteEmbarqueDesaduanamientoSeleccionado().setVolumen(null);
		getTramiteEmbarqueDesaduanamientoSeleccionado().setFlete(null);		
		getTramiteEmbarqueDesaduanamientoSeleccionado().setValorDat(null);
		getTramiteEmbarqueDesaduanamientoSeleccionado().setCartaCredito(null);	
		getTramiteEmbarqueDesaduanamientoSeleccionado().setAdjudicacion(null);
		getTramiteEmbarqueDesaduanamientoSeleccionado().setVentanaInicio(null);		
		getTramiteEmbarqueDesaduanamientoSeleccionado().setVentanaFin(null);
		
		
		return nuevoRegistro(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado());
	}
	
	public String eliminar() {
		elementoSeleccionado = getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado();
		return eliminar(getTramiteEmbarqueService());
	}

	public void guardar(){
		Long auxIdTE = null;
		try {
			System.out.println("GUARDANDO TRAMITE EMBARQUE...");
			tramiteEmbarque = getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado();
			try {
				printObj(tramiteEmbarque);
			}catch(Exception e) {
				System.out.println(e);
			}
			auxIdTE = tramiteEmbarque.getId();

			tramiteEmbarque.setTipo(tipoTramite.name());
			
			getTramiteEmbarqueService().guardar(tramiteEmbarque);
			
			if (auxIdTE == null) {
				System.out.println("-> Actualizando ModPago");
				//llenar datos para tramiteEmbarqueModPago que corresponden al cargamento seleccionado y que son obligatorios
				if(tramiteEmbarque.getValorProforma()!=null)
					getTramiteEmbarqueModPagoController().getTramiteEmbarqueModPagoSeleccionado().getTramiteEmbarqueModPagoSeleccionado().setValorTexto(tramiteEmbarque.getValorProforma().toString());
				getTramiteEmbarqueModPagoController().getTramiteEmbarqueModPagoSeleccionado().getTramiteEmbarqueModPagoSeleccionado().setDireccionBeneficiario(tramiteEmbarque.getAdjudicacion().getActor().getDireccion());
				getTramiteEmbarqueModPagoController().getTramiteEmbarqueModPagoSeleccionado().getTramiteEmbarqueModPagoSeleccionado().setVolumenEmbarque(tramiteEmbarque.getVolumen());
				getTramiteEmbarqueModPagoController().getTramiteEmbarqueModPagoSeleccionado().getTramiteEmbarqueModPagoSeleccionado().setTolerancia(tramiteEmbarque.getAdjudicacion().getMargenTolerancia());
				getTramiteEmbarqueModPagoController().getTramiteEmbarqueModPagoSeleccionado().getTramiteEmbarqueModPagoSeleccionado().setBeneficiario(tramiteEmbarque.getAdjudicacion().getActor().getRazonSocial());
				
				printObj(getTramiteEmbarqueModPagoController().getTramiteEmbarqueModPagoSeleccionado());
				
				getTramiteEmbarqueModPagoSeleccionado().setProveedor(tramiteEmbarque.getAdjudicacion().getActor().getRazonSocial());
				
				System.out.println("-> Actualizando Factura");
				//llenar datos para tramiteEmbarqueFactura que corresponden a la factura y que son obligatorios
				getTramiteEmbarqueFacturaController().getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado().setVolumen(tramiteEmbarque.getVolumen());
				
				//printObj(getTramiteEmbarqueFacturaController().getTramiteEmbarqueFacturaSeleccionado());
			}
			
			//llenar datos para tramiteEmbarqueDesaduanamiento que corresponden al proceso de IMP y que son obligatorios
			System.out.println("-> Actualizando Desaduanamiento");
			if (tramiteEmbarque.getAdjudicacion() != null)
				getTramiteEmbarqueDesaduanamientoSeleccionado().setProveedor(tramiteEmbarque.getAdjudicacion().getActor().getRazonSocial());
			if (tramiteEmbarque.getNombreBuque() != null)
				getTramiteEmbarqueDesaduanamientoSeleccionado().setBuque(tramiteEmbarque.getNombreBuque());
			if (tramiteEmbarque.getAdjudicacion() != null)
				getTramiteEmbarqueDesaduanamientoSeleccionado().setProducto(tramiteEmbarque.getAdjudicacion().getCatalogoProducto().getNombre());
			if (getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado()!= null)
				getTramiteEmbarqueDesaduanamientoSeleccionado().setVolumen(getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado().getVolumen());
			if (getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado()!= null)
				getTramiteEmbarqueDesaduanamientoSeleccionado().setFlete(getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado().getFleteBuque());		
			if (getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado()!= null)
				getTramiteEmbarqueDesaduanamientoSeleccionado().setValorDat(getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado().getValorDat());
			if (getTramiteEmbarqueModPagoSeleccionado().getTramiteEmbarqueModPagoSeleccionado()!= null)
				getTramiteEmbarqueDesaduanamientoSeleccionado().setCartaCredito(getTramiteEmbarqueModPagoSeleccionado().getTramiteEmbarqueModPagoSeleccionado().getNumero());	
			if (tramiteEmbarque.getAdjudicacion()!= null)
				getTramiteEmbarqueDesaduanamientoSeleccionado().setAdjudicacion(tramiteEmbarque.getAdjudicacion().getNumeroContrato());
			if (tramiteEmbarque.getPlanificacionImportacion()!= null)
				getTramiteEmbarqueDesaduanamientoSeleccionado().setVentanaInicio(tramiteEmbarque.getPlanificacionImportacion().getVentanaInicio());		
			if (tramiteEmbarque.getPlanificacionImportacion()!= null)
				getTramiteEmbarqueDesaduanamientoSeleccionado().setVentanaFin(tramiteEmbarque.getPlanificacionImportacion().getVentanaFin());
			
			//printObj(getTramiteEmbarqueDesaduanamientoSeleccionado());
			
			inicializarVariables();
			saveRegistro(AccionCRUD.Registrado, tramiteEmbarque.getId());
			System.out.println("TRAMITE EMBARQUE GUARDADO!");
		} catch (Exception e) {
			addError(e);
		}
	}
	
	public void buscar() {
		try {			
			List<String> criterios = new ArrayList<String>();
			
			if (getTramiteEmbarqueSeleccionado().getFiltroFechaIngTra() != null)
				if (!getTramiteEmbarqueSeleccionado().getFiltroFechaIngTra().equals(""))
					criterios.add("fechaIngreso >='"+getDateOnly(getTramiteEmbarqueSeleccionado().getFiltroFechaIngTra())+"'");
			if (getTramiteEmbarqueSeleccionado().getFiltroFechaFinTra() != null)
				if (!getTramiteEmbarqueSeleccionado().getFiltroFechaFinTra().equals(""))
					criterios.add("fechaIngreso <='"+getDateOnly(getTramiteEmbarqueSeleccionado().getFiltroFechaFinTra())+"'");
			if (getTramiteEmbarqueSeleccionado().getFiltroFechaIniVen() != null)
				if (!getTramiteEmbarqueSeleccionado().getFiltroFechaIniVen().equals(""))
					criterios.add("fechaAdjudicacion >='"+getDateOnly(getTramiteEmbarqueSeleccionado().getFiltroFechaIniVen())+"'");
			if (getTramiteEmbarqueSeleccionado().getFiltroFechaFinVen() != null)
				if (!getTramiteEmbarqueSeleccionado().getFiltroFechaFinVen().equals(""))
					criterios.add("fechaAdjudicacion <='"+getDateOnly(getTramiteEmbarqueSeleccionado().getFiltroFechaFinVen())+"'");
			if (getTramiteEmbarqueSeleccionado().getFiltroProveedor() != null)
				if (!getTramiteEmbarqueSeleccionado().getFiltroProveedor().equals(""))
					criterios.add("Adjudicacion.Actor.id = "+getTramiteEmbarqueSeleccionado().getFiltroProveedor());
			if (getTramiteEmbarqueSeleccionado().getFiltroAdjudicacion() != null)
				if (!getTramiteEmbarqueSeleccionado().getFiltroAdjudicacion().equals(""))
					criterios.add("Adjudicacion.id = "+getTramiteEmbarqueSeleccionado().getFiltroAdjudicacion());
			if (getTramiteEmbarqueSeleccionado().getFiltroTramite() != null) 
				if (!getTramiteEmbarqueSeleccionado().getFiltroTramite().isEmpty())
				criterios.add("numero = "+getTramiteEmbarqueSeleccionado().getFiltroTramite());
			criterios.add("tipo = '"+tipoTramite.name()+"'");
			
			lTramiteEmbarques = getTramiteEmbarqueService().buscarPorCriterios(criterios.toArray(new String[0]));
			System.out.println("-> Tramites encontrados: "+lTramiteEmbarques.size());
			if(lTramiteEmbarques.size()==0)	{
				lTramiteEmbarques = null;
				addMessage("No se encontraron resultados con los filtros de busqueda seleccionados");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getDateOnly(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		return formatter.format(date);
		
	}
	
	public void generarDocumento(){
		
	}
	
	public void setearBanderaTipoGarantia(){
		
	}
	
	public void cargarPlanificacionImp() {
		try {
			System.out.println("AQUI CARGANDO PLANIFICACIONES...");
			planificacionesImp.clear();
			if (getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado() != null) {
				if (getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion() != null){
					System.out.println("-> ADJUDICACION "+getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion().getId());
					planificacionesImp = getPlanificacionImportacionService().obtenerPorPadre(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion());
				}	
				else {
					planificacionesImp = new ArrayList<PlanificacionImportacion>();
					System.out.println("-> NO HAY ADJUDICACION");
				}
			}else {
				planificacionesImp = new ArrayList<PlanificacionImportacion>();
				System.out.println("-> NO HAY EMBARQUE");
			}
			setElementoSeleccionado( getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado() );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void cargarDatosAdjudicacion() {
		//Adjudicacion
		System.out.println("getTramiteEmbarqueSeleccionado().AD "+getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion());
		
		if(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado()!= null &&
				getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion()!=null) {
			System.out.println("->ADJUDICACION: "+getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion().getId());
			getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().setFechaAdjudicacion(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion().getFecha());
			getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().setNegociacion(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion().getCondicion());	
			getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().setProducto(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion().getCatalogoProducto().getNombre());
		}
		else {
			System.out.println("-> NO HAY ADJUDICACION SELECCIONADO");
		}
		
		if(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado()!= null &&
				getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getPlanificacionImportacion()!=null) {
			if (getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getPlanificacionImportacion().getVolumenBls() == null)
				getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().setVolumen(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getPlanificacionImportacion().getVolumenTnl());
			else
				getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().setVolumen(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getPlanificacionImportacion().getVolumenBls());
		}
		else {
			System.out.println("-> NO HAY PLANIFICACION SELECCIONADO");
		}
		
		if(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado()!= null &&
				getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getId()!=null) {
			System.out.println("->TRAMITE: "+getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getId());
			List<TramiteEmbarqueModPago> tramitesModPago = getTramiteEmbarqueModPagoService().obtenerPorPadre(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado());
			if(tramitesModPago!=null && tramitesModPago.size()>0)
				getTramiteEmbarqueModPagoSeleccionado().setTramiteEmbarqueModPagoSeleccionado(tramitesModPago.get(0));
			else{
				if(tramiteEmbarque.getValorProforma()!=null)
					getTramiteEmbarqueModPagoController().getTramiteEmbarqueModPagoSeleccionado().getTramiteEmbarqueModPagoSeleccionado().setValorTexto(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getValorProforma().toString());
				getTramiteEmbarqueModPagoController().getTramiteEmbarqueModPagoSeleccionado().getTramiteEmbarqueModPagoSeleccionado().setDireccionBeneficiario(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion().getActor().getDireccion());
				getTramiteEmbarqueModPagoController().getTramiteEmbarqueModPagoSeleccionado().getTramiteEmbarqueModPagoSeleccionado().setVolumenEmbarque(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getVolumen());
				getTramiteEmbarqueModPagoController().getTramiteEmbarqueModPagoSeleccionado().getTramiteEmbarqueModPagoSeleccionado().setTolerancia(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion().getMargenTolerancia());
				getTramiteEmbarqueModPagoController().getTramiteEmbarqueModPagoSeleccionado().getTramiteEmbarqueModPagoSeleccionado().setBeneficiario(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion().getActor().getRazonSocial());
				
			}
			
			List<TramiteEmbarqueFactura> tramitesFactura = getTramiteEmbarqueFacturaService().obtenerPorPadre(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado());
			if(tramitesFactura!=null && tramitesFactura.size()>0)
				getTramiteEmbarqueFacturaSeleccionado().setTramiteEmbarqueFacturaSeleccionado( tramitesFactura.get(0) );
			else {
				getTramiteEmbarqueFacturaController().getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado().setNumero("0");
				getTramiteEmbarqueFacturaController().getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado().setFecha(new Date());
				getTramiteEmbarqueFacturaController().getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado().setPrecio(BigDecimal.valueOf(0.0));
				getTramiteEmbarqueFacturaController().getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado().setFleteBuque(BigDecimal.valueOf(0.0));
				getTramiteEmbarqueFacturaController().getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado().setValorFob(BigDecimal.valueOf(0.0));
				getTramiteEmbarqueFacturaController().getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado().setValorDat(BigDecimal.valueOf(0.0));
			}
			
			List<TramiteEmbarqueDesaduanamiento> tramitesDesaduanamiento = getTramiteEmbarqueDesaduanamientoService().obtenerPorPadre(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado());
			if(tramitesDesaduanamiento!=null && tramitesDesaduanamiento.size()>0)
				getTramiteEmbarqueDesaduanamientoSeleccionado().setTramiteEmbarqueDesaduanamientoSeleccionado( tramitesDesaduanamiento.get(0) );
			else {
				List<DistritoAduanero> distritosAduaneros = getDistritoAduaneroService().obtenerActivos();
				if(distritosAduaneros!=null && distritosAduaneros.size()>0)
					getTramiteEmbarqueDesaduanamientoSeleccionado().getTramiteEmbarqueDesaduanamientoSeleccionado().setDistritoAduanero( distritosAduaneros.get(0) );
				
				//llenar datos para tramiteEmbarqueDesaduanamiento que corresponden al proceso de IMP y que son obligatorios
				System.out.println("-> Actualizando Desaduanamiento");
				if (getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion() != null)
					getTramiteEmbarqueDesaduanamientoSeleccionado().setProveedor(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion().getActor().getRazonSocial());
				if (getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getNombreBuque() != null)
					getTramiteEmbarqueDesaduanamientoSeleccionado().setBuque(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getNombreBuque());
				if (getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion() != null)
					getTramiteEmbarqueDesaduanamientoSeleccionado().setProducto(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion().getCatalogoProducto().getNombre());
				if (getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado()!= null)
					getTramiteEmbarqueDesaduanamientoSeleccionado().setVolumen(getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado().getVolumen());				
				if (getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado()!= null)
					getTramiteEmbarqueDesaduanamientoSeleccionado().setFlete(getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado().getFleteBuque());		
				if (getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado()!= null)
					getTramiteEmbarqueDesaduanamientoSeleccionado().setValorDat(getTramiteEmbarqueFacturaSeleccionado().getTramiteEmbarqueFacturaSeleccionado().getValorDat());
				if (getTramiteEmbarqueModPagoSeleccionado().getTramiteEmbarqueModPagoSeleccionado()!= null)
					getTramiteEmbarqueDesaduanamientoSeleccionado().setCartaCredito(getTramiteEmbarqueModPagoSeleccionado().getTramiteEmbarqueModPagoSeleccionado().getNumero());	
				if (getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion()!= null)
					getTramiteEmbarqueDesaduanamientoSeleccionado().setAdjudicacion(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion().getNumeroContrato());
				if (getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getPlanificacionImportacion()!= null)
					getTramiteEmbarqueDesaduanamientoSeleccionado().setVentanaInicio(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getPlanificacionImportacion().getVentanaInicio());		
				if (getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getPlanificacionImportacion()!= null)
					getTramiteEmbarqueDesaduanamientoSeleccionado().setVentanaFin(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getPlanificacionImportacion().getVentanaFin());
				
				printObj(getTramiteEmbarqueDesaduanamientoSeleccionado());
			}
		}
		else {
			System.out.println("-> NO HAY EMBARQUE SELECCIONADO");
		}
		
		setElementoSeleccionado(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado());
	}

	//GETS Y SETS
	public abstract TramiteEmbarqueDataManager getTramiteEmbarqueSeleccionado();

	public String getVentana() {
		return ventana;
	}

	public void setVentana(String ventana) {
		this.ventana = ventana;
	}

	public abstract TramiteEmbarqueDesaduanamientoDataManager getTramiteEmbarqueDesaduanamientoSeleccionado();
	
	public abstract TramiteEmbarqueFacturaDataManager getTramiteEmbarqueFacturaSeleccionado();
	
	public abstract TramiteEmbarqueModPagoDataManager getTramiteEmbarqueModPagoSeleccionado();

	public abstract TramiteEmbarqueFacturaController getTramiteEmbarqueFacturaController();
	
	public abstract TramiteEmbarqueModPagoController getTramiteEmbarqueModPagoController();
	
	public abstract TramiteEmbarqueFacturaService getTramiteEmbarqueFacturaService();
	
	public abstract TramiteEmbarqueDesaduanamientoService getTramiteEmbarqueDesaduanamientoService();
	
	public void setlTramiteEmbarques(List<TramiteEmbarque> lTramiteEmbarques) {
		this.lTramiteEmbarques = lTramiteEmbarques;
	}
	
	public List<TramiteEmbarque> getlTramiteEmbarques() {
		if(lTramiteEmbarques==null) {
			lTramiteEmbarques = new ArrayList<>();
			return lTramiteEmbarques;
		}
		if(lTramiteEmbarques.isEmpty())
			inicializarVariables();
		return lTramiteEmbarques;
	}
	
	public List<PlanificacionImportacion> getPlanificacionesImp() {
		if(planificacionesImp.isEmpty()) {
			if (getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado() != null){
				if (getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion() != null){
					planificacionesImp = getPlanificacionImportacionService().obtenerPorPadre(getTramiteEmbarqueSeleccionado().getTramiteEmbarqueSeleccionado().getAdjudicacion());
				}	
				else
					planificacionesImp = getPlanificacionImportacionService().obtenerActivos();
			}else
				planificacionesImp = getPlanificacionImportacionService().obtenerActivos();
		}
		return planificacionesImp;
	}
	
	public List<Adjudicacion> getAdjudicaciones() {
		adjudicaciones.clear();
		if(adjudicaciones.isEmpty())
			System.out.println("-> llenar adjudicaciones");
			//adjudicaciones = getAdjudicacionService().obtenerActivos();
			String recTipo = tipoTramite.name().toString();
			String resTipo = "";
			
			if (recTipo.equals("CARTA_CREDITO"))
				resTipo ="CARTA DE CREDITO";
			if (recTipo.equals("GIRO_DIRECTOCC"))
				resTipo ="GIRO DIRECTO CARTA STAND BY";
			if (recTipo.equals("GIRO_DIRECTO"))
				resTipo ="GIRO DIRECTO";
			if (recTipo.equals("CONVENIO"))
				resTipo ="CONVENIO";
			if (recTipo.equals("PAGO_ANTICIPADO"))
				resTipo ="PAGO ANTICIPADO";
			
			adjudicaciones = getAdjudicacionService().obtenerPorTipoYTipoPago("IMPORTACION",resTipo);
			System.out.println("adjudicaciones Rec="+adjudicaciones.size());
			
		return adjudicaciones;
	}
	
	public List<Actor> getActores() {
		if(actores.isEmpty())
			actores = getActorService().obtenerActivos();
		return actores;
	}
	
	public List<EstadoTramiteEmbarque> getEstadosTramiteEmbarques() {
		if(estadosTramiteEmbarques.isEmpty())
			estadosTramiteEmbarques = getEstadoTramiteEmbarqueService().obtenerActivos();
		return estadosTramiteEmbarques;
	}
	
	public abstract EstadoTramiteEmbarqueService getEstadoTramiteEmbarqueService();

	public abstract TramiteEmbarqueService getTramiteEmbarqueService();
	
	public abstract AdjudicacionService getAdjudicacionService();

	public abstract PlanificacionImportacionService getPlanificacionImportacionService();

	public abstract ActorService getActorService();

	public abstract CompaniaInspectoraService getCompaniaInspectoraService();

	public abstract DistritoAduaneroService getDistritoAduaneroService();
	
	public abstract TramiteEmbarqueModPagoService getTramiteEmbarqueModPagoService();

	public List<CompaniaInspectora> getCompaniasInspectoras() {
		if(companiasInspectoras.isEmpty())
			companiasInspectoras = getCompaniaInspectoraService().obtenerActivos();
		return companiasInspectoras;
	}

	public void setCompaniasInspectoras(
			List<CompaniaInspectora> companiasInspectoras) {
		this.companiasInspectoras = companiasInspectoras;
	}

	public void setTramiteEmbarque(TramiteEmbarque tramiteEmbarque) {
		this.tramiteEmbarque = tramiteEmbarque;
	}

	public void setAdjudicaciones(List<Adjudicacion> adjudicaciones) {
		this.adjudicaciones = adjudicaciones;
	}

	public void setPlanificacionesImp(
			List<PlanificacionImportacion> planificacionesImp) {
		this.planificacionesImp = planificacionesImp;
	}

	public void setActores(List<Actor> actores) {
		this.actores = actores;
	}

	public void setEstadosTramiteEmbarques(
			List<EstadoTramiteEmbarque> estadosTramiteEmbarques) {
		this.estadosTramiteEmbarques = estadosTramiteEmbarques;
	}

	public TramiteEmbarque getTramiteEmbarque() {
		return tramiteEmbarque;
	}

	public UtilidadFechas getUtilidadFechas() {
		return utilidadFechas;
	}

	public void setUtilidadFechas(UtilidadFechas utilidadFechas) {
		this.utilidadFechas = utilidadFechas;
	}

	public TipoTramiteImportacion getTipoTramite() {
		return tipoTramite;
	}

	public void setTipoTramite(TipoTramiteImportacion tipoTramite) {
		this.tipoTramite = tipoTramite;
	}

}
