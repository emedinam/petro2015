package ec.gob.petroecuador.aduanapetro.negocio.ui.data;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueCarga;

@Component("tramiteEmbarqueCargaDataManager")
@Scope("session")
public class TramiteEmbarqueCargaDataManager {
	
	private TramiteEmbarqueCarga tramiteEmbarqueCargaSeleccionado;

	TramiteEmbarqueCargaDataManager() {
		tramiteEmbarqueCargaSeleccionado = new TramiteEmbarqueCarga(); 
	}
	
	public TramiteEmbarqueCarga getTramiteEmbarqueCargaSeleccionado() {
		return tramiteEmbarqueCargaSeleccionado;
	}

	public void setTramiteEmbarqueCargaSeleccionado(
			TramiteEmbarqueCarga tramiteEmbarqueCargaSeleccionado) {
		this.tramiteEmbarqueCargaSeleccionado = tramiteEmbarqueCargaSeleccionado;
	}
		
	public TramiteEmbarqueCarga nuevo() {
		tramiteEmbarqueCargaSeleccionado = new TramiteEmbarqueCarga();
		tramiteEmbarqueCargaSeleccionado.setFecha(new Date());
		// TODO Revisar esta linea
		//tramiteEmbarqueCargaSeleccionado.setProducto(tramiteEmbarqueCargaSeleccionado.getProducto());
		return tramiteEmbarqueCargaSeleccionado;
	}
}

