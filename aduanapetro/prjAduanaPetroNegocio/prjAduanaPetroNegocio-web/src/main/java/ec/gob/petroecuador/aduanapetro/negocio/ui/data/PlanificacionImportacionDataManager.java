package ec.gob.petroecuador.aduanapetro.negocio.ui.data;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Adjudicacion;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionImportacion;

@Component("planificacionImportacionDataManager")
@Scope("session")
public class PlanificacionImportacionDataManager {

	private PlanificacionImportacion planificacionImportacionSeleccionado = new PlanificacionImportacion();
	private String filtroDescripcion;
	private String adjudicacion;
	private Integer proveedor;
	
	public PlanificacionImportacion getPlanificacionImportacionSeleccionado() {
		return planificacionImportacionSeleccionado;
	}
	public void setPlanificacionImportacionSeleccionado(
			PlanificacionImportacion planificacionImportacionSeleccionado) {
		this.planificacionImportacionSeleccionado = planificacionImportacionSeleccionado;
	}
	public String getFiltroDescripcion() {
		return filtroDescripcion;
	}
	public void setFiltroDescripcion(String filtroDescripcion) {
		this.filtroDescripcion = filtroDescripcion;
	}
	public PlanificacionImportacion nuevo(Adjudicacion adjudicacion) {
		planificacionImportacionSeleccionado = new PlanificacionImportacion();
		planificacionImportacionSeleccionado.setTipoPago(adjudicacion.getTipoPago());
		planificacionImportacionSeleccionado.setEmpresaProveedora(adjudicacion.getActor().getRazonSocial());
		return planificacionImportacionSeleccionado;
	}
	public String getAdjudicacion() {
		return adjudicacion;
	}
	public void setAdjudicacion(String adjudicacion) {
		this.adjudicacion = adjudicacion;
	}
	public Integer getProveedor() {
		return proveedor;
	}
	public void setProveedor(Integer proveedor) {
		this.proveedor = proveedor;
	}
}
