package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Actor;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Adjudicacion;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.CatalogoProducto;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Formula;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Pais;
import ec.gob.petroecuador.aduanapetro.negocio.services.ActorService;
import ec.gob.petroecuador.aduanapetro.negocio.services.AdjudicacionService;
import ec.gob.petroecuador.aduanapetro.negocio.services.CatalogoProductoService;
import ec.gob.petroecuador.aduanapetro.negocio.services.FormulaService;
import ec.gob.petroecuador.aduanapetro.negocio.services.PaisService;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.AdjudicacionExportacionDataManager;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("adjudicacionExportacionController")
@Scope("request")
public class AdjudicacionExportacionController extends RegistroComun<Adjudicacion> {

	public AdjudicacionExportacionController() {
		super("adjudicacionExportacionForm.xhtml", "adjudicacionExportacionTabla.xhtml","AdjudicacionExportacion");
	}

	@Autowired
	private AdjudicacionService adjudicacionService;

	@Autowired
	private ActorService actorService;

	@Autowired
	private PaisService paisService;

	@Autowired
	private FormulaService formulaService;

	@Autowired
	private CatalogoProductoService catalogoProductoService;

	@Autowired
	private AdjudicacionExportacionDataManager adjudicacionSeleccionado;
	
	private Adjudicacion adjudicacion = new Adjudicacion();
	private List<Adjudicacion> lAdjudicaciones = new ArrayList<Adjudicacion>();
	private List<CatalogoProducto> catalogoProductos = new ArrayList<CatalogoProducto>();
	private List<Pais> paises = new ArrayList<Pais>();
	private List<Formula> formulas = new ArrayList<Formula>();
	private List<Actor> actores = new ArrayList<Actor>();

	// METODOS
	public void inicializarVariables() {
		lAdjudicaciones = adjudicacionService.obtenerPorTipo("EXPORTACION");
	}

	public String nuevoRegistro() {
		adjudicacion = adjudicacionSeleccionado.nuevoExportacion();
		return nuevoRegistro(adjudicacion);
	}

	public String editar() {
		setElementoSeleccionado( adjudicacionSeleccionado.getAdjudicacionSeleccionado() ); 
		return super.editar();
	}
	
	public String eliminar() {
		elementoSeleccionado = adjudicacionSeleccionado.getAdjudicacionSeleccionado();
		return eliminar(adjudicacionService);
	}

	public void guardar() {
		guardar(adjudicacionService);
	}

	public void buscar() {
		try {
			List<String> criterios = new ArrayList<String>();
			String tipoAdj = "EXPORTACION";
			
			if (adjudicacionSeleccionado.getFechaDesde() != null)
				if (!adjudicacionSeleccionado.getFechaDesde().equals(""))
					criterios.add("fecha >='"+getDateOnly(adjudicacionSeleccionado.getFechaDesde())+"'");
			if (adjudicacionSeleccionado.getFechaHasta() != null)
				if(!adjudicacionSeleccionado.getFechaHasta().equals(""))
					criterios.add("fecha <='"+getDateOnly(adjudicacionSeleccionado.getFechaHasta())+"'");
			if (adjudicacionSeleccionado.getAdjudicacion() != null)
				if(adjudicacionSeleccionado.getAdjudicacion() != "")
					criterios.add("numeroContrato like '%"+adjudicacionSeleccionado.getAdjudicacion()+"%'");
			if (adjudicacionSeleccionado.getProveedor() != null)
				if(!adjudicacionSeleccionado.getProveedor().equals(""))
					criterios.add("Actor.id = "+adjudicacionSeleccionado.getProveedor());
			criterios.add("tipo = '"+tipoAdj+"'");
			
			/*String[] criterios = {"numeroContrato like '%"+adjudicacionSeleccionado.getAdjudicacion()+"%'",
								  "Actor.id = "+adjudicacionSeleccionado.getProveedor(),
								  //"fecha >= '"+adjudicacionSeleccionado.getFechaDesde()+"'",
								  //"fecha <= '"+adjudicacionSeleccionado.getFechaHasta()+"'",
								  "tipo = 'EXPORTACION'"};
			lAdjudicaciones = adjudicacionService.buscarPorCriterios(criterios);*/
			
			lAdjudicaciones = adjudicacionService.buscarPorCriterios(criterios.toArray(new String[0]));
			System.out.println("-> Adjudicaciones Exportacion encontradas: "+lAdjudicaciones.size());
			if(lAdjudicaciones.size()==0)	{
				lAdjudicaciones = null;
				addMessage("No se encontraron resultados con los filtros de busqueda seleccionados");
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getDateOnly(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		return formatter.format(date);
		
	}
	

	public Adjudicacion getAdjudicacion() {
		return adjudicacion;
	}

	public void setAdjudicacion(Adjudicacion adjudicacion) {
		this.adjudicacion = adjudicacion;
	}

	public List<Adjudicacion> getlAdjudicaciones() {
		if(lAdjudicaciones.isEmpty())
			inicializarVariables();
		return lAdjudicaciones;
	}

	public void setlAdjudicaciones(List<Adjudicacion> lAdjudicaciones) {
		this.lAdjudicaciones = lAdjudicaciones;
	}

	public List<CatalogoProducto> getCatalogoProductos() {
		if(catalogoProductos.isEmpty())
			catalogoProductos = catalogoProductoService.obtenerActivos();
		return catalogoProductos;
	}

	public void setCatalogoProductos(List<CatalogoProducto> catalogoProductos) {
		this.catalogoProductos = catalogoProductos;
	}

	public List<Pais> getPaises() {
		if(paises.isEmpty())
			paises = paisService.obtenerActivos();
		return paises;
	}

	public void setPaises(List<Pais> paises) {
		this.paises = paises;
	}

	public List<Formula> getFormulas() {
		if(formulas.isEmpty())
			formulas = formulaService.obtenerActivos();
		return formulas;
	}

	public void setFormulas(List<Formula> formulas) {
		this.formulas = formulas;
	}

	public List<Actor> getActores() {
		if(actores.isEmpty())
			actores = actorService.obtenerActivos();
		return actores;
	}

	public void setActores(List<Actor> actores) {
		this.actores = actores;
	}

	public AdjudicacionExportacionDataManager getAdjudicacionSeleccionado() {
		return adjudicacionSeleccionado;
	}

	public void setAdjudicacionSeleccionado(
			AdjudicacionExportacionDataManager adjudicacionSeleccionado) {
		this.adjudicacionSeleccionado = adjudicacionSeleccionado;
	}

}
