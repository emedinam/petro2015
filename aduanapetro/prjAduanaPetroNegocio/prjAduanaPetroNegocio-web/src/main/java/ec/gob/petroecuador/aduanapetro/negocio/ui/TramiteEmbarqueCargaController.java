package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueCarga;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueCargaService;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueService;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueCargaDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueDataManager;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;


@Component("tramiteEmbarqueCargaController")
@Scope("request")
public class TramiteEmbarqueCargaController  extends RegistroComun<TramiteEmbarqueCarga> {

	public TramiteEmbarqueCargaController() {
		super("", "");
	}

	@Autowired
	private TramiteEmbarqueCargaService tramiteEmbarqueCargaService;

	@Autowired
	private TramiteEmbarqueService tramiteEmbarqueService;
	
	@Autowired
	private TramiteEmbarqueDataManager tramiteEmbarqueSeleccionado;
	
	@Autowired
	private TramiteEmbarqueCargaDataManager tramiteEmbarqueCargaSeleccionado;
	
	private TramiteEmbarqueCarga tramiteEmbarqueCarga = new TramiteEmbarqueCarga();
	private List<TramiteEmbarqueCarga> lTramiteEmbarqueCargas = new ArrayList<>();
	
	// METODOS
	public void inicializarVariables() {
		System.out.println("CARGA - TRAMITE SELECCIONADO "+(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado()!=null 
				&& tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId() != null? 
						tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId() : "NINGUNO" ));
		if (tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado() != null 
			&& tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId() != null) {
			lTramiteEmbarqueCargas = tramiteEmbarqueCargaService.obtenerPorPadre(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado());
			System.out.println("CARGA - BUSCANDO POR PADRE...");
		}
		else
			lTramiteEmbarqueCargas = new ArrayList<TramiteEmbarqueCarga>();
	}	

	public void nuevoRegistro() {
		nuevoRegistro(tramiteEmbarqueCargaSeleccionado.nuevo());		
		tramiteEmbarqueCargaSeleccionado.getTramiteEmbarqueCargaSeleccionado().setProducto(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getAdjudicacion().getCatalogoProducto().getNombre());
	}

	public void eliminar() {
		elementoSeleccionado = tramiteEmbarqueCargaSeleccionado.getTramiteEmbarqueCargaSeleccionado();
		eliminar(tramiteEmbarqueCargaService);
	}

	public void guardar() {
		tramiteEmbarqueCargaSeleccionado.getTramiteEmbarqueCargaSeleccionado().setTramiteEmbarque( tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado() );
		elementoSeleccionado = tramiteEmbarqueCargaSeleccionado.getTramiteEmbarqueCargaSeleccionado();
		guardar(tramiteEmbarqueCargaService);	
	}
	
	public void generarDocumento(){
		
	}
	public TramiteEmbarqueCarga getTramiteEmbarqueCarga() {
		return tramiteEmbarqueCarga;
	}

	public void setTramiteEmbarqueCarga(TramiteEmbarqueCarga tramiteEmbarqueCarga) {
		this.tramiteEmbarqueCarga = tramiteEmbarqueCarga;
	}

	public List<TramiteEmbarqueCarga> getlTramiteEmbarqueCargas() {
		//if(this.lTramiteEmbarqueCargas.isEmpty())
		inicializarVariables();
		return lTramiteEmbarqueCargas;
	}

	public void setlTramiteEmbarqueCargas(
			List<TramiteEmbarqueCarga> lTramiteEmbarqueCargas) {
		this.lTramiteEmbarqueCargas = lTramiteEmbarqueCargas;
	}

	public TramiteEmbarqueCargaService getTramiteEmbarqueCargaService() {
		return tramiteEmbarqueCargaService;
	}

	public void setTramiteEmbarqueCargaService(
			TramiteEmbarqueCargaService tramiteEmbarqueCargaService) {
		this.tramiteEmbarqueCargaService = tramiteEmbarqueCargaService;
	}

	public TramiteEmbarqueService getTramiteEmbarqueService() {
		return tramiteEmbarqueService;
	}

	public void setTramiteEmbarqueService(
			TramiteEmbarqueService tramiteEmbarqueService) {
		this.tramiteEmbarqueService = tramiteEmbarqueService;
	}

	public TramiteEmbarqueDataManager getTramiteEmbarqueSeleccionado() {
		return tramiteEmbarqueSeleccionado;
	}

	public void setTramiteEmbarqueSeleccionado(
			TramiteEmbarqueDataManager tramiteEmbarqueSeleccionado) {
		this.tramiteEmbarqueSeleccionado = tramiteEmbarqueSeleccionado;
	}

	public TramiteEmbarqueCargaDataManager getTramiteEmbarqueCargaSeleccionado() {
		return tramiteEmbarqueCargaSeleccionado;
	}

	public void setTramiteEmbarqueCargaSeleccionado(
			TramiteEmbarqueCargaDataManager tramiteEmbarqueCargaSeleccionado) {
		this.tramiteEmbarqueCargaSeleccionado = tramiteEmbarqueCargaSeleccionado;
	}	
	
}
