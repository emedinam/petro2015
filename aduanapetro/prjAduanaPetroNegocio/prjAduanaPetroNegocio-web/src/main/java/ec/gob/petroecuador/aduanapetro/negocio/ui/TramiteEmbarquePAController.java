package ec.gob.petroecuador.aduanapetro.negocio.ui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.enums.TipoTramiteImportacion;
import ec.gob.petroecuador.aduanapetro.negocio.services.ActorService;
import ec.gob.petroecuador.aduanapetro.negocio.services.AdjudicacionService;
import ec.gob.petroecuador.aduanapetro.negocio.services.CompaniaInspectoraService;
import ec.gob.petroecuador.aduanapetro.negocio.services.DistritoAduaneroService;
import ec.gob.petroecuador.aduanapetro.negocio.services.EstadoTramiteEmbarqueService;
import ec.gob.petroecuador.aduanapetro.negocio.services.PlanificacionImportacionService;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueDesaduanamientoService;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueFacturaService;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueModPagoService;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueService;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueDesaduanamientoDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueFacturaDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueModPagoDataManager;

@Component("tramiteEmbarquePAController")
@Scope("request")
public class TramiteEmbarquePAController extends TramiteEmbarqueComun {
	
	@Autowired
	private TramiteEmbarqueService tramiteEmbarqueService;
	
	@Autowired
	private TramiteEmbarqueModPagoService tramiteEmbarqueModPagoService;
	
	@Autowired
	private AdjudicacionService adjudicacionService;
	
	@Autowired
	private PlanificacionImportacionService planificacionImportacionService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private EstadoTramiteEmbarqueService estadoTramiteEmbarqueService;
	
	@Autowired
	private CompaniaInspectoraService companiaInspectoraService;
	
	@Autowired
	private DistritoAduaneroService distritoAduaneroService;

	@Autowired
	private TramiteEmbarqueDataManager tramiteEmbarqueSeleccionado;
	
	@Autowired
	private TramiteEmbarqueDesaduanamientoDataManager tramiteEmbarqueDesaduanamientoSeleccionado;
	
	@Autowired
	private TramiteEmbarqueFacturaDataManager tramiteEmbarqueFacturaSeleccionado;
	
	@Autowired
	private TramiteEmbarqueModPagoDataManager tramiteEmbarqueModPagoSeleccionado;
	
	@Autowired
	private TramiteEmbarqueModPagoController tramiteEmbarqueModPagoController;
	
	@Autowired
	TramiteEmbarqueFacturaController tramiteEmbarqueFacturaController;
	
	@Autowired
	private TramiteEmbarqueFacturaService tramiteEmbarqueFacturaService;
	
	@Autowired
	private TramiteEmbarqueDesaduanamientoService tramiteEmbarqueDesaduanamientoService;
	
	public TramiteEmbarquePAController() {
		super(TipoTramiteImportacion.PAGO_ANTICIPADO, "tramiteEmbarquePAForm.xhtml", "tramiteEmbarquePATabla.xhtml");
	}
	
	//GETS Y SETS
	public TramiteEmbarqueService getTramiteEmbarqueService() {
		return tramiteEmbarqueService;
	}

	public void setTramiteEmbarqueService(
			TramiteEmbarqueService tramiteEmbarqueService) {
		this.tramiteEmbarqueService = tramiteEmbarqueService;
	}

	public AdjudicacionService getAdjudicacionService() {
		return adjudicacionService;
	}

	public ActorService getActorService() {
		return actorService;
	}

	public TramiteEmbarqueDataManager getTramiteEmbarqueSeleccionado() {
		return tramiteEmbarqueSeleccionado;
	}

	public void setTramiteEmbarqueSeleccionado(
			TramiteEmbarqueDataManager tramiteEmbarqueSeleccionado) {
		this.tramiteEmbarqueSeleccionado = tramiteEmbarqueSeleccionado;
	}

	public EstadoTramiteEmbarqueService getEstadoTramiteEmbarqueService() {
		return estadoTramiteEmbarqueService;
	}

	public String getVentana() {
		return ventana;
	}

	public void setVentana(String ventana) {
		this.ventana = ventana;
	}

	public PlanificacionImportacionService getPlanificacionImportacionService() {
		return planificacionImportacionService;
	}

	public void setPlanificacionImportacionService(
			PlanificacionImportacionService planificacionImportacionService) {
		this.planificacionImportacionService = planificacionImportacionService;
	}

	public CompaniaInspectoraService getCompaniaInspectoraService() {
		return companiaInspectoraService;
	}

	public void setCompaniaInspectoraService(
			CompaniaInspectoraService companiaInspectoraService) {
		this.companiaInspectoraService = companiaInspectoraService;
	}

	public TramiteEmbarqueModPagoService getTramiteEmbarqueModPagoService() {
		return tramiteEmbarqueModPagoService;
	}

	public void setTramiteEmbarqueModPagoService(
			TramiteEmbarqueModPagoService tramiteEmbarqueModPagoService) {
		this.tramiteEmbarqueModPagoService = tramiteEmbarqueModPagoService;
	}

	public TramiteEmbarqueModPagoDataManager getTramiteEmbarqueModPagoSeleccionado() {
		return tramiteEmbarqueModPagoSeleccionado;
	}

	public void setTramiteEmbarqueModPagoSeleccionado(
			TramiteEmbarqueModPagoDataManager tramiteEmbarqueModPagoSeleccionado) {
		this.tramiteEmbarqueModPagoSeleccionado = tramiteEmbarqueModPagoSeleccionado;
	}

	public TramiteEmbarqueDesaduanamientoDataManager getTramiteEmbarqueDesaduanamientoSeleccionado() {
		return tramiteEmbarqueDesaduanamientoSeleccionado;
	}

	public void setTramiteEmbarqueDesaduanamientoSeleccionado(
			TramiteEmbarqueDesaduanamientoDataManager tramiteEmbarqueDesaduanamientoSeleccionado) {
		this.tramiteEmbarqueDesaduanamientoSeleccionado = tramiteEmbarqueDesaduanamientoSeleccionado;
	}

	public TramiteEmbarqueFacturaDataManager getTramiteEmbarqueFacturaSeleccionado() {
		return tramiteEmbarqueFacturaSeleccionado;
	}

	public void setTramiteEmbarqueFacturaSeleccionado(
			TramiteEmbarqueFacturaDataManager tramiteEmbarqueFacturaSeleccionado) {
		this.tramiteEmbarqueFacturaSeleccionado = tramiteEmbarqueFacturaSeleccionado;
	}

	public DistritoAduaneroService getDistritoAduaneroService() {
		return distritoAduaneroService;
	}

	public void setDistritoAduaneroService(
			DistritoAduaneroService distritoAduaneroService) {
		this.distritoAduaneroService = distritoAduaneroService;
	}

	public TramiteEmbarqueModPagoController getTramiteEmbarqueModPagoController() {
		return tramiteEmbarqueModPagoController;
	}

	public void setTramiteEmbarqueModPagoController(
			TramiteEmbarqueModPagoController tramiteEmbarqueModPagoController) {
		this.tramiteEmbarqueModPagoController = tramiteEmbarqueModPagoController;
	}

	public TramiteEmbarqueFacturaController getTramiteEmbarqueFacturaController() {
		return tramiteEmbarqueFacturaController;
	}

	public void setTramiteEmbarqueFacturaController(
			TramiteEmbarqueFacturaController tramiteEmbarqueFacturaController) {
		this.tramiteEmbarqueFacturaController = tramiteEmbarqueFacturaController;
	}

	public TramiteEmbarqueFacturaService getTramiteEmbarqueFacturaService() {
		return tramiteEmbarqueFacturaService;
	}

	public void setTramiteEmbarqueFacturaService(
			TramiteEmbarqueFacturaService tramiteEmbarqueFacturaService) {
		this.tramiteEmbarqueFacturaService = tramiteEmbarqueFacturaService;
	}

	public TramiteEmbarqueDesaduanamientoService getTramiteEmbarqueDesaduanamientoService() {
		return tramiteEmbarqueDesaduanamientoService;
	}

	public void setTramiteEmbarqueDesaduanamientoService(
			TramiteEmbarqueDesaduanamientoService tramiteEmbarqueDesaduanamientoService) {
		this.tramiteEmbarqueDesaduanamientoService = tramiteEmbarqueDesaduanamientoService;
	}		
}
