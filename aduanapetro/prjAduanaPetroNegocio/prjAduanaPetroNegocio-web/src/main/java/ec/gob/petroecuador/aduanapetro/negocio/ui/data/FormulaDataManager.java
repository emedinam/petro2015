package ec.gob.petroecuador.aduanapetro.negocio.ui.data;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Formula;

@Component("formulaDataManager")
@Scope("session")
public class FormulaDataManager {

	private Formula formulaSeleccionado = new Formula();
	private String filtroDescripcion;
	public Formula getFormulaSeleccionado() {
		return formulaSeleccionado;
	}
	public void setFormulaSeleccionado(Formula formulaSeleccionado) {
		this.formulaSeleccionado = formulaSeleccionado;
	}
	public String getFiltroDescripcion() {
		return filtroDescripcion;
	}
	public void setFiltroDescripcion(String filtroDescripcion) {
		this.filtroDescripcion = filtroDescripcion;
	}

}
