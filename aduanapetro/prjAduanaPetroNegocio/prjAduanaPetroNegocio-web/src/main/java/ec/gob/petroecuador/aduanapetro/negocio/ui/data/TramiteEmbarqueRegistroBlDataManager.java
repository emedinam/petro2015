package ec.gob.petroecuador.aduanapetro.negocio.ui.data;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueRegistroBl;

@Component("tramiteEmbarqueRegistroBlDataManager")
@Scope("session")
public class TramiteEmbarqueRegistroBlDataManager {
	
	private TramiteEmbarqueRegistroBl tramiteEmbarqueRegistroBlSeleccionado;

	TramiteEmbarqueRegistroBlDataManager() {
		tramiteEmbarqueRegistroBlSeleccionado = new TramiteEmbarqueRegistroBl();
	}
	
	public TramiteEmbarqueRegistroBl getTramiteEmbarqueRegistroBlSeleccionado() {
		return tramiteEmbarqueRegistroBlSeleccionado;
	}

	public void setTramiteEmbarqueRegistroBlSeleccionado(
			TramiteEmbarqueRegistroBl tramiteEmbarqueRegistroBlSeleccionado) {
		this.tramiteEmbarqueRegistroBlSeleccionado = tramiteEmbarqueRegistroBlSeleccionado;
	}

}

