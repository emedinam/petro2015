package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.PrecioMarcador;
import ec.gob.petroecuador.aduanapetro.negocio.services.PrecioMarcadorService;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("precioMarcadorController")
@Scope("request")
public class PrecioMarcadorController  extends RegistroComun<PrecioMarcador> {

	public PrecioMarcadorController() {
		super("precioMarcadorForm.xhtml", "precioMarcadorTabla.xhtml");
	}
	
	@Autowired
	private PrecioMarcadorService precioMarcadorService;
	
	private PrecioMarcador precioMarcador;
	private List<PrecioMarcador> lPrecioMarcadors = new ArrayList<PrecioMarcador>();
	
	public void inicializarVariables() {	
		try {
			precioMarcador = new PrecioMarcador();
			getlPrecioMarcadors();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String nuevoRegistro(){
		precioMarcador = new PrecioMarcador();
		precioMarcador.setFechaCreacion(new Date());
		return nuevoRegistro(precioMarcador);
	}
	
	public String eliminar() {
		return eliminar(precioMarcadorService);
	}

	public String guardar(){
		return guardar(precioMarcadorService);
	}
	
	public PrecioMarcador getPrecioMarcador() {
		return precioMarcador;
	}
	public void setPrecioMarcador(PrecioMarcador precioMarcador) {
		this.precioMarcador = precioMarcador;
	}
	public List<PrecioMarcador> getlPrecioMarcadors() {
		if(lPrecioMarcadors.isEmpty())
			lPrecioMarcadors = precioMarcadorService.obtenerActivos();
		return lPrecioMarcadors;
	}
	public void setlPrecioMarcadors(List<PrecioMarcador> lPrecioMarcadors) {
		this.lPrecioMarcadors = lPrecioMarcadors;
	}
	
}
