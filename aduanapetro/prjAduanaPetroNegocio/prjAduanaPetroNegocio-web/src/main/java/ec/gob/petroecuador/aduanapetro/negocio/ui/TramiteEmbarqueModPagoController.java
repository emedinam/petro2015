package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueModPago;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueModPagoService;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueDesaduanamientoDataManager;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.TramiteEmbarqueModPagoDataManager;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("tramiteEmbarqueModPagoController")
@Scope("request")
public class TramiteEmbarqueModPagoController extends RegistroComun<TramiteEmbarqueModPago>{
	
	public TramiteEmbarqueModPagoController() {
		super("", "");
	}

	@Autowired
	private TramiteEmbarqueModPagoService tramiteEmbarqueModPagoService;
	
	@Autowired
	private TramiteEmbarqueModPagoDataManager tramiteEmbarqueModPagoSeleccionado;
	
	@Autowired
	private TramiteEmbarqueDataManager tramiteEmbarqueSeleccionado;
	
	private TramiteEmbarqueModPago tramiteEmbarqueModPago =  new TramiteEmbarqueModPago();
	
	private List<TramiteEmbarqueModPago> lTramiteEmbarquesModPago = new ArrayList<TramiteEmbarqueModPago>();
	
	@Autowired
	private TramiteEmbarqueDesaduanamientoDataManager tramiteEmbarqueDesaduanamientoSeleccionado;
	
	//METODOS
	public void inicializarVariables() {	
		try {
			if (tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado() != null && 
				tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getId() != null){
				System.out.println("INGRESA ACA 1");
				lTramiteEmbarquesModPago = tramiteEmbarqueModPagoService.obtenerPorPadre(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado());
				if (!lTramiteEmbarquesModPago.isEmpty()) {
					tramiteEmbarqueModPago = lTramiteEmbarquesModPago.get(0);
					tramiteEmbarqueModPagoSeleccionado.setTramiteEmbarqueModPagoSeleccionado(tramiteEmbarqueModPago);
				}
				else {
					System.out.println("INGRESA ACA 2");
					tramiteEmbarqueModPago = new TramiteEmbarqueModPago();
					//llenarDatosIniciales
					tramiteEmbarqueModPago.setValorTexto(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getValorProforma().toString());
					tramiteEmbarqueModPago.setDireccionBeneficiario(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getAdjudicacion().getActor().getDireccion());
					tramiteEmbarqueModPago.setVolumenEmbarque(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getVolumen());
					tramiteEmbarqueModPago.setTolerancia(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getAdjudicacion().getMargenTolerancia());
					tramiteEmbarqueModPago.setBeneficiario(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getAdjudicacion().getActor().getRazonSocial());
					
					tramiteEmbarqueModPagoSeleccionado.setTramiteEmbarqueModPagoSeleccionado(tramiteEmbarqueModPago);
					tramiteEmbarqueModPagoSeleccionado.setProveedor(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getAdjudicacion().getActor().getRazonSocial());
				}
			}
			else {
				System.out.println("INGRESA ACA 3");
				lTramiteEmbarquesModPago = new ArrayList<TramiteEmbarqueModPago>();
				tramiteEmbarqueModPago = new TramiteEmbarqueModPago();
				tramiteEmbarqueModPagoSeleccionado.setTramiteEmbarqueModPagoSeleccionado(new TramiteEmbarqueModPago());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void nuevoRegistro(){
		tramiteEmbarqueModPagoSeleccionado.setTramiteEmbarqueModPagoSeleccionado(new TramiteEmbarqueModPago());
		nuevoRegistro(tramiteEmbarqueModPagoSeleccionado.getTramiteEmbarqueModPagoSeleccionado());
	}
	
	public void eliminar() {
		elementoSeleccionado = tramiteEmbarqueModPagoSeleccionado.getTramiteEmbarqueModPagoSeleccionado();
		eliminar(tramiteEmbarqueModPagoService);
	}

	public void guardar(){
		tramiteEmbarqueModPagoSeleccionado.getTramiteEmbarqueModPagoSeleccionado().setTramiteEmbarque( tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado() );
		elementoSeleccionado = tramiteEmbarqueModPagoSeleccionado.getTramiteEmbarqueModPagoSeleccionado();
		guardar(tramiteEmbarqueModPagoService);
		
		//llenar datos para tramiteEmbarqueDesaduanamiento que corresponden al proceso de IMP y que son obligatorios
		System.out.println("-> Actualizando Desaduanamiento Datos de CC");
		tramiteEmbarqueDesaduanamientoSeleccionado.setCartaCredito(tramiteEmbarqueModPagoSeleccionado.getTramiteEmbarqueModPagoSeleccionado().getNumero());	
		//printObj(tramiteEmbarqueDesaduanamientoSeleccionado);
	}
		
	public void generarDocumento(){
		
	}
	
	public void actualizarDatosEmbarque(){
		tramiteEmbarqueModPago.setValorTexto(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getValorProforma().toString());
		tramiteEmbarqueModPago.setDireccionBeneficiario(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getAdjudicacion().getActor().getDireccion());
		tramiteEmbarqueModPago.setVolumenEmbarque(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getVolumen());
		tramiteEmbarqueModPago.setTolerancia(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getAdjudicacion().getMargenTolerancia());
		tramiteEmbarqueModPago.setBeneficiario(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getAdjudicacion().getActor().getRazonSocial());
		tramiteEmbarqueModPagoSeleccionado.setTramiteEmbarqueModPagoSeleccionado(tramiteEmbarqueModPago);
		tramiteEmbarqueModPagoSeleccionado.setProveedor(tramiteEmbarqueSeleccionado.getTramiteEmbarqueSeleccionado().getAdjudicacion().getActor().getRazonSocial());
	}
	
	//GETS Y SETS
	
	public TramiteEmbarqueModPagoService getTramiteEmbarqueModPagoService() {
		return tramiteEmbarqueModPagoService;
	}

	public void setTramiteEmbarqueModPagoService(
			TramiteEmbarqueModPagoService tramiteEmbarqueModPagoService) {
		this.tramiteEmbarqueModPagoService = tramiteEmbarqueModPagoService;
	}

	public TramiteEmbarqueModPago getTramiteEmbarqueModPago() {
		return tramiteEmbarqueModPago;
	}

	public void setTramiteEmbarqueModPago(
			TramiteEmbarqueModPago tramiteEmbarqueModPago) {
		this.tramiteEmbarqueModPago = tramiteEmbarqueModPago;
	}

	public List<TramiteEmbarqueModPago> getlTramiteEmbarquesModPago() {
		if(lTramiteEmbarquesModPago.isEmpty())
			inicializarVariables();
		return lTramiteEmbarquesModPago;
	}

	public void setlTramiteEmbarquesModPago(
			List<TramiteEmbarqueModPago> lTramiteEmbarquesModPago) {
		this.lTramiteEmbarquesModPago = lTramiteEmbarquesModPago;
	}

	public TramiteEmbarqueModPagoDataManager getTramiteEmbarqueModPagoSeleccionado() {
		return tramiteEmbarqueModPagoSeleccionado;
	}

	public void setTramiteEmbarqueModPagoSeleccionado(
			TramiteEmbarqueModPagoDataManager tramiteEmbarqueModPagoSeleccionado) {
		this.tramiteEmbarqueModPagoSeleccionado = tramiteEmbarqueModPagoSeleccionado;
	}

	public TramiteEmbarqueDataManager getTramiteEmbarqueSeleccionado() {
		return tramiteEmbarqueSeleccionado;
	}

	public void setTramiteEmbarqueSeleccionado(
			TramiteEmbarqueDataManager tramiteEmbarqueSeleccionado) {
		this.tramiteEmbarqueSeleccionado = tramiteEmbarqueSeleccionado;
	}

	public TramiteEmbarqueDesaduanamientoDataManager getTramiteEmbarqueDesaduanamientoSeleccionado() {
		return tramiteEmbarqueDesaduanamientoSeleccionado;
	}

	public void setTramiteEmbarqueDesaduanamientoSeleccionado(
			TramiteEmbarqueDesaduanamientoDataManager tramiteEmbarqueDesaduanamientoSeleccionado) {
		this.tramiteEmbarqueDesaduanamientoSeleccionado = tramiteEmbarqueDesaduanamientoSeleccionado;
	}
		
}
