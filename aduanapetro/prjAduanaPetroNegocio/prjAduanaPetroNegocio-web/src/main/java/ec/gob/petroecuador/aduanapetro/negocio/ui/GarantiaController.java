package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.enums.TipoGarantia;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Actor;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Adjudicacion;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Garantia;
import ec.gob.petroecuador.aduanapetro.negocio.services.ActorService;
import ec.gob.petroecuador.aduanapetro.negocio.services.AdjudicacionService;
import ec.gob.petroecuador.aduanapetro.negocio.services.GarantiaService;
import ec.gob.petroecuador.aduanapetro.negocio.ui.data.GarantiaDataManager;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("garantiaController")
@Scope("request")
public class GarantiaController extends RegistroComun<Garantia> {
	
	@Autowired
	private GarantiaService garantiaService;
	@Autowired
	private AdjudicacionService adjudicacionService;
	@Autowired
	private ActorService actorService;
	@Autowired
	private GarantiaDataManager garantiaSeleccionado;
	
	private Garantia garantia = new Garantia();
	private List<Garantia> lGarantias = new ArrayList<>();
	
	private List<SelectItem> tipoGarantiaSelectItems = new ArrayList<>();
	private List<Adjudicacion> adjudicaciones = new ArrayList<>();
	private List<Actor> actores = new ArrayList<>();
	
	public GarantiaController() {
		super("garantiaForm.xhtml", "garantiaTabla.xhtml");
	}
	
	//METODOS
	public void inicializarVariables() {	
		try {
			garantia = garantiaSeleccionado.getGarantiaSeleccionado();
			lGarantias = garantiaService.obtenerActivos();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String editar() {
		setElementoSeleccionado( garantiaSeleccionado.getGarantiaSeleccionado() );
		setearBanderaTipoGarantia();
		return super.editar();
	}
	
	public String nuevoRegistro() {
		garantiaSeleccionado.nuevo();
		return nuevoRegistro(garantiaSeleccionado.getGarantiaSeleccionado());
	}
	
	public String eliminar() {
		elementoSeleccionado = garantiaSeleccionado.getGarantiaSeleccionado();
		return eliminar(garantiaService);
	}

	public String guardar() {
		garantia = (Garantia)elementoSeleccionado;
		if(garantiaSeleccionado.getAuxFechaVencimiento()!=null)
			garantia.setFechaVencimiento(garantiaSeleccionado.getAuxFechaVencimiento());
		return guardar(garantiaService);
		
	}
	
	public void guardarNoCerrar() {
		garantia = (Garantia)elementoSeleccionado;
		if(garantiaSeleccionado.getAuxFechaVencimiento()!=null)
			garantia.setFechaVencimiento(garantiaSeleccionado.getAuxFechaVencimiento());
		guardar(garantiaService);
		
	}
	
	public void buscar() {
		try {
			String[] criterios = {"numeroGarantia like '%"+garantiaSeleccionado.getFiltroAval()+"%'",
								  "Actor.id = "+garantiaSeleccionado.getFiltroProveedor(),
								  "Adjudicacion.id = "+garantiaSeleccionado.getFiltroAdjudicacion()};
			lGarantias = garantiaService.buscarPorCriterios(criterios);
			System.out.println("-> Garantias encontradas: "+lGarantias.size());
			if(lGarantias.size()==0)	{
				lGarantias = null;
				addMessage("No se encontraron resultados con los filtros de busqueda seleccionados");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void calcularFechaVencimiento(){
		garantiaSeleccionado.setGarantiaSeleccionado((Garantia)elementoSeleccionado);
		Date fechaInicial = garantiaSeleccionado.getGarantiaSeleccionado().getFechaVencimiento();
		garantiaSeleccionado.setAuxFechaVencimiento(fechaInicial);
		Integer dias = Integer.parseInt(garantiaSeleccionado.getGarantiaSeleccionado().getPlazoRenovacion().toString());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fechaInicial);
		calendar.add(Calendar.DAY_OF_YEAR, dias);
		//Date fechaFinal = utilidadFechas.sumarRestarDiasFecha(new Date(), dias);
		Date fechaFinal = calendar.getTime();
		garantiaSeleccionado.setAuxFechaVencimiento(fechaFinal);
	}
	
	public void asignarDatos(){
		garantiaSeleccionado.setGarantiaSeleccionado((Garantia)elementoSeleccionado);
		garantiaSeleccionado.setAuxFechaVencimiento(garantiaSeleccionado.getGarantiaSeleccionado().getFechaVencimiento());
		
		
	}
	
	public void setearBanderaTipoGarantia(){
		if (garantiaSeleccionado.getGarantiaSeleccionado().getTipo() != null){	
			if(garantiaSeleccionado.getGarantiaSeleccionado().getTipo().equals("SERIEDAD_OFERTA"))
				garantiaSeleccionado.setBanderaTipoGarantia(true);
			else
				garantiaSeleccionado.setBanderaTipoGarantia(false);
		}
	}
	
	public String obtenerEstado(Garantia garantia){
		String estadoProceso = "";
		Date fechaActual = new Date();
		Integer cmp = garantia.getFechaVencimiento().compareTo(fechaActual);
		
		if (garantia.getFechaDevolucion() != null){
			estadoProceso = "DEVUELTA";
		}else{
			if (garantia.getFechaDevolucion() == null && cmp >= 0){
				estadoProceso = "VIGENTE";
			}else{
				if (garantia.getFechaDevolucion() == null && cmp < 0){
					estadoProceso = "VENCIDA";
				}else{
					estadoProceso = "ND";
				}
			}
		}
		return estadoProceso;
	}
	
	public void cargarDatosAdjudicacion() {
		//Adjudicacion
		System.out.println("garantiaSeleccionado.AD "+garantiaSeleccionado.getGarantiaSeleccionado().getAdjudicacion());
		
		if(garantiaSeleccionado.getGarantiaSeleccionado()!= null &&
				garantiaSeleccionado.getGarantiaSeleccionado().getAdjudicacion()!=null) {
			System.out.println("->ADJUDICACION: "+garantiaSeleccionado.getGarantiaSeleccionado().getAdjudicacion().getId());
			garantiaSeleccionado.getGarantiaSeleccionado().setFechaFax(garantiaSeleccionado.getGarantiaSeleccionado().getAdjudicacion().getFecha());
			garantiaSeleccionado.getGarantiaSeleccionado().setProducto(garantiaSeleccionado.getGarantiaSeleccionado().getAdjudicacion().getCatalogoProducto().getNombre());
			garantiaSeleccionado.getGarantiaSeleccionado().setVolumenTotal(garantiaSeleccionado.getGarantiaSeleccionado().getAdjudicacion().getVolumenTotal());
			garantiaSeleccionado.getGarantiaSeleccionado().setTolerancia(garantiaSeleccionado.getGarantiaSeleccionado().getAdjudicacion().getMargenTolerancia());
			garantiaSeleccionado.getGarantiaSeleccionado().setCantidadCargamento(garantiaSeleccionado.getGarantiaSeleccionado().getAdjudicacion().getNumeroCargamento());
			garantiaSeleccionado.getGarantiaSeleccionado().setVolumenCargamento(garantiaSeleccionado.getGarantiaSeleccionado().getAdjudicacion().getVolumenAproximado());
			
		}
		else {
			System.out.println("-> NO HAY ADJUDICACION SELECCIONADO");
		}
		
	}
	
	//GETS Y SETS
	public GarantiaDataManager getGarantiaSeleccionado() {
		return garantiaSeleccionado;
	}
	public void setGarantiaSeleccionado(GarantiaDataManager garantiaSeleccionado) {
		this.garantiaSeleccionado = garantiaSeleccionado;
	}
	public Garantia getGarantia() {
		return garantia;
	}
	public void setGarantia(Garantia garantia) {
		this.garantia = garantia;
	}
	public List<Garantia> getlGarantias() {
		if(lGarantias.isEmpty())
			inicializarVariables();
		return lGarantias;
	}
	public void setlGarantias(List<Garantia> lGarantias) {
		this.lGarantias = lGarantias;
	}
	public List<SelectItem> getTipoGarantiaSelectItems() {
		if(tipoGarantiaSelectItems == null || tipoGarantiaSelectItems.isEmpty()) {
			TipoGarantia[] tg = TipoGarantia.values();
			for (TipoGarantia item : tg) {
				tipoGarantiaSelectItems.add(new SelectItem(item, item.toString()));
			}
		}
		return tipoGarantiaSelectItems;
	}
	public void setTipoGarantiaSelectItems(List<SelectItem> tipoGarantiaSelectItems) {
		this.tipoGarantiaSelectItems = tipoGarantiaSelectItems;
	}
	public List<Adjudicacion> getAdjudicaciones() {
		if(adjudicaciones.isEmpty())
			adjudicaciones = adjudicacionService.obtenerActivos();
		return adjudicaciones;
	}
	public void setAdjudicaciones(List<Adjudicacion> adjudicaciones) {
		this.adjudicaciones = adjudicaciones;
	}
	public List<Actor> getActores() {
		if(actores.isEmpty())
			actores = actorService.obtenerActivos();
		return actores;
	}
	public void setActores(List<Actor> actores) {
		this.actores = actores;
	}
	
}
