package ec.gob.petroecuador.aduanapetro.negocio.ui.data;


import java.util.Calendar;
import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Garantia;

@Component("garantiaDataManager")
@Scope("session")
public class GarantiaDataManager {
	
	private Garantia garantiaSeleccionado;
	private String filtroAval;
	private Integer filtroAdjudicacion;
	private Integer filtroProveedor;
	private Date auxFechaVencimiento;
	private Boolean banderaTipoGarantia;
	
	
	public Garantia getGarantiaSeleccionado() {
		return garantiaSeleccionado;
	}
	public void setGarantiaSeleccionado(Garantia garantiaSeleccionado) {
		this.garantiaSeleccionado = garantiaSeleccionado;
	}
	public String getFiltroAval() {
		return filtroAval;
	}
	public void setFiltroAval(String filtroAval) {
		this.filtroAval = filtroAval;
	}
	public Integer getFiltroAdjudicacion() {
		return filtroAdjudicacion;
	}
	public void setFiltroAdjudicacion(Integer filtroAdjudicacion) {
		this.filtroAdjudicacion = filtroAdjudicacion;
	}
	public Integer getFiltroProveedor() {
		return filtroProveedor;
	}
	public void setFiltroProveedor(Integer filtroProveedor) {
		this.filtroProveedor = filtroProveedor;
	}
	public Date getAuxFechaVencimiento() {
		return auxFechaVencimiento;
	}
	public void setAuxFechaVencimiento(Date auxFechaVencimiento) {
		this.auxFechaVencimiento = auxFechaVencimiento;
	}
	public Boolean getBanderaTipoGarantia() {
		return banderaTipoGarantia;
	}
	public void setBanderaTipoGarantia(Boolean banderaTipoGarantia) {
		this.banderaTipoGarantia = banderaTipoGarantia;
	}
	public void nuevo() {
		garantiaSeleccionado = new Garantia();
		//Asignar por defecto la fecha de vencimiento a 90 dias a partir de la fecha actual del registro 
		Date fechaInicial = new Date();
		Integer dias = 90;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fechaInicial);
		calendar.add(Calendar.DAY_OF_YEAR, dias);
		Date fechaFinal = calendar.getTime();
		garantiaSeleccionado.setFechaVencimiento(fechaFinal);
		
	}
}

