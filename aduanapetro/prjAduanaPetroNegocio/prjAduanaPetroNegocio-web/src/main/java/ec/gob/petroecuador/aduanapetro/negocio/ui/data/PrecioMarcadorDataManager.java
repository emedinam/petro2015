package ec.gob.petroecuador.aduanapetro.negocio.ui.data;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.PrecioMarcador;

@Component("precioMarcadorDataManager")
@Scope("session")
public class PrecioMarcadorDataManager {

	private PrecioMarcador precioMarcadorSeleccionado = new PrecioMarcador();

	public PrecioMarcador getPrecioMarcadorSeleccionado() {
		return precioMarcadorSeleccionado;
	}

	public void setPrecioMarcadorSeleccionado(
			PrecioMarcador precioMarcadorSeleccionado) {
		this.precioMarcadorSeleccionado = precioMarcadorSeleccionado;
	}
}
