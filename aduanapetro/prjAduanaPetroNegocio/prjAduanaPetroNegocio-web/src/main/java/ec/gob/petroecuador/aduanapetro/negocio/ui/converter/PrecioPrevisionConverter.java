package ec.gob.petroecuador.aduanapetro.negocio.ui.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.PrecioPrevision;
import ec.gob.petroecuador.aduanapetro.negocio.services.PrecioPrevisionService;

@Component("precioPrevisionConverter")
public class PrecioPrevisionConverter implements Converter  {

	@Autowired
	private PrecioPrevisionService PrecioPrevisionService;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value == null || value.length() == 0) {
			return null;
		}
		Long id = Long.parseLong(value);
		return PrecioPrevisionService.buscar(id);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		return value instanceof PrecioPrevision ? ((PrecioPrevision) value).getId().toString() : "";
	}

}