package ec.gob.petroecuador.aduanapetro.negocio.ui;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Factura;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.FacturaRetencion;
import ec.gob.petroecuador.aduanapetro.negocio.services.FacturaRetencionService;
import ec.gob.petroecuador.aduanapetro.seguridad.ui.generic.RegistroComun;

@Component("facturaRetencionController")
@Scope("request")
public class FacturaRetencionController extends RegistroComun<FacturaRetencion> {
	
	public FacturaRetencionController() {
		super("", "");
		// TODO Auto-generated constructor stub
	}
	@Autowired
	private FacturaRetencionService facturaRetencionService;
	
	private FacturaRetencion facturaRetencion;
	private List<FacturaRetencion> lFacturaRetenciones = new ArrayList<FacturaRetencion>();
	
	
	//METODOS
	public void inicializarVariables() {	
		facturaRetencion = new FacturaRetencion();
		getlFacturaRetenciones();
	}
	
	public void nuevoRegistro(){
		facturaRetencion = new FacturaRetencion();
	}
	
	public void eliminar() {
		eliminar(facturaRetencionService);
	}

	public void guardar(){
		guardar(facturaRetencionService);
	}
	
	public void obtenerFila() {
		//facturaRetencionSeleccionado.setFacturaRetencionSeleccionado(facturaRetencion);
		inicializarVariables();
	}

	
	//GETS Y SETS
	public FacturaRetencionService getFacturaRetencionService() {
		return facturaRetencionService;
	}
	public void setFacturaRetencionService(
			FacturaRetencionService facturaRetencionService) {
		this.facturaRetencionService = facturaRetencionService;
	}
	public FacturaRetencion getFacturaRetencion() {
		return facturaRetencion;
	}
	public void setFacturaRetencion(FacturaRetencion facturaRetencion) {
		this.facturaRetencion = facturaRetencion;
	}
	public List<FacturaRetencion> getlFacturaRetenciones() {
		if(lFacturaRetenciones.isEmpty()) {
			Factura padre = (Factura)getElemento(Factura.class.getSimpleName());
			if(padre != null)
				lFacturaRetenciones = facturaRetencionService.obtenerPorPadre(padre);
		}
		return lFacturaRetenciones;
	}
	public void setlFacturaRetenciones(List<FacturaRetencion> lFacturaRetenciones) {
		this.lFacturaRetenciones = lFacturaRetenciones;
	}
		
}
