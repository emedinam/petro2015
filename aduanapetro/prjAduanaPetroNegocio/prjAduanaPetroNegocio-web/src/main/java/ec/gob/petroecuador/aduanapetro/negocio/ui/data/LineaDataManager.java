package ec.gob.petroecuador.aduanapetro.negocio.ui.data;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Linea;

@Component("lineaDataManager")
@Scope("session")
public class LineaDataManager {
	
	private Linea lineaSeleccionado = new Linea();

	public Linea getLineaSeleccionado() {
		return lineaSeleccionado;
	}

	public void setLineaSeleccionado(Linea lineaSeleccionado) {
		this.lineaSeleccionado = lineaSeleccionado;
	}

	
}

