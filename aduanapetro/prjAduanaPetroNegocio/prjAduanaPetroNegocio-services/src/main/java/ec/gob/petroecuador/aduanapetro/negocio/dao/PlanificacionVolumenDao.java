

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionVolumen;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase PlanificacionVolumenDao
 @autor dgonzalez
*/
public interface PlanificacionVolumenDao extends GenericDAO<PlanificacionVolumen, Long> {
	
}

   