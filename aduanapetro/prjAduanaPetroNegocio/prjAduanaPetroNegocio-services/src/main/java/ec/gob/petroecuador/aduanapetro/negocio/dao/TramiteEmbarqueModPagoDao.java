

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueModPago;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase TramiteEmbarqueModPagoDao
 @autor dgonzalez
*/
public interface TramiteEmbarqueModPagoDao extends GenericDAO<TramiteEmbarqueModPago, Long> {
	
}

   