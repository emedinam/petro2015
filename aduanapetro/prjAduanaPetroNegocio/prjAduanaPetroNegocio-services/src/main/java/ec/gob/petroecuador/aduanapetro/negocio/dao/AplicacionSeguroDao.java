

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.AplicacionSeguro;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase AplicacionSeguroDao
 @autor dgonzalez
*/
public interface AplicacionSeguroDao extends GenericDAO<AplicacionSeguro, Long> {
	
}

   