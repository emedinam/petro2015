
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.PlanificacionImportacionDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionImportacion;

/**
 Clase PlanificacionImportacionDao
 @autor dgonzalez
*/
@Component("planificacionimportacionDao")
public class PlanificacionImportacionDaoImpl extends GenericDAOImpl<PlanificacionImportacion, Long> implements PlanificacionImportacionDao {
	
}
   