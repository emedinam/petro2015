

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDesaduanamiento;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase TramiteEmbarqueDesaduanamientoDao
 @autor dgonzalez
*/
public interface TramiteEmbarqueDesaduanamientoDao extends GenericDAO<TramiteEmbarqueDesaduanamiento, Long> {
	
}

   