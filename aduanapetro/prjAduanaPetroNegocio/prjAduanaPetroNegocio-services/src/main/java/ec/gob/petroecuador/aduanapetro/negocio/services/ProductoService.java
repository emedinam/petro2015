
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Producto;

/**
 Clase ProductoService
 @autor dgonzalez
*/
public interface ProductoService extends GenericService<Producto, Long> {
	
}
   