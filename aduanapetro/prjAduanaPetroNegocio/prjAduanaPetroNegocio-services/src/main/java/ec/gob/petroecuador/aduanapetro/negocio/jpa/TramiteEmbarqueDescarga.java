package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;


/**
 * The persistent class for the T_TRAMITE_EMBARQUE_DESCARGA database table.
 * 
 */
@NamedQueries({
	@NamedQuery(
		name = "obtenerPorPadreTEDES",
		query = "select p from TramiteEmbarqueDescarga p where p.estado='ACTIVO' and p.TramiteEmbarque.id=:tramiteEmbarque"
	),
})
@Entity
@Table(name = "NG_TRAMITE_EMBARQUE_DESCARGA", schema = "APS_NEGOCIO")
public class TramiteEmbarqueDescarga extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_FIN")
	private Date fechaFin;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_INICIO")
	private Date fechaInicio;

	private BigDecimal volumen;

	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;
	
	@Column(name="USUARIO_ACTUALIZACION")
	private String usuarioActualizacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ACTUALIZACION")
	private Date fechaActualizacion;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	
	//bi-directional many-to-one association to PuertoDescarga
	@ManyToOne
	@JoinColumn(name="IF_PUERTO_DESCARGA")
	private PuertoDescarga PuertoDescarga;

	//bi-directional many-to-one association to TramiteEmbarque
	@ManyToOne
	@JoinColumn(name="IF_TRAMITE_EMBARQUE")
	private TramiteEmbarque TramiteEmbarque;

	//bi-directional many-to-one association to TramiteEmbarqueRegistroBl
	@ManyToOne
	@JoinColumn(name="IF_TRAMITE_EMBARQUE_REG_BL")
	private TramiteEmbarqueRegistroBl TramiteEmbarqueRegistroBl;

	public TramiteEmbarqueDescarga() {
	}

	public Date getFechaFin() {
		return this.fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Date getFechaInicio() {
		return this.fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public BigDecimal getVolumen() {
		return this.volumen;
	}

	public void setVolumen(BigDecimal volumen) {
		this.volumen = volumen;
	}

	public PuertoDescarga getPuertoDescarga() {
		return this.PuertoDescarga;
	}

	public void setPuertoDescarga(PuertoDescarga PuertoDescarga) {
		this.PuertoDescarga = PuertoDescarga;
	}

	public TramiteEmbarque getTramiteEmbarque() {
		return this.TramiteEmbarque;
	}

	public void setTramiteEmbarque(TramiteEmbarque TramiteEmbarque) {
		this.TramiteEmbarque = TramiteEmbarque;
	}

	public TramiteEmbarqueRegistroBl getTramiteEmbarqueRegistroBl() {
		return this.TramiteEmbarqueRegistroBl;
	}

	public void setTramiteEmbarqueRegistroBl(TramiteEmbarqueRegistroBl TramiteEmbarqueRegistroBl) {
		this.TramiteEmbarqueRegistroBl = TramiteEmbarqueRegistroBl;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
}