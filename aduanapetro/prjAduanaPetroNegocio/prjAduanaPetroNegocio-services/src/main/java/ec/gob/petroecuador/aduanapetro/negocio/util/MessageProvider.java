package ec.gob.petroecuador.aduanapetro.negocio.util;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class MessageProvider {

	private static ResourceBundle bundle;
	private static final String VAR_GENERAL_MESSAGES_BUNDLE = "msg";
	private static final String VAR_ERROR_MESSAGES_BUNDLE = "errorMsg";
	private static final String VAR_VALIDATOR_MESSAGES_BUNDLE = "validatorMsg";

	private static ResourceBundle getBundle(String varResourceBoundle) {
		if (bundle == null) {
		//	FacesContext context = FacesContext.getCurrentInstance();
		//	bundle = context.getApplication().getResourceBundle(context, varResourceBoundle);
		}
		return bundle;
	}

	private static String getValue(String key, ResourceBundle bundle) {

		String result = null;
		try {
			result = bundle.getString(key);
		} catch (MissingResourceException e) {
			result = "???" + key + "??? not found";
		}
		return result;
	}

	public static String getGeneralMessage(String key) {
		return getValue(key, getBundle(VAR_GENERAL_MESSAGES_BUNDLE));
	}

	public static String getErrorMessage(String key) {
		return getValue(key, getBundle(VAR_ERROR_MESSAGES_BUNDLE));
	}

	public static final String getValidatorMessage(String key) {
		return getValue(key, getBundle(VAR_VALIDATOR_MESSAGES_BUNDLE));

	}

}
