
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.PolizaDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Poliza;

/**
 Clase PolizaDao
 @autor dgonzalez
*/
@Component("polizaDao")
public class PolizaDaoImpl extends GenericDAOImpl<Poliza, Long> implements PolizaDao {
	
}
   