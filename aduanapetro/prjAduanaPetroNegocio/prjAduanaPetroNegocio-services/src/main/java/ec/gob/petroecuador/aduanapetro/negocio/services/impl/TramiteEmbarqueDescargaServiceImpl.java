
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TramiteEmbarqueDescargaDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDescarga;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueDescargaService;

/**
 Clase TramiteEmbarqueDescargaServiceImpl
 @autor dgonzalez
*/
@Service
public class TramiteEmbarqueDescargaServiceImpl extends GenericServiceImpl<TramiteEmbarqueDescarga, Long> implements TramiteEmbarqueDescargaService {

	@Autowired
	TramiteEmbarqueDescargaDao dao;
	
	@Override
	public GenericDAO<TramiteEmbarqueDescarga, Long> getDao() {
		return dao;
	}

	@Override
	public List<TramiteEmbarqueDescarga> obtenerPorPadre(
			TramiteEmbarque tramiteEmbarque) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("tramiteEmbarque", tramiteEmbarque.getId());
		return dao.findByNamedQuery("obtenerPorPadreTEDES", map);
	}

}
   