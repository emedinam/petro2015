

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.FacturaRetencion;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase FacturaRetencionDao
 @autor dgonzalez
*/
public interface FacturaRetencionDao extends GenericDAO<FacturaRetencion, Long> {
	
}

   