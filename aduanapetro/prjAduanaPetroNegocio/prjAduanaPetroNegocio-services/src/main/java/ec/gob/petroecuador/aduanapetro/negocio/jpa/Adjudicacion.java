package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

@NamedQueries({
	@NamedQuery(
		name = "obtenerPorTipo",
		query = "select c from Adjudicacion c where c.estado='ACTIVO' and c.tipo=:tipo"
	),
	@NamedQuery(
			name = "obtenerPorTipoYTipoPago",
			query = "select c from Adjudicacion c where c.estado='ACTIVO' and c.tipo=:tipo and c.tipoPago=:tipoPago"
		),
	@NamedQuery(
		name = "buscar",
		query = "select c from Adjudicacion c where c.estado='ACTIVO' and c.observacion=:descripcion"
	),
})
@Entity
@Table(name = "NG_ADJUDICACION", schema= "APS_NEGOCIO")
public class Adjudicacion extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_ACTUALIZACION")
	private Date fechaActualiza;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION")
	private Date fechaCrea;

	private String formula;
	
	@Column(name = "VALOR_FORMULA")
	private BigDecimal valorFormula;

	@Column(name = "TIPO_PAGO")
	private String tipoPago;
	
	@Column(name = "GARANTIA_PAGO")
	private String garantiaPago;
	
	@Column(name = "FECHA_PAGO")
	private Integer fechaPago;
	
	@Column(name = "TIPO_DOCUMENTO")
	private String tipoDocumento;

	@Column(name = "MARGEN_TOLERANCIA")
	private BigDecimal margenTolerancia;

	@Column(name = "NUMERO_CARGAMENTO")
	private BigDecimal numeroCargamento;

	@Column(name = "NUMERO_CONTRATO")
	private String numeroContrato;

	private String observacion;

	private String incoterms;
	private String condicion;

	private BigDecimal preciacion;

	@Column(name = "RUTA_ARCHIVO")
	private String rutaArchivo;

	private String tipo;

	@Column(name = "USUARIO_ACTUALIZACION")
	private String usuarioActualiza;

	@Column(name = "USUARIO_CREACION")
	private String usuarioCrea;

	@Column(name = "VOLUMEN_APROXIMADO")
	private BigDecimal volumenAproximado;

	@Column(name = "VOLUMEN_TOTAL")
	private BigDecimal volumenTotal;

	// bi-directional many-to-one association to AplicacionSeguro
	@OneToMany(mappedBy = "Adjudicacion")
	private List<AplicacionSeguro> AplicacionSeguros;

	// bi-directional many-to-one association to Garantia
	@OneToMany(mappedBy = "Adjudicacion")
	private List<Garantia> Garantias;

	// bi-directional many-to-one association to LiquidacionAdjudicacion
	@OneToMany(mappedBy = "Adjudicacion")
	private List<LiquidacionAdjudicacion> LiquidacionAdjudicacions;

	// bi-directional many-to-one association to LiquidacionEmbarque
	@OneToMany(mappedBy = "Adjudicacion")
	private List<LiquidacionEmbarque> LiquidacionEmbarques;

	// bi-directional many-to-one association to PlanificacionExportacion
	@OneToMany(mappedBy = "Adjudicacion")
	private List<PlanificacionExportacion> PlanificacionExportaciones;

	// bi-directional many-to-one association to PlanificacionImportacion
	@OneToMany(mappedBy = "Adjudicacion")
	private List<PlanificacionImportacion> PlanificacionImportacions;

	// bi-directional many-to-one association to TramiteEmbarque
	@OneToMany(mappedBy = "Adjudicacion")
	private List<TramiteEmbarque> TramiteEmbarques;

	// bi-directional many-to-one association to Actor
	@ManyToOne
	@JoinColumn(name = "IF_ACTOR")
	private Actor Actor;

	// bi-directional many-to-one association to Actor
	@ManyToOne
	@JoinColumn(name = "IF_PAIS")
	private Pais Pais;

	// bi-directional many-to-one association to Actor
	@ManyToOne
	@JoinColumn(name = "IF_FORMULA")
	private Formula ifFormula;

	// bi-directional many-to-one association to Actor
	@ManyToOne
	@JoinColumn(name = "IF_CATALOGO_PRODUCTO")
	private CatalogoProducto catalogoProducto;

	public Actor getActor() {
		return Actor;
	}

	public void setActor(Actor actor) {
		Actor = actor;
	}

	public CatalogoProducto getCatalogoProducto() {
		return catalogoProducto;
	}

	public void setCatalogoProducto(CatalogoProducto catalogoProducto) {
		this.catalogoProducto = catalogoProducto;
	}

	public Adjudicacion() {
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Date getFechaActualiza() {
		return this.fechaActualiza;
	}

	public void setFechaActualiza(Date fechaActualiza) {
		this.fechaActualiza = fechaActualiza;
	}

	public Date getFechaCrea() {
		return this.fechaCrea;
	}

	public void setFechaCrea(Date fechaCrea) {
		this.fechaCrea = fechaCrea;
	}

	public String getFormula() {
		return this.formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public BigDecimal getMargenTolerancia() {
		return this.margenTolerancia;
	}

	public void setMargenTolerancia(BigDecimal margenTolerancia) {
		this.margenTolerancia = margenTolerancia;
	}

	public BigDecimal getNumeroCargamento() {
		return this.numeroCargamento;
	}

	public void setNumeroCargamento(BigDecimal numeroCargamento) {
		this.numeroCargamento = numeroCargamento;
	}

	public String getNumeroContrato() {
		return this.numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public BigDecimal getPreciacion() {
		return this.preciacion;
	}

	public void setPreciacion(BigDecimal preciacion) {
		this.preciacion = preciacion;
	}

	public String getRutaArchivo() {
		return this.rutaArchivo;
	}

	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getUsuarioActualiza() {
		return this.usuarioActualiza;
	}

	public void setUsuarioActualiza(String usuarioActualiza) {
		this.usuarioActualiza = usuarioActualiza;
	}

	public String getUsuarioCrea() {
		return this.usuarioCrea;
	}

	public void setUsuarioCrea(String usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}

	public BigDecimal getVolumenAproximado() {
		return this.volumenAproximado;
	}

	public void setVolumenAproximado(BigDecimal volumenAproximado) {
		this.volumenAproximado = volumenAproximado;
	}

	public BigDecimal getVolumenTotal() {
		return this.volumenTotal;
	}

	public void setVolumenTotal(BigDecimal volumenTotal) {
		this.volumenTotal = volumenTotal;
	}

	public List<AplicacionSeguro> getAplicacionSeguros() {
		return this.AplicacionSeguros;
	}

	public void setAplicacionSeguros(List<AplicacionSeguro> AplicacionSeguros) {
		this.AplicacionSeguros = AplicacionSeguros;
	}

	public AplicacionSeguro addAplicacionSeguro(
			AplicacionSeguro AplicacionSeguro) {
		getAplicacionSeguros().add(AplicacionSeguro);
		AplicacionSeguro.setAdjudicacion(this);

		return AplicacionSeguro;
	}

	public AplicacionSeguro removeAplicacionSeguro(
			AplicacionSeguro AplicacionSeguro) {
		getAplicacionSeguros().remove(AplicacionSeguro);
		AplicacionSeguro.setAdjudicacion(null);

		return AplicacionSeguro;
	}

	public List<Garantia> getGarantias() {
		return this.Garantias;
	}

	public void setGarantias(List<Garantia> Garantias) {
		this.Garantias = Garantias;
	}

	public Garantia addGarantia(Garantia Garantia) {
		getGarantias().add(Garantia);
		Garantia.setAdjudicacion(this);

		return Garantia;
	}

	public Garantia removeGarantia(Garantia Garantia) {
		getGarantias().remove(Garantia);
		Garantia.setAdjudicacion(null);

		return Garantia;
	}

	public List<LiquidacionAdjudicacion> getLiquidacionAdjudicacions() {
		return this.LiquidacionAdjudicacions;
	}

	public void setLiquidacionAdjudicacions(
			List<LiquidacionAdjudicacion> LiquidacionAdjudicacions) {
		this.LiquidacionAdjudicacions = LiquidacionAdjudicacions;
	}

	public LiquidacionAdjudicacion addLiquidacionAdjudicacion(
			LiquidacionAdjudicacion LiquidacionAdjudicacion) {
		getLiquidacionAdjudicacions().add(LiquidacionAdjudicacion);
		LiquidacionAdjudicacion.setAdjudicacion(this);

		return LiquidacionAdjudicacion;
	}

	public LiquidacionAdjudicacion removeLiquidacionAdjudicacion(
			LiquidacionAdjudicacion LiquidacionAdjudicacion) {
		getLiquidacionAdjudicacions().remove(LiquidacionAdjudicacion);
		LiquidacionAdjudicacion.setAdjudicacion(null);

		return LiquidacionAdjudicacion;
	}

	public List<LiquidacionEmbarque> getLiquidacionEmbarques() {
		return this.LiquidacionEmbarques;
	}

	public void setLiquidacionEmbarques(
			List<LiquidacionEmbarque> LiquidacionEmbarques) {
		this.LiquidacionEmbarques = LiquidacionEmbarques;
	}

	public LiquidacionEmbarque addLiquidacionEmbarque(
			LiquidacionEmbarque LiquidacionEmbarque) {
		getLiquidacionEmbarques().add(LiquidacionEmbarque);
		LiquidacionEmbarque.setAdjudicacion(this);

		return LiquidacionEmbarque;
	}

	public LiquidacionEmbarque removeLiquidacionEmbarque(
			LiquidacionEmbarque LiquidacionEmbarque) {
		getLiquidacionEmbarques().remove(LiquidacionEmbarque);
		LiquidacionEmbarque.setAdjudicacion(null);

		return LiquidacionEmbarque;
	}

	public List<PlanificacionExportacion> getPlanificacionExportaciones() {
		return this.PlanificacionExportaciones;
	}

	public void setPlanificacionExportaciones(
			List<PlanificacionExportacion> PlanificacionExportaciones) {
		this.PlanificacionExportaciones = PlanificacionExportaciones;
	}

	public PlanificacionExportacion addPlanificacionExportacione(
			PlanificacionExportacion PlanificacionExportacione) {
		getPlanificacionExportaciones().add(PlanificacionExportacione);
		PlanificacionExportacione.setAdjudicacion(this);

		return PlanificacionExportacione;
	}

	public PlanificacionExportacion removePlanificacionExportacione(
			PlanificacionExportacion PlanificacionExportacione) {
		getPlanificacionExportaciones().remove(PlanificacionExportacione);
		PlanificacionExportacione.setAdjudicacion(null);

		return PlanificacionExportacione;
	}

	public List<PlanificacionImportacion> getPlanificacionImportacions() {
		return this.PlanificacionImportacions;
	}

	public void setPlanificacionImportacions(
			List<PlanificacionImportacion> PlanificacionImportacions) {
		this.PlanificacionImportacions = PlanificacionImportacions;
	}

	public PlanificacionImportacion addPlanificacionImportacion(
			PlanificacionImportacion PlanificacionImportacion) {
		getPlanificacionImportacions().add(PlanificacionImportacion);
		PlanificacionImportacion.setAdjudicacion(this);

		return PlanificacionImportacion;
	}

	public PlanificacionImportacion removePlanificacionImportacion(
			PlanificacionImportacion PlanificacionImportacion) {
		getPlanificacionImportacions().remove(PlanificacionImportacion);
		PlanificacionImportacion.setAdjudicacion(null);

		return PlanificacionImportacion;
	}

	public List<TramiteEmbarque> getTramiteEmbarques() {
		return this.TramiteEmbarques;
	}

	public void setTramiteEmbarques(List<TramiteEmbarque> TramiteEmbarques) {
		this.TramiteEmbarques = TramiteEmbarques;
	}

	public TramiteEmbarque addTramiteEmbarque(TramiteEmbarque TramiteEmbarque) {
		getTramiteEmbarques().add(TramiteEmbarque);
		TramiteEmbarque.setAdjudicacion(this);

		return TramiteEmbarque;
	}

	public TramiteEmbarque removeTramiteEmbarque(TramiteEmbarque TramiteEmbarque) {
		getTramiteEmbarques().remove(TramiteEmbarque);
		TramiteEmbarque.setAdjudicacion(null);

		return TramiteEmbarque;
	}

	public String getIncoterms() {
		return incoterms;
	}

	public void setIncoterms(String incoterms) {
		this.incoterms = incoterms;
	}

	public String getCondicion() {
		return condicion;
	}

	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}

	public String getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	
	
	public Pais getPais() {
		return Pais;
	}

	public void setPais(Pais pais) {
		Pais = pais;
	}


	public Formula getIfFormula() {
		return ifFormula;
	}

	public void setIfFormula(Formula ifFormula) {
		this.ifFormula = ifFormula;
	}

	
	
	
	public Integer getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Integer fechaPago) {
		this.fechaPago = fechaPago;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getGarantiaPago() {
		return garantiaPago;
	}

	public void setGarantiaPago(String garantiaPago) {
		this.garantiaPago = garantiaPago;
	}

	public BigDecimal getValorFormula() {
		return valorFormula;
	}

	public void setValorFormula(BigDecimal valorFormula) {
		this.valorFormula = valorFormula;
	}

	// metodos para combos
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof Adjudicacion))
			return false;
		return ((Adjudicacion) obj).getId().equals(this.id);
	}

	@Override
	public String toString() {
		return numeroContrato;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;	
	}

	@Override
	public Estados getEstado() {
		return estado;
	}
}