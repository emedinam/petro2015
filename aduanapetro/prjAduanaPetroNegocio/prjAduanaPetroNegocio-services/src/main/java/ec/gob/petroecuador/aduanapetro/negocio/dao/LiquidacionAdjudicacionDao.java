

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.LiquidacionAdjudicacion;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase LiquidacionAdjudicacionDao
 @autor dgonzalez
*/
public interface LiquidacionAdjudicacionDao extends GenericDAO<LiquidacionAdjudicacion, Long> {
	
}

   