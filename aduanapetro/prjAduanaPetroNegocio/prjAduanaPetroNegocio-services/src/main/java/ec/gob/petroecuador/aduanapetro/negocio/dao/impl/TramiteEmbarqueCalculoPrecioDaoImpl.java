
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TramiteEmbarqueCalculoPrecioDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueCalculoPrecio;

/**
 Clase TramiteEmbarqueCalculoPrecioDao
 @autor dgonzalez
*/
@Component("tramiteembarquecalculoprecioDao")
public class TramiteEmbarqueCalculoPrecioDaoImpl extends GenericDAOImpl<TramiteEmbarqueCalculoPrecio, Long> implements TramiteEmbarqueCalculoPrecioDao {
	
}
   