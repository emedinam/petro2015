

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.FacturaDetalle;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase FacturaDetalleDao
 @autor dgonzalez
*/
public interface FacturaDetalleDao extends GenericDAO<FacturaDetalle, Long> {
	
}

   