
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.DocumentoAdjuntoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.DocumentoAdjunto;
import ec.gob.petroecuador.aduanapetro.negocio.services.DocumentoAdjuntoService;

/**
 Clase DocumentoAdjuntoServiceImpl
 @autor dgonzalez
*/
@Service
public class DocumentoAdjuntoServiceImpl extends GenericServiceImpl<DocumentoAdjunto, Long> implements DocumentoAdjuntoService {

	@Autowired
	DocumentoAdjuntoDao dao;
	
	@Override
	public GenericDAO<DocumentoAdjunto, Long> getDao() {
		return dao;
	}

}
   