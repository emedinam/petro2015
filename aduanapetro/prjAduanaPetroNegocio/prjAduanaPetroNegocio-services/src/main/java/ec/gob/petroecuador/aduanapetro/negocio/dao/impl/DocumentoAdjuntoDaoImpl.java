
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.DocumentoAdjuntoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.DocumentoAdjunto;

/**
 Clase DocumentoAdjuntoDao
 @autor dgonzalez
*/
@Component("documentoadjuntoDao")
public class DocumentoAdjuntoDaoImpl extends GenericDAOImpl<DocumentoAdjunto, Long> implements DocumentoAdjuntoDao {
	
}
   