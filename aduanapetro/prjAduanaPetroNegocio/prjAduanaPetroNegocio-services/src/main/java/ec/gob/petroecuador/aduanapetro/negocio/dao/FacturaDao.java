

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Factura;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase FacturaDao
 @autor dgonzalez
*/
public interface FacturaDao extends GenericDAO<Factura, Long> {
	
}

   