
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.FacturaDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Factura;

/**
 Clase FacturaDao
 @autor dgonzalez
*/
@Component("facturaDao")
public class FacturaDaoImpl extends GenericDAOImpl<Factura, Long> implements FacturaDao {
	
}
   