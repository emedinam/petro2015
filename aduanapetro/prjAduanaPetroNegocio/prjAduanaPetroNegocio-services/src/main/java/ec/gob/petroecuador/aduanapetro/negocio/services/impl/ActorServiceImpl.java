
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.ActorDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Actor;
import ec.gob.petroecuador.aduanapetro.negocio.services.ActorService;

/**
 Clase ActorServiceImpl
 @autor dgonzalez
*/
@Service
public class ActorServiceImpl extends GenericServiceImpl<Actor, Long> implements ActorService {

	@Autowired
	ActorDao dao;
	
	@Override
	public GenericDAO<Actor, Long> getDao() {
		return dao;
	}

}
   