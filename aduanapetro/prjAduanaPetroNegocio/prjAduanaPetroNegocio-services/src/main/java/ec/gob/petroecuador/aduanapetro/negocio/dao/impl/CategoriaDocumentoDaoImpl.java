
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.CategoriaDocumentoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.CategoriaDocumento;

/**
 Clase CategoriaDocumentoDao
 @autor dgonzalez
*/
@Component("categoriadocumentoDao")
public class CategoriaDocumentoDaoImpl extends GenericDAOImpl<CategoriaDocumento, Long> implements CategoriaDocumentoDao {
	
}
   