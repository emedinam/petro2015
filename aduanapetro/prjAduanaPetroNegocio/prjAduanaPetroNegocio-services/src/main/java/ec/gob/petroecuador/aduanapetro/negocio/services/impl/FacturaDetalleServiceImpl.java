
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.FacturaDetalleDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Factura;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.FacturaDetalle;
import ec.gob.petroecuador.aduanapetro.negocio.services.FacturaDetalleService;

/**
 Clase FacturaDetalleServiceImpl
 @autor dgonzalez
*/
@Service
public class FacturaDetalleServiceImpl extends GenericServiceImpl<FacturaDetalle, Long> implements FacturaDetalleService {

	@Autowired
	FacturaDetalleDao dao;
	
	@Override
	public GenericDAO<FacturaDetalle, Long> getDao() {
		return dao;
	}

	@Override
	public List<FacturaDetalle> obtenerPorPadre(Factura factura) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("factura", factura);
		return dao.findByNamedQuery("obtenerPorPadreFD", map);
	}

}
   