

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.ParametroCalculo;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase ParametroCalculoDao
 @autor dgonzalez
*/
public interface ParametroCalculoDao extends GenericDAO<ParametroCalculo, Long> {
	
}

   