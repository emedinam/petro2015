

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.DocumentoAdjunto;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase DocumentoAdjuntoDao
 @autor dgonzalez
*/
public interface DocumentoAdjuntoDao extends GenericDAO<DocumentoAdjunto, Long> {
	
}

   