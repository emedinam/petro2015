package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.CatalogoDao;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Catalogo;
import ec.gob.petroecuador.aduanapetro.negocio.services.CatalogoService;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;

@Service
public class CatalogoServiceImpl extends GenericServiceImpl<Catalogo, Long> implements CatalogoService {

	@Autowired
	CatalogoDao dao;
	
	@Override
	public GenericDAO<Catalogo, Long> getDao() {
		return dao;
	}

}
