/*
 * EntidadNoBorradaException.java
 * 
 * Copyright (c) 2012 SENPLADES
 * Todos los derechos reservados.
 */

package ec.gob.petroecuador.aduanapetro.negocio.exceptions;

/**
 * Class ServicioException.
 *
 * @author 
 * @revision $Revision: $$
 */
//USE @Transactional(rollbackFor = @ServicioException.class) in service
public class ServicioException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Instancia un nuevo entidad no borrada exception.
	 */
	public ServicioException() {
		super();
	}

	/**
	 * Instancia un nuevo entidad no borrada exception.
	 *
	 * @param arg0 the arg0
	 * @param arg1 the arg1
	 */
	public ServicioException(final String arg0, final Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * Instancia un nuevo entidad no borrada exception.
	 *
	 * @param arg0 the arg0
	 */
	public ServicioException(final String arg0) {
		super(arg0);
	}

	/**
	 * Instancia un nuevo entidad no borrada exception.
	 *
	 * @param arg0 the arg0
	 */
	public ServicioException(final Throwable arg0) {
		super(arg0);
	}
}