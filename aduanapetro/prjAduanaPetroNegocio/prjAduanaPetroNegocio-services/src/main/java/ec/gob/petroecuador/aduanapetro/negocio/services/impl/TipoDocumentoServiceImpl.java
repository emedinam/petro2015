
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TipoDocumentoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TipoDocumento;
import ec.gob.petroecuador.aduanapetro.negocio.services.TipoDocumentoService;

/**
 Clase TipoDocumentoServiceImpl
 @autor dgonzalez
*/
@Service
public class TipoDocumentoServiceImpl extends GenericServiceImpl<TipoDocumento, Long> implements TipoDocumentoService {

	@Autowired
	TipoDocumentoDao dao;
	
	@Override
	public GenericDAO<TipoDocumento, Long> getDao() {
		return dao;
	}

}
   