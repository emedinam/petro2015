package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;


/**
 * The persistent class for the CATALOGO database table.
 * 
 */
@Entity
@Table(name = "NG_CATALOGO", schema= "APS_NEGOCIO")
public class Catalogo extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	private String descripcion;

	private String icon;

	private String nombre;

	@OneToMany(mappedBy="catalogo")
	private List<Catalogodetalle> catalogodetalles;

	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	
	public Catalogo() {
	}
	
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getIcon() {
		return this.icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Catalogodetalle> getCatalogodetalles() {
		return this.catalogodetalles;
	}

	public void setCatalogodetalles(List<Catalogodetalle> catalogodetalles) {
		this.catalogodetalles = catalogodetalles;
	}

	public Catalogodetalle addCatalogodetalle(Catalogodetalle catalogodetalle) {
		getCatalogodetalles().add(catalogodetalle);
		catalogodetalle.setCatalogo(this);

		return catalogodetalle;
	}

	public Catalogodetalle removeCatalogodetalle(Catalogodetalle catalogodetalle) {
		getCatalogodetalles().remove(catalogodetalle);
		catalogodetalle.setCatalogo(null);
		return catalogodetalle;
	}

	@Override
	public Estados getEstado() {
		return this.estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

}