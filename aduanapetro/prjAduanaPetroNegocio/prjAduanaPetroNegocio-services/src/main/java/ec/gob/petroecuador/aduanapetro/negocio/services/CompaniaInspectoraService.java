
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.CompaniaInspectora;

/**
 Clase CompaniaInspectoraService
 @autor dgonzalez
*/
public interface CompaniaInspectoraService extends GenericService<CompaniaInspectora, Long> {
	
}
   