
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.GeneracionDocumento;

/**
 Clase GeneracionDocumentoService
 @autor dgonzalez
*/
public interface GeneracionDocumentoService extends GenericService<GeneracionDocumento, Long> {
	
}
   