package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;


/**
 * The persistent class for the PRODUCTO database table.
 * 
 */
@Entity
@Table(name = "NG_PRODUCTO", schema = "APS_NEGOCIO")
public class Producto extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	private String nombre;

	//bi-directional many-to-one association to FacturaDetalle
	@OneToMany(mappedBy="producto")
	private List<FacturaDetalle> facturadetalles;

	//bi-directional many-to-one association to Linea
	@ManyToOne
	@JoinColumn(name="IDLINEA")
	private Linea linea;

	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	
	public Producto() {
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<FacturaDetalle> getFacturadetalles() {
		return this.facturadetalles;
	}

	public void setFacturadetalles(List<FacturaDetalle> facturadetalles) {
		this.facturadetalles = facturadetalles;
	}

	public FacturaDetalle addFacturadetalle(FacturaDetalle facturadetalle) {
		getFacturadetalles().add(facturadetalle);
		facturadetalle.setProducto(this);

		return facturadetalle;
	}

	public FacturaDetalle removeFacturadetalle(FacturaDetalle facturadetalle) {
		getFacturadetalles().remove(facturadetalle);
		facturadetalle.setProducto(null);

		return facturadetalle;
	}

	public Linea getLinea() {
		return this.linea;
	}

	public void setLinea(Linea linea) {
		this.linea = linea;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof Producto))
			return false;
		return ((Producto) obj).getId().equals(this.id);
	}

	@Override
	public String toString() {
		return nombre;
	}
	
	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
}