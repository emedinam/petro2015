package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Catalogodetalle;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

public interface CatalogodetalleDao extends GenericDAO<Catalogodetalle, Long> {

}
