
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TramiteEmbarqueDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;

/**
 Clase TramiteEmbarqueDao
 @autor dgonzalez
*/
@Component("tramiteembarqueDao")
public class TramiteEmbarqueDaoImpl extends GenericDAOImpl<TramiteEmbarque, Long> implements TramiteEmbarqueDao {
	
}
   