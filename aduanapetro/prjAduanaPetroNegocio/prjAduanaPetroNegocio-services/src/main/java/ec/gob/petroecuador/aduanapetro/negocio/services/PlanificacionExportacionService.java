
package ec.gob.petroecuador.aduanapetro.negocio.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Adjudicacion;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionExportacion;

/**
 Clase PlanificacionExportacionService
 @autor dgonzalez
*/
public interface PlanificacionExportacionService extends GenericService<PlanificacionExportacion, Long> {
	List<PlanificacionExportacion> obtenerPorPadre(Adjudicacion adjudicacion);
}
   