
package ec.gob.petroecuador.aduanapetro.negocio.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.ParametroCalculo;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PrecioPrevision;

/**
 Clase PrecioPrevisionService
 @autor dgonzalez
*/
public interface PrecioPrevisionService extends GenericService<PrecioPrevision, Long> {
	List<PrecioPrevision> obtenerPorParametroCalculo(ParametroCalculo parametroCalculo);
}
   