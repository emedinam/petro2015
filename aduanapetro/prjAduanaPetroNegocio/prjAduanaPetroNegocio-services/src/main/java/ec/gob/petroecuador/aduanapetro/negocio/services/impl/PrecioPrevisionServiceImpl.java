
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.PrecioPrevisionDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.ParametroCalculo;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PrecioPrevision;
import ec.gob.petroecuador.aduanapetro.negocio.services.PrecioPrevisionService;

/**
 Clase PrecioPrevisionServiceImpl
 @autor dgonzalez
*/
@Service
public class PrecioPrevisionServiceImpl extends GenericServiceImpl<PrecioPrevision, Long> implements PrecioPrevisionService {

	@Autowired
	PrecioPrevisionDao dao;
	
	@Override
	public GenericDAO<PrecioPrevision, Long> getDao() {
		return dao;
	}

	@Override
	public List<PrecioPrevision> obtenerPorParametroCalculo(
			ParametroCalculo parametroCalculo) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("parametroCalculo", parametroCalculo);
		return dao.findByNamedQuery("obtenerPorParametroCalculo", map);
	}

}
   