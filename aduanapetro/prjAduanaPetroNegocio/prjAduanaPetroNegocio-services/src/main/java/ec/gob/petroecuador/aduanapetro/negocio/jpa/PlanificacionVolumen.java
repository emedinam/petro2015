package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import javax.persistence.*;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the T_PLANIFICACION_VOLUMEN database table.
 * 
 */
@Entity
@Table(name = "NG_PLANIFICACION_VOLUMEN", schema = "APS_NEGOCIO")
public class PlanificacionVolumen extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name="ANIO")
	private BigDecimal anio;

	@Column(name="BARRIL_ABRIL")
	private BigDecimal barrilAbril;

	@Column(name="BARRIL_AGOSTO")
	private BigDecimal barrilAgosto;

	@Column(name="BARRIL_DICIEMBRE")
	private BigDecimal barrilDiciembre;

	@Column(name="BARRIL_ENERO")
	private BigDecimal barrilEnero;

	@Column(name="BARRIL_FEBRERO")
	private BigDecimal barrilFebrero;

	@Column(name="BARRIL_JULIO")
	private BigDecimal barrilJulio;

	@Column(name="BARRIL_JUNIO")
	private BigDecimal barrilJunio;

	@Column(name="BARRIL_MARZO")
	private BigDecimal barrilMarzo;

	@Column(name="BARRIL_MAYO")
	private BigDecimal barrilMayo;

	@Column(name="BARRIL_NOVIEMBRE")
	private BigDecimal barrilNoviembre;

	@Column(name="BARRIL_OCTUBRE")
	private BigDecimal barrilOctubre;

	@Column(name="BARRIL_SEPTIEMBRE")
	private BigDecimal barrilSeptiembre;

	@Column(name="COSTO_CIF")
	private BigDecimal costoCif;

	@Column(name="COSTO_TOTAL")
	private BigDecimal costoTotal;

	@Column(name="GALON_ABRIL")
	private BigDecimal galonAbril;

	@Column(name="GALON_AGOSTO")
	private BigDecimal galonAgosto;

	@Column(name="GALON_DICIEMBRE")
	private BigDecimal galonDiciembre;

	@Column(name="GALON_ENERO")
	private BigDecimal galonEnero;

	@Column(name="GALON_FEBRERO")
	private BigDecimal galonFebrero;

	@Column(name="GALON_JULIO")
	private BigDecimal galonJulio;

	@Column(name="GALON_JUNIO")
	private BigDecimal galonJunio;

	@Column(name="GALON_MARZO")
	private BigDecimal galonMarzo;

	@Column(name="GALON_MAYO")
	private BigDecimal galonMayo;

	@Column(name="GALON_NOVIEMBRE")
	private BigDecimal galonNoviembre;

	@Column(name="GALON_OCTUBRE")
	private BigDecimal galonOctubre;

	@Column(name="GALON_SEPTIEMBRE")
	private BigDecimal galonSeptiembre;

	@Column(name="GASTO")
	private BigDecimal gasto;

	@Column(name="SEGURO_ABRIL")
	private BigDecimal seguroAbril;

	@Column(name="SEGURO_AGOSTO")
	private BigDecimal seguroAgosto;

	@Column(name="SEGURO_DICIEMBRE")
	private BigDecimal seguroDiciembre;

	@Column(name="SEGURO_ENERO")
	private BigDecimal seguroEnero;

	@Column(name="SEGURO_FEBRERO")
	private BigDecimal seguroFebrero;

	@Column(name="SEGURO_JULIO")
	private BigDecimal seguroJulio;

	@Column(name="SEGURO_JUNIO")
	private BigDecimal seguroJunio;

	@Column(name="SEGURO_MARZO")
	private BigDecimal seguroMarzo;

	@Column(name="SEGURO_MAYO")
	private BigDecimal seguroMayo;

	@Column(name="SEGURO_NOVIEMBRE")
	private BigDecimal seguroNoviembre;

	@Column(name="SEGURO_OCTUBRE")
	private BigDecimal seguroOctubre;

	@Column(name="SEGURO_SEPTIEMBRE")
	private BigDecimal seguroSeptiembre;

	@Column(name="TOTAL_BARRIL")
	private BigDecimal totalBarril;

	@Column(name="TOTAL_CIF")
	private BigDecimal totalCif;

	@Column(name="TOTAL_CIF_ABRIL")
	private BigDecimal totalCifAbril;

	@Column(name="TOTAL_CIF_AGOSTO")
	private BigDecimal totalCifAgosto;

	@Column(name="TOTAL_CIF_DICIEMBRE")
	private BigDecimal totalCifDiciembre;

	@Column(name="TOTAL_CIF_ENERO")
	private BigDecimal totalCifEnero;

	@Column(name="TOTAL_CIF_FEBRERO")
	private BigDecimal totalCifFebrero;

	@Column(name="TOTAL_CIF_JULIO")
	private BigDecimal totalCifJulio;

	@Column(name="TOTAL_CIF_JUNIO")
	private BigDecimal totalCifJunio;

	@Column(name="TOTAL_CIF_MARZO")
	private BigDecimal totalCifMarzo;

	@Column(name="TOTAL_CIF_MAYO")
	private BigDecimal totalCifMayo;

	@Column(name="TOTAL_CIF_NOVIEMBRE")
	private BigDecimal totalCifNoviembre;

	@Column(name="TOTAL_CIF_OCTUBRE")
	private BigDecimal totalCifOctubre;

	@Column(name="TOTAL_CIF_SEPTIEMBRE")
	private BigDecimal totalCifSeptiembre;

	@Column(name="TOTAL_DAT")
	private BigDecimal totalDat;

	@Column(name="TOTAL_DAT_ABRIL")
	private BigDecimal totalDatAbril;

	@Column(name="TOTAL_DAT_AGOSTO")
	private BigDecimal totalDatAgosto;

	@Column(name="TOTAL_DAT_DICIEMBRE")
	private BigDecimal totalDatDiciembre;

	@Column(name="TOTAL_DAT_ENERO")
	private BigDecimal totalDatEnero;

	@Column(name="TOTAL_DAT_FEBRERO")
	private BigDecimal totalDatFebrero;

	@Column(name="TOTAL_DAT_JULIO")
	private BigDecimal totalDatJulio;

	@Column(name="TOTAL_DAT_JUNIO")
	private BigDecimal totalDatJunio;

	@Column(name="TOTAL_DAT_MARZO")
	private BigDecimal totalDatMarzo;

	@Column(name="TOTAL_DAT_MAYO")
	private BigDecimal totalDatMayo;

	@Column(name="TOTAL_DAT_NOVIEMBRE")
	private BigDecimal totalDatNoviembre;

	@Column(name="TOTAL_DAT_OCTUBRE")
	private BigDecimal totalDatOctubre;

	@Column(name="TOTAL_DAT_SEPTIEMBRE")
	private BigDecimal totalDatSeptiembre;

	@Column(name="TOTAL_SEGURO")
	private BigDecimal totalSeguro;
	
	// /datos de auditoria
	@Column(name = "USUARIO_CREACION")
	private String usuarioCreacion;

	@Column(name = "USUARIO_ACTUALIZACION")
	private String usuarioActualizacion;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_ACTUALIZACION")
	private Date fechaActualizacion;

	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	
	@Column(name="PRECIO_MARCADOR")
	private String precioMarcador;
	
	//bi-directional many-to-one association to CatalogoProducto
    @ManyToOne
	@JoinColumn(name="IF_PRECIO_PREVISION")
	private PrecioPrevision precioPrevision;

	public PlanificacionVolumen() {
	}

	public BigDecimal getAnio() {
		return this.anio;
	}

	public void setAnio(BigDecimal anio) {
		this.anio = anio;
	}

	public BigDecimal getBarrilAbril() {
		return this.barrilAbril;
	}

	public void setBarrilAbril(BigDecimal barrilAbril) {
		this.barrilAbril = barrilAbril;
	}

	public BigDecimal getBarrilAgosto() {
		return this.barrilAgosto;
	}

	public void setBarrilAgosto(BigDecimal barrilAgosto) {
		this.barrilAgosto = barrilAgosto;
	}

	public BigDecimal getBarrilDiciembre() {
		return this.barrilDiciembre;
	}

	public void setBarrilDiciembre(BigDecimal barrilDiciembre) {
		this.barrilDiciembre = barrilDiciembre;
	}

	public BigDecimal getBarrilEnero() {
		return this.barrilEnero;
	}

	public void setBarrilEnero(BigDecimal barrilEnero) {
		this.barrilEnero = barrilEnero;
	}

	public BigDecimal getBarrilFebrero() {
		return this.barrilFebrero;
	}

	public void setBarrilFebrero(BigDecimal barrilFebrero) {
		this.barrilFebrero = barrilFebrero;
	}

	public BigDecimal getBarrilJulio() {
		return this.barrilJulio;
	}

	public void setBarrilJulio(BigDecimal barrilJulio) {
		this.barrilJulio = barrilJulio;
	}

	public BigDecimal getBarrilJunio() {
		return this.barrilJunio;
	}

	public void setBarrilJunio(BigDecimal barrilJunio) {
		this.barrilJunio = barrilJunio;
	}

	public BigDecimal getBarrilMarzo() {
		return this.barrilMarzo;
	}

	public void setBarrilMarzo(BigDecimal barrilMarzo) {
		this.barrilMarzo = barrilMarzo;
	}

	public BigDecimal getBarrilMayo() {
		return this.barrilMayo;
	}

	public void setBarrilMayo(BigDecimal barrilMayo) {
		this.barrilMayo = barrilMayo;
	}

	public BigDecimal getBarrilNoviembre() {
		return this.barrilNoviembre;
	}

	public void setBarrilNoviembre(BigDecimal barrilNoviembre) {
		this.barrilNoviembre = barrilNoviembre;
	}

	public BigDecimal getBarrilOctubre() {
		return this.barrilOctubre;
	}

	public void setBarrilOctubre(BigDecimal barrilOctubre) {
		this.barrilOctubre = barrilOctubre;
	}

	public BigDecimal getBarrilSeptiembre() {
		return this.barrilSeptiembre;
	}

	public void setBarrilSeptiembre(BigDecimal barrilSeptiembre) {
		this.barrilSeptiembre = barrilSeptiembre;
	}

	public BigDecimal getCostoCif() {
		return this.costoCif;
	}

	public void setCostoCif(BigDecimal costoCif) {
		this.costoCif = costoCif;
	}

	public BigDecimal getCostoTotal() {
		return this.costoTotal;
	}

	public void setCostoTotal(BigDecimal costoTotal) {
		this.costoTotal = costoTotal;
	}

	public BigDecimal getGalonAbril() {
		return this.galonAbril;
	}

	public void setGalonAbril(BigDecimal galonAbril) {
		this.galonAbril = galonAbril;
	}

	public BigDecimal getGalonAgosto() {
		return this.galonAgosto;
	}

	public void setGalonAgosto(BigDecimal galonAgosto) {
		this.galonAgosto = galonAgosto;
	}

	public BigDecimal getGalonDiciembre() {
		return this.galonDiciembre;
	}

	public void setGalonDiciembre(BigDecimal galonDiciembre) {
		this.galonDiciembre = galonDiciembre;
	}

	public BigDecimal getGalonEnero() {
		return this.galonEnero;
	}

	public void setGalonEnero(BigDecimal galonEnero) {
		this.galonEnero = galonEnero;
	}

	public BigDecimal getGalonFebrero() {
		return this.galonFebrero;
	}

	public void setGalonFebrero(BigDecimal galonFebrero) {
		this.galonFebrero = galonFebrero;
	}

	public BigDecimal getGalonJulio() {
		return this.galonJulio;
	}

	public void setGalonJulio(BigDecimal galonJulio) {
		this.galonJulio = galonJulio;
	}

	public BigDecimal getGalonJunio() {
		return this.galonJunio;
	}

	public void setGalonJunio(BigDecimal galonJunio) {
		this.galonJunio = galonJunio;
	}

	public BigDecimal getGalonMarzo() {
		return this.galonMarzo;
	}

	public void setGalonMarzo(BigDecimal galonMarzo) {
		this.galonMarzo = galonMarzo;
	}

	public BigDecimal getGalonMayo() {
		return this.galonMayo;
	}

	public void setGalonMayo(BigDecimal galonMayo) {
		this.galonMayo = galonMayo;
	}

	public BigDecimal getGalonNoviembre() {
		return this.galonNoviembre;
	}

	public void setGalonNoviembre(BigDecimal galonNoviembre) {
		this.galonNoviembre = galonNoviembre;
	}

	public BigDecimal getGalonOctubre() {
		return this.galonOctubre;
	}

	public void setGalonOctubre(BigDecimal galonOctubre) {
		this.galonOctubre = galonOctubre;
	}

	public BigDecimal getGalonSeptiembre() {
		return this.galonSeptiembre;
	}

	public void setGalonSeptiembre(BigDecimal galonSeptiembre) {
		this.galonSeptiembre = galonSeptiembre;
	}

	public BigDecimal getGasto() {
		return this.gasto;
	}

	public void setGasto(BigDecimal gasto) {
		this.gasto = gasto;
	}

	public BigDecimal getSeguroAbril() {
		return this.seguroAbril;
	}

	public void setSeguroAbril(BigDecimal seguroAbril) {
		this.seguroAbril = seguroAbril;
	}

	public BigDecimal getSeguroAgosto() {
		return this.seguroAgosto;
	}

	public void setSeguroAgosto(BigDecimal seguroAgosto) {
		this.seguroAgosto = seguroAgosto;
	}

	public BigDecimal getSeguroDiciembre() {
		return this.seguroDiciembre;
	}

	public void setSeguroDiciembre(BigDecimal seguroDiciembre) {
		this.seguroDiciembre = seguroDiciembre;
	}

	public BigDecimal getSeguroEnero() {
		return this.seguroEnero;
	}

	public void setSeguroEnero(BigDecimal seguroEnero) {
		this.seguroEnero = seguroEnero;
	}

	public BigDecimal getSeguroFebrero() {
		return this.seguroFebrero;
	}

	public void setSeguroFebrero(BigDecimal seguroFebrero) {
		this.seguroFebrero = seguroFebrero;
	}

	public BigDecimal getSeguroJulio() {
		return this.seguroJulio;
	}

	public void setSeguroJulio(BigDecimal seguroJulio) {
		this.seguroJulio = seguroJulio;
	}

	public BigDecimal getSeguroJunio() {
		return this.seguroJunio;
	}

	public void setSeguroJunio(BigDecimal seguroJunio) {
		this.seguroJunio = seguroJunio;
	}

	public BigDecimal getSeguroMarzo() {
		return this.seguroMarzo;
	}

	public void setSeguroMarzo(BigDecimal seguroMarzo) {
		this.seguroMarzo = seguroMarzo;
	}

	public BigDecimal getSeguroMayo() {
		return this.seguroMayo;
	}

	public void setSeguroMayo(BigDecimal seguroMayo) {
		this.seguroMayo = seguroMayo;
	}

	public BigDecimal getSeguroNoviembre() {
		return this.seguroNoviembre;
	}

	public void setSeguroNoviembre(BigDecimal seguroNoviembre) {
		this.seguroNoviembre = seguroNoviembre;
	}

	public BigDecimal getSeguroOctubre() {
		return this.seguroOctubre;
	}

	public void setSeguroOctubre(BigDecimal seguroOctubre) {
		this.seguroOctubre = seguroOctubre;
	}

	public BigDecimal getSeguroSeptiembre() {
		return this.seguroSeptiembre;
	}

	public void setSeguroSeptiembre(BigDecimal seguroSeptiembre) {
		this.seguroSeptiembre = seguroSeptiembre;
	}

	public BigDecimal getTotalBarril() {
		return this.totalBarril;
	}

	public void setTotalBarril(BigDecimal totalBarril) {
		this.totalBarril = totalBarril;
	}

	public BigDecimal getTotalCif() {
		return this.totalCif;
	}

	public void setTotalCif(BigDecimal totalCif) {
		this.totalCif = totalCif;
	}

	public BigDecimal getTotalCifAbril() {
		return this.totalCifAbril;
	}

	public void setTotalCifAbril(BigDecimal totalCifAbril) {
		this.totalCifAbril = totalCifAbril;
	}

	public BigDecimal getTotalCifAgosto() {
		return this.totalCifAgosto;
	}

	public void setTotalCifAgosto(BigDecimal totalCifAgosto) {
		this.totalCifAgosto = totalCifAgosto;
	}

	public BigDecimal getTotalCifDiciembre() {
		return this.totalCifDiciembre;
	}

	public void setTotalCifDiciembre(BigDecimal totalCifDiciembre) {
		this.totalCifDiciembre = totalCifDiciembre;
	}

	public BigDecimal getTotalCifEnero() {
		return this.totalCifEnero;
	}

	public void setTotalCifEnero(BigDecimal totalCifEnero) {
		this.totalCifEnero = totalCifEnero;
	}

	public BigDecimal getTotalCifFebrero() {
		return this.totalCifFebrero;
	}

	public void setTotalCifFebrero(BigDecimal totalCifFebrero) {
		this.totalCifFebrero = totalCifFebrero;
	}

	public BigDecimal getTotalCifJulio() {
		return this.totalCifJulio;
	}

	public void setTotalCifJulio(BigDecimal totalCifJulio) {
		this.totalCifJulio = totalCifJulio;
	}

	public BigDecimal getTotalCifJunio() {
		return this.totalCifJunio;
	}

	public void setTotalCifJunio(BigDecimal totalCifJunio) {
		this.totalCifJunio = totalCifJunio;
	}

	public BigDecimal getTotalCifMarzo() {
		return this.totalCifMarzo;
	}

	public void setTotalCifMarzo(BigDecimal totalCifMarzo) {
		this.totalCifMarzo = totalCifMarzo;
	}

	public BigDecimal getTotalCifMayo() {
		return this.totalCifMayo;
	}

	public void setTotalCifMayo(BigDecimal totalCifMayo) {
		this.totalCifMayo = totalCifMayo;
	}

	public BigDecimal getTotalCifNoviembre() {
		return this.totalCifNoviembre;
	}

	public void setTotalCifNoviembre(BigDecimal totalCifNoviembre) {
		this.totalCifNoviembre = totalCifNoviembre;
	}

	public BigDecimal getTotalCifOctubre() {
		return this.totalCifOctubre;
	}

	public void setTotalCifOctubre(BigDecimal totalCifOctubre) {
		this.totalCifOctubre = totalCifOctubre;
	}

	public BigDecimal getTotalCifSeptiembre() {
		return this.totalCifSeptiembre;
	}

	public void setTotalCifSeptiembre(BigDecimal totalCifSeptiembre) {
		this.totalCifSeptiembre = totalCifSeptiembre;
	}

	public BigDecimal getTotalDat() {
		return this.totalDat;
	}

	public void setTotalDat(BigDecimal totalDat) {
		this.totalDat = totalDat;
	}

	public BigDecimal getTotalDatAbril() {
		return this.totalDatAbril;
	}

	public void setTotalDatAbril(BigDecimal totalDatAbril) {
		this.totalDatAbril = totalDatAbril;
	}

	public BigDecimal getTotalDatAgosto() {
		return this.totalDatAgosto;
	}

	public void setTotalDatAgosto(BigDecimal totalDatAgosto) {
		this.totalDatAgosto = totalDatAgosto;
	}

	public BigDecimal getTotalDatDiciembre() {
		return this.totalDatDiciembre;
	}

	public void setTotalDatDiciembre(BigDecimal totalDatDiciembre) {
		this.totalDatDiciembre = totalDatDiciembre;
	}

	public BigDecimal getTotalDatEnero() {
		return this.totalDatEnero;
	}

	public void setTotalDatEnero(BigDecimal totalDatEnero) {
		this.totalDatEnero = totalDatEnero;
	}

	public BigDecimal getTotalDatFebrero() {
		return this.totalDatFebrero;
	}

	public void setTotalDatFebrero(BigDecimal totalDatFebrero) {
		this.totalDatFebrero = totalDatFebrero;
	}

	public BigDecimal getTotalDatJulio() {
		return this.totalDatJulio;
	}

	public void setTotalDatJulio(BigDecimal totalDatJulio) {
		this.totalDatJulio = totalDatJulio;
	}

	public BigDecimal getTotalDatJunio() {
		return this.totalDatJunio;
	}

	public void setTotalDatJunio(BigDecimal totalDatJunio) {
		this.totalDatJunio = totalDatJunio;
	}

	public BigDecimal getTotalDatMarzo() {
		return this.totalDatMarzo;
	}

	public void setTotalDatMarzo(BigDecimal totalDatMarzo) {
		this.totalDatMarzo = totalDatMarzo;
	}

	public BigDecimal getTotalDatMayo() {
		return this.totalDatMayo;
	}

	public void setTotalDatMayo(BigDecimal totalDatMayo) {
		this.totalDatMayo = totalDatMayo;
	}

	public BigDecimal getTotalDatNoviembre() {
		return this.totalDatNoviembre;
	}

	public void setTotalDatNoviembre(BigDecimal totalDatNoviembre) {
		this.totalDatNoviembre = totalDatNoviembre;
	}

	public BigDecimal getTotalDatOctubre() {
		return this.totalDatOctubre;
	}

	public void setTotalDatOctubre(BigDecimal totalDatOctubre) {
		this.totalDatOctubre = totalDatOctubre;
	}

	public BigDecimal getTotalDatSeptiembre() {
		return this.totalDatSeptiembre;
	}

	public void setTotalDatSeptiembre(BigDecimal totalDatSeptiembre) {
		this.totalDatSeptiembre = totalDatSeptiembre;
	}

	public BigDecimal getTotalSeguro() {
		return this.totalSeguro;
	}

	public void setTotalSeguro(BigDecimal totalSeguro) {
		this.totalSeguro = totalSeguro;
	}

	public PrecioPrevision getPrecioPrevision() {
		return precioPrevision;
	}

	public void setPrecioPrevision(PrecioPrevision precioPrevision) {
		this.precioPrevision = precioPrevision;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	public String getPrecioMarcador() {
		return precioMarcador;
	}

	public void setPrecioMarcador(String precioMarcador) {
		this.precioMarcador = precioMarcador;
	}
	
	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
}