

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Actor;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase ActorDao
 @autor dgonzalez
*/
public interface ActorDao extends GenericDAO<Actor, Long> {
	
}

   