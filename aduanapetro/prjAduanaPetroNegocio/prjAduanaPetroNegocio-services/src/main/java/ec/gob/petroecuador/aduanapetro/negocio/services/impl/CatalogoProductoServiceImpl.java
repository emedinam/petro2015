
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.CatalogoProductoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.CatalogoProducto;
import ec.gob.petroecuador.aduanapetro.negocio.services.CatalogoProductoService;

/**
 Clase CatalogoProductoServiceImpl
 @autor dgonzalez
*/
@Service
public class CatalogoProductoServiceImpl extends GenericServiceImpl<CatalogoProducto, Long> implements CatalogoProductoService {

	@Autowired
	CatalogoProductoDao dao;
	
	@Override
	public GenericDAO<CatalogoProducto, Long> getDao() {
		return dao;
	}

}
   