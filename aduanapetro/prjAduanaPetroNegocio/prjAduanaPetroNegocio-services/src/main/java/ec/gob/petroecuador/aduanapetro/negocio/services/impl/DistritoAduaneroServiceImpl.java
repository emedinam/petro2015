
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.DistritoAduaneroDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.DistritoAduanero;
import ec.gob.petroecuador.aduanapetro.negocio.services.DistritoAduaneroService;

/**
 Clase DistritoAduaneroServiceImpl
 @autor dgonzalez
*/
@Service
public class DistritoAduaneroServiceImpl extends GenericServiceImpl<DistritoAduanero, Long> implements DistritoAduaneroService {

	@Autowired
	DistritoAduaneroDao dao;
	
	@Override
	public GenericDAO<DistritoAduanero, Long> getDao() {
		return dao;
	}

}
   