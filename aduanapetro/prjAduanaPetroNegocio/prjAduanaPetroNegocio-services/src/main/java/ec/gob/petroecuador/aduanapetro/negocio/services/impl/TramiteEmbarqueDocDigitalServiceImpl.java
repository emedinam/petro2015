
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TramiteEmbarqueDocDigitalDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDocDigital;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueDocDigitalService;

/**
 Clase TramiteEmbarqueDocDigitalServiceImpl
 @autor dgonzalez
*/
@Service
public class TramiteEmbarqueDocDigitalServiceImpl extends GenericServiceImpl<TramiteEmbarqueDocDigital, Long> implements TramiteEmbarqueDocDigitalService {

	@Autowired
	TramiteEmbarqueDocDigitalDao dao;
	
	@Override
	public GenericDAO<TramiteEmbarqueDocDigital, Long> getDao() {
		return dao;
	}

	@Override
	public List<TramiteEmbarqueDocDigital> obtenerPorPadre(
			TramiteEmbarque tramiteEmbarque) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("tramiteEmbarque", tramiteEmbarque.getId());
		return dao.findByNamedQuery("obtenerPorPadreTEDD", map);
	}

}
   