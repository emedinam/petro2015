

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase TramiteEmbarqueDao
 @autor dgonzalez
*/
public interface TramiteEmbarqueDao extends GenericDAO<TramiteEmbarque, Long> {
	
}

   