
package ec.gob.petroecuador.aduanapetro.negocio.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;

/**
 Clase TramiteEmbarqueService
 @autor dgonzalez
*/
public interface TramiteEmbarqueService extends GenericService<TramiteEmbarque, Long> {
	public List<TramiteEmbarque> obtenerPorTipo(String tipo);
}
   