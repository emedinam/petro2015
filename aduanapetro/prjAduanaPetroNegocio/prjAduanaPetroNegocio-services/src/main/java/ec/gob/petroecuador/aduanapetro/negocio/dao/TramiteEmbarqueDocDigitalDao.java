

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDocDigital;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase TramiteEmbarqueDocDigitalDao
 @autor dgonzalez
*/
public interface TramiteEmbarqueDocDigitalDao extends GenericDAO<TramiteEmbarqueDocDigital, Long> {
	
}

   