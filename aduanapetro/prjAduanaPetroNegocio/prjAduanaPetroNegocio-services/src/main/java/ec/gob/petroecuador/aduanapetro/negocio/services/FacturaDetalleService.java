
package ec.gob.petroecuador.aduanapetro.negocio.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Factura;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.FacturaDetalle;

/**
 Clase FacturaDetalleService
 @autor dgonzalez
*/
public interface FacturaDetalleService extends GenericService<FacturaDetalle, Long> {
	public List<FacturaDetalle> obtenerPorPadre(Factura factura);
}
   