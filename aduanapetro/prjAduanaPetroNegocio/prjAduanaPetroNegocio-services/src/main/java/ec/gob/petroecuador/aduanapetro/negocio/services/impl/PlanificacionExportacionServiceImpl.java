
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.PlanificacionExportacionDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Adjudicacion;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionExportacion;
import ec.gob.petroecuador.aduanapetro.negocio.services.PlanificacionExportacionService;

/**
 Clase PlanificacionExportacionServiceImpl
 @autor dgonzalez
*/
@Service
public class PlanificacionExportacionServiceImpl extends GenericServiceImpl<PlanificacionExportacion, Long> implements PlanificacionExportacionService {

	@Autowired
	PlanificacionExportacionDao dao;
	
	@Override
	public GenericDAO<PlanificacionExportacion, Long> getDao() {
		return dao;
	}

	@Override
	public List<PlanificacionExportacion> obtenerPorPadre(
			Adjudicacion adjudicacion) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("adjudicacion", adjudicacion.getId());
		return dao.findByNamedQuery("obtenerPorPadrePE", map);
	}

}
   