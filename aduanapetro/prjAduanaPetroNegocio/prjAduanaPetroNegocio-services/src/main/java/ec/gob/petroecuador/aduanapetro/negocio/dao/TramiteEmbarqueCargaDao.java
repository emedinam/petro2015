

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueCarga;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase TramiteEmbarqueCargaDao
 @autor dgonzalez
*/
public interface TramiteEmbarqueCargaDao extends GenericDAO<TramiteEmbarqueCarga, Long> {
	
}

   