package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;


/**
 * The persistent class for the T_TRAMITE_EMBARQUE_DESAD database table.
 * 
 */
@NamedQueries({
	@NamedQuery(
		name = "obtenerPorPadreTED",
		query = "select p from TramiteEmbarqueDesaduanamiento p where p.estado='ACTIVO' and p.TramiteEmbarque.id=:tramiteEmbarque"
	),
})
@Entity
@Table(name = "NG_TRAMITE_EMBARQUE_DESAD", schema = "APS_NEGOCIO")
public class TramiteEmbarqueDesaduanamiento extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	private BigDecimal generado;

	private String numero;
	
	@Column(name="DOCUMENTOS")
	private String documentos;

	@Column(name="RUTA_ARCHIVO")
	private String rutaArchivo;
	
	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;
	
	@Column(name="USUARIO_ACTUALIZACION")
	private String usuarioActualizacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ACTUALIZACION")
	private Date fechaActualizacion;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	//bi-directional many-to-one association to GeneracionDocumento
	@OneToMany(mappedBy="TramiteEmbarqueDesaduanamiento")
	private List<GeneracionDocumento> GeneracionDocumentos;

	//bi-directional many-to-one association to DistritoAduanero
	@ManyToOne
	@JoinColumn(name="IF_DISTRITO_ADUANERO")
	private DistritoAduanero DistritoAduanero;

	//bi-directional many-to-one association to DocumentoAdjunto
	@ManyToOne
	@JoinColumn(name="IF_DOCUMENTO_ADJUNTO")
	private DocumentoAdjunto DocumentoAdjunto;

	//bi-directional many-to-one association to TramiteEmbarque
	@ManyToOne
	@JoinColumn(name="IF_TRAMITE_EMBARQUE")
	private TramiteEmbarque TramiteEmbarque;

	public TramiteEmbarqueDesaduanamiento() {
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getGenerado() {
		return this.generado;
	}

	public void setGenerado(BigDecimal generado) {
		this.generado = generado;
	}

	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getRutaArchivo() {
		return this.rutaArchivo;
	}

	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}

	public List<GeneracionDocumento> getGeneracionDocumentos() {
		return this.GeneracionDocumentos;
	}

	public void setGeneracionDocumentos(List<GeneracionDocumento> GeneracionDocumentos) {
		this.GeneracionDocumentos = GeneracionDocumentos;
	}

	public GeneracionDocumento addGeneracionDocumento(GeneracionDocumento GeneracionDocumento) {
		getGeneracionDocumentos().add(GeneracionDocumento);
		GeneracionDocumento.setTramiteEmbarqueDesaduanamiento(this);

		return GeneracionDocumento;
	}

	public GeneracionDocumento removeGeneracionDocumento(GeneracionDocumento GeneracionDocumento) {
		getGeneracionDocumentos().remove(GeneracionDocumento);
		GeneracionDocumento.setTramiteEmbarqueDesaduanamiento(null);

		return GeneracionDocumento;
	}

	public DistritoAduanero getDistritoAduanero() {
		return this.DistritoAduanero;
	}

	public void setDistritoAduanero(DistritoAduanero DistritoAduanero) {
		this.DistritoAduanero = DistritoAduanero;
	}

	public DocumentoAdjunto getDocumentoAdjunto() {
		return this.DocumentoAdjunto;
	}

	public void setDocumentoAdjunto(DocumentoAdjunto DocumentoAdjunto) {
		this.DocumentoAdjunto = DocumentoAdjunto;
	}

	public TramiteEmbarque getTramiteEmbarque() {
		return this.TramiteEmbarque;
	}

	public void setTramiteEmbarque(TramiteEmbarque TramiteEmbarque) {
		this.TramiteEmbarque = TramiteEmbarque;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	public String getDocumentos() {
		return documentos;
	}

	public void setDocumentos(String documentos) {
		this.documentos = documentos;
	}
	
	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
}