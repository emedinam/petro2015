
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.DocumentoAdjunto;

/**
 Clase DocumentoAdjuntoService
 @autor dgonzalez
*/
public interface DocumentoAdjuntoService extends GenericService<DocumentoAdjunto, Long> {
	
}
   