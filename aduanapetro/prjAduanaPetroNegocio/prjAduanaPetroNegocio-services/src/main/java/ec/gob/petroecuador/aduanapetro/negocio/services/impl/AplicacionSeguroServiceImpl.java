
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.AplicacionSeguroDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.AplicacionSeguro;
import ec.gob.petroecuador.aduanapetro.negocio.services.AplicacionSeguroService;

/**
 Clase AplicacionSeguroServiceImpl
 @autor dgonzalez
*/
@Service
public class AplicacionSeguroServiceImpl extends GenericServiceImpl<AplicacionSeguro, Long> implements AplicacionSeguroService {

	@Autowired
	AplicacionSeguroDao dao;
	
	@Override
	public GenericDAO<AplicacionSeguro, Long> getDao() {
		return dao;
	}

}
   