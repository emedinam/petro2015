

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueCalculoPrecio;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase TramiteEmbarqueCalculoPrecioDao
 @autor dgonzalez
*/
public interface TramiteEmbarqueCalculoPrecioDao extends GenericDAO<TramiteEmbarqueCalculoPrecio, Long> {
	
}

   