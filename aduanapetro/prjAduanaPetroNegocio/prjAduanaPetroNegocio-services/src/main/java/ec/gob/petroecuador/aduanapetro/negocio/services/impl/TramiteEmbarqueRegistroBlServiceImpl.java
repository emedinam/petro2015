
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TramiteEmbarqueRegistroBlDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueRegistroBl;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueRegistroBlService;

/**
 Clase TramiteEmbarqueRegistroBlServiceImpl
 @autor dgonzalez
*/
@Service
public class TramiteEmbarqueRegistroBlServiceImpl extends GenericServiceImpl<TramiteEmbarqueRegistroBl, Long> implements TramiteEmbarqueRegistroBlService {

	@Autowired
	TramiteEmbarqueRegistroBlDao dao;
	
	@Override
	public GenericDAO<TramiteEmbarqueRegistroBl, Long> getDao() {
		return dao;
	}

	@Override
	public List<TramiteEmbarqueRegistroBl> obtenerPorPadre(
			TramiteEmbarque tramiteEmbarque) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("tramiteEmbarque", tramiteEmbarque.getId());
		return dao.findByNamedQuery("obtenerPorPadreTERBL", map);
	}

}
   