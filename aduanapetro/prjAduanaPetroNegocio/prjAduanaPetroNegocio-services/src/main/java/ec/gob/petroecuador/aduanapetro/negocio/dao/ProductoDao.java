

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Producto;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase ProductoDao
 @autor dgonzalez
*/
public interface ProductoDao extends GenericDAO<Producto, Long> {
	
}

   