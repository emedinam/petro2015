
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TramiteEmbarqueFacturaDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueFactura;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueFacturaService;

/**
 Clase TramiteEmbarqueFacturaServiceImpl
 @autor dgonzalez
*/
@Service
public class TramiteEmbarqueFacturaServiceImpl extends GenericServiceImpl<TramiteEmbarqueFactura, Long> implements TramiteEmbarqueFacturaService {

	@Autowired
	TramiteEmbarqueFacturaDao dao;
	
	@Override
	public GenericDAO<TramiteEmbarqueFactura, Long> getDao() {
		return dao;
	}

	@Override
	public List<TramiteEmbarqueFactura> obtenerPorPadre(
			TramiteEmbarque tramiteEmbarque) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("tramiteEmbarque", tramiteEmbarque.getId());
		return dao.findByNamedQuery("obtenerPorPadreTEF", map);
	}

}
   