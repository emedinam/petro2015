
package ec.gob.petroecuador.aduanapetro.negocio.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDescarga;

/**
 Clase TramiteEmbarqueDescargaService
 @autor dgonzalez
*/
public interface TramiteEmbarqueDescargaService extends GenericService<TramiteEmbarqueDescarga, Long> {
	public List<TramiteEmbarqueDescarga> obtenerPorPadre(TramiteEmbarque tramiteEmbarque);
}
   