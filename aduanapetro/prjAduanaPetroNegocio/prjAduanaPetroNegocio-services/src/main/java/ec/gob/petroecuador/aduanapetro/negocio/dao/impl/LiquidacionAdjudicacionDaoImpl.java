
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.LiquidacionAdjudicacionDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.LiquidacionAdjudicacion;

/**
 Clase LiquidacionAdjudicacionDao
 @autor dgonzalez
*/
@Component("liquidacionadjudicacionDao")
public class LiquidacionAdjudicacionDaoImpl extends GenericDAOImpl<LiquidacionAdjudicacion, Long> implements LiquidacionAdjudicacionDao {
	
}
   