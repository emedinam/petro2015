
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.PlanificacionExportacionDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionExportacion;

/**
 Clase PlanificacionExportacionDao
 @autor dgonzalez
*/
@Component("planificacionexportacionDao")
public class PlanificacionExportacionDaoImpl extends GenericDAOImpl<PlanificacionExportacion, Long> implements PlanificacionExportacionDao {
	
}
   