package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import javax.persistence.*;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

import java.util.Date;
import java.util.List;

/**
 * The persistent class for the T_CATALOGO_PRODUCTO database table.
 * 
 */
@Entity
@Table(name = "NG_CATALOGO_PRODUCTO", schema= "APS_NEGOCIO")
public class CatalogoProducto extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "NOMBRE")
	private String nombre;

	// /datos de auditoria
	@Column(name = "USUARIO_CREACION")
	private String usuarioCreacion;

	@Column(name = "USUARIO_ACTUALIZACION")
	private String usuarioActualizacion;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_ACTUALIZACION")
	private Date fechaActualizacion;

	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	// bi-directional many-to-one association to ParametroCalculo
	@OneToMany(mappedBy = "CatalogoProducto")
	private List<ParametroCalculo> ParametroCalculos;

	// bi-directional many-to-one association to Garantia
	@OneToMany(mappedBy = "catalogoProducto")
	private List<Adjudicacion> TAdjudicaciones;

	public List<Adjudicacion> getTAdjudicaciones() {
		return TAdjudicaciones;
	}

	public void setTAdjudicaciones(List<Adjudicacion> tAdjudicaciones) {
		TAdjudicaciones = tAdjudicaciones;
	}

	public CatalogoProducto() {
	}

	// setter && getter
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<ParametroCalculo> getParametroCalculos() {
		return ParametroCalculos;
	}

	public void setParametroCalculos(List<ParametroCalculo> parametroCalculos) {
		ParametroCalculos = parametroCalculos;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof CatalogoProducto))
			return false;
		return ((CatalogoProducto) obj).getId().equals(
				this.id);
	}
	
	@Override
	public String toString() {
		return nombre;
	}
	
	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;	
	}

	@Override
	public Estados getEstado() {
		return estado;
	}
}