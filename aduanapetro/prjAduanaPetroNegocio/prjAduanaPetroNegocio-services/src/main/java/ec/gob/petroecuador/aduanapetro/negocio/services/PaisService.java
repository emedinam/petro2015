
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Pais;

/**
 Clase PaisService
 @autor dgonzalez
*/
public interface PaisService extends GenericService<Pais, Long> {
	
}
   