
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TramiteEmbarqueDesaduanamientoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDesaduanamiento;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueDesaduanamientoService;

/**
 Clase TramiteEmbarqueDesaduanamientoServiceImpl
 @autor dgonzalez
*/
@Service
public class TramiteEmbarqueDesaduanamientoServiceImpl extends GenericServiceImpl<TramiteEmbarqueDesaduanamiento, Long> implements TramiteEmbarqueDesaduanamientoService {

	@Autowired
	TramiteEmbarqueDesaduanamientoDao dao;
	
	@Override
	public GenericDAO<TramiteEmbarqueDesaduanamiento, Long> getDao() {
		return dao;
	}

	@Override
	public List<TramiteEmbarqueDesaduanamiento> obtenerPorPadre(
			TramiteEmbarque tramiteEmbarque) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("tramiteEmbarque", tramiteEmbarque.getId());
		return dao.findByNamedQuery("obtenerPorPadreTED", map);
	}

}
   