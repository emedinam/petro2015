
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.PuertoDescargaDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PuertoDescarga;

/**
 Clase PuertoDescargaDao
 @autor dgonzalez
*/
@Component("puertodescargaDao")
public class PuertoDescargaDaoImpl extends GenericDAOImpl<PuertoDescarga, Long> implements PuertoDescargaDao {
	
}
   