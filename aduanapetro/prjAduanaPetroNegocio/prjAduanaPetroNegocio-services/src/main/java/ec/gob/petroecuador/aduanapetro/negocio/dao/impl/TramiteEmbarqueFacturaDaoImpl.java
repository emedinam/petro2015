
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TramiteEmbarqueFacturaDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueFactura;

/**
 Clase TramiteEmbarqueFacturaDao
 @autor dgonzalez
*/
@Component("tramiteembarquefacturaDao")
public class TramiteEmbarqueFacturaDaoImpl extends GenericDAOImpl<TramiteEmbarqueFactura, Long> implements TramiteEmbarqueFacturaDao {
	
}
   