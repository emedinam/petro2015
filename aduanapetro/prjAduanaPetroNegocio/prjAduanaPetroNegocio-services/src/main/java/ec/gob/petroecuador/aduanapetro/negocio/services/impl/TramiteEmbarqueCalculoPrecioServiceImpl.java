
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TramiteEmbarqueCalculoPrecioDao;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueCalculoPrecio;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueCalculoPrecioService;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;

/**
 Clase TramiteEmbarqueCalculoPrecioServiceImpl
 @autor dgonzalez
*/
@Service
public class TramiteEmbarqueCalculoPrecioServiceImpl extends GenericServiceImpl<TramiteEmbarqueCalculoPrecio, Long> implements TramiteEmbarqueCalculoPrecioService {

	@Autowired
	TramiteEmbarqueCalculoPrecioDao dao;
	
	@Override
	public GenericDAO<TramiteEmbarqueCalculoPrecio, Long> getDao() {
		return dao;
	}
	
	@Override
	public List<TramiteEmbarqueCalculoPrecio> obtenerPorPadre(
			TramiteEmbarque tramiteEmbarque) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("tramiteEmbarque", tramiteEmbarque.getId());
		return dao.findByNamedQuery("TramiteEmbarqueCalculoPrecioConPadre", map);
		
		
	}

}
   