
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.ParametroCalculoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.ParametroCalculo;
import ec.gob.petroecuador.aduanapetro.negocio.services.ParametroCalculoService;

/**
 Clase ParametroCalculoServiceImpl
 @autor dgonzalez
*/
@Service
public class ParametroCalculoServiceImpl extends GenericServiceImpl<ParametroCalculo, Long> implements ParametroCalculoService {

	@Autowired
	ParametroCalculoDao dao;
	
	@Override
	public GenericDAO<ParametroCalculo, Long> getDao() {
		return dao;
	}

}
   