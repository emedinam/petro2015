package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import javax.persistence.*;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the T_PRECIO_PREVISION database table.
 * 
 */
@NamedQueries({
	@NamedQuery(
		name = "obtenerPorParametroCalculo",
		query = "select p from PrecioPrevision p where p.estado='ACTIVO' and p.id=:parametroCalculo"
	),
})
@Entity
@Table(name = "NG_PRECIO_PREVISION", schema = "APS_NEGOCIO")
public class PrecioPrevision extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name="ANIO_ACTUAL")
	private BigDecimal anioActual;

	@Column(name="DIFERENCIAL")
	private BigDecimal diferencial;

	@Column(name="GALON_ABRIL")
	private BigDecimal galonAbril;

	@Column(name="GALON_AGOSTO")
	private BigDecimal galonAgosto;

	@Column(name="GALON_DICIEMBRE")
	private BigDecimal galonDiciembre;

	@Column(name="GALON_ENERO")
	private BigDecimal galonEnero;

	@Column(name="GALON_FEBRERO")
	private BigDecimal galonFebrero;

	@Column(name="GALON_JULIO")
	private BigDecimal galonJulio;

	@Column(name="GALON_JUNIO")
	private BigDecimal galonJunio;

	@Column(name="GALON_MARZO")
	private BigDecimal galonMarzo;

	@Column(name="GALON_MAYO")
	private BigDecimal galonMayo;

	@Column(name="GALON_NOVIEMBRE")
	private BigDecimal galonNoviembre;

	@Column(name="GALON_OCTUBRE")
	private BigDecimal galonOctubre;

	@Column(name="GALON_SEPTIEMBRE")
	private BigDecimal galonSeptiembre;

	@Column(name="PROMEDIO")
	private BigDecimal promedio;

	@Column(name="SUBTOTAL")
	private BigDecimal subtotal;
	
	@Column(name="TOTAL")
	private BigDecimal total;

	@Column(name="VARIACION_PRECIO")
	private BigDecimal variacionPrecio;
	
	///datos de auditoria
	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;
			
	@Column(name="USUARIO_ACTUALIZACION")
	private String usuarioActualizacion;
			
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
			
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ACTUALIZACION")
	private Date fechaActualizacion;
			
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	
	@Column(name="PRECIO_MARCADOR")
	private String precioMarcador;


	//bi-directional many-to-one association to PrecioMarcador
	@ManyToOne
	@JoinColumn(name="IF_PARAMETRO_CALCULO")
	private ParametroCalculo parametroCalculo;
	
	 //bi-directional many-to-one association to ParametroCalculo
  	@OneToMany(mappedBy="precioPrevision")
  	private List<PlanificacionVolumen> PlanificacionVolumens;
	
	public PrecioPrevision() {
	}

	public BigDecimal getAnioActual() {
		return this.anioActual;
	}

	public void setAnioActual(BigDecimal anioActual) {
		this.anioActual = anioActual;
	}

	public BigDecimal getDiferencial() {
		return this.diferencial;
	}

	public void setDiferencial(BigDecimal diferencial) {
		this.diferencial = diferencial;
	}

	public BigDecimal getGalonAbril() {
		return this.galonAbril;
	}

	public void setGalonAbril(BigDecimal galonAbril) {
		this.galonAbril = galonAbril;
	}

	public BigDecimal getGalonAgosto() {
		return this.galonAgosto;
	}

	public void setGalonAgosto(BigDecimal galonAgosto) {
		this.galonAgosto = galonAgosto;
	}

	public BigDecimal getGalonDiciembre() {
		return this.galonDiciembre;
	}

	public void setGalonDiciembre(BigDecimal galonDiciembre) {
		this.galonDiciembre = galonDiciembre;
	}

	public BigDecimal getGalonEnero() {
		return this.galonEnero;
	}

	public void setGalonEnero(BigDecimal galonEnero) {
		this.galonEnero = galonEnero;
	}

	public BigDecimal getGalonFebrero() {
		return this.galonFebrero;
	}

	public void setGalonFebrero(BigDecimal galonFebrero) {
		this.galonFebrero = galonFebrero;
	}

	public BigDecimal getGalonJulio() {
		return this.galonJulio;
	}

	public void setGalonJulio(BigDecimal galonJulio) {
		this.galonJulio = galonJulio;
	}

	public BigDecimal getGalonJunio() {
		return this.galonJunio;
	}

	public void setGalonJunio(BigDecimal galonJunio) {
		this.galonJunio = galonJunio;
	}

	public BigDecimal getGalonMarzo() {
		return this.galonMarzo;
	}

	public void setGalonMarzo(BigDecimal galonMarzo) {
		this.galonMarzo = galonMarzo;
	}

	public BigDecimal getGalonMayo() {
		return this.galonMayo;
	}

	public void setGalonMayo(BigDecimal galonMayo) {
		this.galonMayo = galonMayo;
	}

	public BigDecimal getGalonNoviembre() {
		return this.galonNoviembre;
	}

	public void setGalonNoviembre(BigDecimal galonNoviembre) {
		this.galonNoviembre = galonNoviembre;
	}

	public BigDecimal getGalonOctubre() {
		return this.galonOctubre;
	}

	public void setGalonOctubre(BigDecimal galonOctubre) {
		this.galonOctubre = galonOctubre;
	}

	public BigDecimal getGalonSeptiembre() {
		return this.galonSeptiembre;
	}

	public void setGalonSeptiembre(BigDecimal galonSeptiembre) {
		this.galonSeptiembre = galonSeptiembre;
	}

	public BigDecimal getPromedio() {
		return this.promedio;
	}

	public void setPromedio(BigDecimal promedio) {
		this.promedio = promedio;
	}

	public BigDecimal getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getVariacionPrecio() {
		return this.variacionPrecio;
	}

	public void setVariacionPrecio(BigDecimal variacionPrecio) {
		this.variacionPrecio = variacionPrecio;
	}

	public ParametroCalculo getParametroCalculo() {
		return parametroCalculo;
	}

	public void setParametroCalculo(ParametroCalculo parametroCalculo) {
		this.parametroCalculo = parametroCalculo;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	public List<PlanificacionVolumen> getPlanificacionVolumens() {
		return PlanificacionVolumens;
	}

	public void setPlanificacionVolumens(
			List<PlanificacionVolumen> planificacionVolumens) {
		PlanificacionVolumens = planificacionVolumens;
	}

	public String getPrecioMarcador() {
		return precioMarcador;
	}

	public void setPrecioMarcador(String precioMarcador) {
		this.precioMarcador = precioMarcador;
	}
	
	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

}