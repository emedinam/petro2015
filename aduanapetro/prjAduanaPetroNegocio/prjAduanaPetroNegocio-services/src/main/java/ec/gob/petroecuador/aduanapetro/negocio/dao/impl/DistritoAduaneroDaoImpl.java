
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.DistritoAduaneroDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.DistritoAduanero;

/**
 Clase DistritoAduaneroDao
 @autor dgonzalez
*/
@Component("distritoaduaneroDao")
public class DistritoAduaneroDaoImpl extends GenericDAOImpl<DistritoAduanero, Long> implements DistritoAduaneroDao {
	
}
   