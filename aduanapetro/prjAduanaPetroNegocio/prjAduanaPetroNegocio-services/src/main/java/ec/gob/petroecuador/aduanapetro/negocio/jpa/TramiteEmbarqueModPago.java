package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;


/**
 * The persistent class for the T_TRAMITE_EMBARQUE_MOD_PAGO database table.
 * 
 */
@NamedQueries({
	@NamedQuery(
		name = "obtenerPorPadreTEMP",
		query = "select p from TramiteEmbarqueModPago p where p.estado='ACTIVO' and p.TramiteEmbarque.id=:tramiteEmbarque"
	),
})
@Entity
@Table(name = "NG_TRAMITE_EMBARQUE_MOD_PAGO", schema = "APS_NEGOCIO")
public class TramiteEmbarqueModPago extends DatabaseObject<Long>{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	private String beneficiario;

	@Column(name="DIRECCION_BENEFICIARIO")
	private String direccionBeneficiario;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_APERTURA")
	private Date fechaApertura;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_OFICIO_BCE")
	private Date fechaOficioBce;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_OFICIO_MEF")
	private Date fechaOficioMef;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_PAGO")
	private Date fechaPago;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_VENCIMIENTO")
	private Date fechaVencimiento;

	@Column(name="GENERADO_BCE")
	private BigDecimal generadoBce;

	@Column(name="GENERADO_MEF")
	private BigDecimal generadoMef;

	private String numero;
	
	@Column(name="GIRO")
	private String giro;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_GIRO")
	private Date fechaGiro;

	@Column(name="NUMERO_OFICIO_BCE")
	private String numeroOficioBce;

	@Column(name="NUMERO_OFICIO_MEF")
	private String numeroOficioMef;

	private String observacion;

	@Column(name="RUTA_ARCHIVO")
	private String rutaArchivo;

	private String solicitante;

	private String tipo;

	private BigDecimal tolerancia;

	@Column(name="VALOR_TEXTO")
	private String valorTexto;

	@Column(name="VOLUMEN_EMBARQUE")
	private BigDecimal volumenEmbarque;
	
	@Column(name="NUMERO_DOCUMENTOACC")
	private String numeroDocumentoacc;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_DOCUMENTOACC")
	private Date fechaDocumentoacc;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ANULACIONCC")
	private Date fechaAnulacionacc;
	
	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;
	
	@Column(name="USUARIO_ACTUALIZACION")
	private String usuarioActualizacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCreaCION;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ACTUALIZACION")
	private Date fechaActualizacion;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	
	//bi-directional many-to-one association to GeneracionDocumento
	@OneToMany(mappedBy="TramiteEmbarqueModPago1")
	private List<GeneracionDocumento> GeneracionDocumentos1;

	//bi-directional many-to-one association to GeneracionDocumento
	@OneToMany(mappedBy="TramiteEmbarqueModPago2")
	private List<GeneracionDocumento> GeneracionDocumentos2;

	//bi-directional many-to-one association to TramiteEmbarque
	@ManyToOne
	@JoinColumn(name="IF_TRAMITE_EMBARQUE")
	private TramiteEmbarque TramiteEmbarque;

	public TramiteEmbarqueModPago() {
	}

	public String getBeneficiario() {
		return this.beneficiario;
	}

	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	public String getDireccionBeneficiario() {
		return this.direccionBeneficiario;
	}

	public void setDireccionBeneficiario(String direccionBeneficiario) {
		this.direccionBeneficiario = direccionBeneficiario;
	}

	public Date getFechaApertura() {
		return this.fechaApertura;
	}

	public void setFechaApertura(Date fechaApertura) {
		this.fechaApertura = fechaApertura;
	}

	public Date getFechaOficioBce() {
		return this.fechaOficioBce;
	}

	public void setFechaOficioBce(Date fechaOficioBce) {
		this.fechaOficioBce = fechaOficioBce;
	}

	public Date getFechaOficioMef() {
		return this.fechaOficioMef;
	}

	public void setFechaOficioMef(Date fechaOficioMef) {
		this.fechaOficioMef = fechaOficioMef;
	}

	public Date getFechaPago() {
		return this.fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public Date getFechaVencimiento() {
		return this.fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public BigDecimal getGeneradoBce() {
		return this.generadoBce;
	}

	public void setGeneradoBce(BigDecimal generadoBce) {
		this.generadoBce = generadoBce;
	}

	public BigDecimal getGeneradoMef() {
		return this.generadoMef;
	}

	public void setGeneradoMef(BigDecimal generadoMef) {
		this.generadoMef = generadoMef;
	}

	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNumeroOficioBce() {
		return this.numeroOficioBce;
	}

	public void setNumeroOficioBce(String numeroOficioBce) {
		this.numeroOficioBce = numeroOficioBce;
	}

	public String getNumeroOficioMef() {
		return this.numeroOficioMef;
	}

	public void setNumeroOficioMef(String numeroOficioMef) {
		this.numeroOficioMef = numeroOficioMef;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getRutaArchivo() {
		return this.rutaArchivo;
	}

	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}

	public String getSolicitante() {
		return this.solicitante;
	}

	public void setSolicitante(String solicitante) {
		this.solicitante = solicitante;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public BigDecimal getTolerancia() {
		return this.tolerancia;
	}

	public void setTolerancia(BigDecimal tolerancia) {
		this.tolerancia = tolerancia;
	}

	public String getValorTexto() {
		return this.valorTexto;
	}

	public void setValorTexto(String valorTexto) {
		this.valorTexto = valorTexto;
	}

	public BigDecimal getVolumenEmbarque() {
		return this.volumenEmbarque;
	}

	public void setVolumenEmbarque(BigDecimal volumenEmbarque) {
		this.volumenEmbarque = volumenEmbarque;
	}

	public List<GeneracionDocumento> getGeneracionDocumentos1() {
		return this.GeneracionDocumentos1;
	}

	public void setGeneracionDocumentos1(List<GeneracionDocumento> GeneracionDocumentos1) {
		this.GeneracionDocumentos1 = GeneracionDocumentos1;
	}

	public GeneracionDocumento addGeneracionDocumentos1(GeneracionDocumento GeneracionDocumentos1) {
		getGeneracionDocumentos1().add(GeneracionDocumentos1);
		GeneracionDocumentos1.setTramiteEmbarqueModPago1(this);

		return GeneracionDocumentos1;
	}

	public GeneracionDocumento removeGeneracionDocumentos1(GeneracionDocumento GeneracionDocumentos1) {
		getGeneracionDocumentos1().remove(GeneracionDocumentos1);
		GeneracionDocumentos1.setTramiteEmbarqueModPago1(null);

		return GeneracionDocumentos1;
	}

	public List<GeneracionDocumento> getGeneracionDocumentos2() {
		return this.GeneracionDocumentos2;
	}

	public void setGeneracionDocumentos2(List<GeneracionDocumento> GeneracionDocumentos2) {
		this.GeneracionDocumentos2 = GeneracionDocumentos2;
	}

	public GeneracionDocumento addGeneracionDocumentos2(GeneracionDocumento GeneracionDocumentos2) {
		getGeneracionDocumentos2().add(GeneracionDocumentos2);
		GeneracionDocumentos2.setTramiteEmbarqueModPago2(this);

		return GeneracionDocumentos2;
	}

	public GeneracionDocumento removeGeneracionDocumentos2(GeneracionDocumento GeneracionDocumentos2) {
		getGeneracionDocumentos2().remove(GeneracionDocumentos2);
		GeneracionDocumentos2.setTramiteEmbarqueModPago2(null);

		return GeneracionDocumentos2;
	}

	public TramiteEmbarque getTramiteEmbarque() {
		return this.TramiteEmbarque;
	}

	public void setTramiteEmbarque(TramiteEmbarque TramiteEmbarque) {
		this.TramiteEmbarque = TramiteEmbarque;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public Date getFechaCreaCION() {
		return fechaCreaCION;
	}

	public void setFechaCreaCION(Date fechaCreaCION) {
		this.fechaCreaCION = fechaCreaCION;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	public String getGiro() {
		return giro;
	}

	public void setGiro(String giro) {
		this.giro = giro;
	}

	public Date getFechaGiro() {
		return fechaGiro;
	}

	public void setFechaGiro(Date fechaGiro) {
		this.fechaGiro = fechaGiro;
	}

	public String getNumeroDocumentoacc() {
		return numeroDocumentoacc;
	}

	public void setNumeroDocumentoacc(String numeroDocumentoacc) {
		this.numeroDocumentoacc = numeroDocumentoacc;
	}

	public Date getFechaDocumentoacc() {
		return fechaDocumentoacc;
	}

	public void setFechaDocumentoacc(Date fechaDocumentoacc) {
		this.fechaDocumentoacc = fechaDocumentoacc;
	}

	public Date getFechaAnulacionacc() {
		return fechaAnulacionacc;
	}

	public void setFechaAnulacionacc(Date fechaAnulacionacc) {
		this.fechaAnulacionacc = fechaAnulacionacc;
	}
	
	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
}