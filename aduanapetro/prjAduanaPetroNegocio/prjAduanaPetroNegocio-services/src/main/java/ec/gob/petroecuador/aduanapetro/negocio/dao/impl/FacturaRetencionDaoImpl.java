
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.FacturaRetencionDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.FacturaRetencion;

/**
 Clase FacturaRetencionDao
 @autor dgonzalez
*/
@Component("facturaretencionDao")
public class FacturaRetencionDaoImpl extends GenericDAOImpl<FacturaRetencion, Long> implements FacturaRetencionDao {
	
}
   