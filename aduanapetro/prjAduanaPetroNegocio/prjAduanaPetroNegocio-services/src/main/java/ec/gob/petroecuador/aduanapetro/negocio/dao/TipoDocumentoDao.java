

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TipoDocumento;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase TipoDocumentoDao
 @autor dgonzalez
*/
public interface TipoDocumentoDao extends GenericDAO<TipoDocumento, Long> {
	
}

   