
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.CompaniaSeguroDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.CompaniaSeguro;
import ec.gob.petroecuador.aduanapetro.negocio.services.CompaniaSeguroService;

/**
 Clase CompaniaSeguroServiceImpl
 @autor dgonzalez
*/
@Service
public class CompaniaSeguroServiceImpl extends GenericServiceImpl<CompaniaSeguro, Long> implements CompaniaSeguroService {

	@Autowired
	CompaniaSeguroDao dao;
	
	@Override
	public GenericDAO<CompaniaSeguro, Long> getDao() {
		return dao;
	}

}
   