
package ec.gob.petroecuador.aduanapetro.negocio.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueModPago;

/**
 Clase TramiteEmbarqueModPagoService
 @autor dgonzalez
*/
public interface TramiteEmbarqueModPagoService extends GenericService<TramiteEmbarqueModPago, Long> {
	public List<TramiteEmbarqueModPago> obtenerPorPadre(TramiteEmbarque tramiteEmbarque);
}
   