package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;


/**
 * The persistent class for the T_PARAMETRO_CALCULO database table.
 * 
 */
@Entity
@Table(name = "NG_PARAMETRO_CALCULO", schema = "APS_NEGOCIO")
public class ParametroCalculo extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name="ANIO")
	private BigDecimal anio;

	@Column(name="DIFERENCIAL")
	private BigDecimal diferencial;

	@Column(name="OBSERVACION")
	private String observacion;

	///datos de auditoria
	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;
			
	@Column(name="USUARIO_ACTUALIZACION")
	private String usuarioActualizacion;
			
	@Temporal(TemporalType.DATE)

	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
			

	@Temporal(TemporalType.DATE)

	@Column(name="FECHA_ACTUALIZACION")
	private Date fechaActualizacion;
			
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	@Column(name="PORCENTAJE_GASTO")
	private BigDecimal porcentajeGasto;

	@Column(name="PORCENTAJE_SEGURO")
	private BigDecimal porcentajeSeguro;

	@Column(name="PORCENTAJE_VARIACION")
	private BigDecimal porcentajeVariacion;

	@Column(name="VOLUMEN")
	private BigDecimal volumen;

	//bi-directional many-to-one association to PrecioMarcador
	@ManyToOne
	@JoinColumn(name="IF_PRECIO_MARCADOR")
	private PrecioMarcador PrecioMarcador;
	
	//bi-directional many-to-one association to CatalogoProducto
    @ManyToOne
	@JoinColumn(name="IF_CATALOGO_PRODUCTO")
	private CatalogoProducto CatalogoProducto;
    
    //bi-directional many-to-one association to ParametroCalculo
  	@OneToMany(mappedBy="parametroCalculo")
  	private List<PrecioPrevision> PrecioPrevisions;
  	
  	public ParametroCalculo() {
	}

	public BigDecimal getAnio() {
		return this.anio;
	}

	public void setAnio(BigDecimal anio) {
		this.anio = anio;
	}

	public BigDecimal getDiferencial() {
		return this.diferencial;
	}

	public void setDiferencial(BigDecimal diferencial) {
		this.diferencial = diferencial;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public BigDecimal getPorcentajeGasto() {
		return this.porcentajeGasto;
	}

	public void setPorcentajeGasto(BigDecimal porcentajeGasto) {
		this.porcentajeGasto = porcentajeGasto;
	}

	public BigDecimal getPorcentajeSeguro() {
		return this.porcentajeSeguro;
	}

	public void setPorcentajeSeguro(BigDecimal porcentajeSeguro) {
		this.porcentajeSeguro = porcentajeSeguro;
	}

	public BigDecimal getPorcentajeVariacion() {
		return this.porcentajeVariacion;
	}

	public void setPorcentajeVariacion(BigDecimal porcentajeVariacion) {
		this.porcentajeVariacion = porcentajeVariacion;
	}

	public BigDecimal getVolumen() {
		return this.volumen;
	}

	public void setVolumen(BigDecimal volumen) {
		this.volumen = volumen;
	}

	public PrecioMarcador getPrecioMarcador() {
		return this.PrecioMarcador;
	}

	public void setPrecioMarcador(PrecioMarcador PrecioMarcador) {
		this.PrecioMarcador = PrecioMarcador;
	}

		public CatalogoProducto getCatalogoProducto() {
		return CatalogoProducto;
	}

	public void setCatalogoProducto(CatalogoProducto catalogoProducto) {
		CatalogoProducto = catalogoProducto;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	
	public List<PrecioPrevision> getPrecioPrevisions() {
		return PrecioPrevisions;
	}

	public void setPrecioPrevisions(List<PrecioPrevision> precioPrevisions) {
		PrecioPrevisions = precioPrevisions;
	}

	// metodos para combos

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof ParametroCalculo))
				return false;
		return ((ParametroCalculo) obj).getId().equals(this.id);
	}
	
	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
}