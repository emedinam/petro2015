
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.ActorDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Actor;

/**
 Clase ActorDao
 @autor dgonzalez
*/
@Component("actorDao")
public class ActorDaoImpl extends GenericDAOImpl<Actor, Long> implements ActorDao {
	
}
   