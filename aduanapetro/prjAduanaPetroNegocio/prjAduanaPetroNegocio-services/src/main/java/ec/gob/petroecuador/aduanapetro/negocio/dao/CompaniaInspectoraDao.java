

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.CompaniaInspectora;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase CompaniaInspectoraDao
 @autor dgonzalez
*/
public interface CompaniaInspectoraDao extends GenericDAO<CompaniaInspectora, Long> {
	
}

   