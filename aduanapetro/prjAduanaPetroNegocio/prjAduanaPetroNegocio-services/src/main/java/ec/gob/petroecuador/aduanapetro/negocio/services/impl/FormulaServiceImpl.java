
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.FormulaDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Formula;
import ec.gob.petroecuador.aduanapetro.negocio.services.FormulaService;

/**
 Clase FormulaServiceImpl
 @autor dgonzalez
*/
@Service
public class FormulaServiceImpl extends GenericServiceImpl<Formula, Long> implements FormulaService {

	@Autowired
	FormulaDao dao;
	
	@Override
	public GenericDAO<Formula, Long> getDao() {
		return dao;
	}

}
   