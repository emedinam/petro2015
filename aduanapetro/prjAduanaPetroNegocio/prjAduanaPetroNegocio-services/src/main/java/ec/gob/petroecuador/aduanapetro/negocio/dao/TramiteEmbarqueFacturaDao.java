

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueFactura;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase TramiteEmbarqueFacturaDao
 @autor dgonzalez
*/
public interface TramiteEmbarqueFacturaDao extends GenericDAO<TramiteEmbarqueFactura, Long> {
	
}

   