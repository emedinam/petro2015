
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.LiquidacionEmbarqueDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.LiquidacionEmbarque;

/**
 Clase LiquidacionEmbarqueDao
 @autor dgonzalez
*/
@Component("liquidacionembarqueDao")
public class LiquidacionEmbarqueDaoImpl extends GenericDAOImpl<LiquidacionEmbarque, Long> implements LiquidacionEmbarqueDao {
	
}
   