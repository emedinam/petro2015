

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.PuertoDescarga;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase PuertoDescargaDao
 @autor dgonzalez
*/
public interface PuertoDescargaDao extends GenericDAO<PuertoDescarga, Long> {
	
}

   