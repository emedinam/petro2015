
package ec.gob.petroecuador.aduanapetro.negocio.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueRegistroBl;

/**
 Clase TramiteEmbarqueRegistroBlService
 @autor dgonzalez
*/
public interface TramiteEmbarqueRegistroBlService extends GenericService<TramiteEmbarqueRegistroBl, Long> {

	public List<TramiteEmbarqueRegistroBl> obtenerPorPadre(TramiteEmbarque tramiteEmbarque);
	
}
   