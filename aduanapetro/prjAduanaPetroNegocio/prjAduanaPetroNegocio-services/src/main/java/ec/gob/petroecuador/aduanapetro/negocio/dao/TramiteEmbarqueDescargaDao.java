

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDescarga;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase TramiteEmbarqueDescargaDao
 @autor dgonzalez
*/
public interface TramiteEmbarqueDescargaDao extends GenericDAO<TramiteEmbarqueDescarga, Long> {
	
}

   