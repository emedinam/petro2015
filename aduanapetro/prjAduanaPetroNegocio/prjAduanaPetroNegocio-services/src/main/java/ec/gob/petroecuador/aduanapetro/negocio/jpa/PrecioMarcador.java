package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import javax.persistence.*;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the T_PRECIO_MARCADOR database table.
 * 
 */
@Entity
@Table(name = "NG_PRECIO_MARCADOR", schema = "APS_NEGOCIO")
public class PrecioMarcador extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name="NOMBRE")
	private String nombre;
	
	///datos de auditoria
	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;
	
	@Column(name="USUARIO_ACTUALIZACION")
	private String usuarioActualizacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ACTUALIZACION")
	private Date fechaActualizacion;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	//bi-directional many-to-one association to ParametroCalculo
	@OneToMany(mappedBy="PrecioMarcador")
	private List<ParametroCalculo> ParametroCalculos;

	public PrecioMarcador() {
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<ParametroCalculo> getParametroCalculos() {
		return this.ParametroCalculos;
	}

	public void setParametroCalculos(List<ParametroCalculo> ParametroCalculos) {
		this.ParametroCalculos = ParametroCalculos;
	}

	public ParametroCalculo addParametroCalculo(ParametroCalculo ParametroCalculo) {
		getParametroCalculos().add(ParametroCalculo);
		ParametroCalculo.setPrecioMarcador(this);

		return ParametroCalculo;
	}

	public ParametroCalculo removeParametroCalculo(ParametroCalculo ParametroCalculo) {
		getParametroCalculos().remove(ParametroCalculo);
		ParametroCalculo.setPrecioMarcador(null);

		return ParametroCalculo;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	
	// metodos para combos

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof PrecioMarcador))
			return false;
		return ((PrecioMarcador) obj).getId().equals(this.id);
	}

	@Override
	public String toString() {
		return nombre;
	}
	
	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

}