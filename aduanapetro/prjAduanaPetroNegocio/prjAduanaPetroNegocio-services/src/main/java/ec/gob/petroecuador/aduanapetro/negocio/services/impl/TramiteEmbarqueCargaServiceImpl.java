
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TramiteEmbarqueCargaDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueCarga;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueCargaService;

/**
 Clase TramiteEmbarqueCargaServiceImpl
 @autor dgonzalez
*/
@Service
public class TramiteEmbarqueCargaServiceImpl extends GenericServiceImpl<TramiteEmbarqueCarga, Long> implements TramiteEmbarqueCargaService {

	@Autowired
	TramiteEmbarqueCargaDao dao;
	
	@Override
	public GenericDAO<TramiteEmbarqueCarga, Long> getDao() {
		return dao;
	}

	@Override
	public List<TramiteEmbarqueCarga> obtenerPorPadre(
			TramiteEmbarque tramiteEmbarque) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("tramiteEmbarque", tramiteEmbarque.getId());
		return dao.findByNamedQuery("obtenerPorPadreTEC", map);
	}

}
   