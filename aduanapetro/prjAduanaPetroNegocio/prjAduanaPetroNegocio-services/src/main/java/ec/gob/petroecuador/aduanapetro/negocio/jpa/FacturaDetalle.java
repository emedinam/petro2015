package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import javax.persistence.*;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

import java.math.BigDecimal;


/**
 * The persistent class for the FACTURADETALLE database table.
 * 
 */

@NamedQueries({
	@NamedQuery(
		name = "obtenerPorPadreDF",
		query = "select c from FacturaDetalle c where c.estado='ACTIVO' and c.factura=:factura"
	),
})
@Entity
@Table(name = "NG_FACTURA_DETALLE", schema = "APS_NEGOCIO")
public class FacturaDetalle extends DatabaseObject<Long> {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	private BigDecimal cantidad;

	private BigDecimal subtotal;

	private BigDecimal vunitario;

	//bi-directional many-to-one association to Factura
	@ManyToOne
	@JoinColumn(name="ID_FACTURA")
	private Factura factura;

	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumn(name="ID_PRODUCTO")
	private Producto producto;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	public FacturaDetalle() {
	}

	public BigDecimal getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public BigDecimal getVunitario() {
		return this.vunitario;
	}

	public void setVunitario(BigDecimal vunitario) {
		this.vunitario = vunitario;
	}

	public Factura getFactura() {
		return this.factura;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	
	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
}