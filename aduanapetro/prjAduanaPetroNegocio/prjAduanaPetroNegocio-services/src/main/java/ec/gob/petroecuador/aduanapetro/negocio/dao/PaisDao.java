

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Pais;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase PaisDao
 @autor dgonzalez
*/
public interface PaisDao extends GenericDAO<Pais, Long> {
	
}

   