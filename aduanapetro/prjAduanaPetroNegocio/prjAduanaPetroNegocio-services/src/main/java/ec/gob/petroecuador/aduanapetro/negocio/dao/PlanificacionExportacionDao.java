

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionExportacion;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase PlanificacionExportacionDao
 @autor dgonzalez
*/
public interface PlanificacionExportacionDao extends GenericDAO<PlanificacionExportacion, Long> {
	
}

   