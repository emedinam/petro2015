
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.LiquidacionEmbarqueDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.LiquidacionEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.services.LiquidacionEmbarqueService;

/**
 Clase LiquidacionEmbarqueServiceImpl
 @autor dgonzalez
*/
@Service
public class LiquidacionEmbarqueServiceImpl extends GenericServiceImpl<LiquidacionEmbarque, Long> implements LiquidacionEmbarqueService {

	@Autowired
	LiquidacionEmbarqueDao dao;
	
	@Override
	public GenericDAO<LiquidacionEmbarque, Long> getDao() {
		return dao;
	}

}
   