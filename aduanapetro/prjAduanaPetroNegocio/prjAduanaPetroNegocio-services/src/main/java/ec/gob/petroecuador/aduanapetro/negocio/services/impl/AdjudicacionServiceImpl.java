
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.AdjudicacionDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Adjudicacion;
import ec.gob.petroecuador.aduanapetro.negocio.services.AdjudicacionService;

/**
 Clase AdjudicacionServiceImpl
 @autor dgonzalez
*/
@Service
public class AdjudicacionServiceImpl extends GenericServiceImpl<Adjudicacion, Long> implements AdjudicacionService {

	@Autowired
	AdjudicacionDao dao;
	
	@Override
	public GenericDAO<Adjudicacion, Long> getDao() {
		return dao;
	}

	@Override
	public List<Adjudicacion> obtenerPorTipo(String tipo) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("tipo", tipo);
		return dao.findByNamedQuery("obtenerPorTipo", map);
	}

	@Override
	public List<Adjudicacion> buscar(String descripcion) {
		return null;
	}

	@Override
	public List<Adjudicacion> obtenerPorTipoYTipoPago(String tipo,
			String tipoPago) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("tipo", tipo);
		map.put("tipoPago", tipoPago);
		return dao.findByNamedQuery("obtenerPorTipoYTipoPago", map);
	}

}
   