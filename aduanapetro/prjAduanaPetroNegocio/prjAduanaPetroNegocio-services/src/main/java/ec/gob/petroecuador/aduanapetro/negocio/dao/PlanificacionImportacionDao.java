

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionImportacion;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase PlanificacionImportacionDao
 @autor dgonzalez
*/
public interface PlanificacionImportacionDao extends GenericDAO<PlanificacionImportacion, Long> {
	
}

   