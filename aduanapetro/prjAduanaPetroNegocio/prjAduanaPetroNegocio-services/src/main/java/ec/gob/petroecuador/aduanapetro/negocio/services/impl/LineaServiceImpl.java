
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.LineaDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Linea;
import ec.gob.petroecuador.aduanapetro.negocio.services.LineaService;

/**
 Clase LineaServiceImpl
 @autor dgonzalez
*/
@Service
public class LineaServiceImpl extends GenericServiceImpl<Linea, Long> implements LineaService {

	@Autowired
	LineaDao dao;
	
	@Override
	public GenericDAO<Linea, Long> getDao() {
		return dao;
	}

}
   