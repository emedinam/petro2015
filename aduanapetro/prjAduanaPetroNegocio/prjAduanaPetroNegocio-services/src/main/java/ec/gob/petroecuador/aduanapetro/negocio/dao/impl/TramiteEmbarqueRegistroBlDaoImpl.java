
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TramiteEmbarqueRegistroBlDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueRegistroBl;

/**
 Clase TramiteEmbarqueRegistroBlDao
 @autor dgonzalez
*/
@Component("tramiteembarqueregistroblDao")
public class TramiteEmbarqueRegistroBlDaoImpl extends GenericDAOImpl<TramiteEmbarqueRegistroBl, Long> implements TramiteEmbarqueRegistroBlDao {
	
}
   