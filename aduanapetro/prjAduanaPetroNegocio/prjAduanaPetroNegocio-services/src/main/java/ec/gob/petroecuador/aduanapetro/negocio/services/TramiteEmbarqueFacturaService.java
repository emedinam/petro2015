
package ec.gob.petroecuador.aduanapetro.negocio.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueFactura;

/**
 Clase TramiteEmbarqueFacturaService
 @autor dgonzalez
*/
public interface TramiteEmbarqueFacturaService extends GenericService<TramiteEmbarqueFactura, Long> {
	List<TramiteEmbarqueFactura> obtenerPorPadre(TramiteEmbarque tramiteEmbarqueSeleccionado);	
}
