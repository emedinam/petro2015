
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TramiteEmbarqueDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueService;

/**
 Clase TramiteEmbarqueServiceImpl
 @autor dgonzalez
*/
@Service
public class TramiteEmbarqueServiceImpl extends GenericServiceImpl<TramiteEmbarque, Long> implements TramiteEmbarqueService {

	@Autowired
	TramiteEmbarqueDao dao;
	
	@Override
	public GenericDAO<TramiteEmbarque, Long> getDao() {
		return dao;
	}

	@Override
	public List<TramiteEmbarque> obtenerPorTipo(String tipo) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("tipo", tipo);
		return dao.findByNamedQuery("obtenerPorTipoTE", map);
	}

}
   