package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.CatalogoDao;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Catalogo;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;

@Component("catalogoDao")
public class CatalogoDaoImpl extends GenericDAOImpl<Catalogo, Long> implements CatalogoDao {

}