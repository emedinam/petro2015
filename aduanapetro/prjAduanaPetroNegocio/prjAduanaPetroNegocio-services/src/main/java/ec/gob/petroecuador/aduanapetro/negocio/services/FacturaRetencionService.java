
package ec.gob.petroecuador.aduanapetro.negocio.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Factura;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.FacturaRetencion;

/**
 Clase FacturaRetencionService
 @autor dgonzalez
*/
public interface FacturaRetencionService extends GenericService<FacturaRetencion, Long> {
	public List<FacturaRetencion> obtenerPorPadre(Factura factura);
}
   