
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.PrecioMarcadorDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PrecioMarcador;

/**
 Clase PrecioMarcadorDao
 @autor dgonzalez
*/
@Component("preciomarcadorDao")
public class PrecioMarcadorDaoImpl extends GenericDAOImpl<PrecioMarcador, Long> implements PrecioMarcadorDao {
	
}
   