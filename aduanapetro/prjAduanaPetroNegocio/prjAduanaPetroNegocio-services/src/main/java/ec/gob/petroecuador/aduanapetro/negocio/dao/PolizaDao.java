

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Poliza;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase PolizaDao
 @autor dgonzalez
*/
public interface PolizaDao extends GenericDAO<Poliza, Long> {
	
}

   