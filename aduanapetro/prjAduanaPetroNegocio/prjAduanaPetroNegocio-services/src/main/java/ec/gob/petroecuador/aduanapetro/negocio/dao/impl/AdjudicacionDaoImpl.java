
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.AdjudicacionDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Adjudicacion;

/**
 Clase AdjudicacionDao
 @autor dgonzalez
*/
@Component("adjudicacionDao")
public class AdjudicacionDaoImpl extends GenericDAOImpl<Adjudicacion, Long> implements AdjudicacionDao {
	
}
   