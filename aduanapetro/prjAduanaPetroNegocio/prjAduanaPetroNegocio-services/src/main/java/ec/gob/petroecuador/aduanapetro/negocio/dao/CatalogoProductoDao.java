

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.CatalogoProducto;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase CatalogoProductoDao
 @autor dgonzalez
*/
public interface CatalogoProductoDao extends GenericDAO<CatalogoProducto, Long> {
	
}

   