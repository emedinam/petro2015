
package ec.gob.petroecuador.aduanapetro.negocio.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Adjudicacion;

/**
 Clase AdjudicacionService
 @autor dgonzalez
*/
public interface AdjudicacionService extends GenericService<Adjudicacion, Long> {
	public List<Adjudicacion> obtenerPorTipo(String tipo);
	public List<Adjudicacion> obtenerPorTipoYTipoPago(String tipo, String tipoPago);
	public List<Adjudicacion> buscar(String descripcion);
}
   