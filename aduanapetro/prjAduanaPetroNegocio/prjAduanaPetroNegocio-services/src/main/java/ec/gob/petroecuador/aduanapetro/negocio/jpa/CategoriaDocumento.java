package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import javax.persistence.*;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

import java.util.List;


/**
 * The persistent class for the T_CATEGORIA_DOCUMENTO database table.
 * 
 */
@Entity
@Table(name = "NG_CATEGORIA_DOCUMENTO", schema= "APS_NEGOCIO")
public class CategoriaDocumento extends DatabaseObject<Long> {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	private String nombre;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	//bi-directional many-to-one association to GeneracionDocumento
	@OneToMany(mappedBy="CategoriaDocumento")
	private List<GeneracionDocumento> GeneracionDocumentos;

	public CategoriaDocumento() {
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<GeneracionDocumento> getGeneracionDocumentos() {
		return this.GeneracionDocumentos;
	}

	public void setGeneracionDocumentos(List<GeneracionDocumento> GeneracionDocumentos) {
		this.GeneracionDocumentos = GeneracionDocumentos;
	}

	public GeneracionDocumento addGeneracionDocumento(GeneracionDocumento GeneracionDocumento) {
		getGeneracionDocumentos().add(GeneracionDocumento);
		GeneracionDocumento.setCategoriaDocumento(this);

		return GeneracionDocumento;
	}

	public GeneracionDocumento removeGeneracionDocumento(GeneracionDocumento GeneracionDocumento) {
		getGeneracionDocumentos().remove(GeneracionDocumento);
		GeneracionDocumento.setCategoriaDocumento(null);

		return GeneracionDocumento;
	}

	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

}