
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.LiquidacionAdjudicacion;

/**
 Clase LiquidacionAdjudicacionService
 @autor dgonzalez
*/
public interface LiquidacionAdjudicacionService extends GenericService<LiquidacionAdjudicacion, Long> {
	
}
   