
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.PolizaDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Poliza;
import ec.gob.petroecuador.aduanapetro.negocio.services.PolizaService;

/**
 Clase PolizaServiceImpl
 @autor dgonzalez
*/
@Service
public class PolizaServiceImpl extends GenericServiceImpl<Poliza, Long> implements PolizaService {

	@Autowired
	PolizaDao dao;
	
	@Override
	public GenericDAO<Poliza, Long> getDao() {
		return dao;
	}

}
   