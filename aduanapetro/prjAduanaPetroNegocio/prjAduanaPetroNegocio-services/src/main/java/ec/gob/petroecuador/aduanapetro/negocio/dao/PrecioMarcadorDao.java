

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.PrecioMarcador;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase PrecioMarcadorDao
 @autor dgonzalez
*/
public interface PrecioMarcadorDao extends GenericDAO<PrecioMarcador, Long> {
	
}

   