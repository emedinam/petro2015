

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.GeneracionDocumento;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase GeneracionDocumentoDao
 @autor dgonzalez
*/
public interface GeneracionDocumentoDao extends GenericDAO<GeneracionDocumento, Long> {
	
}

   