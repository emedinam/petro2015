package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;


/**
 * The persistent class for the T_TRAMITE_EMBARQUE_EX database table.
 * 
 */
@NamedQueries({
	@NamedQuery(
		name = "obtenerPorTipoTEmbEx",
		query = "select p from TramiteEmbarqueExportacion p where p.estado='ACTIVO' and p.tipo=:tipo"
	),
})
@Entity
@Table(name = "NG_TRAMITE_EMBARQUE_EX", schema = "APS_NEGOCIO")
public class TramiteEmbarqueExportacion extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ADJUDICACION")
	private Date fechaAdjudicacion;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_INGRESO")
	private Date fechaIngreso;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_VENCECC")
	private Date fechaVenceCC;

	private String numero;
	
	private String numeroCC;

	private String tipo;

	@Column(name="VALOR")
	private BigDecimal valor;
	
	@Column(name="VALOR_TEXTO")
	private String valorTexto;

	private BigDecimal volumen;
	
	@Column(name="BANCO")
	private String banco;
	
	@Column(name="RUTA_ARCHIVO")
	private String rutaArchivo;
	
	@Column(name="OBSERVACION")
	private String observacion;
	
	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;
	
	@Column(name="USUARIO_ACTUALIZACION")
	private String usuarioActualizacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ACTUALIZACION")
	private Date fechaActualizacion;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	
	@Column(name="PRODUCTO")
	private String producto;
	

	//bi-directional many-to-one association to Adjudicacion
	@ManyToOne
	@JoinColumn(name="IF_ADJUDICACION")
	private Adjudicacion Adjudicacion;

	//bi-directional many-to-one association to AplicacionSeguro
	@ManyToOne
	@JoinColumn(name="IF_APLICACION_SEGURO")
	private AplicacionSeguro AplicacionSeguro;
	
	//bi-directional many-to-one association to EstadoTramiteEmbarque
	@ManyToOne
	@JoinColumn(name="IF_ESTADO_TRAMITE_EMBARQUE")
	private EstadoTramiteEmbarque EstadoTramiteEmbarque;
	
	//bi-directional many-to-one association to PlanificacionImportacion
	@ManyToOne
	@JoinColumn(name="IF_PLANIFICACION_EXPORTACION")
	private PlanificacionExportacion PlanificacionExportacion;
	
	//bi-directional many-to-one association to TramiteEmbarqueModPago
	@OneToMany(mappedBy="TramiteEmbarque")
	private List<TramiteEmbarqueModPago> TramiteEmbarqueModPagos;

	//bi-directional many-to-one association to TramiteEmbarqueExSolicitud
	//@OneToMany(mappedBy="TramiteEmbarqueExportacion")
	//private List<TramiteEmbarqueExSolicitud> TramiteEmbarqueExSolicitudes;
	
	//bi-directional many-to-one association to TramiteEmbarqueExFactura
	//@OneToMany(mappedBy="TramiteEmbarqueExportacion")
	//private List<TramiteEmbarqueExFactura> TramiteEmbarqueExFacturas;
	
	//bi-directional many-to-one association to TramiteEmbarqueExRealizada
	//@OneToMany(mappedBy="TramiteEmbarqueExportacion")
	//private List<TramiteEmbarqueExRealizada> TramiteEmbarqueExRealizada;

	//bi-directional many-to-one association to TramiteEmbarqueExDocDigital
	//@OneToMany(mappedBy="TramiteEmbarqueExportacion")
	//private List<TramiteEmbarqueExDocDigital> TramiteEmbarqueExDocDigitals;


	public TramiteEmbarqueExportacion() {
	}
	
	public Date getFechaAdjudicacion() {
		return this.fechaAdjudicacion;
	}

	public void setFechaAdjudicacion(Date fechaAdjudicacion) {
		this.fechaAdjudicacion = fechaAdjudicacion;
	}

	
	public Date getFechaIngreso() {
		return this.fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	
	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	
	public BigDecimal getVolumen() {
		return this.volumen;
	}

	public void setVolumen(BigDecimal volumen) {
		this.volumen = volumen;
	}

	public Adjudicacion getAdjudicacion() {
		return this.Adjudicacion;
	}

	public void setAdjudicacion(Adjudicacion Adjudicacion) {
		this.Adjudicacion = Adjudicacion;
	}

	public AplicacionSeguro getAplicacionSeguro() {
		return this.AplicacionSeguro;
	}

	public void setAplicacionSeguro(AplicacionSeguro AplicacionSeguro) {
		this.AplicacionSeguro = AplicacionSeguro;
	}

	public EstadoTramiteEmbarque getEstadoTramiteEmbarque() {
		return this.EstadoTramiteEmbarque;
	}

	public void setEstadoTramiteEmbarque(EstadoTramiteEmbarque EstadoTramiteEmbarque) {
		this.EstadoTramiteEmbarque = EstadoTramiteEmbarque;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	
		
	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}
	
	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	
	public void setFechaVenceCC(Date fechaVenceCC) {
		this.fechaVenceCC = fechaVenceCC;
	}

	public String getNumeroCC() {
		return numeroCC;
	}

	public void setNumeroCC(String numeroCC) {
		this.numeroCC = numeroCC;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getValorTexto() {
		return valorTexto;
	}

	public void setValorTexto(String valorTexto) {
		this.valorTexto = valorTexto;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getRutaArchivo() {
		return rutaArchivo;
	}

	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}

	public PlanificacionExportacion getPlanificacionExportacion() {
		return PlanificacionExportacion;
	}

	public void setPlanificacionExportacion(
			PlanificacionExportacion planificacionExportacion) {
		PlanificacionExportacion = planificacionExportacion;
	}

	public List<TramiteEmbarqueModPago> getTramiteEmbarqueModPagos() {
		return TramiteEmbarqueModPagos;
	}

	public void setTramiteEmbarqueModPagos(
			List<TramiteEmbarqueModPago> tramiteEmbarqueModPagos) {
		TramiteEmbarqueModPagos = tramiteEmbarqueModPagos;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof TramiteEmbarqueExportacion))
			return false;
		return ((TramiteEmbarqueExportacion) obj).getId().equals(this.id);
	}

	@Override
	public String toString() {
		return numero;
	}
	
	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaVenceCC() {
		return fechaVenceCC;
	}

	
}