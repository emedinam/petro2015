
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Garantia;

/**
 Clase GarantiaService
 @autor dgonzalez
*/
public interface GarantiaService extends GenericService<Garantia, Long> {
	
}
   