
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Formula;

/**
 Clase FormulaService
 @autor dgonzalez
*/
public interface FormulaService extends GenericService<Formula, Long> {
	
}
   