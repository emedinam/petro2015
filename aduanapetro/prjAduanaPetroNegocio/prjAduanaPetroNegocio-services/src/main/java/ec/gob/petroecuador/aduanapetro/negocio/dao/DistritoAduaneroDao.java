

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.DistritoAduanero;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase DistritoAduaneroDao
 @autor dgonzalez
*/
public interface DistritoAduaneroDao extends GenericDAO<DistritoAduanero, Long> {
	
}

   