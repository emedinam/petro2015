

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Formula;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase FormulaDao
 @autor dgonzalez
*/
public interface FormulaDao extends GenericDAO<Formula, Long> {
	
}

   