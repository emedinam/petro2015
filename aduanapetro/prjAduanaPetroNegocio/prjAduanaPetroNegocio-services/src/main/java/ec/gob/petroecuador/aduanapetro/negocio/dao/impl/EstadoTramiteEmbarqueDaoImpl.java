
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.EstadoTramiteEmbarqueDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.EstadoTramiteEmbarque;

/**
 Clase EstadoTramiteEmbarqueDao
 @autor dgonzalez
*/
@Component("estadotramiteembarqueDao")
public class EstadoTramiteEmbarqueDaoImpl extends GenericDAOImpl<EstadoTramiteEmbarque, Long> implements EstadoTramiteEmbarqueDao {
	
}
   