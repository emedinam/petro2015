
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TipoDocumento;

/**
 Clase TipoDocumentoService
 @autor dgonzalez
*/
public interface TipoDocumentoService extends GenericService<TipoDocumento, Long> {
	
}
   