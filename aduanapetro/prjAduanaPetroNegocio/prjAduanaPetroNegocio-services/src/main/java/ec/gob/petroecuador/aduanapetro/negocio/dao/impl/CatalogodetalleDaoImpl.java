package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.CatalogodetalleDao;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Catalogodetalle;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;

@Component("catalogodetalleDao")
public class CatalogodetalleDaoImpl extends GenericDAOImpl<Catalogodetalle, Long> implements CatalogodetalleDao {

}