
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.CatalogoProducto;

/**
 Clase CatalogoProductoService
 @autor dgonzalez
*/
public interface CatalogoProductoService extends GenericService<CatalogoProducto, Long> {
	
}
   