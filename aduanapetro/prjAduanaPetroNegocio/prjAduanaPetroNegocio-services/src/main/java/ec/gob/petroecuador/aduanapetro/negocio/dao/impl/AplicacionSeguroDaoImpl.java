
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.AplicacionSeguroDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.AplicacionSeguro;

/**
 Clase AplicacionSeguroDao
 @autor dgonzalez
*/
@Component("aplicacionseguroDao")
public class AplicacionSeguroDaoImpl extends GenericDAOImpl<AplicacionSeguro, Long> implements AplicacionSeguroDao {
	
}
   