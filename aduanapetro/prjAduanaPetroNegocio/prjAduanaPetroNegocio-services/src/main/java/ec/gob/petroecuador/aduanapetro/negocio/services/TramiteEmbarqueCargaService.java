
package ec.gob.petroecuador.aduanapetro.negocio.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueCarga;

/**
 Clase TramiteEmbarqueCargaService
 @autor dgonzalez
*/
public interface TramiteEmbarqueCargaService extends GenericService<TramiteEmbarqueCarga, Long> {
	public List<TramiteEmbarqueCarga> obtenerPorPadre(TramiteEmbarque tramiteEmbarque);
}
