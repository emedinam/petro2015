package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import javax.persistence.*;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

import java.util.List;


/**
 * The persistent class for the T_FORMULA database table.
 * 
 */
@Entity
@Table(name = "NG_FORMULA", schema = "APS_NEGOCIO")
public class Formula extends DatabaseObject<Long> {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	private String formula;

	//bi-directional many-to-one association to Adjudicacion
	@OneToMany(mappedBy="ifFormula")
	private List<Adjudicacion> Adjudicaciones;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	public Formula() {
	}

	public String getFormula() {
		return this.formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public List<Adjudicacion> getAdjudicaciones() {
		return this.Adjudicaciones;
	}

	public void setAdjudicaciones(List<Adjudicacion> Adjudicaciones) {
		this.Adjudicaciones = Adjudicaciones;
	}

	public Adjudicacion addAdjudicacione(Adjudicacion Adjudicacione) {
		getAdjudicaciones().add(Adjudicacione);
		Adjudicacione.setIfFormula(this);

		return Adjudicacione;
	}

	public Adjudicacion removeAdjudicacione(Adjudicacion Adjudicacione) {
		getAdjudicaciones().remove(Adjudicacione);
		Adjudicacione.setIfFormula(null);

		return Adjudicacione;
	}

	// metodos para combos

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof Formula))
			return false;
		return ((Formula) obj).getId().equals(this.id);
	}

	@Override
	public String toString() {
		return formula;
	}

	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
}