
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TramiteEmbarqueDesaduanamientoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDesaduanamiento;

/**
 Clase TramiteEmbarqueDesaduanamientoDao
 @autor dgonzalez
*/
@Component("tramiteembarquedesaduanamientoDao")
public class TramiteEmbarqueDesaduanamientoDaoImpl extends GenericDAOImpl<TramiteEmbarqueDesaduanamiento, Long> implements TramiteEmbarqueDesaduanamientoDao {
	
}
   