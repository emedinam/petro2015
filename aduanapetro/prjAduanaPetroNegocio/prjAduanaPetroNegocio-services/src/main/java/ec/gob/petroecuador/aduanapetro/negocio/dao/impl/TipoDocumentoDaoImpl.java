
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TipoDocumentoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TipoDocumento;

/**
 Clase TipoDocumentoDao
 @autor dgonzalez
*/
@Component("tipodocumentoDao")
public class TipoDocumentoDaoImpl extends GenericDAOImpl<TipoDocumento, Long> implements TipoDocumentoDao {
	
}
   