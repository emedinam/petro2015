

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.LiquidacionEmbarque;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase LiquidacionEmbarqueDao
 @autor dgonzalez
*/
public interface LiquidacionEmbarqueDao extends GenericDAO<LiquidacionEmbarque, Long> {
	
}

   