
package ec.gob.petroecuador.aduanapetro.negocio.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Adjudicacion;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionImportacion;

/**
 Clase PlanificacionImportacionService
 @autor dgonzalez
*/
public interface PlanificacionImportacionService extends GenericService<PlanificacionImportacion, Long> {
	public List<PlanificacionImportacion> obtenerPorPadre(Adjudicacion adjudicacion);
}
   