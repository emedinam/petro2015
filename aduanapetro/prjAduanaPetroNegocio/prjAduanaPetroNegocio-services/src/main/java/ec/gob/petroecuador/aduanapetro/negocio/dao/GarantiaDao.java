

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Garantia;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase GarantiaDao
 @autor dgonzalez
*/
public interface GarantiaDao extends GenericDAO<Garantia, Long> {
	
}

   