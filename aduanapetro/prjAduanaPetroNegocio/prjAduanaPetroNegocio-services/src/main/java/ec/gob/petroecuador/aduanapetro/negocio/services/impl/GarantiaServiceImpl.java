
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.GarantiaDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Garantia;
import ec.gob.petroecuador.aduanapetro.negocio.services.GarantiaService;

/**
 Clase GarantiaServiceImpl
 @autor dgonzalez
*/
@Service
public class GarantiaServiceImpl extends GenericServiceImpl<Garantia, Long> implements GarantiaService {

	@Autowired
	GarantiaDao dao;
	
	@Override
	public GenericDAO<Garantia, Long> getDao() {
		return dao;
	}

}
   