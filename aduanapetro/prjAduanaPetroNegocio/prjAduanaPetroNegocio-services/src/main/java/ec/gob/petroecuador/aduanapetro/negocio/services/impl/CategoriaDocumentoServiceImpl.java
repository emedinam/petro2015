
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.CategoriaDocumentoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.CategoriaDocumento;
import ec.gob.petroecuador.aduanapetro.negocio.services.CategoriaDocumentoService;

/**
 Clase CategoriaDocumentoServiceImpl
 @autor dgonzalez
*/
@Service
public class CategoriaDocumentoServiceImpl extends GenericServiceImpl<CategoriaDocumento, Long> implements CategoriaDocumentoService {

	@Autowired
	CategoriaDocumentoDao dao;
	
	@Override
	public GenericDAO<CategoriaDocumento, Long> getDao() {
		return dao;
	}

}
   