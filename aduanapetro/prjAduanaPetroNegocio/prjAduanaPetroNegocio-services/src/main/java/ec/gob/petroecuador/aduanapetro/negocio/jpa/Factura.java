package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;


/**
 * The persistent class for the FACTURA database table.
 * 
 */
@Entity
@Table(name = "NG_FACTURA", schema= "APS_NEGOCIO")
public class Factura extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	private String descripcion;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	//bi-directional many-to-one association to FacturaDetalle
	@OneToMany(mappedBy="factura")
	private List<FacturaDetalle> facturadetalles;

	//bi-directional many-to-one association to FacturaRetencion
	@OneToMany(mappedBy="factura")
	private List<FacturaRetencion> facturaretencions;

	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	
	public Factura() {
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public List<FacturaDetalle> getFacturadetalles() {
		return this.facturadetalles;
	}

	public void setFacturadetalles(List<FacturaDetalle> facturadetalles) {
		this.facturadetalles = facturadetalles;
	}

	public FacturaDetalle addFacturadetalle(FacturaDetalle facturadetalle) {
		getFacturadetalles().add(facturadetalle);
		facturadetalle.setFactura(this);

		return facturadetalle;
	}

	public FacturaDetalle removeFacturadetalle(FacturaDetalle facturadetalle) {
		getFacturadetalles().remove(facturadetalle);
		facturadetalle.setFactura(null);

		return facturadetalle;
	}

	public List<FacturaRetencion> getFacturaretencions() {
		return this.facturaretencions;
	}

	public void setFacturaretencions(List<FacturaRetencion> facturaretencions) {
		this.facturaretencions = facturaretencions;
	}

	public FacturaRetencion addFacturaretencion(FacturaRetencion facturaretencion) {
		getFacturaretencions().add(facturaretencion);
		facturaretencion.setFactura(this);

		return facturaretencion;
	}

	public FacturaRetencion removeFacturaretencion(FacturaRetencion facturaretencion) {
		getFacturaretencions().remove(facturaretencion);
		facturaretencion.setFactura(null);

		return facturaretencion;
	}

	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
}