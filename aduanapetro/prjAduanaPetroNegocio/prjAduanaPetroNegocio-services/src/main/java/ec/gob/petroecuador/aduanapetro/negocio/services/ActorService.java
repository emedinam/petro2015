
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Actor;

/**
 Clase ActorService
 @autor dgonzalez
*/
public interface ActorService extends GenericService<Actor, Long> {
	
}
   