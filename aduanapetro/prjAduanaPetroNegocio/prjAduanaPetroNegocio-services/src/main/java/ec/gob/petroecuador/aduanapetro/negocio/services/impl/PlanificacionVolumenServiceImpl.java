
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.PlanificacionVolumenDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionVolumen;
import ec.gob.petroecuador.aduanapetro.negocio.services.PlanificacionVolumenService;

/**
 Clase PlanificacionVolumenServiceImpl
 @autor dgonzalez
*/
@Service
public class PlanificacionVolumenServiceImpl extends GenericServiceImpl<PlanificacionVolumen, Long> implements PlanificacionVolumenService {

	@Autowired
	PlanificacionVolumenDao dao;
	
	@Override
	public GenericDAO<PlanificacionVolumen, Long> getDao() {
		return dao;
	}

}
   