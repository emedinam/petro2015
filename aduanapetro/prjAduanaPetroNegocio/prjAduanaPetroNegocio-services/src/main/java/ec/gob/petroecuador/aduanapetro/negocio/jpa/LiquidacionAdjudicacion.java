package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import javax.persistence.*;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the T_LIQUIDACION_ADJUDICACION database table.
 * 
 */
@Entity
@Table(name = "NG_LIQUIDACION_ADJUDICACION", schema = "APS_NEGOCIO")
public class LiquidacionAdjudicacion extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name="CERTIFICACIO_NAVIERA")
	private BigDecimal certificacioNaviera;

	@Column(name="ESTADO_ADJ")
	private String estadoAdj;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_REGISTRO")
	private Date fechaRegistro;

	private BigDecimal gasto;

	@Column(name="GASTO_BANCARIO")
	private BigDecimal gastoBancario;

	private BigDecimal inspeccion;

	@Column(name="OTRO_GASTO")
	private BigDecimal otroGasto;

	@Column(name="PAGO_ADUANERO")
	private BigDecimal pagoAduanero;

	private BigDecimal seguro;

	private String tipo;

	private BigDecimal total;

	@Column(name="VALOR_ALMACENAJE")
	private BigDecimal valorAlmacenaje;

	@Column(name="VALOR_DAT")
	private BigDecimal valorDat;

	@Column(name="VALOR_FLETE_INTERNACIONAL")
	private BigDecimal valorFleteInternacional;

	@Column(name="VALOR_FOB")
	private BigDecimal valorFob;

	@Column(name="VALOR_PAGADO_AGENTE")
	private BigDecimal valorPagadoAgente;

	//bi-directional many-to-one association to Adjudicacion
	@ManyToOne
	@JoinColumn(name="IF_ADJUDICACION")
	private Adjudicacion Adjudicacion;

	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	
	public LiquidacionAdjudicacion() {
	}

	public BigDecimal getCertificacioNaviera() {
		return this.certificacioNaviera;
	}

	public void setCertificacioNaviera(BigDecimal certificacioNaviera) {
		this.certificacioNaviera = certificacioNaviera;
	}

	public String getEstadoAdj() {
		return estadoAdj;
	}

	public void setEstadoAdj(String estadoAdj) {
		this.estadoAdj = estadoAdj;
	}

	public Date getFechaRegistro() {
		return this.fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public BigDecimal getGasto() {
		return this.gasto;
	}

	public void setGasto(BigDecimal gasto) {
		this.gasto = gasto;
	}

	public BigDecimal getGastoBancario() {
		return this.gastoBancario;
	}

	public void setGastoBancario(BigDecimal gastoBancario) {
		this.gastoBancario = gastoBancario;
	}

	public BigDecimal getInspeccion() {
		return this.inspeccion;
	}

	public void setInspeccion(BigDecimal inspeccion) {
		this.inspeccion = inspeccion;
	}

	public BigDecimal getOtroGasto() {
		return this.otroGasto;
	}

	public void setOtroGasto(BigDecimal otroGasto) {
		this.otroGasto = otroGasto;
	}

	public BigDecimal getPagoAduanero() {
		return this.pagoAduanero;
	}

	public void setPagoAduanero(BigDecimal pagoAduanero) {
		this.pagoAduanero = pagoAduanero;
	}

	public BigDecimal getSeguro() {
		return this.seguro;
	}

	public void setSeguro(BigDecimal seguro) {
		this.seguro = seguro;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getValorAlmacenaje() {
		return this.valorAlmacenaje;
	}

	public void setValorAlmacenaje(BigDecimal valorAlmacenaje) {
		this.valorAlmacenaje = valorAlmacenaje;
	}

	public BigDecimal getValorDat() {
		return this.valorDat;
	}

	public void setValorDat(BigDecimal valorDat) {
		this.valorDat = valorDat;
	}

	public BigDecimal getValorFleteInternacional() {
		return this.valorFleteInternacional;
	}

	public void setValorFleteInternacional(BigDecimal valorFleteInternacional) {
		this.valorFleteInternacional = valorFleteInternacional;
	}

	public BigDecimal getValorFob() {
		return this.valorFob;
	}

	public void setValorFob(BigDecimal valorFob) {
		this.valorFob = valorFob;
	}

	public BigDecimal getValorPagadoAgente() {
		return this.valorPagadoAgente;
	}

	public void setValorPagadoAgente(BigDecimal valorPagadoAgente) {
		this.valorPagadoAgente = valorPagadoAgente;
	}

	public Adjudicacion getAdjudicacion() {
		return this.Adjudicacion;
	}

	public void setAdjudicacion(Adjudicacion Adjudicacion) {
		this.Adjudicacion = Adjudicacion;
	}

	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
}