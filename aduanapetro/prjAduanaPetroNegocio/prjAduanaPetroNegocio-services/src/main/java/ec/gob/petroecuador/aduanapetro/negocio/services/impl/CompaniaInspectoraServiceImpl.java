
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.CompaniaInspectoraDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.CompaniaInspectora;
import ec.gob.petroecuador.aduanapetro.negocio.services.CompaniaInspectoraService;

/**
 Clase CompaniaInspectoraServiceImpl
 @autor dgonzalez
*/
@Service
public class CompaniaInspectoraServiceImpl extends GenericServiceImpl<CompaniaInspectora, Long> implements CompaniaInspectoraService {

	@Autowired
	CompaniaInspectoraDao dao;
	
	@Override
	public GenericDAO<CompaniaInspectora, Long> getDao() {
		return dao;
	}

}
   