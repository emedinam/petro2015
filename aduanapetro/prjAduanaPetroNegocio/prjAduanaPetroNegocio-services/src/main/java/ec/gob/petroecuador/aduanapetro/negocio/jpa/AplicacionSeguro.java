package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import javax.persistence.*;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the T_APLICACION_SEGURO database table.
 * 
 */
@Entity
@Table(name = "NG_APLICACION_SEGURO", schema= "APS_NEGOCIO")
public class AplicacionSeguro extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	private String dirigidoa;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	private String material;

	private String numero;

	private String solicita;

	//bi-directional many-to-one association to Adjudicacion
	@ManyToOne
	@JoinColumn(name="IF_ADJUDICACION")
	private Adjudicacion Adjudicacion;

	//bi-directional many-to-one association to CompaniaSeguro
	@ManyToOne
	@JoinColumn(name="IF_COMPANIA_SEGURO")
	private CompaniaSeguro CompaniaSeguro;

	//bi-directional many-to-one association to PlanificacionImportacion
	@ManyToOne
	@JoinColumn(name="IF_PLANIFICACION_IMPORTACION")
	private PlanificacionImportacion PlanificacionImportacion;

	//bi-directional many-to-one association to Poliza
	@ManyToOne
	@JoinColumn(name="IF_POLIZA")
	private Poliza Poliza;

	//bi-directional many-to-one association to TramiteEmbarque
	@OneToMany(mappedBy="AplicacionSeguro")
	private List<TramiteEmbarque> TramiteEmbarques;
	
	
	//Variables de control de eliminacion y log
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_ACTUALIZACION")
	private Date fechaActualiza;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION")
	private Date fechaCrea;
	
	@Column(name = "USUARIO_ACTUALIZACION")
	private String usuarioActualiza;

	@Column(name = "USUARIO_CREACION")
	private String usuarioCrea;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	public AplicacionSeguro() {
	}

	public String getDirigidoa() {
		return this.dirigidoa;
	}

	public void setDirigidoa(String dirigidoa) {
		this.dirigidoa = dirigidoa;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getMaterial() {
		return this.material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getSolicita() {
		return this.solicita;
	}

	public void setSolicita(String solicita) {
		this.solicita = solicita;
	}

	public Adjudicacion getAdjudicacion() {
		return this.Adjudicacion;
	}

	public void setAdjudicacion(Adjudicacion Adjudicacion) {
		this.Adjudicacion = Adjudicacion;
	}

	public CompaniaSeguro getCompaniaSeguro() {
		return this.CompaniaSeguro;
	}

	public void setCompaniaSeguro(CompaniaSeguro CompaniaSeguro) {
		this.CompaniaSeguro = CompaniaSeguro;
	}

	public PlanificacionImportacion getPlanificacionImportacion() {
		return this.PlanificacionImportacion;
	}

	public void setPlanificacionImportacion(PlanificacionImportacion PlanificacionImportacion) {
		this.PlanificacionImportacion = PlanificacionImportacion;
	}

	public Poliza getPoliza() {
		return this.Poliza;
	}

	public void setPoliza(Poliza Poliza) {
		this.Poliza = Poliza;
	}

	public List<TramiteEmbarque> getTramiteEmbarques() {
		return this.TramiteEmbarques;
	}

	public void setTramiteEmbarques(List<TramiteEmbarque> TramiteEmbarques) {
		this.TramiteEmbarques = TramiteEmbarques;
	}

	public TramiteEmbarque addTramiteEmbarque(TramiteEmbarque TramiteEmbarque) {
		getTramiteEmbarques().add(TramiteEmbarque);
		TramiteEmbarque.setAplicacionSeguro(this);

		return TramiteEmbarque;
	}

	public TramiteEmbarque removeTramiteEmbarque(TramiteEmbarque TramiteEmbarque) {
		getTramiteEmbarques().remove(TramiteEmbarque);
		TramiteEmbarque.setAplicacionSeguro(null);

		return TramiteEmbarque;
	}

	public Date getFechaActualiza() {
		return fechaActualiza;
	}

	public void setFechaActualiza(Date fechaActualiza) {
		this.fechaActualiza = fechaActualiza;
	}

	public Date getFechaCrea() {
		return fechaCrea;
	}

	public void setFechaCrea(Date fechaCrea) {
		this.fechaCrea = fechaCrea;
	}

	public String getUsuarioActualiza() {
		return usuarioActualiza;
	}

	public void setUsuarioActualiza(String usuarioActualiza) {
		this.usuarioActualiza = usuarioActualiza;
	}

	public String getUsuarioCrea() {
		return usuarioCrea;
	}

	public void setUsuarioCrea(String usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}

	// metodos para combos
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof AplicacionSeguro))
			return false;
		return ((AplicacionSeguro) obj).getId().equals(
				this.id);
	}

	@Override
	public String toString() {
		return numero;
	}


	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;	
	}

	@Override
	public Estados getEstado() {
		return estado;
	}
}