package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;


/**
 * The persistent class for the T_PLANIFICACION_EXPORTACION database table.
 * 
 */
@NamedQueries({
	@NamedQuery(
		name = "obtenerPorPadrePE",
		query = "select c from PlanificacionExportacion c where c.estado='ACTIVO' and c.Adjudicacion.id=:adjudicacion"
	),
})
@Entity
@Table(name = "NG_PLANIFICACION_EXPORTACION", schema = "APS_NEGOCIO")
public class PlanificacionExportacion extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ACTUALIZACION")
	private Date fechaActualiza;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCrea;

	@Column(name="USUARIO_ACTUALIZACION")
	private String usuarioActualiza;

	@Column(name="USUARIO_CREACION")
	private String usuarioCrea;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_REGISTRO")
	private Date fechaRegistro;

	@Column(name="MARGEN_TOLERANCIA")
	private BigDecimal margenTolerancia;

	private String observacion;

	@Temporal(TemporalType.DATE)
	@Column(name="VENTANA_FIN")
	private Date ventanaFin;

	@Temporal(TemporalType.DATE)
	@Column(name="VENTANA_INICIO")
	private Date ventanaInicio;

	private BigDecimal volumen;

	//bi-directional many-to-one association to Adjudicacion
	@ManyToOne
	@JoinColumn(name="IF_ADJUDICACION")
	private Adjudicacion Adjudicacion;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	public PlanificacionExportacion() {
	}

	public Date getFechaRegistro() {
		return this.fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public BigDecimal getMargenTolerancia() {
		return this.margenTolerancia;
	}

	public void setMargenTolerancia(BigDecimal margenTolerancia) {
		this.margenTolerancia = margenTolerancia;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Date getVentanaFin() {
		return this.ventanaFin;
	}

	public void setVentanaFin(Date ventanaFin) {
		this.ventanaFin = ventanaFin;
	}

	public Date getVentanaInicio() {
		return this.ventanaInicio;
	}

	public void setVentanaInicio(Date ventanaInicio) {
		this.ventanaInicio = ventanaInicio;
	}

	public BigDecimal getVolumen() {
		return this.volumen;
	}

	public void setVolumen(BigDecimal volumen) {
		this.volumen = volumen;
	}

	public Adjudicacion getAdjudicacion() {
		return this.Adjudicacion;
	}

	public void setAdjudicacion(Adjudicacion Adjudicacion) {
		this.Adjudicacion = Adjudicacion;
	}

	public Date getFechaActualiza() {
		return fechaActualiza;
	}

	public void setFechaActualiza(Date fechaActualiza) {
		this.fechaActualiza = fechaActualiza;
	}

	public Date getFechaCrea() {
		return fechaCrea;
	}

	public void setFechaCrea(Date fechaCrea) {
		this.fechaCrea = fechaCrea;
	}

	public String getUsuarioActualiza() {
		return usuarioActualiza;
	}

	public void setUsuarioActualiza(String usuarioActualiza) {
		this.usuarioActualiza = usuarioActualiza;
	}

	public String getUsuarioCrea() {
		return usuarioCrea;
	}

	public void setUsuarioCrea(String usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}
	
	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

}