

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.CompaniaSeguro;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase CompaniaSeguroDao
 @autor dgonzalez
*/
public interface CompaniaSeguroDao extends GenericDAO<CompaniaSeguro, Long> {
	
}

   