package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Catalogodetalle;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;

public interface CatalogodetalleService extends GenericService<Catalogodetalle, Long> {
	
}
