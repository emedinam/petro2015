
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.CatalogoProductoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.CatalogoProducto;

/**
 Clase CatalogoProductoDao
 @autor dgonzalez
*/
@Component("catalogoproductoDao")
public class CatalogoProductoDaoImpl extends GenericDAOImpl<CatalogoProducto, Long> implements CatalogoProductoDao {
	
}
   