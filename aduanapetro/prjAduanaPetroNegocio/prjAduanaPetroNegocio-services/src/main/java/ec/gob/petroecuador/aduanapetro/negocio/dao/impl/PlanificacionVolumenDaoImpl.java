
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.PlanificacionVolumenDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionVolumen;

/**
 Clase PlanificacionVolumenDao
 @autor dgonzalez
*/
@Component("planificacionvolumenDao")
public class PlanificacionVolumenDaoImpl extends GenericDAOImpl<PlanificacionVolumen, Long> implements PlanificacionVolumenDao {
	
}
   