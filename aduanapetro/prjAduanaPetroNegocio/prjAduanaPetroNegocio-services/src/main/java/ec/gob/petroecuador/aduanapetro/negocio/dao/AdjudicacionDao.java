

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Adjudicacion;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase AdjudicacionDao
 @autor dgonzalez
*/
public interface AdjudicacionDao extends GenericDAO<Adjudicacion, Long> {
	
}

   