package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

/**
 * The persistent class for the T_TRAMITE_EMBARQUE_FACTURA database table.
 * 
 */
@NamedQueries({
	@NamedQuery(
		name = "obtenerPorPadreTEF",
		query = "select p from TramiteEmbarqueFactura p where p.estado='ACTIVO' and p.TramiteEmbarque.id=:tramiteEmbarque"
	),
})
@Entity
@Table(name = "NG_TRAMITE_EMBARQUE_FACTURA", schema = "APS_NEGOCIO")
public class TramiteEmbarqueFactura extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_DOCUMENTO")
	private Date fechaDocumento;

	@Column(name="FLETE_BUQUE")
	private BigDecimal fleteBuque;

	private BigDecimal generado;

	private String numero;

	@Column(name="NUMERO_OFICIO")
	private String numeroOficio;

	private String observacion;

	@Column(name="RUTA_ARCHIVO")
	private String rutaArchivo;

	@Column(name="VALOR_DAT")
	private BigDecimal valorDat;

	@Column(name="VALOR_FOB")
	private BigDecimal valorFob;

	private BigDecimal volumen;
	
	@Column(name="PRECIO")
	private BigDecimal precio;
	
	@Column(name="GIRO")
	private String giro;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_GIRO")
	private Date fechaGiro;
	
	@Column(name="PROVEEDOR")
	private String proveedor;
	
	@Column(name="DIRECCION")
	private String direccion;
	
	@Column(name="BANCO")
	private String banco;
	
	@Column(name="CUENTA")
	private String cuenta;
	
	@Column(name="SWIFT")
	private String swift;
	
	@Column(name="ABA")
	private String aba;
	
	@Column(name="CONCEPTO")
	private String concepto;
	
	@Column(name="NUMERO_OFICIOMEF")
	private String numeroOficioMef;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_DOCUMENTOMEF")
	private Date fechaDocumentoMef;
	
	@Column(name="GENERADOMEF")
	private BigDecimal generadoMef;
	
	@Column(name="RUTA_ARCHIVOMEF")
	private String rutaArchivoMef;

	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;
	
	@Column(name="USUARIO_ACTUALIZACION")
	private String usuarioActualizacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCreaCION;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ACTUALIZACION")
	private Date fechaActualizacion;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	
	//bi-directional many-to-one association to GeneracionDocumento
	@OneToMany(mappedBy="TramiteEmbarqueFactura")
	private List<GeneracionDocumento> GeneracionDocumentos;

	//bi-directional many-to-one association to TramiteEmbarque
	@ManyToOne
	@JoinColumn(name="IF_TRAMITE_EMBARQUE")
	private TramiteEmbarque TramiteEmbarque;

	public TramiteEmbarqueFactura() {
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Date getFechaDocumento() {
		return this.fechaDocumento;
	}

	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}

	public BigDecimal getFleteBuque() {
		return this.fleteBuque;
	}

	public void setFleteBuque(BigDecimal fleteBuque) {
		this.fleteBuque = fleteBuque;
	}

	public BigDecimal getGenerado() {
		return this.generado;
	}

	public void setGenerado(BigDecimal generado) {
		this.generado = generado;
	}

	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNumeroOficio() {
		return this.numeroOficio;
	}

	public void setNumeroOficio(String numeroOficio) {
		this.numeroOficio = numeroOficio;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getRutaArchivo() {
		return this.rutaArchivo;
	}

	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}

	public BigDecimal getValorDat() {
		return this.valorDat;
	}

	public void setValorDat(BigDecimal valorDat) {
		this.valorDat = valorDat;
	}

	public BigDecimal getValorFob() {
		return this.valorFob;
	}

	public void setValorFob(BigDecimal valorFob) {
		this.valorFob = valorFob;
	}

	public BigDecimal getVolumen() {
		return this.volumen;
	}

	public void setVolumen(BigDecimal volumen) {
		this.volumen = volumen;
	}

	public List<GeneracionDocumento> getGeneracionDocumentos() {
		return this.GeneracionDocumentos;
	}

	public void setGeneracionDocumentos(List<GeneracionDocumento> GeneracionDocumentos) {
		this.GeneracionDocumentos = GeneracionDocumentos;
	}

	public GeneracionDocumento addGeneracionDocumento(GeneracionDocumento GeneracionDocumento) {
		getGeneracionDocumentos().add(GeneracionDocumento);
		GeneracionDocumento.setTramiteEmbarqueFactura(this);

		return GeneracionDocumento;
	}

	public GeneracionDocumento removeGeneracionDocumento(GeneracionDocumento GeneracionDocumento) {
		getGeneracionDocumentos().remove(GeneracionDocumento);
		GeneracionDocumento.setTramiteEmbarqueFactura(null);

		return GeneracionDocumento;
	}

	public TramiteEmbarque getTramiteEmbarque() {
		return this.TramiteEmbarque;
	}

	public void setTramiteEmbarque(TramiteEmbarque TramiteEmbarque) {
		this.TramiteEmbarque = TramiteEmbarque;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public Date getFechaCreaCION() {
		return fechaCreaCION;
	}

	public void setFechaCreaCION(Date fechaCreaCION) {
		this.fechaCreaCION = fechaCreaCION;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}
	
	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getGiro() {
		return giro;
	}

	public void setGiro(String giro) {
		this.giro = giro;
	}

	public Date getFechaGiro() {
		return fechaGiro;
	}

	public void setFechaGiro(Date fechaGiro) {
		this.fechaGiro = fechaGiro;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getSwift() {
		return swift;
	}

	public void setSwift(String swift) {
		this.swift = swift;
	}

	public String getAba() {
		return aba;
	}

	public void setAba(String aba) {
		this.aba = aba;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getNumeroOficioMef() {
		return numeroOficioMef;
	}

	public void setNumeroOficioMef(String numeroOficioMef) {
		this.numeroOficioMef = numeroOficioMef;
	}

	public Date getFechaDocumentoMef() {
		return fechaDocumentoMef;
	}

	public void setFechaDocumentoMef(Date fechaDocumentoMef) {
		this.fechaDocumentoMef = fechaDocumentoMef;
	}

	public BigDecimal getGeneradoMef() {
		return generadoMef;
	}

	public void setGeneradoMef(BigDecimal generadoMef) {
		this.generadoMef = generadoMef;
	}

	public String getRutaArchivoMef() {
		return rutaArchivoMef;
	}

	public void setRutaArchivoMef(String rutaArchivoMef) {
		this.rutaArchivoMef = rutaArchivoMef;
	}
}