
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.DistritoAduanero;

/**
 Clase DistritoAduaneroService
 @autor dgonzalez
*/
public interface DistritoAduaneroService extends GenericService<DistritoAduanero, Long> {
	
}
   