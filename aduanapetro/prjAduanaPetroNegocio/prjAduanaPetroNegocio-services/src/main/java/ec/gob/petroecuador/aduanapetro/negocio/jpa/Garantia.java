package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;


/**
 * The persistent class for the T_GARANTIA database table.
 * 
 */
@Entity
@Table(name = "NG_GARANTIA", schema = "APS_NEGOCIO")
public class Garantia extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name="BANCO_EXTERIOR")
	private String bancoExterior;

	@Column(name="CANTIDAD_CARGAMENTO")
	private BigDecimal cantidadCargamento;

	@Column(name="COMPANIA")
	private String compania;
	
	@Column(name="NUMERO_FAX")
	private String numeroFax;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_FAX")
	private Date fechaFax;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_REGISTRO")
	private Date fechaRegistro;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_VENCIMIENTO")
	private Date fechaVencimiento;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CANCELACION")
	private Date fechaCancelacion;

	@Column(name="NUMERO_CONTRAGARANTIA")
	private String numeroContragarantia;

	@Column(name="NUMERO_GARANTIA")
	private String numeroGarantia;
	
	@Column(name="PRODUCTO")
	private String producto;

	@Column(name="RUTA_ARCHIVO")
	private String rutaArchivo;

	@Column(name="TIPO")
	private String tipo;
	
	@Column(name="TOLERANCIA")
	private BigDecimal tolerancia;

	@Column(name="VALOR")
	private BigDecimal valor;

	@Column(name="VOLUMEN_CARGAMENTO")
	private BigDecimal volumenCargamento;

	@Column(name="VOLUMEN_TOTAL")
	private BigDecimal volumenTotal;
	
	@Column(name="PLAZO_VIGENCIA")
	private BigDecimal plazoVigencia;
	
	@Column(name="TEXTO_DEVOLUCION")
	private String textoDevolucion;

	@Column(name="TEXTO_RENOVACION")
	private String textoRenovacion;
	
	@Column(name="PLAZO_RENOVACION")
	private BigDecimal plazoRenovacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_DEVOLUCION")
	private Date fechaDevolucion;
	
	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;
	
	@Column(name="USUARIO_ACTUALIZACION")
	private String usuarioActualizacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ACTUALIZACION")
	private Date fechaActualizacion;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	//bi-directional many-to-one association to Actor
	@ManyToOne
	@JoinColumn(name="IF_ACTOR")
	private Actor Actor;
		
	//bi-directional many-to-one association to Adjudicacion
	@ManyToOne
	@JoinColumn(name="IF_ADJUDICACION")
	private Adjudicacion Adjudicacion;

	public Garantia() {
	}

	public String getBancoExterior() {
		return this.bancoExterior;
	}

	public void setBancoExterior(String bancoExterior) {
		this.bancoExterior = bancoExterior;
	}

	public BigDecimal getCantidadCargamento() {
		return this.cantidadCargamento;
	}

	public void setCantidadCargamento(BigDecimal cantidadCargamento) {
		this.cantidadCargamento = cantidadCargamento;
	}

	public String getCompania() {
		return this.compania;
	}

	public void setCompania(String compania) {
		this.compania = compania;
	}

	public String getNumeroFax() {
		return numeroFax;
	}

	public void setNumeroFax(String numeroFax) {
		this.numeroFax = numeroFax;
	}

	public Date getFechaFax() {
		return this.fechaFax;
	}

	public void setFechaFax(Date fechaFax) {
		this.fechaFax = fechaFax;
	}

	public Date getFechaRegistro() {
		return this.fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Date getFechaVencimiento() {
		return this.fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public Date getFechaCancelacion() {
		return fechaCancelacion;
	}

	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	public String getNumeroContragarantia() {
		return this.numeroContragarantia;
	}

	public void setNumeroContragarantia(String numeroContragarantia) {
		this.numeroContragarantia = numeroContragarantia;
	}

	public String getNumeroGarantia() {
		return this.numeroGarantia;
	}

	public void setNumeroGarantia(String numeroGarantia) {
		this.numeroGarantia = numeroGarantia;
	}

	public String getProducto() {
		return this.producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getRutaArchivo() {
		return this.rutaArchivo;
	}

	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public BigDecimal getTolerancia() {
		return this.tolerancia;
	}

	public void setTolerancia(BigDecimal tolerancia) {
		this.tolerancia = tolerancia;
	}

	public BigDecimal getValor() {
		return this.valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getVolumenCargamento() {
		return this.volumenCargamento;
	}

	public void setVolumenCargamento(BigDecimal volumenCargamento) {
		this.volumenCargamento = volumenCargamento;
	}

	public BigDecimal getVolumenTotal() {
		return this.volumenTotal;
	}

	public void setVolumenTotal(BigDecimal volumenTotal) {
		this.volumenTotal = volumenTotal;
	}

	public Actor getActor() {
		return Actor;
	}

	public void setActor(Actor actor) {
		Actor = actor;
	}

	public Adjudicacion getAdjudicacion() {
		return this.Adjudicacion;
	}

	public void setAdjudicacion(Adjudicacion Adjudicacion) {
		this.Adjudicacion = Adjudicacion;
	}

	public BigDecimal getPlazoVigencia() {
		return plazoVigencia;
	}

	public void setPlazoVigencia(BigDecimal plazoVigencia) {
		this.plazoVigencia = plazoVigencia;
	}

	public String getTextoDevolucion() {
		return textoDevolucion;
	}

	public void setTextoDevolucion(String textoDevolucion) {
		this.textoDevolucion = textoDevolucion;
	}

	public String getTextoRenovacion() {
		return textoRenovacion;
	}

	public void setTextoRenovacion(String textoRenovacion) {
		this.textoRenovacion = textoRenovacion;
	}

	public BigDecimal getPlazoRenovacion() {
		return plazoRenovacion;
	}

	public void setPlazoRenovacion(BigDecimal plazoRenovacion) {
		this.plazoRenovacion = plazoRenovacion;
	}

	public Date getFechaDevolucion() {
		return fechaDevolucion;
	}

	public void setFechaDevolucion(Date fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	
	//metodos para combos
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof Garantia))
			return false;
		return ((Garantia) obj).getId().equals(this.id);
	}


	@Override
	public String toString() {
		return numeroGarantia;
	}
	
	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
}