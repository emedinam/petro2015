
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TramiteEmbarqueDescargaDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDescarga;

/**
 Clase TramiteEmbarqueDescargaDao
 @autor dgonzalez
*/
@Component("tramiteembarquedescargaDao")
public class TramiteEmbarqueDescargaDaoImpl extends GenericDAOImpl<TramiteEmbarqueDescarga, Long> implements TramiteEmbarqueDescargaDao {
	
}
   