package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;


/**
 * The persistent class for the T_DISTRITO_ADUANERO database table.
 * 
 */
@Entity
@Table(name = "NG_DISTRITO_ADUANERO", schema= "APS_NEGOCIO")
public class DistritoAduanero extends DatabaseObject<Long> {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	private String nombre;
	
	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;
	
	@Column(name="USUARIO_ACTUALIZACION")
	private String usuarioActualizacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ACTUALIZACION")
	private Date fechaActualizacion;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	//bi-directional many-to-one association to TramiteEmbarqueDesaduanamiento
	@OneToMany(mappedBy="DistritoAduanero")
	private List<TramiteEmbarqueDesaduanamiento> TramiteEmbarqueDesaduanamientos;

	public DistritoAduanero() {
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<TramiteEmbarqueDesaduanamiento> getTramiteEmbarqueDesaduanamientos() {
		return this.TramiteEmbarqueDesaduanamientos;
	}

	public void setTramiteEmbarqueDesaduanamientos(List<TramiteEmbarqueDesaduanamiento> TramiteEmbarqueDesaduanamientos) {
		this.TramiteEmbarqueDesaduanamientos = TramiteEmbarqueDesaduanamientos;
	}

	public TramiteEmbarqueDesaduanamiento addTramiteEmbarqueDesaduanamiento(TramiteEmbarqueDesaduanamiento TramiteEmbarqueDesaduanamiento) {
		getTramiteEmbarqueDesaduanamientos().add(TramiteEmbarqueDesaduanamiento);
		TramiteEmbarqueDesaduanamiento.setDistritoAduanero(this);

		return TramiteEmbarqueDesaduanamiento;
	}

	public TramiteEmbarqueDesaduanamiento removeTramiteEmbarqueDesaduanamiento(TramiteEmbarqueDesaduanamiento TramiteEmbarqueDesaduanamiento) {
		getTramiteEmbarqueDesaduanamientos().remove(TramiteEmbarqueDesaduanamiento);
		TramiteEmbarqueDesaduanamiento.setDistritoAduanero(null);

		return TramiteEmbarqueDesaduanamiento;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	//metodos para combos
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof DistritoAduanero))
			return false;
		return ((DistritoAduanero) obj).getId().equals(this.id);
	}

	@Override
	public String toString() {
		return nombre;
	}
	
	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

}