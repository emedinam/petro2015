package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

/**
 * The persistent class for the T_DOCUMENTO_ADJUNTO database table.
 * 
 */
@Entity
@Table(name = "NG_DOCUMENTO_ADJUNTO", schema= "APS_NEGOCIO")
public class DocumentoAdjunto extends DatabaseObject<Long> {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	private String nombre;

	public Date getFechaActualiza() {
		return fechaActualiza;
	}

	public void setFechaActualiza(Date fechaActualiza) {
		this.fechaActualiza = fechaActualiza;
	}

	public Date getFechaCrea() {
		return fechaCrea;
	}

	public void setFechaCrea(Date fechaCrea) {
		this.fechaCrea = fechaCrea;
	}

	public String getUsuarioActualiza() {
		return usuarioActualiza;
	}

	public void setUsuarioActualiza(String usuarioActualiza) {
		this.usuarioActualiza = usuarioActualiza;
	}

	public String getUsuarioCrea() {
		return usuarioCrea;
	}

	public void setUsuarioCrea(String usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}

	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_ACTUALIZACION")
	private Date fechaActualiza;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION")
	private Date fechaCrea;

	@Column(name = "USUARIO_ACTUALIZACION")
	private String usuarioActualiza;

	@Column(name = "USUARIO_CREACION")
	private String usuarioCrea;

	// bi-directional many-to-one association to TramiteEmbarqueDesaduanamiento
	@OneToMany(mappedBy = "DocumentoAdjunto")
	private List<TramiteEmbarqueDesaduanamiento> TramiteEmbarqueDesaduanamientos;

	public DocumentoAdjunto() {
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<TramiteEmbarqueDesaduanamiento> getTramiteEmbarqueDesaduanamientos() {
		return this.TramiteEmbarqueDesaduanamientos;
	}

	public void setTramiteEmbarqueDesaduanamientos(
			List<TramiteEmbarqueDesaduanamiento> TramiteEmbarqueDesaduanamientos) {
		this.TramiteEmbarqueDesaduanamientos = TramiteEmbarqueDesaduanamientos;
	}

	public TramiteEmbarqueDesaduanamiento addTramiteEmbarqueDesaduanamiento(
			TramiteEmbarqueDesaduanamiento TramiteEmbarqueDesaduanamiento) {
		getTramiteEmbarqueDesaduanamientos()
				.add(TramiteEmbarqueDesaduanamiento);
		TramiteEmbarqueDesaduanamiento.setDocumentoAdjunto(this);

		return TramiteEmbarqueDesaduanamiento;
	}

	public TramiteEmbarqueDesaduanamiento removeTramiteEmbarqueDesaduanamiento(
			TramiteEmbarqueDesaduanamiento TramiteEmbarqueDesaduanamiento) {
		getTramiteEmbarqueDesaduanamientos().remove(
				TramiteEmbarqueDesaduanamiento);
		TramiteEmbarqueDesaduanamiento.setDocumentoAdjunto(null);

		return TramiteEmbarqueDesaduanamiento;
	}
	
	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

}