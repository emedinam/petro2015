package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;


/**
 * The persistent class for the T_TRAMITE_EMBARQUE_CARGA database table.
 * 
 */
@NamedQueries({
	@NamedQuery(
		name = "obtenerPorPadreTEC",
		query = "select p from TramiteEmbarqueCarga p where p.estado='ACTIVO' and p.TramiteEmbarque.id=:tramiteEmbarque"
	),
})
@Entity
@Table(name = "NG_TRAMITE_EMBARQUE_CARGA", schema = "APS_NEGOCIO")
public class TramiteEmbarqueCarga extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA")
	private Date fecha;
	
	@Column(name="CARTA")
	private String carta;
	
	@Column(name="PROVIDENCIA")
	private String providencia;
	
	@Column(name="PRODUCTO")
	private String producto;

	private BigDecimal volumen;
	
	@Column(name="TRANSPORTE")
	private String transporte;
	
	@Column(name="VEHICULO")
	private String vehiculo;
	
	@Column(name="CONDUCTOR")
	private String conductor;
	
	@Column(name="DESTINO")
	private String destino;
	
	@Column(name="DOCUMENTO")
	private String documento;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_DOCUMENTO")
	private Date fechaDocumento;
	
	@Column(name="GENERADO")
	private BigDecimal generado;
	
	@Column(name="OBSERVACION")
	private String observacion;
	
	@Column(name="SEGURO")
	private String seguro;

	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;
	
	@Column(name="USUARIO_ACTUALIZACION")
	private String usuarioActualizacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ACTUALIZACION")
	private Date fechaActualizacion;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	
	//bi-directional many-to-one association to TramiteEmbarque
	@ManyToOne
	@JoinColumn(name="IF_TRAMITE_EMBARQUE")
	private TramiteEmbarque TramiteEmbarque;

	
	public TramiteEmbarqueCarga() {
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getCarta() {
		return carta;
	}

	public void setCarta(String carta) {
		this.carta = carta;
	}
	
	public String getProvidencia() {
		return providencia;
	}

	public void setProvidencia(String providencia) {
		this.providencia = providencia;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public BigDecimal getVolumen() {
		return volumen;
	}

	public void setVolumen(BigDecimal volumen) {
		this.volumen = volumen;
	}

	public String getTransporte() {
		return transporte;
	}

	public void setTransporte(String transporte) {
		this.transporte = transporte;
	}

	public String getVehiculo() {
		return vehiculo;
	}

	public void setVehiculo(String vehiculo) {
		this.vehiculo = vehiculo;
	}

	public String getConductor() {
		return conductor;
	}

	public void setConductor(String conductor) {
		this.conductor = conductor;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public Date getFechaDocumento() {
		return fechaDocumento;
	}

	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}

	public BigDecimal getGenerado() {
		return generado;
	}

	public void setGenerado(BigDecimal generado) {
		this.generado = generado;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getSeguro() {
		return seguro;
	}

	public void setSeguro(String seguro) {
		this.seguro = seguro;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	public TramiteEmbarque getTramiteEmbarque() {
		return TramiteEmbarque;
	}

	public void setTramiteEmbarque(TramiteEmbarque tramiteEmbarque) {
		TramiteEmbarque = tramiteEmbarque;
	}
	
	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
}