

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.CategoriaDocumento;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase CategoriaDocumentoDao
 @autor dgonzalez
*/
public interface CategoriaDocumentoDao extends GenericDAO<CategoriaDocumento, Long> {
	
}

   