
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.PrecioMarcadorDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PrecioMarcador;
import ec.gob.petroecuador.aduanapetro.negocio.services.PrecioMarcadorService;

/**
 Clase PrecioMarcadorServiceImpl
 @autor dgonzalez
*/
@Service
public class PrecioMarcadorServiceImpl extends GenericServiceImpl<PrecioMarcador, Long> implements PrecioMarcadorService {

	@Autowired
	PrecioMarcadorDao dao;
	
	@Override
	public GenericDAO<PrecioMarcador, Long> getDao() {
		return dao;
	}

}
   