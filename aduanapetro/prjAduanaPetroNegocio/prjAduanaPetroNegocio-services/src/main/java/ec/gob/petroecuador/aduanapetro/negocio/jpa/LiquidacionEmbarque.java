package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import javax.persistence.*;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the T_LIQUIDACION_EMBARQUE database table.
 * 
 */
@Entity
@Table(name = "NG_LIQUIDACION_EMBARQUE", schema = "APS_NEGOCIO")
public class LiquidacionEmbarque extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name="ESTADO_EMB")
	private String estadoEmb;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_REGISTRO")
	private Date fechaRegistro;

	@Column(name="GASTO_BANCARIO")
	private BigDecimal gastoBancario;

	@Column(name="RUTA_ARCHIVO")
	private String rutaArchivo;

	private BigDecimal seguro;

	private String tipo;

	private BigDecimal total;

	private BigDecimal valor;

	@Column(name="VALOR_DAT")
	private BigDecimal valorDat;

	@Column(name="VALOR_FLETE_INTERNACIONAL")
	private BigDecimal valorFleteInternacional;

	@Column(name="VALOR_FOB")
	private BigDecimal valorFob;

	@Column(name="VALOR_PAGADO_AGENTE")
	private BigDecimal valorPagadoAgente;

	private BigDecimal volumen;

	//bi-directional many-to-one association to Adjudicacion
	@ManyToOne
	@JoinColumn(name="IF_ADJUDICACION")
	private Adjudicacion Adjudicacion;

	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	
	public LiquidacionEmbarque() {
	}

	public String getEstadoEmb() {
		return estadoEmb;
	}

	public void setEstadoEmb(String estadoEmb) {
		this.estadoEmb = estadoEmb;
	}

	public Date getFechaRegistro() {
		return this.fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public BigDecimal getGastoBancario() {
		return this.gastoBancario;
	}

	public void setGastoBancario(BigDecimal gastoBancario) {
		this.gastoBancario = gastoBancario;
	}

	public String getRutaArchivo() {
		return this.rutaArchivo;
	}

	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}

	public BigDecimal getSeguro() {
		return this.seguro;
	}

	public void setSeguro(BigDecimal seguro) {
		this.seguro = seguro;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getValor() {
		return this.valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getValorDat() {
		return this.valorDat;
	}

	public void setValorDat(BigDecimal valorDat) {
		this.valorDat = valorDat;
	}

	public BigDecimal getValorFleteInternacional() {
		return this.valorFleteInternacional;
	}

	public void setValorFleteInternacional(BigDecimal valorFleteInternacional) {
		this.valorFleteInternacional = valorFleteInternacional;
	}

	public BigDecimal getValorFob() {
		return this.valorFob;
	}

	public void setValorFob(BigDecimal valorFob) {
		this.valorFob = valorFob;
	}

	public BigDecimal getValorPagadoAgente() {
		return this.valorPagadoAgente;
	}

	public void setValorPagadoAgente(BigDecimal valorPagadoAgente) {
		this.valorPagadoAgente = valorPagadoAgente;
	}

	public BigDecimal getVolumen() {
		return this.volumen;
	}

	public void setVolumen(BigDecimal volumen) {
		this.volumen = volumen;
	}

	public Adjudicacion getAdjudicacion() {
		return this.Adjudicacion;
	}

	public void setAdjudicacion(Adjudicacion Adjudicacion) {
		this.Adjudicacion = Adjudicacion;
	}

	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
}