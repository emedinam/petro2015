
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.GeneracionDocumentoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.GeneracionDocumento;

/**
 Clase GeneracionDocumentoDao
 @autor dgonzalez
*/
@Component("generaciondocumentoDao")
public class GeneracionDocumentoDaoImpl extends GenericDAOImpl<GeneracionDocumento, Long> implements GeneracionDocumentoDao {
	
}
   