
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.FormulaDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Formula;

/**
 Clase FormulaDao
 @autor dgonzalez
*/
@Component("formulaDao")
public class FormulaDaoImpl extends GenericDAOImpl<Formula, Long> implements FormulaDao {
	
}
   