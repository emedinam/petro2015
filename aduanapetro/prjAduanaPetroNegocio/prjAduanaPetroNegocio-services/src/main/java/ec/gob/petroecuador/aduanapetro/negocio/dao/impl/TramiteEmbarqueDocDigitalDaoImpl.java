
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TramiteEmbarqueDocDigitalDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDocDigital;

/**
 Clase TramiteEmbarqueDocDigitalDao
 @autor dgonzalez
*/
@Component("tramiteembarquedocdigitalDao")
public class TramiteEmbarqueDocDigitalDaoImpl extends GenericDAOImpl<TramiteEmbarqueDocDigital, Long> implements TramiteEmbarqueDocDigitalDao {
	
}
   