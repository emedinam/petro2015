

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.EstadoTramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase EstadoTramiteEmbarqueDao
 @autor dgonzalez
*/
public interface EstadoTramiteEmbarqueDao extends GenericDAO<EstadoTramiteEmbarque, Long> {
	
}

   