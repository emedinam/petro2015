
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.CompaniaSeguro;

/**
 Clase CompaniaSeguroService
 @autor dgonzalez
*/
public interface CompaniaSeguroService extends GenericService<CompaniaSeguro, Long> {
	
}
   