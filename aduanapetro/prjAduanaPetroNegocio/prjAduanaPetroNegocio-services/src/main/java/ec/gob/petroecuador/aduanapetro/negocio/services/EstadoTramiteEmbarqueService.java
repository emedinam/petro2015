
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.EstadoTramiteEmbarque;

/**
 Clase EstadoTramiteEmbarqueService
 @autor dgonzalez
*/
public interface EstadoTramiteEmbarqueService extends GenericService<EstadoTramiteEmbarque, Long> {
	
}
   