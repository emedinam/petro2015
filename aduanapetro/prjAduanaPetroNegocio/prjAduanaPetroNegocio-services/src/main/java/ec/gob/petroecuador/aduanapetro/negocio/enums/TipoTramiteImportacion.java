package ec.gob.petroecuador.aduanapetro.negocio.enums;

public enum TipoTramiteImportacion {
	CARTA_CREDITO, GIRO_DIRECTOCC, GIRO_DIRECTO, CONVENIO, PAGO_ANTICIPADO;

}
