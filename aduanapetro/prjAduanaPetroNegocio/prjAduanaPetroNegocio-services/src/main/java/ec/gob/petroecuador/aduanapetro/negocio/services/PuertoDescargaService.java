
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PuertoDescarga;

/**
 Clase PuertoDescargaService
 @autor dgonzalez
*/
public interface PuertoDescargaService extends GenericService<PuertoDescarga, Long> {
	
}
   