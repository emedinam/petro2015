
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.FacturaDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Factura;
import ec.gob.petroecuador.aduanapetro.negocio.services.FacturaService;

/**
 Clase FacturaServiceImpl
 @autor dgonzalez
*/
@Service
public class FacturaServiceImpl extends GenericServiceImpl<Factura, Long> implements FacturaService {

	@Autowired
	FacturaDao dao;
	
	@Override
	public GenericDAO<Factura, Long> getDao() {
		return dao;
	}

}
   