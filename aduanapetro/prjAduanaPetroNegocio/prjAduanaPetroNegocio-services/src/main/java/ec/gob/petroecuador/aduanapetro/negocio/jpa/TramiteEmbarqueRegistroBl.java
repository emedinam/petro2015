package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;


/**
 * The persistent class for the T_TRAMITE_EMBARQUE_REGISTRO_BL database table.
 * 
 */
@NamedQueries({
	@NamedQuery(
		name = "obtenerPorPadreTERBL",
		query = "select p from TramiteEmbarqueRegistroBl p where p.estado='ACTIVO' and p.TramiteEmbarque.id=:tramiteEmbarque"
	),
})
@Entity
@Table(name = "NG_TRAMITE_EMBARQUE_REGISTRO_BL", schema = "APS_NEGOCIO")
public class TramiteEmbarqueRegistroBl extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	private String numero;

	private BigDecimal valor;

	private BigDecimal volumen;
	
	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;
	
	@Column(name="USUARIO_ACTUALIZACION")
	private String usuarioActualizacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ACTUALIZACION")
	private Date fechaActualizacion;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	//bi-directional many-to-one association to TramiteEmbarqueDescarga
	@OneToMany(mappedBy="TramiteEmbarqueRegistroBl")
	private List<TramiteEmbarqueDescarga> TramiteEmbarqueDescargas;

	//bi-directional many-to-one association to TramiteEmbarque
	@ManyToOne
	@JoinColumn(name="IF_TRAMITE_EMBARQUE")
	private TramiteEmbarque TramiteEmbarque;

	//bi-directional many-to-one association to TramiteEmbarqueCalculoPrecio
	@OneToMany(mappedBy="TramiteEmbarqueRegistroBl")
	private List<TramiteEmbarqueCalculoPrecio> TramiteEmbarqueCalculoPrecios;

	public TramiteEmbarqueRegistroBl() {
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public BigDecimal getValor() {
		return this.valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getVolumen() {
		return this.volumen;
	}

	public void setVolumen(BigDecimal volumen) {
		this.volumen = volumen;
	}

	public List<TramiteEmbarqueDescarga> getTramiteEmbarqueDescargas() {
		return this.TramiteEmbarqueDescargas;
	}

	public void setTramiteEmbarqueDescargas(List<TramiteEmbarqueDescarga> TramiteEmbarqueDescargas) {
		this.TramiteEmbarqueDescargas = TramiteEmbarqueDescargas;
	}

	public TramiteEmbarqueDescarga addTramiteEmbarqueDescarga(TramiteEmbarqueDescarga TramiteEmbarqueDescarga) {
		getTramiteEmbarqueDescargas().add(TramiteEmbarqueDescarga);
		TramiteEmbarqueDescarga.setTramiteEmbarqueRegistroBl(this);

		return TramiteEmbarqueDescarga;
	}

	public TramiteEmbarqueDescarga removeTramiteEmbarqueDescarga(TramiteEmbarqueDescarga TramiteEmbarqueDescarga) {
		getTramiteEmbarqueDescargas().remove(TramiteEmbarqueDescarga);
		TramiteEmbarqueDescarga.setTramiteEmbarqueRegistroBl(null);

		return TramiteEmbarqueDescarga;
	}

	public TramiteEmbarque getTramiteEmbarque() {
		return this.TramiteEmbarque;
	}

	public void setTramiteEmbarque(TramiteEmbarque TramiteEmbarque) {
		this.TramiteEmbarque = TramiteEmbarque;
	}

	public List<TramiteEmbarqueCalculoPrecio> getTramiteEmbarqueCalculoPrecios() {
		return this.TramiteEmbarqueCalculoPrecios;
	}

	public void setTramiteEmbarqueCalculoPrecios(List<TramiteEmbarqueCalculoPrecio> TramiteEmbarqueCalculoPrecios) {
		this.TramiteEmbarqueCalculoPrecios = TramiteEmbarqueCalculoPrecios;
	}

	public TramiteEmbarqueCalculoPrecio addTramiteEmbarqueCalculoPrecio(TramiteEmbarqueCalculoPrecio TramiteEmbarqueCalculoPrecio) {
		getTramiteEmbarqueCalculoPrecios().add(TramiteEmbarqueCalculoPrecio);
		TramiteEmbarqueCalculoPrecio.setTramiteEmbarqueRegistroBl(this);

		return TramiteEmbarqueCalculoPrecio;
	}

	public TramiteEmbarqueCalculoPrecio removeTramiteEmbarqueCalculoPrecio(TramiteEmbarqueCalculoPrecio TramiteEmbarqueCalculoPrecio) {
		getTramiteEmbarqueCalculoPrecios().remove(TramiteEmbarqueCalculoPrecio);
		TramiteEmbarqueCalculoPrecio.setTramiteEmbarqueRegistroBl(null);

		return TramiteEmbarqueCalculoPrecio;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
	// metodos para combos

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof TramiteEmbarqueRegistroBl))
				return false;
		return ((TramiteEmbarqueRegistroBl) obj).getId().equals(this.id);
	}
	
	@Override
	public String toString() {
		return fecha.toString();
	}
}