
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TramiteEmbarqueModPagoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueModPago;
import ec.gob.petroecuador.aduanapetro.negocio.services.TramiteEmbarqueModPagoService;

/**
 Clase TramiteEmbarqueModPagoServiceImpl
 @autor dgonzalez
*/
@Service
public class TramiteEmbarqueModPagoServiceImpl extends GenericServiceImpl<TramiteEmbarqueModPago, Long> implements TramiteEmbarqueModPagoService {

	@Autowired
	TramiteEmbarqueModPagoDao dao;
	
	@Override
	public GenericDAO<TramiteEmbarqueModPago, Long> getDao() {
		return dao;
	}

	@Override
	public List<TramiteEmbarqueModPago> obtenerPorPadre(TramiteEmbarque tramiteEmbarque) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("tramiteEmbarque", tramiteEmbarque.getId());
		return dao.findByNamedQuery("obtenerPorPadreTEMP", map);
	}

}
   