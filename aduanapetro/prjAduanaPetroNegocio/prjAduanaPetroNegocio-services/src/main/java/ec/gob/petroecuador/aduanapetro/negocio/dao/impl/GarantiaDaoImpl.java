
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.GarantiaDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Garantia;

/**
 Clase GarantiaDao
 @autor dgonzalez
*/
@Component("garantiaDao")
public class GarantiaDaoImpl extends GenericDAOImpl<Garantia, Long> implements GarantiaDao {
	
}
   