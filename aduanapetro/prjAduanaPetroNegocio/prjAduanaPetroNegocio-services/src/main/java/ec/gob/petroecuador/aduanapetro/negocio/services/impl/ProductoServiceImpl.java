
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.ProductoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Producto;
import ec.gob.petroecuador.aduanapetro.negocio.services.ProductoService;

/**
 Clase ProductoServiceImpl
 @autor dgonzalez
*/
@Service
public class ProductoServiceImpl extends GenericServiceImpl<Producto, Long> implements ProductoService {

	@Autowired
	ProductoDao dao;
	
	@Override
	public GenericDAO<Producto, Long> getDao() {
		return dao;
	}

}
   