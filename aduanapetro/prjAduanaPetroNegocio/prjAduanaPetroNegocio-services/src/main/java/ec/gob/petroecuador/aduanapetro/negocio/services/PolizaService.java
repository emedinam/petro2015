
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Poliza;

/**
 Clase PolizaService
 @autor dgonzalez
*/
public interface PolizaService extends GenericService<Poliza, Long> {
	
}
   