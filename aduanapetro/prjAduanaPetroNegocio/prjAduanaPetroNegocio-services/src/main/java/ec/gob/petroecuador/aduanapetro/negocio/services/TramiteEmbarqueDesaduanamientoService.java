
package ec.gob.petroecuador.aduanapetro.negocio.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDesaduanamiento;

/**
 Clase TramiteEmbarqueDesaduanamientoService
 @autor dgonzalez
*/
public interface TramiteEmbarqueDesaduanamientoService extends GenericService<TramiteEmbarqueDesaduanamiento, Long> {
	public List<TramiteEmbarqueDesaduanamiento> obtenerPorPadre(TramiteEmbarque tramiteEmbarque);
}
   