
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.ParametroCalculoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.ParametroCalculo;

/**
 Clase ParametroCalculoDao
 @autor dgonzalez
*/
@Component("parametrocalculoDao")
public class ParametroCalculoDaoImpl extends GenericDAOImpl<ParametroCalculo, Long> implements ParametroCalculoDao {
	
}
   