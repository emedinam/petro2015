package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.CatalogodetalleDao;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Catalogodetalle;
import ec.gob.petroecuador.aduanapetro.negocio.services.CatalogodetalleService;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;

@Service
public class CatalogodetalleServiceImpl extends GenericServiceImpl<Catalogodetalle, Long> implements CatalogodetalleService {

	@Autowired
	CatalogodetalleDao dao;
	
	@Override
	public GenericDAO<Catalogodetalle, Long> getDao() {
		return dao;
	}

}
