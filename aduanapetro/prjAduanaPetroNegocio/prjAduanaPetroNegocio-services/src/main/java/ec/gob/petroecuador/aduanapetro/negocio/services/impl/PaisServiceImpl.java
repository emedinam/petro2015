
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.PaisDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Pais;
import ec.gob.petroecuador.aduanapetro.negocio.services.PaisService;

/**
 Clase PaisServiceImpl
 @autor dgonzalez
*/
@Service
public class PaisServiceImpl extends GenericServiceImpl<Pais, Long> implements PaisService {

	@Autowired
	PaisDao dao;
	
	@Override
	public GenericDAO<Pais, Long> getDao() {
		return dao;
	}

}
   