
package ec.gob.petroecuador.aduanapetro.negocio.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueCalculoPrecio;

/**
 Clase TramiteEmbarqueCalculoPrecioService
 @autor dgonzalez
*/
public interface TramiteEmbarqueCalculoPrecioService extends GenericService<TramiteEmbarqueCalculoPrecio, Long> {
	public List<TramiteEmbarqueCalculoPrecio> obtenerPorPadre(TramiteEmbarque tramiteEmbarque);
	
}
   