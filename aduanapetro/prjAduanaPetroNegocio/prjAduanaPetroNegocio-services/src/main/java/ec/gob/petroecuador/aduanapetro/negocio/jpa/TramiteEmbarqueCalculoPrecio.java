package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;


/**
 * The persistent class for the T_TR_EB_CC_PO database table.
 * 
 */
@NamedQueries({
	@NamedQuery(name="TramiteEmbarqueCalculoPrecioConPadre",
			query="Select c from TramiteEmbarqueCalculoPrecio c where c.TramiteEmbarque.id=:tramiteEmbarque and c.estado='ACTIVO'"
	),
})
@Entity
@Table(name = "NG_TRAMITE_EMBARQUE_CALP", schema = "APS_NEGOCIO")
public class TramiteEmbarqueCalculoPrecio extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name="CALCULO_VOLUMEN_PUERTO")
	private BigDecimal calculoVolumenPuerto;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_BL")
	private Date fechaBl;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CATORCE")
	private Date fechaCatorce;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CINCO")
	private Date fechaCinco;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CUATRO")
	private Date fechaCuatro;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_DIECISEIS")
	private Date fechaDieciseis;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_DIECISIETE")
	private Date fechaDiecisiete;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_DIEZ")
	private Date fechaDiez;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_DOCE")
	private Date fechaDoce;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_DOS")
	private Date fechaDos;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_NUEVE")
	private Date fechaNueve;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_OCHO")
	private Date fechaOcho;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ONCE")
	private Date fechaOnce;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_QUINCE")
	private Date fechaQuince;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_SEIS")
	private Date fechaSeis;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_SIETE")
	private Date fechaSiete;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_TRECE")
	private Date fechaTrece;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_TRES")
	private Date fechaTres;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_UNO")
	private Date fechaUno;

	private BigDecimal libertad;

	@Column(name="NUMERO_BL")
	private String numeroBl;

	private BigDecimal precio;

	@Column(name="PRECIO_BARRIL_BL")
	private BigDecimal precioBarrilBl;

	@Column(name="PRECIO_BARRIL_CATORCE")
	private BigDecimal precioBarrilCatorce;

	@Column(name="PRECIO_BARRIL_CINCO")
	private BigDecimal precioBarrilCinco;

	@Column(name="PRECIO_BARRIL_CUATRO")
	private BigDecimal precioBarrilCuatro;

	@Column(name="PRECIO_BARRIL_DIECISEIS")
	private BigDecimal precioBarrilDieciseis;

	@Column(name="PRECIO_BARRIL_DIECISIETE")
	private BigDecimal precioBarrilDiecisiete;

	@Column(name="PRECIO_BARRIL_DIEZ")
	private BigDecimal precioBarrilDiez;

	@Column(name="PRECIO_BARRIL_DOCE")
	private BigDecimal precioBarrilDoce;

	@Column(name="PRECIO_BARRIL_DOS")
	private BigDecimal precioBarrilDos;

	@Column(name="PRECIO_BARRIL_NUEVE")
	private BigDecimal precioBarrilNueve;

	@Column(name="PRECIO_BARRIL_OCHO")
	private BigDecimal precioBarrilOcho;

	@Column(name="PRECIO_BARRIL_ONCE")
	private BigDecimal precioBarrilOnce;

	@Column(name="PRECIO_BARRIL_QUINCE")
	private BigDecimal precioBarrilQuince;

	@Column(name="PRECIO_BARRIL_SEIS")
	private BigDecimal precioBarrilSeis;

	@Column(name="PRECIO_BARRIL_SIETE")
	private BigDecimal precioBarrilSiete;

	@Column(name="PRECIO_BARRIL_TRECE")
	private BigDecimal precioBarrilTrece;

	@Column(name="PRECIO_BARRIL_TRES")
	private BigDecimal precioBarrilTres;

	@Column(name="PRECIO_BARRIL_UNO")
	private BigDecimal precioBarrilUno;

	@Column(name="PRECIO_GALON_BL")
	private BigDecimal precioGalonBl;

	@Column(name="PRECIO_GALON_CATORCE")
	private BigDecimal precioGalonCatorce;

	@Column(name="PRECIO_GALON_CINCO")
	private BigDecimal precioGalonCinco;

	@Column(name="PRECIO_GALON_CUATRO")
	private BigDecimal precioGalonCuatro;

	@Column(name="PRECIO_GALON_DIECISEIS")
	private BigDecimal precioGalonDieciseis;

	@Column(name="PRECIO_GALON_DIECISIETE")
	private BigDecimal precioGalonDiecisiete;

	@Column(name="PRECIO_GALON_DIEZ")
	private BigDecimal precioGalonDiez;

	@Column(name="PRECIO_GALON_DOCE")
	private BigDecimal precioGalonDoce;

	@Column(name="PRECIO_GALON_DOS")
	private BigDecimal precioGalonDos;

	@Column(name="PRECIO_GALON_NUEVE")
	private BigDecimal precioGalonNueve;

	@Column(name="PRECIO_GALON_OCHO")
	private BigDecimal precioGalonOcho;

	@Column(name="PRECIO_GALON_ONCE")
	private BigDecimal precioGalonOnce;

	@Column(name="PRECIO_GALON_QUINCE")
	private BigDecimal precioGalonQuince;

	@Column(name="PRECIO_GALON_SEIS")
	private BigDecimal precioGalonSeis;

	@Column(name="PRECIO_GALON_SIETE")
	private BigDecimal precioGalonSiete;

	@Column(name="PRECIO_GALON_TRECE")
	private BigDecimal precioGalonTrece;

	@Column(name="PRECIO_GALON_TRES")
	private BigDecimal precioGalonTres;

	@Column(name="PRECIO_GALON_UNO")
	private BigDecimal precioGalonUno;

	@Column(name="PRECIO_PUERTO")
	private BigDecimal precioPuerto;
	
	@Column(name="VOLUMEN_PARENA")
	private BigDecimal volumenPuertoArena; 
	
	@Column(name="PRECIO_UNO")
	private BigDecimal precioUno;

	private BigDecimal premio;

	@Column(name="PROMEDIO_BARRIL")
	private BigDecimal promedioBarril;

	@Column(name="PROMEDIO_GALON")
	private BigDecimal promedioGalon;

	private BigDecimal prorateo;

	private BigDecimal subtotal;

	private BigDecimal total;

	@Column(name="VOLUMEN_DESCARGADO_PUERTO")
	private BigDecimal volumenDescargadoPuerto;

	@Column(name="VOLUMEN_INICIAL")
	private BigDecimal volumenInicial;

	@Column(name="VOLUMEN_PRORATEO")
	private BigDecimal volumenProrateo;
	
	///datos de auditoria
	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;
				
	@Column(name="USUARIO_ACTUALIZACION")
	private String usuarioActualizacion;
				
	@Temporal(TemporalType.DATE)

	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
				
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ACTUALIZACION")
	private Date fechaActualizacion;
				
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	
	//bi-directional many-to-one association to TramiteEmbarque
	@ManyToOne
	@JoinColumn(name="IF_TRAMITE_EMBARQUE")
	private TramiteEmbarque TramiteEmbarque;

	//bi-directional many-to-one association to TramiteEmbarqueRegistroBl
	@ManyToOne
	@JoinColumn(name="IF_TRAMITE_EMBARQUE_REG_BL")
	private TramiteEmbarqueRegistroBl TramiteEmbarqueRegistroBl;

	public TramiteEmbarqueCalculoPrecio() {
	}

	public BigDecimal getCalculoVolumenPuerto() {
		return this.calculoVolumenPuerto;
	}

	public void setCalculoVolumenPuerto(BigDecimal calculoVolumenPuerto) {
		this.calculoVolumenPuerto = calculoVolumenPuerto;
	}

	public Date getFechaBl() {
		return this.fechaBl;
	}

	public void setFechaBl(Date fechaBl) {
		this.fechaBl = fechaBl;
	}

	public Date getFechaCatorce() {
		return this.fechaCatorce;
	}

	public void setFechaCatorce(Date fechaCatorce) {
		this.fechaCatorce = fechaCatorce;
	}

	public Date getFechaCinco() {
		return this.fechaCinco;
	}

	public void setFechaCinco(Date fechaCinco) {
		this.fechaCinco = fechaCinco;
	}

	public Date getFechaCuatro() {
		return this.fechaCuatro;
	}

	public void setFechaCuatro(Date fechaCuatro) {
		this.fechaCuatro = fechaCuatro;
	}

	public Date getFechaDieciseis() {
		return this.fechaDieciseis;
	}

	public void setFechaDieciseis(Date fechaDieciseis) {
		this.fechaDieciseis = fechaDieciseis;
	}

	public Date getFechaDiecisiete() {
		return this.fechaDiecisiete;
	}

	public void setFechaDiecisiete(Date fechaDiecisiete) {
		this.fechaDiecisiete = fechaDiecisiete;
	}

	public Date getFechaDiez() {
		return this.fechaDiez;
	}

	public void setFechaDiez(Date fechaDiez) {
		this.fechaDiez = fechaDiez;
	}

	public Date getFechaDoce() {
		return this.fechaDoce;
	}

	public void setFechaDoce(Date fechaDoce) {
		this.fechaDoce = fechaDoce;
	}

	public Date getFechaDos() {
		return this.fechaDos;
	}

	public void setFechaDos(Date fechaDos) {
		this.fechaDos = fechaDos;
	}

	public Date getFechaNueve() {
		return this.fechaNueve;
	}

	public void setFechaNueve(Date fechaNueve) {
		this.fechaNueve = fechaNueve;
	}

	public Date getFechaOcho() {
		return this.fechaOcho;
	}

	public void setFechaOcho(Date fechaOcho) {
		this.fechaOcho = fechaOcho;
	}

	public Date getFechaOnce() {
		return this.fechaOnce;
	}

	public void setFechaOnce(Date fechaOnce) {
		this.fechaOnce = fechaOnce;
	}

	public Date getFechaQuince() {
		return this.fechaQuince;
	}

	public void setFechaQuince(Date fechaQuince) {
		this.fechaQuince = fechaQuince;
	}

	public Date getFechaSeis() {
		return this.fechaSeis;
	}

	public void setFechaSeis(Date fechaSeis) {
		this.fechaSeis = fechaSeis;
	}

	public Date getFechaSiete() {
		return this.fechaSiete;
	}

	public void setFechaSiete(Date fechaSiete) {
		this.fechaSiete = fechaSiete;
	}

	public Date getFechaTrece() {
		return this.fechaTrece;
	}

	public void setFechaTrece(Date fechaTrece) {
		this.fechaTrece = fechaTrece;
	}

	public Date getFechaTres() {
		return this.fechaTres;
	}

	public void setFechaTres(Date fechaTres) {
		this.fechaTres = fechaTres;
	}

	public Date getFechaUno() {
		return this.fechaUno;
	}

	public void setFechaUno(Date fechaUno) {
		this.fechaUno = fechaUno;
	}

	public BigDecimal getLibertad() {
		return this.libertad;
	}

	public void setLibertad(BigDecimal libertad) {
		this.libertad = libertad;
	}

	public String getNumeroBl() {
		return this.numeroBl;
	}

	public void setNumeroBl(String numeroBl) {
		this.numeroBl = numeroBl;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public BigDecimal getPrecioBarrilBl() {
		return this.precioBarrilBl;
	}

	public void setPrecioBarrilBl(BigDecimal precioBarrilBl) {
		this.precioBarrilBl = precioBarrilBl;
	}

	public BigDecimal getPrecioBarrilCatorce() {
		return this.precioBarrilCatorce;
	}

	public void setPrecioBarrilCatorce(BigDecimal precioBarrilCatorce) {
		this.precioBarrilCatorce = precioBarrilCatorce;
	}

	public BigDecimal getPrecioBarrilCinco() {
		return this.precioBarrilCinco;
	}

	public void setPrecioBarrilCinco(BigDecimal precioBarrilCinco) {
		this.precioBarrilCinco = precioBarrilCinco;
	}

	public BigDecimal getPrecioBarrilCuatro() {
		return this.precioBarrilCuatro;
	}

	public void setPrecioBarrilCuatro(BigDecimal precioBarrilCuatro) {
		this.precioBarrilCuatro = precioBarrilCuatro;
	}

	public BigDecimal getPrecioBarrilDieciseis() {
		return this.precioBarrilDieciseis;
	}

	public void setPrecioBarrilDieciseis(BigDecimal precioBarrilDieciseis) {
		this.precioBarrilDieciseis = precioBarrilDieciseis;
	}

	public BigDecimal getPrecioBarrilDiecisiete() {
		return this.precioBarrilDiecisiete;
	}

	public void setPrecioBarrilDiecisiete(BigDecimal precioBarrilDiecisiete) {
		this.precioBarrilDiecisiete = precioBarrilDiecisiete;
	}

	public BigDecimal getPrecioBarrilDiez() {
		return this.precioBarrilDiez;
	}

	public void setPrecioBarrilDiez(BigDecimal precioBarrilDiez) {
		this.precioBarrilDiez = precioBarrilDiez;
	}

	public BigDecimal getPrecioBarrilDoce() {
		return this.precioBarrilDoce;
	}

	public void setPrecioBarrilDoce(BigDecimal precioBarrilDoce) {
		this.precioBarrilDoce = precioBarrilDoce;
	}

	public BigDecimal getPrecioBarrilDos() {
		return this.precioBarrilDos;
	}

	public void setPrecioBarrilDos(BigDecimal precioBarrilDos) {
		this.precioBarrilDos = precioBarrilDos;
	}

	public BigDecimal getPrecioBarrilNueve() {
		return this.precioBarrilNueve;
	}

	public void setPrecioBarrilNueve(BigDecimal precioBarrilNueve) {
		this.precioBarrilNueve = precioBarrilNueve;
	}

	public BigDecimal getPrecioBarrilOcho() {
		return this.precioBarrilOcho;
	}

	public void setPrecioBarrilOcho(BigDecimal precioBarrilOcho) {
		this.precioBarrilOcho = precioBarrilOcho;
	}

	public BigDecimal getPrecioBarrilOnce() {
		return this.precioBarrilOnce;
	}

	public void setPrecioBarrilOnce(BigDecimal precioBarrilOnce) {
		this.precioBarrilOnce = precioBarrilOnce;
	}

	public BigDecimal getPrecioBarrilQuince() {
		return this.precioBarrilQuince;
	}

	public void setPrecioBarrilQuince(BigDecimal precioBarrilQuince) {
		this.precioBarrilQuince = precioBarrilQuince;
	}

	public BigDecimal getPrecioBarrilSeis() {
		return this.precioBarrilSeis;
	}

	public void setPrecioBarrilSeis(BigDecimal precioBarrilSeis) {
		this.precioBarrilSeis = precioBarrilSeis;
	}

	public BigDecimal getPrecioBarrilSiete() {
		return this.precioBarrilSiete;
	}

	public void setPrecioBarrilSiete(BigDecimal precioBarrilSiete) {
		this.precioBarrilSiete = precioBarrilSiete;
	}

	public BigDecimal getPrecioBarrilTrece() {
		return this.precioBarrilTrece;
	}

	public void setPrecioBarrilTrece(BigDecimal precioBarrilTrece) {
		this.precioBarrilTrece = precioBarrilTrece;
	}

	public BigDecimal getPrecioBarrilTres() {
		return this.precioBarrilTres;
	}

	public void setPrecioBarrilTres(BigDecimal precioBarrilTres) {
		this.precioBarrilTres = precioBarrilTres;
	}

	public BigDecimal getPrecioBarrilUno() {
		return this.precioBarrilUno;
	}

	public void setPrecioBarrilUno(BigDecimal precioBarrilUno) {
		this.precioBarrilUno = precioBarrilUno;
	}

	public BigDecimal getPrecioGalonBl() {
		return this.precioGalonBl;
	}

	public void setPrecioGalonBl(BigDecimal precioGalonBl) {
		this.precioGalonBl = precioGalonBl;
	}

	public BigDecimal getPrecioGalonCatorce() {
		return this.precioGalonCatorce;
	}

	public void setPrecioGalonCatorce(BigDecimal precioGalonCatorce) {
		this.precioGalonCatorce = precioGalonCatorce;
	}

	public BigDecimal getPrecioGalonCinco() {
		return this.precioGalonCinco;
	}

	public void setPrecioGalonCinco(BigDecimal precioGalonCinco) {
		this.precioGalonCinco = precioGalonCinco;
	}

	public BigDecimal getPrecioGalonCuatro() {
		return this.precioGalonCuatro;
	}

	public void setPrecioGalonCuatro(BigDecimal precioGalonCuatro) {
		this.precioGalonCuatro = precioGalonCuatro;
	}

	public BigDecimal getPrecioGalonDieciseis() {
		return this.precioGalonDieciseis;
	}

	public void setPrecioGalonDieciseis(BigDecimal precioGalonDieciseis) {
		this.precioGalonDieciseis = precioGalonDieciseis;
	}

	public BigDecimal getPrecioGalonDiecisiete() {
		return this.precioGalonDiecisiete;
	}

	public void setPrecioGalonDiecisiete(BigDecimal precioGalonDiecisiete) {
		this.precioGalonDiecisiete = precioGalonDiecisiete;
	}

	public BigDecimal getPrecioGalonDiez() {
		return this.precioGalonDiez;
	}

	public void setPrecioGalonDiez(BigDecimal precioGalonDiez) {
		this.precioGalonDiez = precioGalonDiez;
	}

	public BigDecimal getPrecioGalonDoce() {
		return this.precioGalonDoce;
	}

	public void setPrecioGalonDoce(BigDecimal precioGalonDoce) {
		this.precioGalonDoce = precioGalonDoce;
	}

	public BigDecimal getPrecioGalonDos() {
		return this.precioGalonDos;
	}

	public void setPrecioGalonDos(BigDecimal precioGalonDos) {
		this.precioGalonDos = precioGalonDos;
	}

	public BigDecimal getPrecioGalonNueve() {
		return this.precioGalonNueve;
	}

	public void setPrecioGalonNueve(BigDecimal precioGalonNueve) {
		this.precioGalonNueve = precioGalonNueve;
	}

	public BigDecimal getPrecioGalonOcho() {
		return this.precioGalonOcho;
	}

	public void setPrecioGalonOcho(BigDecimal precioGalonOcho) {
		this.precioGalonOcho = precioGalonOcho;
	}

	public BigDecimal getPrecioGalonOnce() {
		return this.precioGalonOnce;
	}

	public void setPrecioGalonOnce(BigDecimal precioGalonOnce) {
		this.precioGalonOnce = precioGalonOnce;
	}

	public BigDecimal getPrecioGalonQuince() {
		return this.precioGalonQuince;
	}

	public void setPrecioGalonQuince(BigDecimal precioGalonQuince) {
		this.precioGalonQuince = precioGalonQuince;
	}

	public BigDecimal getPrecioGalonSeis() {
		return this.precioGalonSeis;
	}

	public void setPrecioGalonSeis(BigDecimal precioGalonSeis) {
		this.precioGalonSeis = precioGalonSeis;
	}

	public BigDecimal getPrecioGalonSiete() {
		return this.precioGalonSiete;
	}

	public void setPrecioGalonSiete(BigDecimal precioGalonSiete) {
		this.precioGalonSiete = precioGalonSiete;
	}

	public BigDecimal getPrecioGalonTrece() {
		return this.precioGalonTrece;
	}

	public void setPrecioGalonTrece(BigDecimal precioGalonTrece) {
		this.precioGalonTrece = precioGalonTrece;
	}

	public BigDecimal getPrecioGalonTres() {
		return this.precioGalonTres;
	}

	public void setPrecioGalonTres(BigDecimal precioGalonTres) {
		this.precioGalonTres = precioGalonTres;
	}

	public BigDecimal getPrecioGalonUno() {
		return this.precioGalonUno;
	}

	public void setPrecioGalonUno(BigDecimal precioGalonUno) {
		this.precioGalonUno = precioGalonUno;
	}

	public BigDecimal getPrecioPuerto() {
		return this.precioPuerto;
	}

	public void setPrecioPuerto(BigDecimal precioPuerto) {
		this.precioPuerto = precioPuerto;
	}

	public BigDecimal getPremio() {
		return this.premio;
	}

	public void setPremio(BigDecimal premio) {
		this.premio = premio;
	}

	public BigDecimal getPromedioBarril() {
		return this.promedioBarril;
	}

	public void setPromedioBarril(BigDecimal promedioBarril) {
		this.promedioBarril = promedioBarril;
	}

	public BigDecimal getPromedioGalon() {
		return this.promedioGalon;
	}

	public void setPromedioGalon(BigDecimal promedioGalon) {
		this.promedioGalon = promedioGalon;
	}

	public BigDecimal getProrateo() {
		return this.prorateo;
	}

	public void setProrateo(BigDecimal prorateo) {
		this.prorateo = prorateo;
	}

	public BigDecimal getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getVolumenDescargadoPuerto() {
		return this.volumenDescargadoPuerto;
	}

	public void setVolumenDescargadoPuerto(BigDecimal volumenDescargadoPuerto) {
		this.volumenDescargadoPuerto = volumenDescargadoPuerto;
	}

	public BigDecimal getVolumenInicial() {
		return this.volumenInicial;
	}

	public void setVolumenInicial(BigDecimal volumenInicial) {
		this.volumenInicial = volumenInicial;
	}

	public BigDecimal getVolumenProrateo() {
		return this.volumenProrateo;
	}

	public void setVolumenProrateo(BigDecimal volumenProrateo) {
		this.volumenProrateo = volumenProrateo;
	}

	public TramiteEmbarque getTramiteEmbarque() {
		return this.TramiteEmbarque;
	}

	public void setTramiteEmbarque(TramiteEmbarque TramiteEmbarque) {
		this.TramiteEmbarque = TramiteEmbarque;
	}

	public TramiteEmbarqueRegistroBl getTramiteEmbarqueRegistroBl() {
		return this.TramiteEmbarqueRegistroBl;
	}

	public void setTramiteEmbarqueRegistroBl(TramiteEmbarqueRegistroBl TramiteEmbarqueRegistroBl) {
		this.TramiteEmbarqueRegistroBl = TramiteEmbarqueRegistroBl;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	public BigDecimal getVolumenPuertoArena() {
		return volumenPuertoArena;
	}

	public void setVolumenPuertoArena(BigDecimal volumenPuertoArena) {
		this.volumenPuertoArena = volumenPuertoArena;
	}

	public BigDecimal getPrecioUno() {
		return precioUno;
	}

	public void setPrecioUno(BigDecimal precioUno) {
		this.precioUno = precioUno;
	}
	
	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
}