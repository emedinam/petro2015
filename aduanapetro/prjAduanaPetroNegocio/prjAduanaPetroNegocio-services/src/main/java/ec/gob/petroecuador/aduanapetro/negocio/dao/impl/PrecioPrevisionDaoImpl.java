
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.PrecioPrevisionDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PrecioPrevision;

/**
 Clase PrecioPrevisionDao
 @autor dgonzalez
*/
@Component("precioprevisionDao")
public class PrecioPrevisionDaoImpl extends GenericDAOImpl<PrecioPrevision, Long> implements PrecioPrevisionDao {
	
}
   