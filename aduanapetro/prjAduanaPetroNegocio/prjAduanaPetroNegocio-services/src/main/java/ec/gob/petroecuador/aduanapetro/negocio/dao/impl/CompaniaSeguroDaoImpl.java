
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.CompaniaSeguroDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.CompaniaSeguro;

/**
 Clase CompaniaSeguroDao
 @autor dgonzalez
*/
@Component("companiaseguroDao")
public class CompaniaSeguroDaoImpl extends GenericDAOImpl<CompaniaSeguro, Long> implements CompaniaSeguroDao {
	
}
   