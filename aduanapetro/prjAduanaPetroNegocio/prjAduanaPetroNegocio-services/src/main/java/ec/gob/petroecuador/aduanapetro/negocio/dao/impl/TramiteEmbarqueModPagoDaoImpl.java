
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TramiteEmbarqueModPagoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueModPago;

/**
 Clase TramiteEmbarqueModPagoDao
 @autor dgonzalez
*/
@Component("tramiteembarquemodpagoDao")
public class TramiteEmbarqueModPagoDaoImpl extends GenericDAOImpl<TramiteEmbarqueModPago, Long> implements TramiteEmbarqueModPagoDao {
	
}
   