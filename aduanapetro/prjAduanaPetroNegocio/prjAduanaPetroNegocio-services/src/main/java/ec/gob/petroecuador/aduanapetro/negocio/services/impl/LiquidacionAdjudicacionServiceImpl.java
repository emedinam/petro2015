
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.LiquidacionAdjudicacionDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.LiquidacionAdjudicacion;
import ec.gob.petroecuador.aduanapetro.negocio.services.LiquidacionAdjudicacionService;

/**
 Clase LiquidacionAdjudicacionServiceImpl
 @autor dgonzalez
*/
@Service
public class LiquidacionAdjudicacionServiceImpl extends GenericServiceImpl<LiquidacionAdjudicacion, Long> implements LiquidacionAdjudicacionService {

	@Autowired
	LiquidacionAdjudicacionDao dao;
	
	@Override
	public GenericDAO<LiquidacionAdjudicacion, Long> getDao() {
		return dao;
	}

}
   