package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import javax.persistence.*;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

import java.util.Date;
import java.util.List;

/**
 * The persistent class for the T_COMPANIA_SEGURO database table.
 * 
 */
@Entity
@Table(name = "NG_COMPANIA_SEGURO", schema= "APS_NEGOCIO")
public class CompaniaSeguro extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	private String nombre;

	// Variables de control de eliminacion y log
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_ACTUALIZACION")
	private Date fechaActualiza;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION")
	private Date fechaCrea;

	@Column(name = "USUARIO_ACTUALIZACION")
	private String usuarioActualiza;

	@Column(name = "USUARIO_CREACION")
	private String usuarioCrea;

	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	// bi-directional many-to-one association to AplicacionSeguro
	@OneToMany(mappedBy = "CompaniaSeguro")
	private List<AplicacionSeguro> AplicacionSeguros;

	public CompaniaSeguro() {
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<AplicacionSeguro> getAplicacionSeguros() {
		return this.AplicacionSeguros;
	}

	public void setAplicacionSeguros(List<AplicacionSeguro> AplicacionSeguros) {
		this.AplicacionSeguros = AplicacionSeguros;
	}

	public AplicacionSeguro addAplicacionSeguro(
			AplicacionSeguro AplicacionSeguro) {
		getAplicacionSeguros().add(AplicacionSeguro);
		AplicacionSeguro.setCompaniaSeguro(this);

		return AplicacionSeguro;
	}

	public AplicacionSeguro removeAplicacionSeguro(
			AplicacionSeguro AplicacionSeguro) {
		getAplicacionSeguros().remove(AplicacionSeguro);
		AplicacionSeguro.setCompaniaSeguro(null);

		return AplicacionSeguro;
	}

	public Date getFechaActualiza() {
		return fechaActualiza;
	}

	public void setFechaActualiza(Date fechaActualiza) {
		this.fechaActualiza = fechaActualiza;
	}

	public Date getFechaCrea() {
		return fechaCrea;
	}

	public void setFechaCrea(Date fechaCrea) {
		this.fechaCrea = fechaCrea;
	}

	public String getUsuarioActualiza() {
		return usuarioActualiza;
	}

	public void setUsuarioActualiza(String usuarioActualiza) {
		this.usuarioActualiza = usuarioActualiza;
	}

	public String getUsuarioCrea() {
		return usuarioCrea;
	}

	public void setUsuarioCrea(String usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}

	// metodos para combos
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof CompaniaSeguro))
			return false;
		return ((CompaniaSeguro) obj).getId().equals(
				this.id);
	}

	@Override
	public String toString() {
		return nombre;
	}

	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

}