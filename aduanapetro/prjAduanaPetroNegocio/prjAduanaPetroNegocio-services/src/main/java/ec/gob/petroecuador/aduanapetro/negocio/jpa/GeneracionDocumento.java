package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import javax.persistence.*;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the T_GENERACION_DOCUMENTO database table.
 * 
 */
@Entity
@Table(name = "NG_GENERACION_DOCUMENTO", schema = "APS_NEGOCIO")
public class GeneracionDocumento extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	private String asunto;

	@Column(name="CARGO_FUNCIONARIO_EXTERNO")
	private String cargoFuncionarioExterno;

	@Column(name="DESTINATARIO_EXTERNO")
	private String destinatarioExterno;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_INGRESO")
	private Date fechaIngreso;

	@Column(name="FUNCIONARIO_EXTERNO")
	private String funcionarioExterno;

	@Column(name="NUMERO_DOCUMENTO")
	private String numeroDocumento;

	@Column(name="NUMERO_TRAMITE")
	private String numeroTramite;

	private String rol;

	@Column(name="RUTA_DOCUMENTO")
	private String rutaDocumento;

	@Column(name="RUTA_FILENET")
	private String rutaFilenet;

	private BigDecimal urgente;

	//bi-directional many-to-one association to CategoriaDocumento
	@ManyToOne
	@JoinColumn(name="IF_CATEGORIA_DOCUMENTO")
	private CategoriaDocumento CategoriaDocumento;

	//bi-directional many-to-one association to TramiteEmbarqueDesaduanamiento
	@ManyToOne
	@JoinColumn(name="IF_TRAMITE_EMBARQUE_DESAD")
	private TramiteEmbarqueDesaduanamiento TramiteEmbarqueDesaduanamiento;

	//bi-directional many-to-one association to TramiteEmbarque
	@ManyToOne
	@JoinColumn(name="IF_TRAMITE_EMBARQUE")
	private TramiteEmbarque TramiteEmbarque;

	//bi-directional many-to-one association to TramiteEmbarqueDocDigital
	@ManyToOne
	@JoinColumn(name="IF_TRAMITE_EMBARQUE_DOC_DIG")
	private TramiteEmbarqueDocDigital TramiteEmbarqueDocDigital;

	//bi-directional many-to-one association to TramiteEmbarqueFactura
	@ManyToOne
	@JoinColumn(name="IF_TRAMITE_EMBARQUE_FACTURA")
	private TramiteEmbarqueFactura TramiteEmbarqueFactura;

	//bi-directional many-to-one association to TramiteEmbarqueModPago
	@ManyToOne
	@JoinColumn(name="IF_TRAMITE_EMB_MOD_PAGO_BCE")
	private TramiteEmbarqueModPago TramiteEmbarqueModPago1;

	//bi-directional many-to-one association to TramiteEmbarqueModPago
	@ManyToOne
	@JoinColumn(name="IF_TRAMITE_EMB_MOD_PAGO_MEF")
	private TramiteEmbarqueModPago TramiteEmbarqueModPago2;

	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	
	public GeneracionDocumento() {
	}

	public String getAsunto() {
		return this.asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getCargoFuncionarioExterno() {
		return this.cargoFuncionarioExterno;
	}

	public void setCargoFuncionarioExterno(String cargoFuncionarioExterno) {
		this.cargoFuncionarioExterno = cargoFuncionarioExterno;
	}

	public String getDestinatarioExterno() {
		return this.destinatarioExterno;
	}

	public void setDestinatarioExterno(String destinatarioExterno) {
		this.destinatarioExterno = destinatarioExterno;
	}

	public Date getFechaIngreso() {
		return this.fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getFuncionarioExterno() {
		return this.funcionarioExterno;
	}

	public void setFuncionarioExterno(String funcionarioExterno) {
		this.funcionarioExterno = funcionarioExterno;
	}

	public String getNumeroDocumento() {
		return this.numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getNumeroTramite() {
		return this.numeroTramite;
	}

	public void setNumeroTramite(String numeroTramite) {
		this.numeroTramite = numeroTramite;
	}

	public String getRol() {
		return this.rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getRutaDocumento() {
		return this.rutaDocumento;
	}

	public void setRutaDocumento(String rutaDocumento) {
		this.rutaDocumento = rutaDocumento;
	}

	public String getRutaFilenet() {
		return this.rutaFilenet;
	}

	public void setRutaFilenet(String rutaFilenet) {
		this.rutaFilenet = rutaFilenet;
	}

	public BigDecimal getUrgente() {
		return this.urgente;
	}

	public void setUrgente(BigDecimal urgente) {
		this.urgente = urgente;
	}

	public CategoriaDocumento getCategoriaDocumento() {
		return this.CategoriaDocumento;
	}

	public void setCategoriaDocumento(CategoriaDocumento CategoriaDocumento) {
		this.CategoriaDocumento = CategoriaDocumento;
	}

	public TramiteEmbarqueDesaduanamiento getTramiteEmbarqueDesaduanamiento() {
		return this.TramiteEmbarqueDesaduanamiento;
	}

	public void setTramiteEmbarqueDesaduanamiento(TramiteEmbarqueDesaduanamiento TramiteEmbarqueDesaduanamiento) {
		this.TramiteEmbarqueDesaduanamiento = TramiteEmbarqueDesaduanamiento;
	}

	public TramiteEmbarque getTramiteEmbarque() {
		return this.TramiteEmbarque;
	}

	public void setTramiteEmbarque(TramiteEmbarque TramiteEmbarque) {
		this.TramiteEmbarque = TramiteEmbarque;
	}

	public TramiteEmbarqueDocDigital getTramiteEmbarqueDocDigital() {
		return this.TramiteEmbarqueDocDigital;
	}

	public void setTramiteEmbarqueDocDigital(TramiteEmbarqueDocDigital TramiteEmbarqueDocDigital) {
		this.TramiteEmbarqueDocDigital = TramiteEmbarqueDocDigital;
	}

	public TramiteEmbarqueFactura getTramiteEmbarqueFactura() {
		return this.TramiteEmbarqueFactura;
	}

	public void setTramiteEmbarqueFactura(TramiteEmbarqueFactura TramiteEmbarqueFactura) {
		this.TramiteEmbarqueFactura = TramiteEmbarqueFactura;
	}

	public TramiteEmbarqueModPago getTramiteEmbarqueModPago1() {
		return this.TramiteEmbarqueModPago1;
	}

	public void setTramiteEmbarqueModPago1(TramiteEmbarqueModPago TramiteEmbarqueModPago1) {
		this.TramiteEmbarqueModPago1 = TramiteEmbarqueModPago1;
	}

	public TramiteEmbarqueModPago getTramiteEmbarqueModPago2() {
		return this.TramiteEmbarqueModPago2;
	}

	public void setTramiteEmbarqueModPago2(TramiteEmbarqueModPago TramiteEmbarqueModPago2) {
		this.TramiteEmbarqueModPago2 = TramiteEmbarqueModPago2;
	}

	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
}