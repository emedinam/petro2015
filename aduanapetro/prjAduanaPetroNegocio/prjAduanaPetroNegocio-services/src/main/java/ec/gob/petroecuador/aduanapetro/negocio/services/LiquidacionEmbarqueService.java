
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.LiquidacionEmbarque;

/**
 Clase LiquidacionEmbarqueService
 @autor dgonzalez
*/
public interface LiquidacionEmbarqueService extends GenericService<LiquidacionEmbarque, Long> {
	
}
   