
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.EstadoTramiteEmbarqueDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.EstadoTramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.services.EstadoTramiteEmbarqueService;

/**
 Clase EstadoTramiteEmbarqueServiceImpl
 @autor dgonzalez
*/
@Service
public class EstadoTramiteEmbarqueServiceImpl extends GenericServiceImpl<EstadoTramiteEmbarque, Long> implements EstadoTramiteEmbarqueService {

	@Autowired
	EstadoTramiteEmbarqueDao dao;
	
	@Override
	public GenericDAO<EstadoTramiteEmbarque, Long> getDao() {
		return dao;
	}

}
   