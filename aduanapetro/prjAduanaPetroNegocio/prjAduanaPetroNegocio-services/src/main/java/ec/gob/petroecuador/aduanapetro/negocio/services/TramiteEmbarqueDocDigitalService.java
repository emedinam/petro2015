
package ec.gob.petroecuador.aduanapetro.negocio.services;

import java.util.List;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarque;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueDocDigital;

/**
 Clase TramiteEmbarqueDocDigitalService
 @autor dgonzalez
*/
public interface TramiteEmbarqueDocDigitalService extends GenericService<TramiteEmbarqueDocDigital, Long> {
	public List<TramiteEmbarqueDocDigital> obtenerPorPadre(TramiteEmbarque tramiteEmbarque);
}
   