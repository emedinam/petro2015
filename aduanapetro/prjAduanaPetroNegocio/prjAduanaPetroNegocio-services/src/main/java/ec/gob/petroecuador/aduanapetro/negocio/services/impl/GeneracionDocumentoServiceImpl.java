
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.GeneracionDocumentoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.GeneracionDocumento;
import ec.gob.petroecuador.aduanapetro.negocio.services.GeneracionDocumentoService;

/**
 Clase GeneracionDocumentoServiceImpl
 @autor dgonzalez
*/
@Service
public class GeneracionDocumentoServiceImpl extends GenericServiceImpl<GeneracionDocumento, Long> implements GeneracionDocumentoService {

	@Autowired
	GeneracionDocumentoDao dao;
	
	@Override
	public GenericDAO<GeneracionDocumento, Long> getDao() {
		return dao;
	}

}
   