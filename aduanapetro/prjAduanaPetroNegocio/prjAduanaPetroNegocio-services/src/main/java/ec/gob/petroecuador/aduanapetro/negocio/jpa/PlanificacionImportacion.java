package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

/**
 * The persistent class for the T_PLANIFICACION_IMPORTACION database table.
 * 
 */
@NamedQueries({
	@NamedQuery(
		name = "obtenerPorPadrePI",
		query = "select p from PlanificacionImportacion p where p.estado='ACTIVO' and p.Adjudicacion.id=:adjudicacion"
	),
})
@Entity
@Table(name = "NG_PLANIFICACION_IMPORTACION", schema = "APS_NEGOCIO")
public class PlanificacionImportacion extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	private BigDecimal anio;

	@Column(name = "EMPRESA_PROVEEDORA")
	private String empresaProveedora;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_REGISTRO")
	private Date fechaRegistro;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_ACTUALIZACION")
	private Date fechaActualiza;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION")
	private Date fechaCrea;

	@Column(name = "USUARIO_ACTUALIZACION")
	private String usuarioActualiza;

	@Column(name = "USUARIO_CREACION")
	private String usuarioCrea;

	private String mes;

	@Column(name = "MOTIVO_CAMBIO")
	private String motivoCambio;

	private String observacion;

	private BigDecimal precio;

	private String semana;

	@Column(name = "TIPO_PAGO")
	private String tipoPago;

	private BigDecimal total;

	@Temporal(TemporalType.DATE)
	@Column(name = "VENTANA_FIN")
	private Date ventanaFin;

	@Temporal(TemporalType.DATE)
	@Column(name = "VENTANA_INICIO")
	private Date ventanaInicio;

	@Column(name = "VOLUMEN_BLS")
	private BigDecimal volumenBls;

	@Column(name = "VOLUMEN_TNL")
	private BigDecimal volumenTnl;

	// bi-directional many-to-one association to Adjudicacion
	@ManyToOne
	@JoinColumn(name = "IF_ADJUDICACION")
	private Adjudicacion Adjudicacion;

	// bi-directional many-to-one association to Actor
	@ManyToOne
	@JoinColumn(name = "IF_ACTOR")
	private Actor Actor;

	// bi-directional many-to-one association to TramiteEmbarque
	@OneToMany(mappedBy = "PlanificacionImportacion")
	private List<TramiteEmbarque> TramiteEmbarques;

	public Actor getActor() {
		return Actor;
	}

	public void setActor(Actor actor) {
		Actor = actor;
	}

	// bi-directional many-to-one association to AplicacionSeguro
	@OneToMany(mappedBy = "PlanificacionImportacion")
	private List<AplicacionSeguro> AplicacionSeguros;

	public PlanificacionImportacion() {
	}
	
	public BigDecimal getAnio() {
		return this.anio;
	}

	public void setAnio(BigDecimal anio) {
		this.anio = anio;
	}

	public String getEmpresaProveedora() {
		return this.empresaProveedora;
	}

	public void setEmpresaProveedora(String empresaProveedora) {
		this.empresaProveedora = empresaProveedora;
	}

	public Date getFechaRegistro() {
		return this.fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getMes() {
		return this.mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public String getMotivoCambio() {
		return this.motivoCambio;
	}

	public void setMotivoCambio(String motivoCambio) {
		this.motivoCambio = motivoCambio;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public String getSemana() {
		return this.semana;
	}

	public void setSemana(String semana) {
		this.semana = semana;
	}

	public String getTipoPago() {
		return this.tipoPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Date getVentanaFin() {
		return this.ventanaFin;
	}

	public void setVentanaFin(Date ventanaFin) {
		this.ventanaFin = ventanaFin;
	}

	public Date getVentanaInicio() {
		return this.ventanaInicio;
	}

	public void setVentanaInicio(Date ventanaInicio) {
		this.ventanaInicio = ventanaInicio;
	}

	public BigDecimal getVolumenBls() {
		return this.volumenBls;
	}

	public void setVolumenBls(BigDecimal volumenBls) {
		this.volumenBls = volumenBls;
	}

	public BigDecimal getVolumenTnl() {
		return this.volumenTnl;
	}

	public void setVolumenTnl(BigDecimal volumenTnl) {
		this.volumenTnl = volumenTnl;
	}

	public Adjudicacion getAdjudicacion() {
		return this.Adjudicacion;
	}

	public void setAdjudicacion(Adjudicacion Adjudicacion) {
		this.Adjudicacion = Adjudicacion;
	}

	public List<AplicacionSeguro> getAplicacionSeguros() {
		return this.AplicacionSeguros;
	}

	public void setAplicacionSeguros(List<AplicacionSeguro> AplicacionSeguros) {
		this.AplicacionSeguros = AplicacionSeguros;
	}

	public AplicacionSeguro addAplicacionSeguro(
			AplicacionSeguro AplicacionSeguro) {
		getAplicacionSeguros().add(AplicacionSeguro);
		AplicacionSeguro.setPlanificacionImportacion(this);

		return AplicacionSeguro;
	}

	public AplicacionSeguro removeAplicacionSeguro(
			AplicacionSeguro AplicacionSeguro) {
		getAplicacionSeguros().remove(AplicacionSeguro);
		AplicacionSeguro.setPlanificacionImportacion(null);

		return AplicacionSeguro;
	}

	public Date getFechaActualiza() {
		return fechaActualiza;
	}

	public void setFechaActualiza(Date fechaActualiza) {
		this.fechaActualiza = fechaActualiza;
	}

	public Date getFechaCrea() {
		return fechaCrea;
	}

	public void setFechaCrea(Date fechaCrea) {
		this.fechaCrea = fechaCrea;
	}

	public String getUsuarioActualiza() {
		return usuarioActualiza;
	}

	public void setUsuarioActualiza(String usuarioActualiza) {
		this.usuarioActualiza = usuarioActualiza;
	}

	public String getUsuarioCrea() {
		return usuarioCrea;
	}

	public void setUsuarioCrea(String usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}

	// metodos para combos

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof PlanificacionImportacion))
			return false;
		return ((PlanificacionImportacion) obj).getId()
				.equals(this.id);
	}

	@Override
	public String toString() {
		return ventanaInicio.toString()+" "+ventanaFin.toString();
	}

	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public List<TramiteEmbarque> getTramiteEmbarques() {
		return TramiteEmbarques;
	}

	public void setTramiteEmbarques(List<TramiteEmbarque> tramiteEmbarques) {
		TramiteEmbarques = tramiteEmbarques;
	}
}