
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.PlanificacionImportacionDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Adjudicacion;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionImportacion;
import ec.gob.petroecuador.aduanapetro.negocio.services.PlanificacionImportacionService;

/**
 Clase PlanificacionImportacionServiceImpl
 @autor dgonzalez
*/
@Service
public class PlanificacionImportacionServiceImpl extends GenericServiceImpl<PlanificacionImportacion, Long> implements PlanificacionImportacionService {

	@Autowired
	PlanificacionImportacionDao dao;
	
	@Override
	public GenericDAO<PlanificacionImportacion, Long> getDao() {
		return dao;
	}

	@Override
	public List<PlanificacionImportacion> obtenerPorPadre(
			Adjudicacion adjudicacion) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("adjudicacion", adjudicacion.getId());
		return dao.findByNamedQuery("obtenerPorPadrePI", map);
	}

}
   