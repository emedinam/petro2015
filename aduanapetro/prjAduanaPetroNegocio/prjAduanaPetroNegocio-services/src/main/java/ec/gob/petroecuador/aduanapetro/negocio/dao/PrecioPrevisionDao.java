

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.PrecioPrevision;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase PrecioPrevisionDao
 @autor dgonzalez
*/
public interface PrecioPrevisionDao extends GenericDAO<PrecioPrevision, Long> {
	
}

   