
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.PuertoDescargaDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PuertoDescarga;
import ec.gob.petroecuador.aduanapetro.negocio.services.PuertoDescargaService;

/**
 Clase PuertoDescargaServiceImpl
 @autor dgonzalez
*/
@Service
public class PuertoDescargaServiceImpl extends GenericServiceImpl<PuertoDescarga, Long> implements PuertoDescargaService {

	@Autowired
	PuertoDescargaDao dao;
	
	@Override
	public GenericDAO<PuertoDescarga, Long> getDao() {
		return dao;
	}

}
   