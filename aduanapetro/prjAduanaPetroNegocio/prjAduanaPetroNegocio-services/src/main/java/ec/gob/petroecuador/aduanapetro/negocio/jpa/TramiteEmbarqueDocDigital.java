package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;


/**
 * The persistent class for the T_TRAMITE_EMBARQUE_DOC_DIGITAL database table.
 * 
 */
@NamedQueries({
	@NamedQuery(
		name = "obtenerPorPadreTEDD",
		query = "select p from TramiteEmbarqueDocDigital p where p.estado='ACTIVO' and p.TramiteEmbarque.id=:tramiteEmbarque"
	),
})
@Entity
@Table(name = "T_TRAMITE_EMBARQUE_DOC_DIGITAL", schema = "APS_NEGOCIO")
public class TramiteEmbarqueDocDigital extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	private BigDecimal cargado;

	private BigDecimal enviado;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ENVIO")
	private Date fechaEnvio;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_RECEPCION")
	private Date fechaRecepcion;

	private String numero;

	private String observacion;

	private BigDecimal receptado;

	private String ruta;
	
	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;
	
	@Column(name="USUARIO_ACTUALIZACION")
	private String usuarioActualizacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ACTUALIZACION")
	private Date fechaActualizacion;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	//bi-directional many-to-one association to GeneracionDocumento
	@OneToMany(mappedBy="TramiteEmbarqueDocDigital")
	private List<GeneracionDocumento> GeneracionDocumentos;

	//bi-directional many-to-one association to TramiteEmbarque
	@ManyToOne
	@JoinColumn(name="IF_TRAMITE_EMBARQUE")
	private TramiteEmbarque TramiteEmbarque;
	
	//bi-directional many-to-one association to TramiteEmbarque
	@ManyToOne
	@JoinColumn(name="IF_TIPO_DOCUMENTO")
	private TipoDocumento TipoDocumento;

	public TramiteEmbarqueDocDigital() {
	}

	public BigDecimal getCargado() {
		return this.cargado;
	}

	public void setCargado(BigDecimal cargado) {
		this.cargado = cargado;
	}

	public BigDecimal getEnviado() {
		return this.enviado;
	}

	public void setEnviado(BigDecimal enviado) {
		this.enviado = enviado;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Date getFechaEnvio() {
		return this.fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public Date getFechaRecepcion() {
		return this.fechaRecepcion;
	}

	public void setFechaRecepcion(Date fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}

	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public BigDecimal getReceptado() {
		return this.receptado;
	}

	public void setReceptado(BigDecimal receptado) {
		this.receptado = receptado;
	}

	public String getRuta() {
		return this.ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public List<GeneracionDocumento> getGeneracionDocumentos() {
		return this.GeneracionDocumentos;
	}

	public void setGeneracionDocumentos(List<GeneracionDocumento> GeneracionDocumentos) {
		this.GeneracionDocumentos = GeneracionDocumentos;
	}

	public GeneracionDocumento addGeneracionDocumento(GeneracionDocumento GeneracionDocumento) {
		getGeneracionDocumentos().add(GeneracionDocumento);
		GeneracionDocumento.setTramiteEmbarqueDocDigital(this);

		return GeneracionDocumento;
	}

	public GeneracionDocumento removeGeneracionDocumento(GeneracionDocumento GeneracionDocumento) {
		getGeneracionDocumentos().remove(GeneracionDocumento);
		GeneracionDocumento.setTramiteEmbarqueDocDigital(null);

		return GeneracionDocumento;
	}

	public TramiteEmbarque getTramiteEmbarque() {
		return this.TramiteEmbarque;
	}

	public void setTramiteEmbarque(TramiteEmbarque TramiteEmbarque) {
		this.TramiteEmbarque = TramiteEmbarque;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	
	public TipoDocumento getTipoDocumento() {
		return TipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		TipoDocumento = tipoDocumento;
	}

	
	//metodos para combos
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof TramiteEmbarqueDocDigital))
			return false;
		return ((TramiteEmbarqueDocDigital) obj).getId().equals(this.id);
	}


	@Override
	public String toString() {
		return numero;
	}
	
	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

}