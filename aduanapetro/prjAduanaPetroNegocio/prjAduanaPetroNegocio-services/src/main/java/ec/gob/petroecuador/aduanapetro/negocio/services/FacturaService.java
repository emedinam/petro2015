
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Factura;

/**
 Clase FacturaService
 @autor dgonzalez
*/
public interface FacturaService extends GenericService<Factura, Long> {
	
}
   