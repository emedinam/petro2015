package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;

/**
 * The persistent class for the T_ACTOR database table.
 * 
 */
@Entity
@Table(name = "NG_ACTOR", schema= "APS_NEGOCIO")
public class Actor extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "CEDULA_RUC_PAS")
	private String cedulaRucPas;

	private String contacto;

	private String direccion;

	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_ACTUALIZACION")
	private Date fechaActualizacion;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;

	private String observacion;

	@Column(name = "RAZON_SOCIAL")
	private String razonSocial;

	private String telefono;

	@Column(name = "TIPO_ACTOR")
	private String tipoActor;

	@Column(name = "USUARIO_ACTUALIZACION")
	private String usuarioActualizacion;

	@Column(name = "USUARIO_CREACION")
	private String usuarioCreacion;

	// bi-directional many-to-one association to Garantia
	@OneToMany(mappedBy = "Actor")
	private List<Garantia> TGarantias;

	// bi-directional many-to-one association to Garantia
	@OneToMany(mappedBy = "Actor")
	private List<PlanificacionImportacion> TPlanificacionesImportaciones;

	// bi-directional many-to-one association to Garantia
	@OneToMany(mappedBy = "Actor")
	private List<Adjudicacion> TAdjudicaciones;

	public List<Adjudicacion> getTAdjudicaciones() {
		return TAdjudicaciones;
	}

	public void setTAdjudicaciones(List<Adjudicacion> tAdjudicaciones) {
		TAdjudicaciones = tAdjudicaciones;
	}

	public List<PlanificacionImportacion> getTPlanificacionesImportaciones() {
		return TPlanificacionesImportaciones;
	}

	public void setTPlanificacionesImportaciones(
			List<PlanificacionImportacion> tPlanificacionesImportaciones) {
		TPlanificacionesImportaciones = tPlanificacionesImportaciones;
	}

	public Actor() {
	}

	public String getCedulaRucPas() {
		return this.cedulaRucPas;
	}

	public void setCedulaRucPas(String cedulaRucPas) {
		this.cedulaRucPas = cedulaRucPas;
	}

	public String getContacto() {
		return this.contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public Date getFechaActualizacion() {
		return this.fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getRazonSocial() {
		return this.razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTipoActor() {
		return this.tipoActor;
	}

	public void setTipoActor(String tipoActor) {
		this.tipoActor = tipoActor;
	}

	public String getUsuarioActualizacion() {
		return this.usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public String getUsuarioCreacion() {
		return this.usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public List<Garantia> getTGarantias() {
		return this.TGarantias;
	}

	public void setTGarantias(List<Garantia> TGarantias) {
		this.TGarantias = TGarantias;
	}

	public Garantia addTGarantia(Garantia TGarantia) {
		getTGarantias().add(TGarantia);
		TGarantia.setActor(this);

		return TGarantia;
	}

	public Garantia removeTGarantia(Garantia TGarantia) {
		getTGarantias().remove(TGarantia);
		TGarantia.setActor(null);

		return TGarantia;
	}

	// metodos para combos

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof Actor))
			return false;
		return ((Actor) obj).getId().equals(this.id);
	}

	@Override
	public String toString() {
		return razonSocial;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;	
	}

	@Override
	public Estados getEstado() {
		return estado;
	}

}