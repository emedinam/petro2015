
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.TramiteEmbarqueCargaDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueCarga;

/**
 Clase TramiteEmbarqueCargaDao
 @autor dgonzalez
*/
@Component("tramiteembarquecargaDao")
public class TramiteEmbarqueCargaDaoImpl extends GenericDAOImpl<TramiteEmbarqueCarga, Long> implements TramiteEmbarqueCargaDao {
	
}
   