
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.ParametroCalculo;

/**
 Clase ParametroCalculoService
 @autor dgonzalez
*/
public interface ParametroCalculoService extends GenericService<ParametroCalculo, Long> {
	
}
   