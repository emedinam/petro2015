
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PrecioMarcador;

/**
 Clase PrecioMarcadorService
 @autor dgonzalez
*/
public interface PrecioMarcadorService extends GenericService<PrecioMarcador, Long> {
	
}
   