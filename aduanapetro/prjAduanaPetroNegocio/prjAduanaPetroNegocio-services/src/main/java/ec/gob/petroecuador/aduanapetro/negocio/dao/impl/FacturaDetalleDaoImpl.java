
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.FacturaDetalleDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.FacturaDetalle;

/**
 Clase FacturaDetalleDao
 @autor dgonzalez
*/
@Component("facturadetalleDao")
public class FacturaDetalleDaoImpl extends GenericDAOImpl<FacturaDetalle, Long> implements FacturaDetalleDao {
	
}
   