

package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.TramiteEmbarqueRegistroBl;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

/**
 Clase TramiteEmbarqueRegistroBlDao
 @autor dgonzalez
*/
public interface TramiteEmbarqueRegistroBlDao extends GenericDAO<TramiteEmbarqueRegistroBl, Long> {
	
}

   