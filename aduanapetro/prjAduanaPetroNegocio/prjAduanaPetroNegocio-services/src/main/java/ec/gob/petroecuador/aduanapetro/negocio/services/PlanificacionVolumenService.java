
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.PlanificacionVolumen;

/**
 Clase PlanificacionVolumenService
 @autor dgonzalez
*/
public interface PlanificacionVolumenService extends GenericService<PlanificacionVolumen, Long> {
	
}
   