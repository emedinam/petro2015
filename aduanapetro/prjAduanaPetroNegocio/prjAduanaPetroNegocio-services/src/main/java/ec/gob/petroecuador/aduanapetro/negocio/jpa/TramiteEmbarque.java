package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;


/**
 * The persistent class for the T_TRAMITE_EMBARQUE database table.
 * 
 */
@NamedQueries({
	@NamedQuery(
		name = "obtenerPorTipoTE",
		query = "select p from TramiteEmbarque p where p.estado='ACTIVO' and p.tipo=:tipo"
	),
})
@Entity
@Table(name = "NG_TRAMITE_EMBARQUE", schema = "APS_NEGOCIO")
public class TramiteEmbarque extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ADJUDICACION")
	private Date fechaAdjudicacion;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_GENERACION")
	private Date fechaGeneracion;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_INGRESO")
	private Date fechaIngreso;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_PROFORMA")
	private Date fechaProforma;

	private BigDecimal generado;

	private String negociacion;

	@Column(name="NOMBRE_BUQUE")
	private String nombreBuque;

	private String numero;

	@Column(name="NUMERO_MEMORANDO")
	private String numeroMemorando;

	@Column(name="NUMERO_PROFORMA")
	private String numeroProforma;

	@Column(name="PUERTO_EMBARQUE")
	private String puertoEmbarque;

	private String tipo;

	@Column(name="VALOR_PROFORMA")
	private BigDecimal valorProforma;

	private BigDecimal volumen;
	
	@Column(name="OBSERVACION")
	private String observacion;
	
	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;
	
	@Column(name="USUARIO_ACTUALIZACION")
	private String usuarioActualizacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ACTUALIZACION")
	private Date fechaActualizacion;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;
	
	@Column(name="PRODUCTO")
	private String producto;
	
	@Column(name="ADCPUERTO_DESCARGA")
	private String adcPuertoDescarga;


	//bi-directional many-to-one association to GeneracionDocumento
	@OneToMany(mappedBy="TramiteEmbarque")
	private List<GeneracionDocumento> GeneracionDocumentos;

	//bi-directional many-to-one association to TramiteEmbarqueDesaduanamiento
	@OneToMany(mappedBy="TramiteEmbarque")
	private List<TramiteEmbarqueDesaduanamiento> TramiteEmbarqueDesaduanamientos;

	//bi-directional many-to-one association to Adjudicacion
	@ManyToOne
	@JoinColumn(name="IF_ADJUDICACION")
	private Adjudicacion Adjudicacion;

	//bi-directional many-to-one association to AplicacionSeguro
	@ManyToOne
	@JoinColumn(name="IF_APLICACION_SEGURO")
	private AplicacionSeguro AplicacionSeguro;

	//bi-directional many-to-one association to CompaniaInspectora
	@ManyToOne
	@JoinColumn(name="IF_COMPANIA_INSPECTORA")
	private CompaniaInspectora CompaniaInspectora;

	//bi-directional many-to-one association to EstadoTramiteEmbarque
	@ManyToOne
	@JoinColumn(name="IF_ESTADO_TRAMITE_EMBARQUE")
	private EstadoTramiteEmbarque EstadoTramiteEmbarque;

	//bi-directional many-to-one association to PuertoDescarga
	@ManyToOne
	@JoinColumn(name="IF_PUERTO_DESCARGA")
	private PuertoDescarga PuertoDescarga;
	
	//bi-directional many-to-one association to PlanificacionImportacion
	@ManyToOne
	@JoinColumn(name="IF_PLANIFICACION_IMPORTACION")
	private PlanificacionImportacion PlanificacionImportacion;
	
	//bi-directional many-to-one association to TramiteEmbarqueDescarga
	@OneToMany(mappedBy="TramiteEmbarque")
	private List<TramiteEmbarqueDescarga> TramiteEmbarqueDescargas;

	//bi-directional many-to-one association to TramiteEmbarqueDocDigital
	@OneToMany(mappedBy="TramiteEmbarque")
	private List<TramiteEmbarqueDocDigital> TramiteEmbarqueDocDigitals;

	//bi-directional many-to-one association to TramiteEmbarqueFactura
	@OneToMany(mappedBy="TramiteEmbarque")
	private List<TramiteEmbarqueFactura> TramiteEmbarqueFacturas;

	//bi-directional many-to-one association to TramiteEmbarqueModPago
	@OneToMany(mappedBy="TramiteEmbarque")
	private List<TramiteEmbarqueModPago> TramiteEmbarqueModPagos;

	//bi-directional many-to-one association to TramiteEmbarqueRegistroBl
	@OneToMany(mappedBy="TramiteEmbarque")
	private List<TramiteEmbarqueRegistroBl> TramiteEmbarqueRegistroBls;

	//bi-directional many-to-one association to TramiteEmbarqueCalculoPrecio
	@OneToMany(mappedBy="TramiteEmbarque")
	private List<TramiteEmbarqueCalculoPrecio> TramiteEmbarqueCalculoPrecios;

	public TramiteEmbarque() {
	}
	
	public Date getFechaAdjudicacion() {
		return this.fechaAdjudicacion;
	}

	public void setFechaAdjudicacion(Date fechaAdjudicacion) {
		this.fechaAdjudicacion = fechaAdjudicacion;
	}

	public Date getFechaGeneracion() {
		return this.fechaGeneracion;
	}

	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}

	public Date getFechaIngreso() {
		return this.fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Date getFechaProforma() {
		return this.fechaProforma;
	}

	public void setFechaProforma(Date fechaProforma) {
		this.fechaProforma = fechaProforma;
	}

	public BigDecimal getGenerado() {
		return this.generado;
	}

	public void setGenerado(BigDecimal generado) {
		this.generado = generado;
	}

	public String getNegociacion() {
		return this.negociacion;
	}

	public void setNegociacion(String negociacion) {
		this.negociacion = negociacion;
	}

	public String getNombreBuque() {
		return this.nombreBuque;
	}

	public void setNombreBuque(String nombreBuque) {
		this.nombreBuque = nombreBuque;
	}

	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNumeroMemorando() {
		return this.numeroMemorando;
	}

	public void setNumeroMemorando(String numeroMemorando) {
		this.numeroMemorando = numeroMemorando;
	}

	public String getNumeroProforma() {
		return this.numeroProforma;
	}

	public void setNumeroProforma(String numeroProforma) {
		this.numeroProforma = numeroProforma;
	}

	public String getPuertoEmbarque() {
		return this.puertoEmbarque;
	}

	public void setPuertoEmbarque(String puertoEmbarque) {
		this.puertoEmbarque = puertoEmbarque;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public BigDecimal getValorProforma() {
		return this.valorProforma;
	}

	public void setValorProforma(BigDecimal valorProforma) {
		this.valorProforma = valorProforma;
	}

	public BigDecimal getVolumen() {
		return this.volumen;
	}

	public void setVolumen(BigDecimal volumen) {
		this.volumen = volumen;
	}

	public List<GeneracionDocumento> getGeneracionDocumentos() {
		return this.GeneracionDocumentos;
	}

	public void setGeneracionDocumentos(List<GeneracionDocumento> GeneracionDocumentos) {
		this.GeneracionDocumentos = GeneracionDocumentos;
	}

	public GeneracionDocumento addGeneracionDocumento(GeneracionDocumento GeneracionDocumento) {
		getGeneracionDocumentos().add(GeneracionDocumento);
		GeneracionDocumento.setTramiteEmbarque(this);

		return GeneracionDocumento;
	}

	public GeneracionDocumento removeGeneracionDocumento(GeneracionDocumento GeneracionDocumento) {
		getGeneracionDocumentos().remove(GeneracionDocumento);
		GeneracionDocumento.setTramiteEmbarque(null);

		return GeneracionDocumento;
	}

	public List<TramiteEmbarqueDesaduanamiento> getTramiteEmbarqueDesaduanamientos() {
		return this.TramiteEmbarqueDesaduanamientos;
	}

	public void setTramiteEmbarqueDesaduanamientos(List<TramiteEmbarqueDesaduanamiento> TramiteEmbarqueDesaduanamientos) {
		this.TramiteEmbarqueDesaduanamientos = TramiteEmbarqueDesaduanamientos;
	}

	public TramiteEmbarqueDesaduanamiento addTramiteEmbarqueDesaduanamiento(TramiteEmbarqueDesaduanamiento TramiteEmbarqueDesaduanamiento) {
		getTramiteEmbarqueDesaduanamientos().add(TramiteEmbarqueDesaduanamiento);
		TramiteEmbarqueDesaduanamiento.setTramiteEmbarque(this);

		return TramiteEmbarqueDesaduanamiento;
	}

	public TramiteEmbarqueDesaduanamiento removeTramiteEmbarqueDesaduanamiento(TramiteEmbarqueDesaduanamiento TramiteEmbarqueDesaduanamiento) {
		getTramiteEmbarqueDesaduanamientos().remove(TramiteEmbarqueDesaduanamiento);
		TramiteEmbarqueDesaduanamiento.setTramiteEmbarque(null);

		return TramiteEmbarqueDesaduanamiento;
	}

	public Adjudicacion getAdjudicacion() {
		return this.Adjudicacion;
	}

	public void setAdjudicacion(Adjudicacion Adjudicacion) {
		this.Adjudicacion = Adjudicacion;
	}

	public AplicacionSeguro getAplicacionSeguro() {
		return this.AplicacionSeguro;
	}

	public void setAplicacionSeguro(AplicacionSeguro AplicacionSeguro) {
		this.AplicacionSeguro = AplicacionSeguro;
	}

	public CompaniaInspectora getCompaniaInspectora() {
		return this.CompaniaInspectora;
	}

	public void setCompaniaInspectora(CompaniaInspectora CompaniaInspectora) {
		this.CompaniaInspectora = CompaniaInspectora;
	}

	public EstadoTramiteEmbarque getEstadoTramiteEmbarque() {
		return this.EstadoTramiteEmbarque;
	}

	public void setEstadoTramiteEmbarque(EstadoTramiteEmbarque EstadoTramiteEmbarque) {
		this.EstadoTramiteEmbarque = EstadoTramiteEmbarque;
	}

	public PuertoDescarga getPuertoDescarga() {
		return this.PuertoDescarga;
	}

	public void setPuertoDescarga(PuertoDescarga PuertoDescarga) {
		this.PuertoDescarga = PuertoDescarga;
	}

	public List<TramiteEmbarqueDescarga> getTramiteEmbarqueDescargas() {
		return this.TramiteEmbarqueDescargas;
	}

	public void setTramiteEmbarqueDescargas(List<TramiteEmbarqueDescarga> TramiteEmbarqueDescargas) {
		this.TramiteEmbarqueDescargas = TramiteEmbarqueDescargas;
	}

	public TramiteEmbarqueDescarga addTramiteEmbarqueDescarga(TramiteEmbarqueDescarga TramiteEmbarqueDescarga) {
		getTramiteEmbarqueDescargas().add(TramiteEmbarqueDescarga);
		TramiteEmbarqueDescarga.setTramiteEmbarque(this);

		return TramiteEmbarqueDescarga;
	}

	public TramiteEmbarqueDescarga removeTramiteEmbarqueDescarga(TramiteEmbarqueDescarga TramiteEmbarqueDescarga) {
		getTramiteEmbarqueDescargas().remove(TramiteEmbarqueDescarga);
		TramiteEmbarqueDescarga.setTramiteEmbarque(null);

		return TramiteEmbarqueDescarga;
	}

	public List<TramiteEmbarqueDocDigital> getTramiteEmbarqueDocDigitals() {
		return this.TramiteEmbarqueDocDigitals;
	}

	public void setTramiteEmbarqueDocDigitals(List<TramiteEmbarqueDocDigital> TramiteEmbarqueDocDigitals) {
		this.TramiteEmbarqueDocDigitals = TramiteEmbarqueDocDigitals;
	}

	public TramiteEmbarqueDocDigital addTramiteEmbarqueDocDigital(TramiteEmbarqueDocDigital TramiteEmbarqueDocDigital) {
		getTramiteEmbarqueDocDigitals().add(TramiteEmbarqueDocDigital);
		TramiteEmbarqueDocDigital.setTramiteEmbarque(this);

		return TramiteEmbarqueDocDigital;
	}

	public TramiteEmbarqueDocDigital removeTramiteEmbarqueDocDigital(TramiteEmbarqueDocDigital TramiteEmbarqueDocDigital) {
		getTramiteEmbarqueDocDigitals().remove(TramiteEmbarqueDocDigital);
		TramiteEmbarqueDocDigital.setTramiteEmbarque(null);

		return TramiteEmbarqueDocDigital;
	}

	public List<TramiteEmbarqueFactura> getTramiteEmbarqueFacturas() {
		return this.TramiteEmbarqueFacturas;
	}

	public void setTramiteEmbarqueFacturas(List<TramiteEmbarqueFactura> TramiteEmbarqueFacturas) {
		this.TramiteEmbarqueFacturas = TramiteEmbarqueFacturas;
	}

	public TramiteEmbarqueFactura addTramiteEmbarqueFactura(TramiteEmbarqueFactura TramiteEmbarqueFactura) {
		getTramiteEmbarqueFacturas().add(TramiteEmbarqueFactura);
		TramiteEmbarqueFactura.setTramiteEmbarque(this);

		return TramiteEmbarqueFactura;
	}

	public TramiteEmbarqueFactura removeTramiteEmbarqueFactura(TramiteEmbarqueFactura TramiteEmbarqueFactura) {
		getTramiteEmbarqueFacturas().remove(TramiteEmbarqueFactura);
		TramiteEmbarqueFactura.setTramiteEmbarque(null);

		return TramiteEmbarqueFactura;
	}

	public List<TramiteEmbarqueModPago> getTramiteEmbarqueModPagos() {
		return this.TramiteEmbarqueModPagos;
	}

	public void setTramiteEmbarqueModPagos(List<TramiteEmbarqueModPago> TramiteEmbarqueModPagos) {
		this.TramiteEmbarqueModPagos = TramiteEmbarqueModPagos;
	}

	public TramiteEmbarqueModPago addTramiteEmbarqueModPago(TramiteEmbarqueModPago TramiteEmbarqueModPago) {
		getTramiteEmbarqueModPagos().add(TramiteEmbarqueModPago);
		TramiteEmbarqueModPago.setTramiteEmbarque(this);

		return TramiteEmbarqueModPago;
	}

	public TramiteEmbarqueModPago removeTramiteEmbarqueModPago(TramiteEmbarqueModPago TramiteEmbarqueModPago) {
		getTramiteEmbarqueModPagos().remove(TramiteEmbarqueModPago);
		TramiteEmbarqueModPago.setTramiteEmbarque(null);

		return TramiteEmbarqueModPago;
	}

	public List<TramiteEmbarqueRegistroBl> getTramiteEmbarqueRegistroBls() {
		return this.TramiteEmbarqueRegistroBls;
	}

	public void setTramiteEmbarqueRegistroBls(List<TramiteEmbarqueRegistroBl> TramiteEmbarqueRegistroBls) {
		this.TramiteEmbarqueRegistroBls = TramiteEmbarqueRegistroBls;
	}

	public TramiteEmbarqueRegistroBl addTramiteEmbarqueRegistroBl(TramiteEmbarqueRegistroBl TramiteEmbarqueRegistroBl) {
		getTramiteEmbarqueRegistroBls().add(TramiteEmbarqueRegistroBl);
		TramiteEmbarqueRegistroBl.setTramiteEmbarque(this);

		return TramiteEmbarqueRegistroBl;
	}

	public TramiteEmbarqueRegistroBl removeTramiteEmbarqueRegistroBl(TramiteEmbarqueRegistroBl TramiteEmbarqueRegistroBl) {
		getTramiteEmbarqueRegistroBls().remove(TramiteEmbarqueRegistroBl);
		TramiteEmbarqueRegistroBl.setTramiteEmbarque(null);

		return TramiteEmbarqueRegistroBl;
	}

	public List<TramiteEmbarqueCalculoPrecio> getTramiteEmbarqueCalculoPrecios() {
		return this.TramiteEmbarqueCalculoPrecios;
	}

	public void setTramiteEmbarqueCalculoPrecios(List<TramiteEmbarqueCalculoPrecio> TramiteEmbarqueCalculoPrecios) {
		this.TramiteEmbarqueCalculoPrecios = TramiteEmbarqueCalculoPrecios;
	}

	public TramiteEmbarqueCalculoPrecio addTramiteEmbarqueCalculoPrecio(TramiteEmbarqueCalculoPrecio TramiteEmbarqueCalculoPrecio) {
		getTramiteEmbarqueCalculoPrecios().add(TramiteEmbarqueCalculoPrecio);
		TramiteEmbarqueCalculoPrecio.setTramiteEmbarque(this);

		return TramiteEmbarqueCalculoPrecio;
	}

	public TramiteEmbarqueCalculoPrecio removeTramiteEmbarqueCalculoPrecio(TramiteEmbarqueCalculoPrecio TramiteEmbarqueCalculoPrecio) {
		getTramiteEmbarqueCalculoPrecios().remove(TramiteEmbarqueCalculoPrecio);
		TramiteEmbarqueCalculoPrecio.setTramiteEmbarque(null);

		return TramiteEmbarqueCalculoPrecio;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	
	public PlanificacionImportacion getPlanificacionImportacion() {
		return PlanificacionImportacion;
	}

	public void setPlanificacionImportacion(
			PlanificacionImportacion planificacionImportacion) {
		PlanificacionImportacion = planificacionImportacion;
	}
	
	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}
	
	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	
	public String getAdcPuertoDescarga() {
		return adcPuertoDescarga;
	}

	public void setAdcPuertoDescarga(String adcPuertoDescarga) {
		this.adcPuertoDescarga = adcPuertoDescarga;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof TramiteEmbarque))
			return false;
		return ((TramiteEmbarque) obj).getId().equals(this.id);
	}

	@Override
	public String toString() {
		return numero;
	}
	
	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
}