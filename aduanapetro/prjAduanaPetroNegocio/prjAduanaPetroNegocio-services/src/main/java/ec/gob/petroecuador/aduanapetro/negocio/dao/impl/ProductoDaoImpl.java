
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.ProductoDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Producto;

/**
 Clase ProductoDao
 @autor dgonzalez
*/
@Component("productoDao")
public class ProductoDaoImpl extends GenericDAOImpl<Producto, Long> implements ProductoDao {
	
}
   