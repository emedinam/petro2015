package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Catalogo;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;

public interface CatalogoService extends GenericService<Catalogo, Long> {
	
}
