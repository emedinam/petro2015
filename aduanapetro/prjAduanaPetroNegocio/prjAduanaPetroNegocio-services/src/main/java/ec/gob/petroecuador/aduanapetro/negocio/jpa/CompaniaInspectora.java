package ec.gob.petroecuador.aduanapetro.negocio.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.petroecuador.aduanapetro.seguridad.enums.Estados;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dbo.DatabaseObject;


/**
 * The persistent class for the T_COMPANIA_INSPECTORA database table.
 * 
 */
@Entity
@Table(name = "NG_COMPANIA_INSPECTORA", schema= "APS_NEGOCIO")
public class CompaniaInspectora extends DatabaseObject<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	private String nombre;
	
	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;
	
	@Column(name="USUARIO_ACTUALIZACION")
	private String usuarioActualizacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ACTUALIZACION")
	private Date fechaActualizacion;
	
	/**
	 * Estado
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="estado", length=8)
	private Estados estado;

	//bi-directional many-to-one association to TramiteEmbarque
	@OneToMany(mappedBy="CompaniaInspectora")
	private List<TramiteEmbarque> TramiteEmbarques;

	public CompaniaInspectora() {
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<TramiteEmbarque> getTramiteEmbarques() {
		return this.TramiteEmbarques;
	}

	public void setTramiteEmbarques(List<TramiteEmbarque> TramiteEmbarques) {
		this.TramiteEmbarques = TramiteEmbarques;
	}

	public TramiteEmbarque addTramiteEmbarque(TramiteEmbarque TramiteEmbarque) {
		getTramiteEmbarques().add(TramiteEmbarque);
		TramiteEmbarque.setCompaniaInspectora(this);

		return TramiteEmbarque;
	}

	public TramiteEmbarque removeTramiteEmbarque(TramiteEmbarque TramiteEmbarque) {
		getTramiteEmbarques().remove(TramiteEmbarque);
		TramiteEmbarque.setCompaniaInspectora(null);

		return TramiteEmbarque;
	}
	
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof CompaniaInspectora))
			return false;
		return ((CompaniaInspectora) obj).getId().equals(this.id);
	}

	@Override
	public String toString() {
		return nombre;
	}
	
	@Override
	public Estados getEstado() {
		return estado;
	}

	@Override
	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

}