
package ec.gob.petroecuador.aduanapetro.negocio.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.petroecuador.aduanapetro.negocio.dao.FacturaRetencionDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.impl.GenericServiceImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Factura;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.FacturaRetencion;
import ec.gob.petroecuador.aduanapetro.negocio.services.FacturaRetencionService;

/**
 Clase FacturaRetencionServiceImpl
 @autor dgonzalez
*/
@Service
public class FacturaRetencionServiceImpl extends GenericServiceImpl<FacturaRetencion, Long> implements FacturaRetencionService {

	@Autowired
	FacturaRetencionDao dao;
	
	@Override
	public GenericDAO<FacturaRetencion, Long> getDao() {
		return dao;
	}

	@Override
	public List<FacturaRetencion> obtenerPorPadre(Factura factura) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("factura", factura);
		return dao.findByNamedQuery("obtenerPorPadreFR", map);
	}

}
   