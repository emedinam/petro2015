package ec.gob.petroecuador.aduanapetro.negocio.dao;

import ec.gob.petroecuador.aduanapetro.negocio.jpa.Catalogo;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.GenericDAO;

public interface CatalogoDao extends GenericDAO<Catalogo, Long> {

}
