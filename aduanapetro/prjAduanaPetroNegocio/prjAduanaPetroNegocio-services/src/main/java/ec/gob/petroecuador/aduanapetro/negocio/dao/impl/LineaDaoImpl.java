
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.LineaDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Linea;

/**
 Clase LineaDao
 @autor dgonzalez
*/
@Component("lineaDao")
public class LineaDaoImpl extends GenericDAOImpl<Linea, Long> implements LineaDao {
	
}
   