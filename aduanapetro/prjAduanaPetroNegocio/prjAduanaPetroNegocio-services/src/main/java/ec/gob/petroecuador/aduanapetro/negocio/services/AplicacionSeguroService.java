
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.AplicacionSeguro;

/**
 Clase AplicacionSeguroService
 @autor dgonzalez
*/
public interface AplicacionSeguroService extends GenericService<AplicacionSeguro, Long> {
	
}
   