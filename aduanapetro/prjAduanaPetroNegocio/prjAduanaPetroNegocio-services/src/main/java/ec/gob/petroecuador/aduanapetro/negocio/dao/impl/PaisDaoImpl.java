
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.PaisDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.Pais;

/**
 Clase PaisDao
 @autor dgonzalez
*/
@Component("paisDao")
public class PaisDaoImpl extends GenericDAOImpl<Pais, Long> implements PaisDao {
	
}
   