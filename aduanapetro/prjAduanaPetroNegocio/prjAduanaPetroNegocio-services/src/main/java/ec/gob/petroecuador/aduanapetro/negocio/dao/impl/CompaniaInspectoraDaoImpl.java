
package ec.gob.petroecuador.aduanapetro.negocio.dao.impl;

import org.springframework.stereotype.Component;

import ec.gob.petroecuador.aduanapetro.negocio.dao.CompaniaInspectoraDao;
import ec.gob.petroecuador.aduanapetro.seguridad.generic.dao.impl.GenericDAOImpl;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.CompaniaInspectora;

/**
 Clase CompaniaInspectoraDao
 @autor dgonzalez
*/
@Component("companiainspectoraDao")
public class CompaniaInspectoraDaoImpl extends GenericDAOImpl<CompaniaInspectora, Long> implements CompaniaInspectoraDao {
	
}
   