
package ec.gob.petroecuador.aduanapetro.negocio.services;

import ec.gob.petroecuador.aduanapetro.seguridad.generic.service.GenericService;
import ec.gob.petroecuador.aduanapetro.negocio.jpa.CategoriaDocumento;

/**
 Clase CategoriaDocumentoService
 @autor dgonzalez
*/
public interface CategoriaDocumentoService extends GenericService<CategoriaDocumento, Long> {
	
}
   